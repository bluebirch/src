# $Id$
#
# $Log$
# Revision 1.20  2002/09/21 06:26:14  stefan
# Tog bort tr�ningskalendern.
#
# Revision 1.19  2002/04/16 20:43:20  stefan
# Lade till .htgroup och .htpasswd
#
# Revision 1.18  2002/02/06 17:40:45  stefan
# Lade till en enkel tr�ningskalender
#
# Revision 1.17  2002/02/01 23:02:28  stefan
# Anpassningar f�r nya datorn
#
# Revision 1.16  2002/02/01 21:26:47  stefan
# New runmode to edit comments for a specific date. Changed parameters
# of the 'defaults' method to implement a more flexible use. It checks
# for buttons pressed, and so on.
#
# Revision 1.15  2002/01/07 16:00:17  stefan
# Changed default limit in search page.
#
# Revision 1.14  2002/01/07 15:44:41  stefan
# Lade till s�kfunktion
#
# Revision 1.13  2002/01/07 14:10:45  stefan
# Added comments in calendar.
#
# Revision 1.12  2002/01/07 13:37:00  stefan
# Adapted all viewing runmodes to the new 'defaults' method. However,
# this method can not be used on edit runmodes, so I have to do
# something else for that matter.
#
# Revision 1.11  2002/01/07 00:18:08  stefan
# Added a small calendar in the navigation bar.
#
# Revision 1.10  2002/01/06 17:16:25  stefan
# Added calendary events in viewday and viewdiary run modes.
#
# Revision 1.9  2001/12/17 12:51:54  stefan
# Improved date calculation in viewdiary method.
#
# Revision 1.8  2001/12/17 00:04:38  stefan
# Added new run mode 'viewdiary'.
#
# Revision 1.7  2001/12/07 10:05:01  stefan
# Changed the diaryedit method to use also the users name when deciding
# what time to give the diary note (when calling WDDBI->maxtime).
#
# Revision 1.6  2001/11/12 23:25:37  stefan
# Added two new run modes: 'imageedit', which allows the user to add an
# image, and 'gallery', which shows a list of all images in the
# database.
#
# Revision 1.5  2001/11/11 20:43:35  stefan
# Added support for viewing pictures.
#
# Revision 1.4  2001/10/25 20:03:30  stefan
# Added run mode "latest", which shows the latest entries that have
# changed.
#
# Revision 1.3  2001/10/23 22:44:58  stefan
# Added decoding and displaying of mails.
#

package WebDiary;
use base 'CGI::Application';
use strict;
use MIME::Parser;
use calendar;
use WDDBI;
use Calendar;

my $db;  # Database object (global)
my $q;   # CGI query object
my $cal; # Calendar object

sub setup {
    my $self = shift;

    # Set default run mode
    $self->start_mode( 'viewday' );

    # Define run modes
    $self->run_modes( 'viewday'   => 'viewday',
		      'viewdiary' => 'viewdiary',
		      'calendar'  => 'calendar',
		      'smsedit'   => 'smsedit',
		      'diaryedit' => 'diaryedit',
		      'imageedit' => 'imageedit',
		      'commentedit' => 'commentedit',
#  		      'workoutedit' => 'workoutedit',
		      'latest'    => 'latest',
		      'search'    => 'search',
		      'gallery'   => 'gallery',
		      'raw'       => 'raw' );

    # Set path to templates (for HTML::Template)
    $self->tmpl_path( 'HTML_Templates/' );

    # Create database object
    $db = new WDDBI;

    # Set up MySQL parameters
    $db->dsn( 'DBI:mysql:stefan_diary:localhost' );
    $db->user( 'stefan' );
    $db->password( 'slempropp' );

    # Log in and initialize queries
    unless ($db->init( $ENV{REMOTE_USER} ? $ENV{REMOTE_USER} : $ENV{LOGNAME} ))
      {
	  die "Unable to initialize database engine";
      }

    # Get CGI query object
    $q = $self->query();
    $self->{_q} = $q; # make them instance specific, not global

    # Create calendar object
    $cal = new Calendar;
    $self->{_cal} = $cal;

}

######################################################################
# viewday - view all events for a single day
#
sub viewday {
    my $self = shift;

    # MIME::Parser object
    my $parser;

    # Get default parameters
    my ($q, $tmpl, $cal) = $self->defaults( rm => 'viewday',
					    template => 'viewday.html' );

    # Find things in database
    my $events = $db->getday( $q->param( 'date' ) );

    my (@diary, @icq, @sms, @mail, @pic, @unknown);

    # Split events into separate lists. Fetch needed data from the
    # database.
    foreach my $rec (@$events) {

	# Diary
	if ($$rec{type} eq 'diary') {
	    $$rec{lines} = $self->split_paragraph(
	      $db->getdata( $$rec{date}, $$rec{time}, $$rec{person}, 
			    $$rec{type} ));
	    push @diary, $rec;

	# ICQ message
	} elsif ($$rec{type} eq 'icq-message') {
	    $$rec{lines} = $self->split_paragraph(
	      $db->getdata( $$rec{date}, $$rec{time}, $$rec{person}, 
			    $$rec{type} ));
	    push @icq, $rec;

	# SMS message
	} elsif ($$rec{type} eq 'sms-message') {
	    $$rec{data} = $db->getdata( $$rec{date}, $$rec{time},
					$$rec{person}, $$rec{type} );

	    $$rec{lines} = $self->split_paragraph(
	      $db->getdata( $$rec{date}, $$rec{time}, $$rec{person},
			    'sms-comment' ));
	    push @sms, $rec;

	# Picture
	} elsif ($$rec{type} =~ m/(jpeg|gif)-image/) {
	    # Get comment from database
	    $$rec{comment} = $db->getdata( $$rec{date}, $$rec{time},
					   $$rec{person}, 'image-comment' );

	    # Push to list of pictures
	    push @pic, $rec;

	# Mail
	} elsif ($$rec{type} eq 'mail') {

	    # Get data from database
	    my $data = $db->getdata( $$rec{date}, $$rec{time},
					$$rec{person}, $$rec{type} );

	    # Initialize MIME::Parser object
	    $parser = new MIME::Parser unless ($parser);

	    # Output everything to memory
	    $parser->output_to_core(1);

	    # Use memory as temporary storage (yes, the parser will be
	    # very heavy on memory, but this reduces disk usage
	    # significantly).
	    $parser->tmp_to_core(1);

	    # Don't extract nested messages. I want to do this
	    # manually, if I need to do it at all.
	    $parser->extract_nested_messages(0);

	    # Decode headers according to RFC 1522
	    $parser->decode_headers(1);

	    # Things below here might move to a recursive sub

	    # Parse MIME message
	    my $mail = $parser->parse_data( $data );

	    # Get MIME head
	    my $head = $mail->head;

	    # Store mail header
	    $$rec{mail_from} = $head->get( 'From', 0 );
	    $$rec{mail_to} = $head->get( 'To', 0 );
	    $$rec{mail_subject} = $head->get( 'Subject', 0 );
	    $$rec{mail_date} = $head->get( 'Date', 0 );
	    $$rec{mail_type} = $mail->mime_type;

	    # Get body
	    if ($mail->mime_type eq 'text/plain') {
		$$rec{data} = $mail->bodyhandle->as_string;
	    } elsif ($$rec{mail_type} =~ m/^multipart\//) {
		foreach my $part ($mail->parts) {
		    if ($part->mime_type eq 'text/plain') {
			$$rec{data} .= $part->bodyhandle->as_string . "\n\n";
		    } else {
			$$rec{data} .= "Bilaga: " . $part->mime_type 
			  . "\n";
		    }
		}
	    } else {
		$$rec{data} = '(Mail kan ej visas)';
	    }

	    # Push to array
	    push @mail, $rec;

	# Anything else
	} else {
	    push @unknown, $rec;
	}
    }

    # Set template parameters
    $tmpl->param( diary => \@diary );
    $tmpl->param( icq => \@icq );
    $tmpl->param( sms => \@sms );
    $tmpl->param( mail => \@mail );
    $tmpl->param( pic => \@pic );
    $tmpl->param( unknown => \@unknown );

    # Return restult
    return $tmpl->output;
}

######################################################################
# viewdiary - view a diary for a person
#
sub viewdiary {
    my $self = shift;

    # Set default parameters
    my ($q, $tmpl, $cal) = $self->defaults( rm => 'viewdiary',
					    template => 'viewdiary.html' );

    # Store date in a variable
    my $date = $q->param( 'date' );

    unless ($cal->valid( $q->param( 'start_date' ))) {
	$q->param( 'start_date', $cal->start_of_month( $date ) );
    }
    if ($q->param( 'end_date' )) {
	unless ($cal->valid( $q->param( 'end_date')) && ($q->param( 'end_date' ) ge $q->param( 'start_date' ))) {
	    $q->delete( 'end_date' );
	}
    }
    unless ($q->param( 'end_date' )) {
	$q->param( 'end_date', $cal->end_of_month( $q->param( 'start_date' ) ) );
    }
    unless ($q->param( 'person' )) {
	$q->param( 'person', $db->webuser() );
    }

    # Find stuff
    my $events = $db->get_range( $q->param( 'start_date' ),
				 $q->param( 'end_date' ),
				 'diary',
				 $q->param( 'person' ) );

    # Read the real data and split into paragraphs
    foreach my $i (0..$#$events) {
	$$events[$i]{lines} = $self->split_paragraph(
	    $db->getdata( $$events[$i]{date}, $$events[$i]{time},
			  $$events[$i]{person},$$events[$i]{type} ));
	$$events[$i]{datestr} = ucfirst( datestring( $$events[$i]{date} ) );
	$$events[$i]{calendar_event} = $cal->event( $$events[$i]{date} );
    }


    # Set template parameters
    $tmpl->param( start_date => $q->param( 'start_date' ) );
    $tmpl->param( end_date => $q->param( 'end_date' ) );
    $tmpl->param( person => $q->param( 'person' ) );

    $tmpl->param( events => $events );

    # A hack. Can't decide if it is ugly or really nice. Used to
    # determine which menu alternative should be selected.
    $tmpl->param( $q->param( 'person' ) => 1 );

    # Return template output
    return $tmpl->output;
}

######################################################################
# calendar - view a big calendar
#
sub calendar {
    my $self = shift;

    # Set default parameters
    my ($q, $tmpl, $cal) = $self->defaults( rm => 'calendar',
					    template => 'calendar.html' );

    # Store date in a variable
    my $date = $q->param( 'date' );

    # Create calendar month
    my $m = $cal->get_month( $date );

    # Set start and end dates
    my $start_date = $cal->a2s( $m->year, $m->month, 1 );
    my $end_date = $cal->a2s( $m->year, $m->month, $m->days );

    # Add parameters to calendar
    my %overview = $db->get_overview( $start_date, $end_date );
    foreach my $day (keys %overview) {
	foreach my $type (keys %{$overview{$day}}) {
	    $m->param( ($cal->s2a( $day ))[2], $type, 1 );
	}
    }

    # Add events and other parameters
    foreach my $day (1..$m->days) {
	$m->param( $day, 'date', $cal->a2s( $m->year, $m->month, $day ) );
	my $event = $cal->event( $cal->a2s( $m->year, $m->month, $day ) );
	$m->param( $day, 'event', $event ) if ($event);
	$event = $db->event( $cal->a2s( $m->year, $m->month, $day ) );
	$m->param( $day, 'comment', $event ) if ($event);
#  	$event = $db->get_workout( $cal->a2s( $m->year, $m->month, $day ),
#  				   $db->webuser );
#  	$m->param( $day, 'workout_calendar', $event ) if ($event);
    }

    # Set parameter "selected" for current day
    $m->param( ($cal->s2a( $date ))[2] , 'selected', 1 );

    # Get matrix
    my @calendar = $m->matrix;

    # Store in template
    $tmpl->param( calendar => \@calendar );

    # Return template output
    return $tmpl->output;
}

######################################################################
# latest - display the latest things in database
#
sub latest {
    my $self = shift;

    # Set default parameters
    my ($q, $tmpl, $cal) = $self->defaults( rm => 'viewday',
					    template => 'latest.html' );

    # Get parameters
    unless ($q->param( 'limit' ) + 0 > 0) {
	$q->param( 'limit', 20 );
    }

    # Find things in database
    my $events = $db->latest( $q->param( 'limit' ) );

    # Set some comments on things
    foreach my $rec (@$events) {
	if ($$rec{type} eq 'diary') {
	    $$rec{name} = "$$rec{person}s dagbok";
	} elsif ($$rec{type} eq 'sms-message') {
	    $$rec{name} = "SMS fr�n $$rec{person}";
	} elsif ($$rec{type} eq 'jpeg-image') {
	    $$rec{name} = "Bild av $$rec{person}";
	} else {
	    $$rec{name} = "Sak fr�n $$rec{person}";
	}
#	$$rec{datestr} = &datestring( $$rec{date} );
	$$rec{mdate} =~ m/(\d\d\d\d-\d\d-\d\d)/;
	$$rec{modify_date} = $1;
    }

    # Set template parameters
    $tmpl->param( events => $events );
    $tmpl->param( limit => $q->param( 'limit' ) );

    # Return result
    return $tmpl->output;
}

######################################################################
# search - search database
#
sub search {
    my $self = shift;

    # Set default parameters
    my ($q, $tmpl, $cal) = $self->defaults( rm => 'viewday',
					    template => 'search.html' );

    # Get parameters
    unless ($q->param( 'limit' ) + 0 > 0) {
	$q->param( 'limit', 50 );
    }

    # Find things in database
    my $events;
    if ($q->param( 'S�k' ) && $q->param( 'search' )) {
	$events = $db->search( $q->param( 'search' ), $q->param( 'limit' ) );
    } elsif ($q->param( 'Senaste' )) {
	$events = $db->latest( $q->param( 'limit' ) );
    }

    # Set some comments on things
    foreach my $rec (@$events) {
	if ($$rec{type} eq 'diary') {
	    $$rec{name} = "$$rec{person}s dagbok";
	} elsif ($$rec{type} eq 'sms-message') {
	    $$rec{name} = "SMS fr�n $$rec{person}";
	} elsif ($$rec{type} eq 'sms-comment') {
	    $$rec{name} = "SMS-kommentar av $$rec{person}";
	    $$rec{type} = 'sms-message';
	} elsif ($$rec{type} eq 'image-comment') {
	    $$rec{name} = "Bild av $$rec{person}";
	    $$rec{type} = 'jpeg-image';
	} elsif ($$rec{type} eq 'mail') {
	    $$rec{name} = "Mail fr�n $$rec{person}";
	} elsif ($$rec{type} eq 'icq-message') {
	    $$rec{name} = "ICQ fr�n $$rec{person}";
	} else {
	    $$rec{name} = "Sak fr�n $$rec{person}";
	}
#	$$rec{datestr} = &datestring( $$rec{date} );
	$$rec{mdate} =~ m/(\d\d\d\d-\d\d-\d\d)/;
	$$rec{modify_date} = $1;
    }

    # Set template parameters
    $tmpl->param( events => $events );
    $tmpl->param( limit => $q->param( 'limit' ) );
    $tmpl->param( search => $q->param( 'search' ) );

    # Return result
    return $tmpl->output;
}


######################################################################
# gallery - display a gallery with links to pictures
#
sub gallery {
    my $self = shift;

    # Set default parameters
    my ($q, $tmpl, $cal) = $self->defaults( rm => 'viewday',
					    template => 'gallery.html' );

    # Find things in database
    my $events = $db->gallery;

    # Set template parameters
    $tmpl->param( events => $events );

    # Return result
    return $tmpl->output;
}

######################################################################
# smsedit - edit a SMS message
#
sub smsedit {
    my $self = shift;
    my $errmsg;

    # Check form submit
    if ($q->param( 'Spara' )) {
	# Verify values
	if (!valid_date( $q->param( 'date' ))) {
	    $errmsg = "Ogiltigt datum.";
	} elsif (!valid_time( $q->param( 'time' ))) {
	    $errmsg = "Ogiltig tidsangivelse.";
	} elsif (!$q->param( 'person' )) {
	    $errmsg = "Ingen avs�ndare angiven.";
	} elsif (!$q->param( 'data' )) {
	    $errmsg = "Ingen meddelandetext angiven.";
	} else {

	    # If everything is verified, update database.

	    # Process comment text.
	    my $comment = $q->param( 'comment' );
	    $comment =~ s/\r//gs;               # Remove CR
	    $comment =~ s/(?<!\n)\n(?!\n)/ /gs; # Remove single LF
	    $comment =~ s/\n{2,}/\n/gs;         # Convert double LF to single LF

	    # Build array of data
	    my @types = ( 'sms-message', 'sms-comment' );
	    my @data = ( $q->param( 'data' ), $q->param( 'comment' ) );

	    # Update
	    my $updated = $db->multi_update( $q->param( 'origdate' ),
					     $q->param( 'origtime' ),
					     $q->param( 'origperson' ),
					     $q->param( 'date' ),
					     $q->param( 'time' ),
					     $q->param( 'person' ),
					     \@types,
					     \@data );

	    # Check success or failure
	    if ($updated != 2) {
		$errmsg = "Det uppstod ett fel vid uppdateringen av databasen ($updated av 2 poster uppdaterade).";
	    } else {
		return $self->redirect();
	    }
	}
    }

    # Delete -> Yes
    elsif ($q->param( 'Ja' )) {
	$db->erase( $q->param( 'origdate' ), $q->param( 'origtime' ),
		    $q->param( 'origperson' ), 'sms-comment' );
	$db->erase( $q->param( 'origdate' ), $q->param( 'origtime' ),
		    $q->param( 'origperson' ), 'sms-message' );

	# Never mind error messages.
	return $self->redirect();
    }

    # Cancel
    elsif ($q->param( 'Avbryt' )) {
	return $self->redirect;
    }

    # Set default values
    else {

	# Set default date
	unless (valid_date( $q->param( 'date' ))) {
	    $q->param( 'date', local_date );
	}

	# Set default time
	unless (valid_time( $q->param( 'time' ))) {
	    $q->param( 'time', local_time );
	}

	# Set default person
	unless ($q->param( 'person' )) {
	    # FIXME: Can't do better than this, I guess. Ugly hard-coded data. :-)
	    if (uc($db->webuser()) eq 'STEFAN') {
		$q->param( 'person', 'Jenny' );
	    } elsif (uc($db->webuser()) eq 'JENNY') {
		$q->param( 'person', 'Stefan' );
	    }
	}

	# Get data from database
	unless ($q->param( 'data' )) {
	    my $data = $db->getdata( $q->param( 'date' ), $q->param( 'time' ),
				     $q->param( 'person' ), 'sms-message' );
	    $q->param( 'data', $data ) if ($data);
	}

	# Get comment from database
	unless ($q->param( 'comment' )) {
	    my $comment = $db->getdata( $q->param( 'date' ), $q->param( 'time' ),
					$q->param( 'person' ), 'sms-comment' );
	    $q->param( 'comment', $comment ) if ($comment);
	}
    }

    # Init template
    my $template = $self->load_tmpl( 'smsedit.html',
				     die_on_bad_params => 0 );

    # Set template parameters
    $template->param( origdate => $q->param( 'date' ));
    $template->param( origtime => $q->param( 'time' ));
    $template->param( origperson => $q->param( 'person' ));
    $template->param( errmsg => $errmsg ) if ($errmsg);
    $template->param( verify => 1 ) if ($q->param( 'Radera' ));
    $template->param( date => $q->param( 'date' ));
    $template->param( time => $q->param( 'time' ));
    $template->param( person => $q->param( 'person' ));
    $template->param( data => $q->param( 'data' ));
    $template->param( comment => $q->param( 'comment' ));

    # Return result
    return $template->output;
}


######################################################################
# diaryedit - edit a diary entry
#
sub diaryedit {
    my $self = shift;
    my $errmsg;

    # Check if this is a form submit
    if ($q->param( 'Spara' )) {
	# Run code to verify and save data. This ended up being a very
	# ugly and complicated function. Must be an easier way, right?
	# Yes, there is. And this method can be seen below.

	# Verify values
	if (!valid_date( $q->param( 'date' ))) {
	    $errmsg = "Ogiltigt datum.";
	} elsif (!$q->param( 'person' )) {
	    $errmsg = "Ingen avs�ndare angiven.";
	} elsif (!$q->param( 'data' )) {
	    $errmsg = "Ingen dagbokstext inskriven.";
	} else {

	    # If date has changed, get a new value for the time
	    # parameter (so we don't accidentially (how to spell that
	    # word?) try to overwrite an existing diary entry
	    if ($q->param( 'date' ) ne $q->param( 'origdate' )) {
		my $maxtime = $db->maxtime( $q->param( 'date' ),
					    'diary',
					    $q->param( 'person' ) );
		if ($maxtime) {
		    $q->param( 'time', &time_calc( $maxtime, 1 ) );
		} else {
		    $q->param( 'time', '00:00:00' );
		}
	    }

	    # Process data. Join all single linefeeds to paragraphs,
	    # and convert double (or more) line feeds to a single line
	    # feed. This way, we get a text format with a single line
	    # feed for each new paragraph. I guess some browsers send
	    # CRLF, so let's just delete all CR characters as well.
	    my $data = $q->param( 'data' );
	    $data =~ s/\r//gs;
	    $data =~ s/(?<!\n)\n(?!\n)/ /gs;
	    $data =~ s/\n{2,}/\n/gs;

	    # Build array of data
	    my @types = ( 'diary' );
	    my @data = ( $data );

	    # Update (or insert)
	    my $updated = $db->multi_update( $q->param( 'origdate' ),
					     $q->param( 'origtime' ),
					     $q->param( 'origperson' ),
					     $q->param( 'date' ),
					     $q->param( 'time' ),
					     $q->param( 'person' ),
					     \@types,
					     \@data );

	    # Error checking
	    if ($updated != 1) {
		$errmsg = "Fel vid uppdatering ($updated).";
	    } else {
		# If update was successfull, redirect client browser
		return $self->redirect();
	    }
	}
    }

    # Delete -> Yes
    elsif ($q->param( "Ja" )) {
	if ($db->erase( $q->param( 'origdate' ), $q->param( 'origtime' ),
			$q->param( 'origperson' ), 'diary' )) {
	    return $self->redirect();
	}
	$errmsg = "Radering misslyckades.";
    }

    # Cancel
    elsif ($q->param( "Avbryt" )) {
	return $self->redirect();
    }

    # Set default values
    else {
	# Set default person
	unless ($q->param( 'person' )) {
	    $q->param( 'person', ucfirst( $db->webuser() ) );
	}

	# Set default date
	unless (valid_date( $q->param( 'date' ))) {
	    $q->param( 'date', local_date );
	}

	# Set default time (which is not used in this form)
	unless (valid_time( $q->param( 'time' ))) {
	    my $maxtime = $db->maxtime( $q->param( 'date' ),
					'diary',
					$q->param( 'person' ) );
	    if ($maxtime) {
		$q->param( 'time', &time_calc( $maxtime, 1 ) );
	    } else {
		$q->param( 'time', '00:00:00' );
	    }
	}

	# Get data from database, if available
	unless ($q->param( 'data' )) {
	    my $data = $db->getdata( $q->param( 'date' ), $q->param( 'time' ),
				     $q->param( 'person' ), 'diary' );

	    if ($data) {

		# Make double line feeds between each paragraph
		$data =~ s/\n/\n\n/g;

		$q->param( 'data', $data );
	    }
	}
    }

    # Init template (FIXME: error handling)
    my $template = $self->load_tmpl( 'diaryedit.html',
				     die_on_bad_params => 0 );

    # Set template parameters
    $template->param( origdate => $q->param( 'date' ));
    $template->param( origtime => $q->param( 'time' ));
    $template->param( origperson => $q->param( 'person' ));
    $template->param( errmsg => $errmsg ) if ($errmsg);
    $template->param( verify => 1 ) if ($q->param( 'Radera' ));
    $template->param( date => $q->param( 'date' ));
    $template->param( time => $q->param( 'time' ));
    $template->param( person => $q->param( 'person' ));
    $template->param( data => $q->param( 'data' ));
    $template->param( number => seconds( $q->param( 'time' ) ) + 1 );

    # Return result
    return $template->output;
}

######################################################################
# imageedit - edit a JPEG or GIF image with comments
#
sub imageedit {
    my $self = shift;
    my $errmsg;

    # Check form submit
    if ($q->param( 'Spara' )) {
  	# Verify values
  	if (!valid_date( $q->param( 'date' ))) {
  	    $errmsg = "Ogiltigt datum.";
  	} elsif (!valid_time( $q->param( 'time' ))) {
  	    $errmsg = "Ogiltig tidsangivelse.";
  	} elsif (!$q->param( 'person' )) {
  	    $errmsg = "Ingen avs�ndare angiven.";
  	} elsif (!$q->param( 'comment' )) {
  	    $errmsg = "Ingen bildtext inskriven.";
  	} else {

	    # All parameters are verified. Update database with
	    # picture and picture text.

	    # Get filehandle
	    my $fh = $q->upload( 'image' );

	    # If filehandle is set, read the file and store it in the
	    # $image variable. Otherwise, get the picture data from
	    # the database. We need it anyway in the update function,
	    # and this makes things much easier, although I transfer
	    # som unnecessary data.
	    my $image;
	    if ($fh) {

		# Check content type. Only image/jpeg is allowed.
		my $content_type = $q->uploadInfo( $q->param( 'image' ) )->
		  {'Content-Type'};

		if ($content_type eq 'image/jpeg') {
		    local $/ = undef;
		    $image = <$fh>;
		    my $size = length( $image );
		} else {
		    # Not a JPEG image
		    $errmsg = "Felaktigt filformat ($content_type). Du kan bara lagra en bild i JPEG-format.";
		}
	    } elsif ($q->param( 'update' )) {
		$image = $db->getdata( $q->param( 'origdate' ),
				       $q->param( 'origtime' ),
				       $q->param( 'origperson' ),
				       'jpeg-image' );
	    }

	    # Get comment
	    my $comment = $q->param( 'comment' );

	    # Decide what to do
	    if ($image && $comment) {

		# Run the ordinary update procedure.

		# If date has changed, get a new value for the time
		# parameter. This is exactly the same procedure as in
		# diaryedit, and I should actually do something to
		# compress this code.
		if ($q->param( 'date' ) ne $q->param( 'origdate' )) {
		    my $maxtime = $db->maxtime( $q->param( 'date' ), 'jpeg-image' );
		    if ($maxtime) {
			$q->param( 'time', &time_calc( $maxtime, 1 ) );
		    } else {
			$q->param( 'time', '00:00:00' );
		    }
		}

		# Build array of data
		my @types = ( 'jpeg-image', 'image-comment' );
		my @data = ( $image, $comment );

		# Update (or insert)
		my $updated = $db->multi_update( $q->param( 'origdate' ),
						 $q->param( 'origtime' ),
						 $q->param( 'origperson' ),
						 $q->param( 'date' ),
						 $q->param( 'time' ),
						 $q->param( 'person' ),
						 \@types,
						 \@data );

		# Error checking
		if ($updated != 2) {
		    $errmsg = "Fel vid uppdatering ($updated).";
		} else {
		    # If update was successfull, redirect client browser
		    return $self->redirect();
		}
	    } else {
		$errmsg = "Bild eller bildtext saknas.";
	    }
	}
    }

    # Delete -> Yes
    elsif ($q->param( 'Ja' )) {
	$db->erase( $q->param( 'origdate' ), $q->param( 'origtime' ),
		    $q->param( 'origperson' ), 'jpeg-image' );
	$db->erase( $q->param( 'origdate' ), $q->param( 'origtime' ),
		    $q->param( 'origperson' ), 'image-comment' );

	# Never mind error messages.
	return $self->redirect();
    }

    # Cancel
    elsif ($q->param( 'Avbryt' )) {
	return $self->redirect;
    }

    # Set default values
    else {

	# Set default date
	unless (valid_date( $q->param( 'date' ))) {
	    $q->param( 'date', local_date );
	}

	# Set default time (which is not used in this form)
	unless (valid_time( $q->param( 'time' ))) {
	    my $maxtime = $db->maxtime( $q->param( 'date' ), 'jpeg-image' );
	    if ($maxtime) {
		$q->param( 'time', &time_calc( $maxtime, 1 ) );
	    } else {
		$q->param( 'time', '00:00:00' );
	    }
	}

	# Set default person
	unless ($q->param( 'person' )) {
	    $q->param( 'person', $db->webuser() );
	}

	# Get comment from database
	unless ($q->param( 'comment' )) {
	    my $comment = $db->getdata( $q->param( 'date' ), $q->param( 'time' ),
					$q->param( 'person' ), 'image-comment' );
	    $q->param( 'comment', $comment ) if ($comment);
	}
    }

    # Init template
    my $template = $self->load_tmpl( 'imageedit.html',
				     die_on_bad_params => 0 );

    # See if a picture exists
    if ($db->exist( $q->param( 'date' ), $q->param( 'time' ),
		    $q->param( 'person' ), 'jpeg-image' )
       ) {
	$template->param( showpic => 1 );
    }

    # Set template parameters
    $template->param( origdate => $q->param( 'date' ));
    $template->param( origtime => $q->param( 'time' ));
    $template->param( origperson => $q->param( 'person' ));
    $template->param( errmsg => $errmsg ) if ($errmsg);
    $template->param( verify => 1 ) if ($q->param( 'Radera' ));
    $template->param( date => $q->param( 'date' ));
    $template->param( time => $q->param( 'time' ));
    $template->param( person => $q->param( 'person' ));
    $template->param( type => 'jpeg-image' );
    $template->param( image => $q->param( 'image' ));
    $template->param( comment => $q->param( 'comment' ));
    $template->param( number => seconds( $q->param( 'time' ) ) + 1 );

    # Return result
    return $template->output;
}

######################################################################
# commentedit - redigera kommentar
#
sub commentedit {
    my $self = shift;

    # Set default parameters
    my ($q, $tmpl, $cal) = $self->defaults( rm => 'viewday',
					    template => 'commentedit.html',
					    fix_date => 0,
					    verify => 0 );

    # Modify runmode if return parameter is set
    if ($q->param( 'return' )) {
	$tmpl->param( return => $q->param( 'return' ) );
    }

    # Get date
    my $date = $q->param( 'date' );

    # Get comment
    unless ($q->param( 'comment' ) || $q->param( 'save' )) {
	$q->param( 'comment', $db->event( $date ) );
    }

    # Save comment
    if ($q->param( 'save' )) {
	$db->set_event( $date, $q->param( 'comment' ));
	# Never mind error handling
	return $self->redirect( $q->param( 'return' ) );
    }

    # Cancel
    if ($q->param( 'cancel' )) {
	return $self->redirect( $q->param( 'return' ) );
    }

    # Set template parameters
    $tmpl->param( comment => $q->param( 'comment' ) );

    # Return template
    return $tmpl->output;
}

#  ######################################################################
#  # workoutedit - redigera tr�ningskalender
#  #
#  sub workoutedit {
#      my $self = shift;

#      # Set default parameters
#      my ($q, $tmpl, $cal) = $self->defaults( rm => 'viewday',
#  					    template => 'workoutedit.html',
#  					    fix_date => 0,
#  					    verify => 0 );

#      # Modify runmode if return parameter is set
#      if ($q->param( 'return' )) {
#  	$tmpl->param( return => $q->param( 'return' ) );
#      }

#      # Get date
#      my $date = $q->param( 'date' );

#      # Get comment
#      unless ($q->param( 'comment' ) || $q->param( 'save' )) {
#  	$q->param( 'comment', $db->get_workout( $date, $db->webuser ) );
#      }

#      # Save comment
#      if ($q->param( 'save' )) {
#  	$db->set_workout( $date, $db->webuser, $q->param( 'comment' ));
#  	# Never mind error handling
#  	return $self->redirect( $q->param( 'return' ) );
#      }

#      # Cancel
#      if ($q->param( 'cancel' )) {
#  	return $self->redirect( $q->param( 'return' ) );
#      }

#      # Set template parameters
#      $tmpl->param( comment => $q->param( 'comment' ) );

#      # Return template
#      return $tmpl->output;
#  }


######################################################################
# defaults - set up defaults
#
# Set up default values and create common values that is needed in
# each run mode. Takes the following arguments:
#
# ($runmode, $template_file)
#
# Returns an array with the following values:
#
# ($query_object, $template_object, $calendar_object)
#
sub defaults {
    my $self = shift;
    #my ($self, $runmode, $template) = @_;

    # Set default parameters
    my %options = ( rm => 'viewday',
		    template => 'viewday.html',
		    fix_date => 1,
		    save_button => 'Spara',
		    cancel_button => 'Avbryt',
		    yes_button => 'Ja',
		    verify => 1 );

    # Load in options supplied to method
    for (my $x = 0; $x <= $#_; $x += 2) {
	die unless (defined($_[($x + 1)]));
	$options{lc($_[$x])} = $_[($x + 1)];
    }

    # Get query object
    my $q = $self->{_q};

    # Get calendar object
    my $cal = $self->{_cal};

    # Initialize template
    my $tmpl = $self->load_tmpl( $options{template},
				 die_on_bad_params => 0,
				 global_vars => 1 );

    # Error checking - doesn't work anyway
    unless ($tmpl) {
	die "oh, no template";
    }

    # Check if save button was pressed
    if ($q->param( $options{save_button} )) {
	if ($options{verify}) {
	    $q->param( 'verify', 1 );
	} else {
	    $q->param( 'save', 1 );
	}
    }

    # Check if verify button was pressed
    if ($q->param( $options{yes_button} ) && $q->param( 'verify' )) {
	$q->param( 'save', 1 );
    }

    # Check if cancel button was pressed
    if ($q->param( $options{cancel_button} )) {
	$q->param( 'cancel', 1 );
    }

    # Make sure date is valid
    my $date = $cal->make_valid( $q->param( 'date' ) );

    # Update CGI parameters with valid date if requested
    if ($options{fix_date}) {
	$q->param( 'date', $date );
    }

    # Get month object of current date; used to build month matrix
    my $month = $cal->get_month( $date );

    # Set parameter "selected" for current day
    $month->param( ($cal->s2a( $date ))[2] , 'selected', 1 );

    # Get calendar matrix
    my @matrix = $month->matrix;

#      # Check last date for workout
#      my $no_workout_in_days = $cal->delta( $db->last_workout( $db->webuser() ),
#  					  $cal->today );

#      if ($no_workout_in_days == 0) {
#  	$no_workout_in_days = 'idag';
#      } elsif ($no_workout_in_days == 1) {
#  	$no_workout_in_days = 'ig�r';
#      } elsif ($no_workout_in_days == 2) {
#  	$no_workout_in_days = 'i f�rrg�r';
#      } elsif ($no_workout_in_days >= 7) {
#  	$no_workout_in_days = "f�r mer �n en vecka sedan. Du �r lat";
#      } else {
#  	$no_workout_in_days = "f�r $no_workout_in_days dagar sedan";
#      }

    # Set default or global template parameters
    $tmpl->param( rm => $options{rm} );
    $tmpl->param( date => $q->param( 'date' ) );
    $tmpl->param( datestr => ucfirst( datestring( $date ) ) );
    $tmpl->param( date_event => $cal->event( $date ) );
    $tmpl->param( date_comment => $db->event( $date ) );
#      $tmpl->param( last_workout => $no_workout_in_days );
#      $tmpl->param( workout => $db->get_workout( $date, $db->webuser ) );
    $tmpl->param( prevdate => $cal->add_delta( $date, -1 ) );
    $tmpl->param( nextdate => $cal->add_delta( $date, +1 ) );
    $tmpl->param( calendar_matrix => \@matrix );
    $tmpl->param( calendar_monthname => $month->name );
    $tmpl->param( calendar_year => $month->year );
    $tmpl->param( calendar_month => $month->month );
    $tmpl->param( calendar_next => $cal->next_month( $date ) );
    $tmpl->param( calendar_prev => $cal->prev_month( $date ) );

    # Return objects
    return ($q, $tmpl, $cal);
}

######################################################################
# raw - display raw data
#
# Display a specific record from the database in "raw" mode, that is,
# dump it "as is" to the client, with a apropriate MIME type set.
#
sub raw {
    my $self = shift;

    if ($q->param( 'date' ) && $q->param( 'time' )
	&& $q->param( 'person' ) && $q->param( 'type' )) {

	# Get record from database
	my $data = $db->get( $q->param( 'date' ), $q->param( 'time' ),
			     $q->param( 'person' ), $q->param( 'type' ) );

	# If the record exists, set MIME type and return data
	if ($data) {
	    $self->header_props( -type=>$$data{content_type} );
	    return $$data{data};
	}
    }
    return '<html><head><title>nothing</title></head><body><p>nothing</p></body></html>';
}

######################################################################
# split_paragraph
#
# Splits a flowing text into paragraphs and returns a scalar reference
# of hash references of each line. I don't know if this works, but it
# seem to... :-)
#
sub split_paragraph {
    my ($self, $data) = @_;
    my (@result);

    my @lines = split /\n+/, $data;

    foreach (@lines) {
	push @result, { line => $_ };
    }
    return \@result;
}

######################################################################
# redirect
#
sub redirect {
    my $self = shift;
    my $runmode = $_[0] ? $_[0] : 'viewday';

    # Set HTTP-headers and return an empty string
    $self->header_type( 'redirect' );
    $self->header_props( -uri=>$q->url() . "?rm=$runmode&date=" . $q->param( 'date' ) );
    return '';
}

1;
