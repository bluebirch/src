# $Id$
#
# WDDBI - an object oriented database interface for the webdiary
#
# Written by Stefan Svensson <stefan@raggmunk.nu>
#
# $Log$
# Revision 1.14  2002/09/21 06:26:14  stefan
# Tog bort träningskalendern.
#
# Revision 1.13  2002/02/06 17:40:45  stefan
# Lade till en enkel träningskalender
#
# Revision 1.12  2002/02/01 21:21:45  stefan
# Changed method to add new events.
#
# Revision 1.11  2002/01/07 15:59:40  stefan
# Changed maximum limit in search method.
#
# Revision 1.10  2002/01/07 15:44:41  stefan
# Lade till sökfunktion
#
# Revision 1.9  2002/01/07 14:10:25  stefan
# New methods 'event' and 'new_event'.
#
# Revision 1.8  2002/01/07 13:34:42  stefan
# New method 'get_overview'. Reconstructed from memory, since I couldn't
# resurrect the deleted db_api.pm from the CVS repository.
#
# Revision 1.7  2001/12/17 00:04:06  stefan
# Added new method 'get_range', used to get a range of events from the
# database. Optionally, selection can be restricted on event types and
# person.
#
# Revision 1.6  2001/12/07 10:02:47  stefan
# Added optional third parameter to the 'maxtime' method, which
# specifies the name to look for.
#
# Removed some ugly debug output.
#
# Revision 1.5  2001/11/12 23:23:14  stefan
# Added two methods: 'exist', which checks if a record exists in the
# database, and 'gallery', which returns a list of all images in the
# database..
#
# Revision 1.4  2001/11/11 20:44:15  stefan
# Added field "content_type" to the get method.
#
# Revision 1.3  2001/10/25 20:05:04  stefan
# Added method "latest", which finds the latest changed records in the
# database. The mdate field should have an index to increase
# performance.
#
# Revision 1.2  2001/10/20 23:01:03  stefan
# The new WebDiary application
#
# Revision 1.1  2001/09/09 00:21:47  stefan
# Begin rewrite of db_api in an object-oriented style. The class is
# designed to handle persistent database connections, perhaps with
# mod_perl and Apache::DBI, which will improve performance on slow
# machines.
#
package WDDBI;

use strict;
use DBI;       # Use DBI (or perhaps Apache::DBI); using as base class
               # didn't work
use Carp;      # Use for error checking and backtrace
use calendar;  # Date checking functions

# Global class variables
my $Debugging = 0;
my $prefix = 'DEBUG>>>';


######################################################################
# new - class constructor
#
# A rather basic thing, taken from the book. Sort of. I don't like
# Perl, but objects are a good thing.
#
sub new {
    my $proto         = shift;
    my $class         = ref($proto) || $proto;
    my $self          = {};
    $self->{dsn}      = 'DBI:mysql:stefan_diary:localhost';
    $self->{user}     = undef;
    $self->{password} = undef;
    $self->{webuser}  = undef;

    # Database handle
    $self->{dbh}      = undef;

    # Statement handles
    $self->{get}      = undef;
    $self->{typenum}  = undef;

    # Bless is the most stupid function name I have ever seen
    bless( $self, $class );
    return $self;
}

######################################################################
# DESTROY - class destructor
#
# When an instance (and there should really not be more than one) is
# destroyed, all statement handles are finished and the DBI connection
# is disconnected. This ensures a nice and smooth shutdown.
#
sub DESTROY {
    my $self = shift;

    # Invalidate statement handles
    $self->{get}->finish if ($self->{get});
    $self->{typenum}->finish if ($self->{typenum});

    # Disconnect DBI
    $self->{dbh}->disconnect if ($self->{dbh});
}

######################################################################
# debug - set class debugging
#
# Set debug level for entire class. This could really be changed so it
# handles every instance, but who cares? And why should I change it?
# Who need debugging of every instance, anyway? And, by the way, there
# should only be one instance.
#
sub debug {
    my $class = shift;
    if (ref $class) {
	confess "Class method called as object method";
    }
    unless (@_ == 1) {
	confess "usage: WDDBI->debug(level)"
    }
    $Debugging = shift;
}

######################################################################
# init - initialize database connection
#
# This method must be called after new() and before any of the
# database operations. It initializes DBI, sets up variables and
# statement handles.
#
sub init {
    my $self = shift;

    # Check for parameters
    unless (@_ == 1) {
	confess "Method called without parameters";
    }

    # Initialize DBI
#      warn "Initialize database; DSN=".$self->{dsn}.
#        "; USER=".$self->{user}."; PASSWORD=".$self->{password};
    $self->{dbh} = DBI->connect( $self->{dsn}, $self->{user},
				 $self->{password} );
    unless ($self->{dbh}) {
	return 0;
    }

    # Set user name
    unless( $self->webuser( $_[0] ) ) {
	return 0;
    }

    # Prepare queries
    $self->{get} = $self->{dbh}->prepare( q{
        SELECT data.date, data.time, data.person, data_types.type,
               data.data, data.mdate, data.muser, data.cdate, data.cuser,
               data_types.content_type
        FROM data, data_types
        WHERE data.date=? AND data.time=? AND data.person=? AND data.type=?
              AND data.type=data_types.id
	});
    return 0 unless ($self->{get});

    $self->{getdata} = $self->{dbh}->prepare( q{
        SELECT data
        FROM data
        WHERE date=? AND time=? AND person=? AND type=?
	});
    return 0 unless ($self->{getdata});

    $self->{exist} = $self->{dbh}->prepare( q{
        SELECT type
        FROM data
        WHERE date=? AND time=? AND person=? AND type=?
	});
    return 0 unless ($self->{exist});

    $self->{getday} = $self->{dbh}->prepare( q{
	SELECT data.date, data.time, data.person, data_types.type,
	       data.cdate, data.mdate, data.muser,
               data.cuser
	FROM data, data_types
	WHERE data.date = ? AND data.type=data_types.id AND data.type < 100
	ORDER BY data.time
	});
    return 0 unless ($self->{getday});

    $self->{get_range} = $self->{dbh}->prepare( q{
	SELECT data.date, data.time, data.person, data_types.type,
	       data.cdate, data.mdate, data.muser,
               data.cuser
	FROM data, data_types
	WHERE data.date >= ? AND data.date <= ? AND data.type=data_types.id
              AND data_types.type LIKE ? AND data.person LIKE ?
	ORDER BY data.date, data.time
        });
    return 0 unless ($self->{get_range});

    $self->{get_overview} = $self->{dbh}->prepare( q{
	SELECT DISTINCT data.date, data_types.type
	FROM data, data_types
	WHERE data.date >= ? AND data.date <= ? AND data.type=data_types.id
	ORDER BY data.date
    });
    return 0 unless ($self->{get_range});

    $self->{typenum} = $self->{dbh}->prepare( q{
        SELECT id FROM data_types WHERE type=?
        });
    return 0 unless ($self->{typenum});

    $self->{update} = $self->{dbh}->prepare( q{
        UPDATE data SET date=?, time=?, person=?, type=?, data=?, mdate=?,
               muser=?
        WHERE date=? AND time=? AND person=? AND type=?
        });
    return 0 unless ($self->{update});

    $self->{insert} = $self->{dbh}->prepare( q{
	INSERT INTO data
	(date, time, person, type, data, mdate, muser, cdate, cuser)
	VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? )
	});
    return 0 unless ($self->{insert});

    $self->{delete} = $self->{dbh}->prepare( q{
        DELETE FROM data
        WHERE date=? AND time=? AND person=? AND type=?
        });
    return 0 unless ($self->{delete});

    $self->{maxtime} = $self->{dbh}->prepare( q{
  	SELECT max(time)
        FROM data
  	WHERE date=? AND type=?
        });
    return 0 unless ($self->{maxtime});

    $self->{maxtimeperson} = $self->{dbh}->prepare( q{
  	SELECT max(time)
        FROM data
  	WHERE date=? AND type=? AND person=?
        });
    return 0 unless ($self->{maxtimeperson});

    $self->{latest} = $self->{dbh}->prepare( q{
	SELECT data.date, data.time, data.person, data_types.type,
	       data.cdate, data.mdate, data.muser,
               data.cuser
	FROM data, data_types
	WHERE data.type=data_types.id
              AND (data.type = 1 OR data.type = 4 OR data.type=5
                   OR data.type=6)
	ORDER BY data.mdate DESC
        LIMIT ?
        });
    return 0 unless ($self->{latest});

    $self->{search} = $self->{dbh}->prepare( q{
	SELECT data.date, data.time, data.person, data_types.type,
	       data.cdate, data.mdate, data.muser,
               data.cuser
	FROM data, data_types
	WHERE data.data REGEXP ?
              AND data.type=data_types.id
              AND (data.type <= 4 or data.type >=100)
	ORDER BY data.date DESC
        LIMIT ?
        });
    return 0 unless ($self->{search});

    $self->{gallery} = $self->{dbh}->prepare( q{
	SELECT data.date, data.time, data.person, data_types.type,
	       data.cdate, data.mdate, data.muser,
               data.cuser, data.data
	FROM data, data_types
	WHERE data.type=data_types.id AND data.type = 105
	ORDER BY data.date, data.time
        });
    return 0 unless ($self->{gallery});

    # Daily events
    $self->{get_event} = $self->{dbh}->prepare( q{
        SELECT date, comment
        FROM events
        WHERE date=?
        });
    return 0 unless ($self->{get_event});

    $self->{insert_event} = $self->{dbh}->prepare( q{
        INSERT INTO events
        VALUES (?, ?)
        });
    return 0 unless ($self->{insert_event});

    $self->{update_event} = $self->{dbh}->prepare( q{
        UPDATE events SET comment=? WHERE date=?
        });
    return 0 unless ($self->{update_event});

    $self->{delete_event} = $self->{dbh}->prepare( q{
        DELETE FROM events WHERE date=?
        });
    return 0 unless ($self->{update_event});

#      # Workout training calendar
#      $self->{get_workout} = $self->{dbh}->prepare( q{
#          SELECT text FROM workout WHERE date=? AND person=?
#          }) || return 0;
#      $self->{max_workout} = $self->{dbh}->prepare( q{
#          SELECT max(date) FROM workout WHERE person=?
#          }) || return 0;
#      $self->{insert_workout} = $self->{dbh}->prepare( q{
#          INSERT INTO workout VALUES (?, ?, ?)
#          }) || return 0;
#      $self->{update_workout} = $self->{dbh}->prepare( q{
#          UPDATE workout SET text=? WHERE date=? AND person=?
#          }) || return 0;
#      $self->{delete_workout} = $self->{dbh}->prepare( q{
#          DELETE FROM workout WHERE date=? AND person=?
#          }) || return 0;

}

######################################################################
# typenum - get type number
#
# The type-to-number table was stored in a hash in db_api, but now we
# do database lookups for everything. This ensures valid data for
# persistent database connections.
#
sub typenum {
    my ($self, $type) = @_;
    my @result;

#    $self->{typenum}->trace(2);

    # Sök
    $self->{typenum}->execute( $type ) || die $self->{typenum}->errstr;

    # Returnera svaret
    if (@result = $self->{typenum}->fetchrow_array) {
  	return $result[0];
    }
    return 0;
}

######################################################################
# get - get a single record
#
# Get a single record from the webdiary and return it as a hash
# reference.
#
sub get {
    my $self = shift;

    # Verify key
    return undef if (not $self->valid_key( @_[0,1,2,3] ));

    # Initialize variables
    my ($date, $time, $person, $type) = @_;

    # Execute query
    if ($self->{get}->execute( $date, $time, $person,
			       $self->typenum( $type ) )) {
	return $self->{get}->fetchrow_hashref;
    }
    return undef;
}

######################################################################
# getdata - get data from a single record
#
# Get a single record and return the data as a scalar
#
sub getdata {
    my $self = shift;

    # Verify key
    return undef if (not $self->valid_key( @_[0,1,2,3] ));

    # Initialize variables
    my ($date, $time, $person, $type) = @_;

    # Execute query
    if ($self->{getdata}->execute( $date, $time, $person,
				   $self->typenum( $type ) )) {
  	my $data = $self->{getdata}->fetchrow_array;
	$self->{getdata}->finish;
 	return $data;
    }
    return undef;
}

######################################################################
# exist - check if record exists
#
# Check if a record exists. Return true or false.
#
sub exist {
    my $self = shift;

    # Verify key
    return undef if (not $self->valid_key( @_[0,1,2,3] ));

    # Initialize variables
    my ($date, $time, $person, $type) = @_;

    # Execute query
    $self->{exist}->execute( $date, $time, $person, $self->typenum( $type ) );

    # Get number of rows
    my $rows = $self->{exist}->rows;;

    # Invalidate statement handle
    $self->{exist}->finish;

    return $rows;
}


######################################################################
# getday - get events of a particular day
#
# The getday method takes a single date as an argument and returns
# keys for all events on that day. Note that the data field is
# excluded, since the amount of data returned could be seriously
# large.
#
# getday returns a reference to a list of hash references, suitable
# for feeding HTML::Template.
#
# If nothing is found, an empty list is returned. If an error occurs,
# an undefined value is returned.
#
sub getday {
    my $self = shift;

    # Verify date
    return undef unless (&valid_date( $_[0] ));

    # Execute query
    unless ($self->{getday}->execute( $_[0] )) {
	print STDERR $self->{getday}->errstr;
	return undef; # On error, return undef
    }

    # Get it
    return $self->{getday}->fetchall_arrayref({});
}

######################################################################
# get_range
#
# Gets a range of records, from a specific date to another
# date. Optionally, the search can be limited by specifying the person
# and data type for wich the search should be done.
#
# Returns a list referense like getday. See above.
#
sub get_range {
    my ($self, $start_date, $end_date, $type, $person) = @_;

    # Verify dates
    return undef unless (&valid_date( $start_date )
			 && &valid_date( $end_date ));

    # Set values for type and person
    $type = '%' unless ($type);
    $person = '%' unless ($person);

    # Do search
    unless ($self->{get_range}->execute( $start_date, $end_date,
					 $type, $person )) {

	# Something went wrong; print error and return undef
	print STDERR $self->{get_range}->errstr;
	return undef;
    }

    # Return result to caller
    return $self->{get_range}->fetchall_arrayref({});
}

######################################################################
# get_overview - get an overview for a specified range
#
# Returns a hash-of-hash reference of all dates and existing data
# types for each date. Suitable for creating overviews or a calendar.
#
sub get_overview {
    my ($self, $start_date, $end_date) = @_;

    # Do search
    unless ($self->{get_overview}->execute( $start_date, $end_date )) {

	# Something went wrong; print error and return undef
	print STDERR $self->{get_overview}->errstr;
	return undef;
    }

    my %overview;

    # Step through result and build hash
    while (my @row = $self->{get_overview}->fetchrow_array) {
	$overview{ $row[0] }{ $row[1] } = 1;
	$overview{ $row[0] }{events} = 1 unless ($overview{ $row[0] }{events});
    }

    return %overview;
}

######################################################################
# insert - insert record into webdiary
#
# Inserts a new record into the webdiary data table. Returns true on
# success, false on any error.
#
# FIXME: This error checking should be changed. It is impossible to
# determine if an insert failed due to an error or an already existing
# record.
#
sub insert {
    my $self = shift;

    # Verify key
    return 0 unless ($self->valid_key( @_[0,1,2,3] ));

    # Initialize variables
    my $timestamp = &timestamp;

    # Execute insert query
#    $self->{insert}->trace(2);
    unless ($self->{insert}->execute( @_[0,1,2], $self->typenum( $_[3] ),
				      $_[4], $timestamp,
				      $self->webuser(), $timestamp,
				      $self->webuser() )) {
	warn $self->{insert}->errstr;
	return undef; # On error, return undef
    }

    return $self->{insert}->rows == 1 ? 1 : 0;
}

######################################################################
# update - update record
#
# Update record. Takes original key, new key and data value.
#
sub update {
    my $self = shift;

    # Verify key
    return 0 unless ($self->valid_key( @_[0,1,2,3] ));
    return 0 unless ($self->valid_key( @_[4,5,6,7] ));

#      for (my $i = 0; $i <= $#_; $i++ ) {
#  	print STDERR "param $i = '$_[$i]'\n";
#      }

    # Variables
    my $timestamp = &timestamp;

    # Execute update query
#    $self->{update}->trace(2);
    unless ($self->{update}->execute( @_[4,5,6], $self->typenum( $_[7] ),
				      $_[8], $timestamp,
				      $self->webuser(), @_[0,1,2],
				      $self->typenum( $_[3] ))) {
	warn $self->{update}->errstr;
	return undef; # On error, return undef
    }

    return $self->{update}->rows == 1 ? 1 : 0;
}

######################################################################
# multi_update - update/insert/delete several data types at once
#
# Parameters are old date, old time, old person, current date, current
# time, current person, arrayref of types and arrayref of data. Empty
# or undefined data deletes the record. If records does not exist,
# they are created.
#
# Returns the number of successfully updated/inserted/deleted records.
#

sub multi_update {
    my $self = shift;

    # A list for result codes
    my @resultcodes;

    # Time stamp
    my $timestamp = &timestamp;

    # Get parameters
    my ($old_date, $old_time, $old_person, $date, $time, $person,
	$typeref, $dataref) = @_;

    # Return error if number of array elements does not match
    return 0 if (scalar @$typeref != scalar @$dataref);

    # Step through data
    for (my $i=0; $i < scalar @$typeref; $i++) {

	# Check if data is set. If so, try to update. Otherwise,
	# delete.
	if ($$dataref[$i]) {
	    # Update table
	    $self->{update}->execute( $date, $time, $person,
				      $self->typenum( $$typeref[$i] ),
				      $$dataref[$i], $timestamp,
				      $self->webuser(),
				      $old_date, $old_time, $old_person,
				      $self->typenum( $$typeref[$i] ) );
	    # Get result code
	    $resultcodes[$i] = $self->{update}->rows;

	    # Print ugly debugging
	    print STDERR "updated $old_date-$old_time-$old_person-$$typeref[$i] (result: $resultcodes[$i])\n";

	    # If nothing was updated, try to INSERT data instead.
	    if ($resultcodes[$i] == 0) {
		$self->{insert}->execute( $date, $time, $person,
					  $self->typenum( $$typeref[$i] ),
					  $$dataref[$i],,
					  $timestamp, $self->webuser(),
					  $timestamp, $self->webuser() );
		# Get new result code
		$resultcodes[$i] = $self->{insert}->rows;

		# Print ugly debugging
		print STDERR "inserted $old_date-$old_time-$old_person-$$typeref[$i] (result: $resultcodes[$i])\n";
	    }

	} else {
	    # Delete record (this is scary, if something goes wrong).
	    $self->{delete}->execute( $old_date, $old_time, $old_person,
				      $self->typenum( $$typeref[$i] ) );

	    # Get result code (# of affected rows)
	    #$resultcodes[$i] = $self->{delete}->rows;
	    $resultcodes[$i] = 1; # Delete should always succeed!

	    # Print some ugly debugging; should be removed
	    print STDERR "deleted $old_date-$old_time-$old_person-$$typeref[$i] (result: $resultcodes[$i])\n";

	}
    }

    # Sum results
    my $total_success;
    $total_success += $_ foreach (@resultcodes);

    # DEBUG
    print STDERR "total success: $total_success\n";

    return $total_success;
}

######################################################################
# erase - delete a record
#
sub erase {
    my $self = shift;

    # Verify parameters
    return 0 unless ($self->valid_key( @_[0,1,2,3] ));

    # Run query
    $self->{delete}->execute( @_[0,1,2], $self->typenum( $_[3] ) );

    # Return number of affected rows (1 or 0)
    return $self->{delete}->rows;
}

######################################################################
# maxtime - return maximum time value for a specified date, type and
# person
#
# This function is used to calculate the next available slot for data
# types that does not use the time value, such as diary entries and
# pictures. If person is omitted, it is ignored.
#
sub maxtime {
    my $self = shift;

    # Check parameters
    return undef unless (&valid_date( $_[0] ) && $self->typenum( $_[1] ) > 0);

    # Find things
    if ($_[2]) {
	return scalar $self->{dbh}->selectrow_array( $self->{maxtimeperson},
						     undef,
						     $_[0],
						     $self->typenum( $_[1] ),
						     $_[2] );
    } else {
	return scalar $self->{dbh}->selectrow_array( $self->{maxtime},
						     undef,
						     $_[0],
						     $self->typenum( $_[1] ) );
    }
}

######################################################################
# latest - get latest entries in database
#
# The latest method is similar to the getday function, with the
# difference that it returns the lastest n entries. If n is not
# specified, a default of 20 is chosen.
#
sub latest {
    my ($self, $limit) = @_;

    # Set default limit
    $limit = $limit + 0; # Force numeric
    $limit = 20 if ($limit <= 0 || $limit >=100);

    # Execute query
    unless ($self->{latest}->execute( $limit )) {
	print STDERR $self->{latest}->errstr;
	return undef; # On error, return undef
    }

    # Get it
    return $self->{latest}->fetchall_arrayref({});
}

######################################################################
# search - search database
#
sub search {
    my ($self, $search, $limit) = @_;

    # Set default limit
    $limit = $limit + 0; # Force numeric
    $limit = 20 if ($limit <= 0 || $limit >=1000);

    # Set search string
    #$search = '%'.$search.'%';

    #$self->{search}->trace(2);

    # Execute query
    unless ($self->{search}->execute( $search, $limit )) {
	print STDERR $self->{search}->errstr;
	return undef; # On error, return undef
    }

    # Get it
    return $self->{search}->fetchall_arrayref({});
}


######################################################################
# gallery - get a list of all pictures in the data base
#
# This function returns all pictures in the database, or, to be more
# specific, all picture comments. This can be used to build a
# gallery. If the database grows with hundreds of pictures, I will
# need to build in some restictions. This is, however, not necessary
# today. I think there is a total of five picture in the
# database... :-)
#
sub gallery {
    my $self = shift;

    # Execute query
    unless ($self->{gallery}->execute()) {
	print STDERR $self->{gallery}->errstr;
	return undef; # On error, return undef
    }

    # Get it
    return $self->{gallery}->fetchall_arrayref({});
}

######################################################################
# event - get event for specific date
#
sub event {
    my ($self, $date) = @_;

    # Execute query
    unless ($self->{get_event}->execute( $date )) {
	print STDERR $self->{get_event}->errstr;
	return undef; # On error, return undef
    }

    my $comment;

    if ($self->{get_event}->rows >= 1) {
	$comment = ($self->{get_event}->fetchrow_array)[1];
	$self->{get_event}->finish;
    }

    # Get it
    return $comment;
}

######################################################################
# set_event - insert event for specific date
#
sub set_event {
    my ($self, $date, $comment) = @_;

    # If comment is an empty string or not set, delete it instead
    unless ($comment) {
	$self->{delete_event}->execute( $date );
	return ($self->{delete_event}->rows == 1) ? 1 : 0;
    }

    # Try update
    $self->{update_event}->execute( $comment, $date );

    # If update failed, try insert
    if ($self->{update_event}->rows == 0) {
	$self->{insert_event}->execute( $date, $comment );
	return ($self->{insert_event}->rows == 1) ? 1 : 0;
    }
    return 1;
}

#  ######################################################################
#  # get_workout - get workout text for specified person/day
#  #
#  sub get_workout {
#      my ($self, $date, $person) = @_;

#      # Execute query
#      unless ($self->{get_workout}->execute( $date, $person )) {
#  	print STDERR $self->{get_workout}->errstr;
#  	return undef; # On error, return undef
#      }

#      my $comment;

#      if ($self->{get_workout}->rows >= 1) {
#  	$comment = ($self->{get_workout}->fetchrow_array)[0];
#  	$self->{get_workout}->finish;
#      }

#      # Get it
#      return $comment;
#  }

#  ######################################################################
#  # latest_workout - get latest workout date for person
#  #
#  sub last_workout {
#      my ($self, $person) = @_;

#      # Execute query
#      unless ($self->{max_workout}->execute( $person )) {
#  	print STDERR $self->{max_workout}->errstr;
#  	return undef; # On error, return undef
#      }

#      my $date;

#      if ($self->{max_workout}->rows >= 1) {
#  	$date = ($self->{max_workout}->fetchrow_array)[0];
#  	$self->{max_workout}->finish;
#      }

#      # Get it
#      return $date;
#  }

#  ######################################################################
#  # set_workout
#  #
#  sub set_workout {
#      my ($self, $date, $person, $comment) = @_;

#      $self->{dbh}->trace(2);

#      # If comment is an empty string or not set, delete it instead
#      unless ($comment) {
#  	$self->{delete_workout}->execute( $date, $person );
#  	return ($self->{delete_workout}->rows == 1) ? 1 : 0;
#      }

#      # Try update
#      $self->{update_workout}->execute( $comment, $date, $person );

#      # If update failed, try insert
#      if ($self->{update_workout}->rows == 0) {
#  	$self->{insert_workout}->execute( $date, $person, $comment );
#  	return ($self->{insert_workout}->rows == 1) ? 1 : 0;
#      }
#      return 1;
#  }


######################################################################
# valid_key - verify database key
#
# Verify that the index key has valid values. Returns true or
# false. Does no database lookups.
#
sub valid_key {
    my $self = shift;

    if (scalar @_ != 4) {
	print STDERR "verify_key: key must have exactly 4 fields\n"
	  if ($Debugging);
	return 0;
    }
    if ( ! ($_[0] && $_[1] && $_[2] && $_[3]) ) {
	print STDERR "verify_key: undefined key value not allowed\n"
	  if ($Debugging);
	return 0;
    }
    if ( $_[0] !~ m/^\d\d\d\d-\d\d-\d\d$/ ) {
	print STDERR "verify_key: invalid date '$_[0]'\n"
	  if ($Debugging);
	return 0;
    }
    if ( $_[1] !~ m/^\d\d:\d\d:\d\d$/ ) {
	print STDERR "verify_key: invalid time '$_[1]'\n"
	  if ($Debugging);
	return 0;
    }
    if ( $self->typenum( $_[3] ) == 0 ) {
  	print STDERR "verify_key: unknown type '$_[3]'\n"
  	  if ($Debugging);
  	return 0;
    }
    return 1;
}

######################################################################
# dsn - get or set DBI dsn
#
sub dsn {
    my $self = shift;
    if (@_) { $self->{dsn} = shift };
    return $self->{dsn}
}

######################################################################
# user - get or set DBI username
#
sub user {
    my $self = shift;
    if (@_) { $self->{user} = shift };
    return $self->{user}
}

######################################################################
# password - get or set DBI password
#
sub password {
    my $self = shift;
    if (@_) { $self->{password} = shift };
    return $self->{password}
}

######################################################################
# webuser - get or set webdiary username
#
sub webuser {
    my $self = shift;
    if (@_) {
	if ($self->{dbh}) {
	    # If we have a database conection, look up the user name
	    $self->{webuser} = $self->{dbh}->selectrow_array(
              'SELECT user FROM users WHERE user=?', undef, $_[0] );
	} else {
	    # Without a database connection... well, what can we do?
	    $self->{webuser} = undef;
	}
    }
    return $self->{webuser}
}

1;
