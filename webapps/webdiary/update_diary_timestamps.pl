#!/usr/bin/perl -w
#
# $Id$
#
# Ugly program.
#

use strict;
use File::Basename;
use File::Find;
use DBI;

my $last_filename;
my $filename;
my %cdate;
my %mdate;

&find( \&google, '/home/stefan/jos' );

my $dbh = DBI->connect( 'DBI:mysql:webdiary:localhost', 'webdiary', 'n0rrlan6' ) or die;

foreach my $date (sort keys %cdate) {
    foreach my $person (sort keys %{$cdate{$date}}) {
	unless ($mdate{$date}{$person}) {
	    $mdate{$date}{$person} = $cdate{$date}{$person};
	}
	my $rows = $dbh->do( q{
	    UPDATE data
	    SET cdate=?, cuser=?, mdate=?, muser=?
	    WHERE date=? AND time=? AND person=? AND type=1
	    },
			     undef,
			     $cdate{$date}{$person},
			     $person,
			     $mdate{$date}{$person},
			     $person,
			     $date,
			     '00:00:00',
			     $person );
	if ($rows != 1) {
	    print STDERR "NOT UPDATED $date $person\n";
	}
    }
}

$dbh->disconnect;

exit;

sub google
{
    if (m/^dagbok\.txt\.(\d\d\d\d)(\d\d)(\d\d)\.(\d\d)(\d\d)(\d\d)$/) {
	$filename = $_;
	my $date = "$1-$2-$3";
	my $time = "$4:$5:$6";
	if ($last_filename) {
	    my @diff = `diff $last_filename $filename`;
	    foreach (@diff) {
		if (m/\>\s+\#\#\#(\d\d\d\d-\d\d-\d\d)\/(\w+)/) {
		    if ($cdate{$1}{$2}) {
			$mdate{$1}{$2} = "$date $time";
		    } else {
			$cdate{$1}{$2} = "$date $time";
		    }
		}
	    }
	}
	$last_filename = $filename;
    }
}
