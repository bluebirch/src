#!/usr/bin/perl -w
#
# $Id$
#
# Parse mailbox and dump/update mail in database
#
# $Log$
# Revision 1.6  2002/01/07 09:20:18  stefan
# Adapted program to new WDDBI module. It amazes me a bit that I have
# not done this before.
#

use strict;
use Time::Local;
use Date::Parse;
use WDDBI;
use calendar;

my $mailbox = '/home/stefan/mail/familjen/jenny';

my %month = ( 'Jan' => '01',
	      'Feb' => '02',
	      'Mar' => '03',
	      'Apr' => '04',
	      'May' => '05',
	      'Jun' => '06',
	      'Jul' => '07',
	      'Aug' => '08',
	      'Sep' => '09',
	      'Oct' => '10',
	      'Nov' => '11',
	      'Dec' => '12' );

my $db = new WDDBI;
$db->dsn( 'DBI:mysql:webdiary:localhost' );
$db->user( 'webdiary' );
$db->password( 'n0rrlan6' );

$db->init( 'Robot' ) or die "Can't init database";
open MAILBOX, $mailbox or die "Can't open mailbox $mailbox";

my $nl = 1; # newline flag
my $headers = 0; # header flag
my $date; # date
my $time;
my $unixtime;
#my $tz; # timezone
my $from; # sender
my $person;
my @data;

while (<MAILBOX>) {
    if ($nl && m/^From ([\w@\.-]+)\s+(\w{3}\s+\w{3}\s+\d{1,2}\s+\d\d:\d\d:\d\d \d\d\d\d)/) {
	# Starting a new mail
	&addmail if (@data);
	$from = $1;
	$unixtime = str2time( $2 );
	$date = &local_date( localtime( $unixtime  ) );
	$time = &local_time( localtime( $unixtime ) );
	$headers = 1;
	@data = ();
    } else {
	push @data, $_ if ($_);
	if ($headers && !$nl) {
	    my ($hdr, $val) = split( ':\s+', $_, 2 );
	    chomp( $val ) if ($val);
	    if ($hdr eq 'From') {
		$from = $val;
	    } elsif ($hdr eq 'Date') {
		$unixtime = str2time( $val );
		$date = &local_date( localtime( $unixtime ) );
		$time = &local_time( localtime( $unixtime ) );
	    }
	} elsif ($headers && $nl) {
	    $headers = 0;
	} elsif (m/^From / ) {
	    print STDERR "something lost my mind\n";
	    die;
	}
    }
    if ($_ eq "\n") {
	$nl = 1;
    } else {
	$nl = 0;
    }
}
&addmail if (@data);

close MAILBOX;

sub addmail {
    local($1,$2,$3,$4,$5,$6,$7,$8);

    if ($from =~ m/stefan/i) {
	$person = 'Stefan';
    } elsif ($from =~ m/jenny/i) {
	$person = 'Jenny';
    } else {
	return;
    }

    my $data = join( '', @data );

    my $thing = $db->get( $date, $time, $person, 'mail' );
    if ($thing) {
	if ($$thing{data} ne $data) {
	    #$db->update( $date, $time, $person, 'mail', $data ) or
	    #  die "update failed for $person, $date $time\n";
	    print "already exist $person $date $time\n";
	}
    } else {
	$db->insert( $date, $time, $person, 'mail', $data ) or
	  die "insert failed for $person, $date $time\n";
	print "inserted $person $date $time\n";
    }
}
