#!/usr/bin/perl -w
#
# $Id$
#
# Read licq history and update WebDiary database.
#
# $Log$
# Revision 1.3  2001/12/05 22:34:42  stefan
# Still extremly ugly, but faster and more accurate.
#
#

use strict;
use WDDBI;
use calendar;

my %type = ( 1 => 'msg',
	     4 => 'url' );

my $licq_history = "/home/stefan/.licq/history/61453835.history";
my $history_history = "/home/stefan/.licq/history/licq-update";

# Create WDDBI object
my $db = new WDDBI;

# Set up MySQL parameters
$db->dsn( 'DBI:mysql:webdiary:localhost' );
$db->user( 'webdiary' );
$db->password( 'n0rrlan6' );

# Initialize database connection
$db->init( 'Robot' ) || die "Unable to initialize database engine";

my $lastline = 0;
my $line = 0;

# Open history file
if (open( LAST, $history_history )) {
    $_ = <LAST>;
    chomp;
    $lastline = 0 + $_;
    close( LAST );
}

# Open licq log file
open( HISTORY, $licq_history ) || die "Could not open $licq_history";

# Skip lines
#print STDERR "Skipping $lastline lines\n";
for( my $i=0; $i < $lastline; $i++ ) { <HISTORY>; $line++ };

# Variables
my $action = '';
my $type = '';
my $unixtime = 0;
my @msg = ();
my $lastunixtime;
my $reallasttime = 0;
my $lastperson = '';

while (<HISTORY>) {
    $line++;
    chomp;
    if (m/^\[\s(\w)\s\|\s0*(\d+)\s\|\s0*(\d+)\s\|\s0*(\d+)\s\|\s(-?\d+)\s\]/) {
	&add if (scalar @msg > 0);
	$action = $1;
	$type = $2;
	$unixtime = $5;
	@msg = ();
    } elsif (m/^:(.+)/) {
	push @msg, $1;
    } elsif (!m/^:?$/) {
	die "Can't handle input line '$_'";
    }
}
&add if (scalar @msg > 0);
close( HISTORY );

#$db->shutdown;

open( LAST, ">".$history_history );
#print STDERR "Last line is $line\n";
print LAST $line;
close( LAST );

exit;

# Add a message; use global variables, no parameters
sub add {

    my $date = &local_date( localtime( $unixtime ) );
    my $time = &local_time( localtime( $unixtime ) );
    my $msg = join( "\n", @msg );
    my $person;
    if ($action eq 'S') {
	$person = 'Stefan';
    } elsif ($action eq 'R') {
	$person = 'Jenny';
    } else {
	die "Unknown action";
    }
    $msg .= "\n";

    my $event = "[$person $date $time $line]";

    # Verify time; i have some stupid times in the log.
    if (not defined $lastunixtime) {
	$lastunixtime = $unixtime-1;
    }

    # Check for doubles
    if ($unixtime == $reallasttime && $person eq $lastperson) {
	$reallasttime = $unixtime;
	$unixtime = $lastunixtime+1;
#	print STDERR "$event: duplicate; add 1 sec\n";
	$date = &local_date( localtime( $unixtime ) );
	$time = &local_time( localtime( $unixtime ) );
    } else {
	$reallasttime = $unixtime;
    }

    # Set event identifier for printouts; i don't care about
    # performance in this code. Not right now, at least.
    my $elapsed = $unixtime - $lastunixtime;

    # To begin with, check if event occurs in the future.
    if ($unixtime > time) {
#	print STDERR "$event: event occurs in the future; fix\n";
	$unixtime = $lastunixtime+1;
	$date = &local_date( localtime( $unixtime ) );
	$time = &local_time( localtime( $unixtime ) );
	$elapsed = $unixtime - $lastunixtime;
    }

    # Check for ludicrous times
    if ($elapsed < -7200 || $elapsed > 2419200) {
#	print STDERR "$event: elapsed time $elapsed; fix\n";

	my $thing = $db->get( $date, $time, $person, 'icq-message' );
#  	if ($thing) {
#  	    print STDERR "--- OOOPS! ---\n";
#  	}

	$unixtime = $lastunixtime+1;
	$date = &local_date( localtime( $unixtime ) );
	$time = &local_time( localtime( $unixtime ) );
	$elapsed = $unixtime - $lastunixtime;
    }

    # Check for lag.
    if ($elapsed < 0) {
#	print STDERR "$event: lag $elapsed seconds\n";
	if ($elapsed < -7200) {
	    die "Lag more than two hours"
	}
    }

    $event = "[$person $date $time $unixtime]";

    # Check if data exists
    my $thing = $db->get( $date, $time, $person, 'icq-message' );
    if ($thing) {

	# If it does, verify the data
	if ($msg ne $$thing{data}) {
#	    print STDERR "$event: changed\n";
	    $db->update( $date, $time, $person, 'icq-message',
			 $date, $time, $person, 'icq-message', $msg ) or
			   print STDERR "update failed: $date-$time-$person\n";
	}
    } else {
#	print STDERR "$event: new\n";
	$db->insert( $date, $time, $person, 'icq-message', $msg ) or
	  print STDERR "$event: insert failed\n";
    }
    $lastunixtime = $unixtime;
    $lastperson = $person;
}

