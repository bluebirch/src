######################################################################
#
# $Id$
#
# Primitive functions for date and calendar handling.
#
# Written by Stefan Svensson <stefan@raggmunk.nu>
#
######################################################################

package calendar;

use strict;
use warnings;
use Time::Local;
use POSIX qw(strftime);
use POSIX qw(locale_h);

BEGIN {
    use Exporter   ();
    our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

    $VERSION = do { my @r = (q$Revision$ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };
    @ISA         = qw(Exporter);
    @EXPORT      = qw(&valid_date &valid_time &date_calc &time_calc &local_date &local_time &seconds &datestring &timestamp);
    %EXPORT_TAGS = ();
    @EXPORT_OK   = qw();

    setlocale( LC_ALL, "sv_SE.ISO8859-1" );
}

our @EXPORT_OK;

######################################################################
# valid_date    Check if date is valid. Return true or false.
#               This is probably an incredibly ineffectite algorithm,
#               but who cares? It works.

sub valid_date( $ ) {

    # Get the date to be checked
    return 0 unless(my $date = $_[0]);

    # Now, this is fun. First, check if the date is a valid date
    # string. If it is, convert it to UNIX time and back again. If it
    # is still the same, the date must be valid, right? Thakes a
    # little bit of time, perhaps, but why check things on your own
    # when you can let the operating system do the job for you?

    if ($date =~ m/^(\d\d\d\d)-0?(\d{1,2})-0?(\d{1,2})$/) {
	my $time = timelocal( 0, 0, 0, $3, $2-1, $1-1900);
	my ($year, $month, $day);
	(undef, undef, undef, $day, $month, $year,
	 undef, undef, undef) = localtime( $time );
	$year += 1900;
	$month++;
	my $realdate = sprintf( "%04d-%02d-%02d", $year, $month, $day );
	if ($date eq $realdate) {
	    return 1;
	}
    }
    return 0;
}

######################################################################
# valid_time    Verify that time string is valid.
#

sub valid_time( $ ) {
    return 0 unless( my $time = $_[0] );

    if ($time =~ m/^(\d\d):(\d\d):(\d\d)$/) {
	if ($1 >= 0 && $1 <= 23 && $2 >= 0 && $2 <= 59 && $3 >= 0 && $3 <= 59)
	{
	    return 1;
	}
    }
    return 0;
}

######################################################################
# date_calc     Add or substract days from date.
#

sub date_calc {

    return &local_date unless ($_[0] && $_[1]);

    my ($date, $delta) = @_[0,1];


    if ($date =~ m/^(\d\d\d\d)-0?(\d{1,2})-0?(\d{1,2})$/) {
	return strftime( '%Y-%m-%d', localtime( timelocal( 0, 0, 0, $3+$delta, $2-1, $1-1900 ) ) );
    } else {
	return &local_date;
    }
}

######################################################################
# time_calc     Add or substract seconds from time.
#

sub time_calc {
    my $sec = &seconds( $_[0] ) + $_[1];
    return sprintf( '%02d:%02d:%02d', $sec / 3600, ($sec % 3600) / 60,
		    $sec % 60 );
}

######################################################################
# local_date    Return current date (or supplied date)
#

sub local_date {
    my @time = @_ ? @_ : localtime;
    return strftime( '%Y-%m-%d', @time );
}

######################################################################
# local_time    Return current (or supplied) time
#

sub local_time {
    my @time = @_ ? @_ : localtime;
    return strftime( '%H:%M:%S', @time );
}

######################################################################
# seconds       Return seconds since midnight.
#

sub seconds {
    return 0 unless ($_[0]);

    if ($_[0] =~ m/^(\d\d):(\d\d):(\d\d)$/) {
	if ($1 >= 0 && $1 <= 23 && $2 >= 0 && $2 <= 59 && $3 >= 0 && $3 <= 59)
	{
	    return $1*3600 + $2*60 + $3;
	}
    }
    return 0;
}

######################################################################
# datestring    Return the date as a readable string. Use strftime.
#

sub datestring {
    return undef unless ($_[0]);

    if ($_[0] =~ m/^(\d\d\d\d)-0?(\d{1,2})-0?(\d{1,2})$/) {
	return strftime( '%A %e %B %Y',
			 localtime( timelocal( 0, 0, 0, $3, $2-1, $1-1900
					       )
				    )
			 );
    }
    return undef;
}

######################################################################
# timestamp     Returns the current time as a timestamp, suitable for
#               updating the MySQL table.
#
sub timestamp {
    my @t = localtime;
    return sprintf( "%04d%02d%02d%02d%02d%02d", $t[5]+1900, $t[4]+1,
		    $t[3], $t[2], $t[1], $t[0] );
}

1;
