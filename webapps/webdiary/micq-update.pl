#!/usr/bin/perl -w
#
# $Id$
#

use strict;
use db_api;

my $micq_log = '/home/stefan/micq.log/61453835.log';

my %month = ( 'Jan' => '01',
	      'Feb' => '02',
	      'Mar' => '03',
	      'Apr' => '04',
	      'May' => '05',
	      'Jun' => '06',
	      'Jul' => '07',
	      'Aug' => '08',
	      'Sep' => '09',
	      'Oct' => '10',
	      'Nov' => '11',
	      'Dec' => '12' );

open( MICQ_LOG, $micq_log ) or die "Micq log file not found";

&db_init( 'Robot' ) or die "Could not initialize DB";

my ($date, $time, $action, $person, @message);

while( <MICQ_LOG> ) {
    if (m/^(Sun|Mon|Tue|Wed|Thu|Fri|Sat)\s+(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s+(\d+)\s+(\d\d:\d\d:\d\d)\s+(\d\d\d\d)\s+(.+)$/) {

	if (@message) {
	    while ((@message) && ($message[$#message] eq "\n")) {
		$#message--;
	    }
	    if (@message) {
		my $message = join( '', @message );
		# Clean message from newlines and carriage returns
		$message =~ s/\r//g;
		$message =~ s/\n\n/\n/g;
		# Check if message is a URL
		if ($message =~ /�/) {
		    chomp( $message );
		    my ($desc, $url) = split ( /�/, $message );
		    $message = "URL: $url\nBeskrivning: $desc\n";
		}
		if ($action eq 'sent') {
		    $person = 'Stefan';
		} elsif ($action eq 'received') {
		    $person = 'Jenny';
		} else {
		    die "No, no, don't do that!";
		}

		&db_insert( $date, $time, $person, 'icq-message', $message )
		    or die "db_insert failed";
	    }
	}

	$date = $5.'-'.$month{$2}.'-'.sprintf( '%02d', $3 );
	$time = $4;

	if ($6 =~ m/(received|sent)/) {
	    $action = $1;
	} else {
	    $action = '';
	}
	@message = ();
    } else {
	push @message, $_ if ($_ !~ m/^(>|<)$/);
    }
}
&db_shutdown;

exit 0;
