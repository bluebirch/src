#! /usr/bin/perl -w
#
# $Id$
#
# Add things in the database

use strict;
use db_api;
use calendar;
use Getopt::Std;

$db_api::User='stefan';
$db_api::Password='gurgel';

# parse command line
my %opt;

getopts( 'd:t:T:f:', \%opt );

my $date = $opt{d} ? $opt{d} : &local_date;
my $time = $opt{t} ? $opt{t} : &local_time;
my $type = $opt{T} ? $opt{T} : 'general-binary';
my $file = $opt{f} ? $opt{f} : '/dev/stdin';

my $person = $ARGV[0];

unless (&valid_date($date) && &valid_time($time) && $type && $file && $person)
{
    print STDERR "invalid parameters\n";
    exit 1;
}

if (&db_init( $ENV{LOGNAME} )) {

#      unless (&db_preinsert( \$date, \$time, \$person, \$type )) {
#  	print STDERR "error\n";
#  	exit 1;
#      }

    my $thing = &db_get( $date, $time, $person, $type );

    if ($thing) {
	&db_shutdown;
	print STDERR "thing already exists in database\n";
	exit 1;
    }

    # read data
    local $/ = undef;
    unless( open( DATA, $file ) ) {
	&db_shudown;
	print STDERR "file not found: $file\n";
	exit 1;
    }

    my $data = <DATA>;
    close( DATA );

    if (not &db_insert( $date, $time, $person, $type, $data )) {
  	print STDERR "insert failed\n";
    }

    &db_shutdown;

} else {
    print STDERR "init failed\n";
    exit 1;
}
