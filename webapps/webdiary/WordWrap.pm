# $Id$
#
# Wordwrap a scalar and return an array of lines, or, if a scalar is
# expected, return a scalar with inserted newline characters.
#
# This package was originally written to be included in my
# diary/calendar program. I didn't like the Text::Wrap package, so I
# wrote my own.
#
# If you think the source code is poorly documented, you are
# absolutely right. However, nobody forced you to use it, so don't
# blame me.
#
# Stefan Svensson <stefan@raggmunk.nu>
#
# $Log$

package WordWrap;

require Exporter;

@ISA = qw(Exporter);
@EXPORT = qw(wordwrap);
@EXPORT_OK = qw($columns);

$VERSION = do { my @r = (q$Revision$ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

use vars qw($VERSION $columns);
use strict;

BEGIN {
    $columns = 80;
}

sub wordwrap {
    my $paragraph = join( ' ', @_ );
    my @result;
    my $buffer = '';

    my @words = split /\s+/, $paragraph;

    foreach (@words) {
	if ($buffer) {
	    if ((length( $buffer ) + length() + 1) > $columns) {
		push @result, $buffer;
		$buffer = '';
	    } else {
		$buffer .= ' ';
	    }
	}
	$buffer .= $_;
    }
    push @result, $buffer if ($buffer);
    
    return wantarray ? @result : join( /\n/, @result );
}
