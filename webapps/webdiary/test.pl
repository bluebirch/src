#! /usr/bin/perl -w
#
# $Id$
#
# Test program

use strict;
use WDDBI;

# Connect database
my $db = new WDDBI;

$db->dsn( 'DBI:mysql:webdiary:localhost' );
$db->user( $ENV{LOGNAME} );
$db->password( 'gurgel' );

# Log in and initialize queries
$db->init( $ENV{LOGNAME} );

print "Logged in as ", $db->webuser, "\n";

my $spjonk = $db->getday( '2001-09-07' );

foreach my $au (@$spjonk) {
    print "$au\n";
    foreach (keys %$au) {
	print "$_ = $$au{$_}\n";
    }
}

my $spam = $db->get( '2001-01-01', '00:00:00', 'Jenny', 'diary' );

foreach (sort keys %$spam) {
    print "$_\n";
}

# End
print "finish\n";
