#!/usr/bin/perl -w
#
# $Id$
#
# Import old diary

use strict;
use db_api;

my $diary = "/home/stefan/jos/dagbok.txt";

&db_init( $ENV{LOGNAME} ) || die "db_init failed";

open( DIARY, $diary ) || die "open failed";

my $note;
my $date;
my $person;
my $nl;

while(<DIARY>) {
    chomp;
    if (m/^\#\#\#(\d\d\d\d-\d\d-\d\d)\/(\w+)/) {
	&add if ($note);
	
	$note = undef;
	$date = $1;
	$person = $2;
	$nl = 1;
    } elsif (length == 0) {
	$note .= "\n";
	$nl = 1;
    } else {
	if ($nl) {
	    $note .= $_;
	    $nl = 0;
	} else {
	    $note .= ' '.$_;
	}
    }
}
&add if ($note);

close( DIARY );
&db_shutdown;

exit;

sub add {
    &db_insert( $date, '00:00:00', $person, 'diary', $note );
#      print "$person, $date\n";
#      print $note;
#      print "\n";
}
