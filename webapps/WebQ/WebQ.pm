# $Id$
#
# $Log$
# Revision 1.10  2009-09-28 13:55:39  skalman
# Nya WebQ
#
# Revision 1.9  2008-02-19 10:09:59  skalman
# Uppdateringar.
#
# Revision 1.8  2008/02/11 13:10:13  skalman
# Changed format of nominal scales. Some bugfixes. Added logging.
#
# Revision 1.7  2008-02-10 18:41:10  skalman
# Moved contact info.
#
package WebQ;
use strict;
use base 'CGI::Application';
use CGI::Carp;
use File::Find;
use FileHandle;
use Data::Dumper;
#use Config::Tiny;
#use Config::IniFiles;

my $StyleSheet=<<END;
body { width: 50em; font-family: sans-serif; }
th { align: left; font-weight: normal; }
li { margin-bottom: 1em }
p.contact {font-size: 60%; margin-top: 0em; }
.item { }
.response { font-size: 75% }
.small { font-size: 80% }
.itemname { font-size: 50%; color: red }
.odd { background-color: #eeeeee }
.even {}
END

sub setup {
    my $self = shift;
    $self->mode_param( 'action' );
    $self->start_mode( 'begin' );
    $self->run_modes( [qw(begin prepareform displayform saveform download spss feedback)] );

    # Read config file
    open CONFIG, "<config";
    while (<CONFIG>) {
        chomp;
        next if (m/^\s*$/ || m/^#/);
        my ($directive, $data) = split m/\s+/, $_, 2;
        $directive = uc( $directive );
        if ($directive eq 'CODE') {
            my ($code, $form) = split m/\s*=\s*/, $data, 2;
            $self->{codes}->{uc $code} = $form;
        } elsif ($directive eq 'WELCOME')  {
	    $self->{welcome} = $data;
        } elsif ($directive eq 'TITLE')  {
	    $self->{title} = $data;
	}
    }
    close CONFIG;
    
    # Some debugging stuff
    $self->log( "Starting up for ",  $self->query->remote_host, "." );
    my @params = $self->query->param;
    $self->log( "Parameters: ", join( "; ", map( "$_=" . $self->query->param( $_ ), $self->query->param )), "." );
}

sub start_page {
    my ($self, $title) = @_;
    $title = $title ? "WebQ: $title" : "WebQ";
    my $q = $self->query;
    my $t = $q->start_html( -title => $title,
                            -lang => 'sv-SE',
                            -style => { -code => $StyleSheet } );
    return $t;
}

sub begin {
    my $self = shift;
    my $q = $self->query;

    my $t = $self->start_page( $self->{title} );
    $t .= $q->h1( $self->{title} ) if ($self->{title});
    if ($self->{welcome}) {
	$t .= $q->p( $self->{welcome} );
    }
    $t .= $q->start_form( -action => $q->script_name );
    $q->param( 'action', 'prepareform' );
    $t .= $q->hidden( -name => 'action' );
    $t .= $q->p( "Ange din kod: ", $q->password_field( -name=>'code' ),
                 $q->submit( -name => 'submit', -value => 'G� vidare' ) );
    $t .= $q->end_form;
    $t .= $q->end_html;
    return $t;
}

sub prepareform {
    my $self = shift;
    my $q = $self->query;

    my $code = uc( $q->param( 'code' ) );

    if (!$self->{codes}->{$code}) {
        return $self->errorpage( "Felaktig kod" );
    }

    # Select form
    my $form = $self->{codes}->{$code};
    $q->param( 'form', $form );
    if (!$self->parseform( $form )) {
        return $self->errorpage( "Fel vid inl�sning av f�ljande formul�r: $form." );
    }

    # Create participant ID
    my $chars = 'ABCDEFGHKLMNPRSTUV23456789';
    my $participant = '';
    do {
        for (my $i = 1; $i <= 4; $i++) {
            $participant .= substr $chars, int(rand(length($chars))), 1;
        }
    } until (! -f "$form.answer.$participant.txt" );
    $q->param( 'participant', $participant );

    # Next action
    $q->param( 'action', 'displayform' );

    $self->log( "Form $form prepared, participant $participant." );

    # Create output page
    my $t = $self->start_page( $self->{form} );
    $t .= $q->h1( $self->{form} );
    if ($self->{codeinfo}) {
	$t .= $q->p( $self->{codeinfo} );
    }
    $t .= $q->p( "Din svarskod �r <strong>$participant</strong>." );
    $t .= $self->contact;
    $t .= $q->start_form( -action => $q->script_name() );
    $t .= $q->hidden( -name => 'action' );
    $t .= $q->hidden( -name => 'form' );
    $t .= $q->hidden( -name => 'participant' );
    $t .= $q->submit( -name => 'submit', -value => 'Vidare till enk�ten' );
    $t .= $q->end_form;
    $t .= $q->end_html;
    return $t;
}

sub displayform {
    my $self = shift;
    my $q = $self->query;

    # Get and parse form
    my $form = $q->param( 'form' );
    if (!$form) {
        return $self->errorpage( "Inget formul�r angivet." );
    }
    if (!$self->parseform( $form )) {
        return $self->errorpage( "Fel vid inl�sning av f�ljande formul�r: $form." );
    }

    # Get participant
    my $participant = $q->param( 'participant' );
    if (!$participant) {
        $participant = 'XXXX';
        $q->param( 'participant', $participant );
    }

    my $title = ($self->{form} || 'Unnamed form');

    my $t = $self->start_page( $title );
    $t .= $q->h1( $title );
    $t .= $q->start_form( -action => $q->script_name );
    $q->param( 'action', 'saveform' );
    $t .= $q->hidden( -name => 'action' );
    $t .= $q->hidden( -name => 'form' );
    $t .= $q->hidden( -name => 'participant' );

    my $in_table = 0;
    my $number = 1;

    # Step through all items
    foreach my $ref (@{$self->{items}}) {
        my $item = $ref->{item};
        my $type = $ref->{type};
        my $text = $ref->{text};

        # Non-data items
        if ($type =~ m/^(INFO|SECTION|HLINE)$/) {
            if ($in_table) {
                $t .= $q->end_table;
                $in_table = 0;
            }
            if ($type eq 'INFO') {
                $t .= $q->p( $text );
            }
            elsif ($type eq 'SECTION') {
                $t .= $q->h2( $text );
            }
            elsif ($type eq 'HLINE' ) {
                $t .= $q->hr;
            }
        }

        # Data items
        else {

            # Begin list item
            if (!$in_table) {
                $t .= $q->start_table( { -border => 0, -cellspacing => 0 } );
                $in_table = 1;
            }
	    my $class = $number % 2 == 0 ? 'odd' : 'even';
            $t .= $q->start_Tr( { -class => $class } ) . $q->td( $number . "." );
            $number++;

            # Display item text
            $t .= $q->td( { -class => "item" }, $text ) if ($text);

            # Item
            $t .= " " . $q->td( { -class => "itemname" }, $item) if ($self->{showitems});

            # Linebreak
#            $t .= $q->br;

            # Display the input section of the item. This is typeset
            # differently somehow.
            $t .= $q->start_td({ -class => 'response' });

            # Display built-in item types
            if ($type eq 'TEXT') {
                $t .= $q->textarea( -name => $item,
                                    -rows => 3,
                                    -columns => 68 );
            } elsif ($type eq 'STRING') {
                $t .= $q->textfield( -name => $item,
                                     -size => 68 );
            } elsif ($type eq 'NUMERIC') {
                $t .= $q->textfield( -name => $item,
                                     -size => 8 );
            }

            # Display user defined item type
            elsif ($self->{types}->{$type}) {

                if (!$self->{typecache}->{$type}) {

                    # Get basic type
                    my $basetype = uc( shift @{$self->{types}->{$type}} );

                    # Labeled Likert scale
                    if ($basetype =~ m/^LIKERT/) {

                        # Get starting value
                        my $startvalue = shift @{$self->{types}->{$type}};

                        # Get levels
                        my $levels = $basetype eq 'LIKERTSCALE' ?
                                     shift @{$self->{types}->{$type}} :
                                     scalar @{$self->{types}->{$type}};

                        # Create values (there should be an more elegant
                        # way of doing this?)
                        my @values;
                        for my $i (0..$levels-1) {
                            push @values, $startvalue+$i;
                        };

                        # Create labels
                        my %labels;
                        my $i = $startvalue;
                        if ($basetype eq 'LIKERTSCALE') {
                            %labels = map { $i++ => '' } @values;
                        } else {
                            %labels = map { $i++ => $_ } @{$self->{types}->{$type}};
                        }

                        # Set parameters to radio_group
                        my %params = ( -name => '@@',
                                       -values => \@values,
                                       -default => '-' );


                        # Set headings and clear labels if LIKERTSCALE
                       if ($basetype eq 'LIKERTSCALE') {
                            $params{-colheaders} = \@values;
                             $params{-rows} = 1;
                       }

                        # Set colums if LIKERT
                        elsif ($basetype eq 'LIKERT') {
                            $params{-columns} = 1;
                        }

                        # Set labels
                        $params{-labels} = \%labels;

                        # Create radio buttons
                        $self->{typecache}->{$type} = $q->radio_group( %params );
                    }

                    # Nominal data
                    elsif ($basetype =~ m/^(NOMINAL|ORDINAL)/) {
                        my (%labels, @values, $longest);
                        foreach (@{$self->{types}->{$type}}) {
                            my ($value, $label) = split m/\s*=\s*/, $_, 2;
                            $label = $value unless ($label);
                            $labels{$value} = $label;
                            push @values, $value;
                        }

                        $self->{typecache}->{$type} =
                            $q->radio_group( -name => '@@',
                                             -values => \@values,
                                             -labels => \%labels,
                                             -default => '-'
					   );
                    }
                }

                # Get cached data and replace @@ with current item name.
                my $tmp = $self->{typecache}->{$type};
                $tmp =~ s/@@/$item/g;

                # Add user defined to output
                $t .= $tmp;
            } else {
                $t .= "(UNKNOWN ITEM TYPE: $type)";
            }

            # Finish font section
            $t .= $q->end_td();

#           # Add another linebreak
#           $t .= $q->br . '&nbsp;';

            # End item
            $t .= $q->end_Tr();

        }
    }

    # End ordered list
    $t .= $q->end_table if ($in_table);

    $t .= $q->submit( -name => 'save', -value => 'Skicka' );
    $t .= $q->end_form;
    $t .= $self->contact;
    $t .= $q->end_html;

    $self->log( "Form $form displayed, participant=$participant." );

    return $t;
}

sub saveform {
    my $self = shift;
    my $q = $self->query;

    my $form = $q->param( 'form' );
    my $participant = $q->param( 'participant' );

    if (!$form) {
        return $self->errorpage( "Inget formul�r angivet." );
    }

    if (!$participant) {
        $participant = 'unknown';
    }

    if (!$self->parseform( $form )) {
        return $self->errorpage( "Error: $form." );
    }

    # Open file to save data in.
    open DATA, ">$form.answer.$participant.txt";

    print DATA "# Date: ", scalar localtime, "\n";
    print DATA "# From: ", $q->remote_host(), "\n";
    print DATA "# Using: ", $q->user_agent(), "\n";

    # Parse all items according to form; this caches items without
    # answer, and eliminates risk of duplicate/new items.
    foreach my $ref (@{$self->{items}}) {
        my $item = $ref->{item};
        print DATA "$item=", $self->printable( $q->param( $item ) ), "\n" if ($item);
    }

    close DATA;

    my $t = $self->start_page( $self->{form} );
    $t .= $q->h1( $self->{form} );
    $t .= $q->p( "Tack f�r din medverkan!" );
    $t .= $q->p( "Kom ih�g din svarskod: <strong>$participant</strong>." );
    $t .= $self->contact;
    $t .= $q->end_html;

    $self->log( "Form $form, participant $participant saved." );

    return $t;
}

sub download {
    my $self = shift;
    my $q = $self->query;
    $self->header_add( -type => 'text/plain' );

    my $form = $q->param( 'form' );
    
    unless ($self->parseform( $form )) {
        $self->log( "No data." );
        return "# no data";
    }
    
    $self->header_add( '-content-disposition' => "attachment; filename=$form.data" );

    # Build array of items to use
    my (@item,@numerical);
    foreach my $itemref (@{$self->{items}}) {

        # Get type of current item
        my $type = $itemref->{type};

        # Check if this item is a valid data type
        if ($type =~ m/(TEXT|STRING|NUMERIC)/i || $self->{types}->{$type}) {

            # Check if this is a numerical type
            push @numerical, $self->{types}->{$type} && $self->{types}->{$type}->[0] =~ m/LIKERT/i ? 1 : 0;

            # Store item name
            push @item, $itemref->{item};
        }
    }

    my $t = "";
    foreach (@item) {
        $t .= " " if ($t);
        $t .= "\"$_\"";
    }
    $t .= "\n";

    # Find all files with data
    my @files = glob( "$form.answer.*" );

    # Step through files
    foreach my $file (@files) {

        # Get response code
        $file =~ m/answer\.([A-Z1-9]{4})/;
        my $id = $1 || next;

        my %data;

        # Read data file and add all item=data lines to %data-hash
        open DATA, "<$file";
        while (<DATA>) {
            chomp;
            if (m/^(\w+)=(.*)/) {
                $data{$1} = $2;
            }
        }
        close DATA;

        # Print columns
        $t .= "\"$id\"";
        for my $i (0..$#item) {
            $t .= " ";
            if ($numerical[$i]) {
                $t .= $data{$item[$i]} ? $data{$item[$i]} : 'NA';
            } else {
                if (defined $data{$item[$i]}) {
                    # Escape quotes
                    $data{$item[$i]} =~ s/"/\\"/g;
                    # Change ellipsis
                    $data{$item[$i]} =~ s/(?<=\d)-(?=\d)/--/g;
                    
                    $t .= "\"$data{$item[$i]}\"";
                }
                else {
                    $t .= '""';
                }
            }
        }
        $t .= "\n";
    }

#    $self->header_props( -type => 'application/octet-data' );

    $self->log( "Data downloaded." );

    return $t;
}

sub spss {
    my $self = shift;
    my $q = $self->query;
    $self->header_add( -type => 'text/plain' );

    my $form = $q->param( 'form' );
    
    unless ($self->parseform( $form )) {
        $self->log( "No data." );
        return "* no data";
    }
    
    $self->header_add( '-content-disposition' => "attachment; filename=$form.sps" );

    # Build array of items to use
    my (@item, @spss_item, %used_items);
    foreach my $itemref (@{$self->{items}}) {

        # Get type of current item
        my $type = $itemref->{type};

        # Check if this item is a valid data type
        if ($type =~ m/(TEXT|STRING|NUMERIC|ORDINAL)/i || $self->{types}->{$type}) {

            # Store item name
	    push @item, $itemref->{item};
	    $used_items{$itemref->{item}} = 1;

            # Store item type
            if ($self->{types}->{$type} && $self->{types}->{$type}->[0] =~ m/(LIKERT|ORDINAL|NUMERIC|NOMINAL)/i) {
		push @spss_item, $itemref->{item} . ' (F3.0)';
	    }
	    else {
		push @spss_item, $itemref->{item} . ' (A)';
	    }

        }
    }

    # Display header
    my $t = "DATA LIST LIST (\",\") / code (A4) " . join( " ", @spss_item) . ".\n";
    $t .= "BEGIN DATA\n";

    # Find all files with data
    my @files = glob( "$form.answer.*" );

    # Step through files
    foreach my $file (@files) {

        # Get response code
        $file =~ m/answer\.([A-Z1-9]{4})/;
        my $id = $1 || next;

        my %data;

        # Read data file and add all item=data lines to %data-hash
        open DATA, "<$file";
        while (<DATA>) {
            chomp;
            if (m/^(\w+)=(.*)/) {
                $data{$1} = $2;
            }
        }
        close DATA;

        # Print columns
        $t .= $id;
        for my $i (0..$#item) {
            $t .= ",";
	    $t .= $data{$item[$i]} if (defined $data{$item[$i]});
        }
        $t .= "\n";
    }

    $t .= "END DATA.\n";

    foreach my $itemref (@{$self->{items}}) {
	if ($used_items{$itemref->{item}}) {
	    $t .= "VARIABLE LABELS $itemref->{item} '$itemref->{text}'.\n";
	    my $type = $itemref->{type};
            if ($self->{types}->{$type} && $self->{types}->{$type}->[0] =~ m/(LIKERT|ORDINAL)/i) {
		$t .= "VARIABLE LEVEL $itemref->{item} (ORDINAL).\n";
	    } else {
		$t .= "VARIABLE LEVEL $itemref->{item} (NOMINAL).\n";
	    }
	}
    }

    $self->log( "Data downloaded." );

    return $t;
}

sub feedback {
    my $self = shift;
    my $q = $self->query;

    # Get participant
    my $code = $q->param( 'code' );
    if (!$code) {
	return $self->errorpage( "Ingen kod angiven" );
    }

    # Find all possible files for specified participang
    my @files = glob( "*.answer.$code*" );

    # Check that we have found exactly one file
    if (scalar @files == 0) {
	return $self->errorpage( "Ingen deltagare med kod $code." );
    } elsif (scalar @files > 1) {
	return $self->errorpage( "Kod $code har l�mnat flera svar." );
    }

    # Read data file and add all item=data lines to %data-hash
    my %data;
    open DATA, "<$files[0]";
    while (<DATA>) {
	chomp;
	if (m/^(\w+)=(.*)/) {
	    $data{$1} = $2;
	}
    }
    close DATA;

    # Reverse CRI items
    foreach my $item (qw(cri05 cri36 cri49 cri51 cri58 cri59)) {
	$data{$item} = 5 - $data{$item};
    }

    # Calculate scales
    my $cog = 0;
    $cog += $data{$_} foreach qw(cri03 cri06 cri07 cri11 cri12 cri18 
                                 cri23 cri59 cri55);
    my $soc = 0;
    $soc += $data{$_} foreach qw(cri04 cri08 cri09 cri15 cri16 cri25
                                 cri27 cri28 cri30 cri35 cri50 cri53
                                 cri59);
    my $emo = 0;
    $emo += $data{$_} foreach qw(cri02 cri14 cri17 cri19 cri24 cri29
                                 cri31 cri34 cri37 cri39 cri40 cri45
                                 cri47 cri54 cri57 cri59);
    my $sp = 0;
    $sp +=  $data{$_} foreach qw(cri10 cri20 cri22 cri32 cri33 cri38
                                 cri41 cri44 cri46 cri48 cri52);
    my $phy = 0;
    $phy += $data{$_} foreach qw(cri01 cri05 cri13 cri21 cri26 cri36
                                 cri42 cri43 cri51 cri56 cri60);


    # HTML output
    my $t = $self->start_page( $self->{form} );
    $t .= $q->h1( "Feedback" );

    $t .= $q->table( $q->Tr( $q->th( "Skala" ),
			     $q->th( "R�po�ng" ) ),
		     $q->Tr( $q->td( "Kognitiv (COG)" ),
			     $q->td( $cog ) ),
		     $q->Tr( $q->td( "Social (SOC)" ),
			     $q->td( $soc ) ),
		     $q->Tr( $q->td( "Emotionell (EMO)" ),
			     $q->td( $emo ) ),
		     $q->Tr( $q->td( "Andlig/filosofisk (S/P)" ),
			     $q->td( $sp ) ),
		     $q->Tr( $q->td( "Fysisk (PHY)" ),
			     $q->td( $phy ) ),
		     $q->Tr( $q->td( "Total" ),
			     $q->td( $cog+$soc+$emo+$sp+$phy ) )
		   );

    return $t;

}

sub parseform {
    my ($self, $form) = @_;
    open FORM, "<$form.form" or return 0;
    while (my $line = <FORM>) {
        next if (length($line) < 2 || $line =~ m/^[#;%]/);
        chomp $line;
        # Read more if line ends with backslash
        while ($line =~ s/\s*\\$/ /) {
            my $stuff = <FORM>;
            chomp $stuff;
            $stuff =~ s/^\s+//;
            $line .= $stuff;
        }
        my ($directive, $line) = split m/\s+/, $line, 2;
        $directive = lc( $directive );
        if ($directive eq 'form') {
            $self->{form} = $line;
        }
        elsif ($directive eq 'shortform') {
            $self->{shortform} = $line;
        }
	elsif ($directive eq 'codeinfo') {
	    $self->{codeinfo} = $line;
	}
        elsif ($directive eq 'item') {
            $line =~ m/(\w+)\s*\((\w+)\)\s*=\s*(.*)/;
            push @{$self->{items}}, { item => $1,
                                      type => uc($2),
                                      text => $3 };
        }
        elsif ($directive eq 'type') {
            $line =~ m/(\w+)\s*=\s*(.*)/;
            my $type = uc( $1 );
            my $data = [ split m/\s*;\s*/, $2 ];
            $self->{types}->{$type} = $data;
        }
        elsif ($directive =~ m/^(info|instruct)/) {
            push @{$self->{items}}, { type => 'INFO',
                                      text => $line };
        }
        elsif ($directive eq 'section') {
            push @{$self->{items}}, { type => 'SECTION',
                                      text => $line };
        }
        elsif ($directive eq 'hline') {
            push @{$self->{items}}, { type => 'HLINE' };
        }
        elsif ($directive eq 'showitems') {
            $self->{showitems} = 1;
        }
        elsif ($directive eq 'contact') {
            my ($name, $phone, $email) = split m/\s*;\s*/, $line, 3;
            $self->{contact_name} = $name;
            $self->{contact_phone} = $phone;
            $self->{contact_email} = $email;
        }
    }
    close FORM;
#     my $cfg = Config::Tiny->new();
#     $cfg->{items} = $self->{items};
#     $cfg->write( "testx.conf" );
    return 1;
}

sub contact {
    my $self = shift;
    my $q = $self->query;
    return $q->p( "Kontaktperson: $self->{contact_name},",
                  "$self->{contact_phone},",
                  $q->a( {-href=>"mailto:$self->{contact_email}"},
                         $self->{contact_email} ) . "." );
}

sub errorpage {
    my ($self, $message) = @_;
    my $q = $self->query;
    $self->log( "Error: $message" );
    return $self->start_page . $q->h1( "Felmeddelande" ) . $q->p( $message ) . $q->end_html;
}

sub printable {
    my ($self, $string) = @_;
    return '' if (not defined $string);
    $string =~ s/\n/\\n/g;
    $string =~ s/\r/\\r/g;
    return $string
}

sub log {
    my $self = shift;
    if (!$self->{log}) {
        $self->{log} = new FileHandle ">>WebQ.log";
    }
    my $log = $self->{log};
    print $log scalar localtime, ": ", @_, "\n";
}

sub teardown {
    my $self = shift;
    $self->log( "Shutting down." );
    if ($self->{log}) {
        $self->{log}->close;
    }
}

1;
