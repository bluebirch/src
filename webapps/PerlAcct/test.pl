#!/usr/bin/perl -w
use strict;
use warnings;

use Acct::Transaction;
use Acct::SubTransaction;

my $t = Acct::Transaction->new();

$t->add_subtransaction( Acct::SubTransaction->new( amount => 20 ) );
$t->add_subtransaction( Acct::SubTransaction->new( amount => -50 ) );

$t->dump;

print "sum: ", $t->sum, "\n";
