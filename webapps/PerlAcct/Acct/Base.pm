# $Id$
#
# A base object for transactions and other stuff.
#
package Acct::Base;
use strict;
use warnings;

sub new {
    my $this = shift;
    my $class = ref($this) || $this;
    my $self = {};
    bless $self, $class;

    # Get parameters
    if (scalar @_ & 1) {
	warn "Odd number of parameters";
    }
    for (my $i = 0; $i < $#_; $i += 2) {
	$self->{$_[$i]} = $_[$i+1];
    }

    # Initialize
    $self->init();

    return $self;
}

sub init {}

sub dump {
    my $self = shift;

    foreach my $key (sort keys %$self) {
	print STDERR "$key => $$self{$key}\n";
    }
}

1;
