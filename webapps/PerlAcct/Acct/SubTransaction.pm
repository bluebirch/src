# $Id$
#
# A base object for transactions and other stuff.
#
package Acct::SubTransaction;
use base 'Acct::Base';
use strict;
use warnings;

sub init {
    my $self = shift;

    $self->{amount} = 0 unless (defined $self->{amount});
}

sub amount_real {
    my $self = shift;
    return $self->{amount}+0;
}

1;
