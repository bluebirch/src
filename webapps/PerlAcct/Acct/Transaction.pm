# $Id$
#
# A base object for transactions and other stuff.
#
package Acct::Transaction;
use base 'Acct::Base';
use strict;
use warnings;

sub init {
    my $self = shift;

    $self->{subtransactionlist} = [];
}

sub add_subtransaction {
    my $self = shift;
    my $subtransaction = shift;

    push @{ $self->{subtransactionlist} }, $subtransaction;
}

sub sum {
    my $self = shift;
    my $sum = 0;
    foreach my $subtransaction (@{ $self->{subtransactionlist} }) {
	$sum += $subtransaction->amount_real;
    }

    return $sum;
}

1;
