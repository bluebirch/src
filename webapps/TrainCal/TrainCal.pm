# $Id$
#
# $Log$
# Revision 1.40  2004/01/16 15:50:44  zerblat
# Tog bort lite debuginformation. V�rst vad ofta jag gl�mmer kvar s�dant...
#
# Revision 1.39  2004/01/16 15:37:52  zerblat
# Uppgifter om databasen anges i index.cgi. Inloggning sker med epostadress.
# En ny sida kan anv�ndas f�r att skicka l�senordsp�minnelser.
#
# Revision 1.38  2004/01/14 07:25:56  zerblat
# Anpassade tr�ningskalendern till nya servern (neltus.org).
#
# Revision 1.37  2003/04/07 17:37:53  stefan
# Oj d�. Gl�mde lite debugprylar i koden.
#
# Revision 1.36  2003/04/07 17:31:32  stefan
# Fixade bugg.
#
# Revision 1.35  2003/03/20 07:28:44  stefan
# Lade till m�jligheten att visa min- och maxv�rden i viktgrafen. Vid
# redigering av tr�ningspass �terv�nder man till data-vyn.
#
# Revision 1.34  2003/01/18 21:32:55  stefan
# Removed weeks from toplist view.
#
# Revision 1.33  2003/01/18 21:30:59  stefan
# Top list counts only last 6 months.
#
# Revision 1.32  2003/01/09 14:41:33  stefan
# Tog bort de mest elaka kommentarerna.
#
# Revision 1.31  2002/11/16 07:03:47  stefan
# F�rs�kte fixa lite buggar.
#
# Revision 1.30  2002/11/12 14:14:52  stefan
# Har stuvat om en del och lagt till nya runmodes. S�rskilt en ny
# statistiksida. Massor med f�r�ndringar. Dags f�r sj�s�ttning av en ny
# tr�ningskalender!
#
# Revision 1.29  2002/11/02 07:40:46  stefan
# �ndrade algoritmen i viktgrafen. Den tar nu med n�gra extra v�rden f�r
# att f� ett bra v�rde p� medelvikten redan i grafens b�rjan.
#
# Revision 1.28  2002/10/14 20:19:45  stefan
# Har lagt till grunden f�r en statistikgraf och en statistiksida.
#
# Revision 1.27  2002/10/08 06:51:35  stefan
# Lade till ett glidande medelv�rde �ver de 6 senaste m�tningarna i
# grafen.
#
# Revision 1.26  2002/10/04 10:08:06  stefan
# Lade till m�jligheten att v�lja antal dagar i viktgrafen.
#
# Revision 1.25  2002/10/04 08:01:44  stefan
# Lade till y_long_ticks i grafen.
#
# Revision 1.24  2002/10/04 07:52:49  stefan
# Lade till en viktkurva
#
# Revision 1.23  2002/10/03 05:34:44  stefan
# Viktsidan sorterade bara efter datum. Fixat.
#
# Revision 1.22  2002/10/01 20:42:05  stefan
# Lade till viktregistrering.
#
# Revision 1.21  2002/09/10 06:56:33  stefan
# Fixed bug in SQL query.
#
# Revision 1.20  2002/08/16 07:06:20  stefan
# Average calculations count from today, not last training session.
#
# Revision 1.19  2002/06/27 15:45:23  stefan
# Added real time and factor to list of latest training sessions.
#
# Revision 1.18  2002/06/27 15:28:42  stefan
# Removed average statistics if training period is less than a
# week. Added number of weeks of training. Added factor to activities to
# be able to count less (or more) hours for some activities. All changes
# are in SQL, actually.
#
package TrainCal;
use base 'CGI::Application';
use strict;
use DBIx::Easy;
use CookieAuth;
use Date::Calc qw(Today Add_Delta_Days This_Year Week_of_Year);
use Calendar;
use CGI::Logger qw(:all);
use POSIX qw(strftime);
use GD::Graph::linespoints;
use GD::Graph::mixed;
use Mail::Send;

sub setup {
    my $self = shift;

    my $q = $self->query;
    my $u = new CookieAuth;

    $self->run_modes( 'start' => \&start,
		      'summary' => \&summary,
		      'toplist' => \&toplist,
		      'stats' => \&stats,
		      'edit' => \&edit,
		      'login' => \&login,
		      'logout' => \&logout,
		      'userinfo' => \&userinfo,
		      'reminder' => \&reminder,
		      'weight' => \&weight,
		      'weightedit' => \&weight_edit,
		      'weightgraph' => \&weightgraph,
		      'statgraph' => \&statgraph,
		      'calendar' => \&calendar,
		      'data' => \&data );

    $self->start_mode( 'start' );

    my $cookie = $q->cookie( 'session_key' );

    if ($u->validate_key( $cookie )) {

	# Get user information
	my $uid = $u->param( 'uid' );
	my $sth = $self->dbi->process( "select user,fullname,invisible from users where id='$uid'" );
	my $userdata = $sth->fetchrow_hashref;
	$sth->finish;

	# Check that user exists
	if ($userdata) {
	    $u->import_params( $userdata );

#   	    logmsg( "user=", $u->user, "; uid=", $u->param( 'uid' ),
#   		    "; invisible=", $u->param( 'invisible' ),
#   		    "; runmode=", ($q->param( 'rm' ) || '(default)'));
	    $self->user( $u );
	}
	else {
#	    logmsg( "invalid uid=", $u->param( 'uid' ) );
	}
    }
}

sub dbi {
    my $self = shift;
    unless ($self->{dbi}) {
	$self->{dbi} = new DBIx::Easy $self->param( 'dbtype' ), $self->param( 'database' ), $self->param( 'user' ), $self->param( 'password' );
    }
    return $self->{dbi};
}

#
# Display start/welcome page
#
sub start {
    my $self = shift;

    my $u = $self->user;
    my $uid = $u ? $u->param( 'uid' ) : 0;

    # Get latest and statistics (if logged in)
    my $stats = {};
    if ($u) {

	# Statistics
	my $sth = $self->dbi->process( "SELECT to_days(max(date))-to_days(min(date)) as days,time_format(sec_to_time(sum(duration)*60),'%k:%i') AS total,time_format(sec_to_time(sum(duration)*420/(to_days(curdate())-to_days(min(date)))),'%k:%i') AS average, to_days(now())-to_days(max(date)) AS last FROM training WHERE user=$uid" );
	$stats = $sth->fetchrow_hashref;
	$sth->finish;

	# Add some comments
	if (defined $$stats{last}) {
	    if ($$stats{last} == 0) {
		$$stats{last_day} = 'idag';
	    } elsif ($$stats{last} == 1) {
		$$stats{last_day} = 'ig�r';
	    } elsif ($$stats{last} == 2) {
		$$stats{last_day} = 'i f�rrg�r';
	    } else {
		$$stats{last_day} = "f�r $$stats{last} dagar sedan";
	    }
	    if ($$stats{last} > 31) {
		$$stats{comment} = 'Du �r ett hoppl�st fall.';
	    } elsif ($$stats{last} > 14) {
		$$stats{comment} = 'Du �r verkligen lat.';
	    } elsif ($$stats{last} >= 7) {
		$$stats{comment} = 'Du �r lat.';
	    } elsif ($$stats{last} >= 3) {
		$$stats{comment} = 'Kom igen nu!';
	    } elsif ($$stats{last} == 2) {
		$$stats{comment} = 'Dags att tr�na igen!';
	    } elsif ($$stats{last} == 1) {
		$$stats{comment} = 'Du f�rtj�nar en dags vila.';
	    } else {
		$$stats{comment} = 'Du �r duktig!';
	    }
	}
	delete $$stats{last};

    }

    # Load template
    my $t = $self->load_tmpl( 'start.tmpl' );

    # Set default template parameters
    $self->set_defaults( $t );

    # Set all statistics parameters
    $t->param( $stats );

    return $t->output;
}

#
# Display a summary of all training records for all persons
#
sub summary {
    my $self = shift;

    my $u = $self->user;
    my $uid = $u ? $u->param( 'uid' ) : 0;

    # Get toplist
    my $dbi = $self->dbi;
    my $sth = $dbi->process( "select users.id, users.fullname, time_format(sec_to_time(sum(training.duration*activities.factor)*60),'%k:%i') as duration_totals, if( to_days( now() ) - to_days( min( training.date ) ) >= 7, time_format(sec_to_time(sum(training.duration*activities.factor)*420/(to_days(now())-to_days(min(training.date)))),'%k:%i'), '-') as average, truncate((to_days(now())-to_days(min(training.date)))/7,0) as weeks, truncate(sum(training.duration*activities.factor),0) as duration from training,users,activities where training.user=users.id and training.activity=activities.id and (users.invisible != 1 or users.id=$uid) group by users.fullname order by average desc" );
    my $data = $sth->fetchall_arrayref({});

    # Get winner of the week and last week
    $sth = $dbi->process( q{select fullname,time_format(sec_to_time(sum(duration)*60),'%k:%i') as duration from training,users where week(date,1)=week(now(),1) and training.user=users.id and users.invisible != 1 group by fullname order by duration desc limit 3} );
    my $week_winners = $sth->fetchall_arrayref({});

    $sth = $dbi->process( "select week(now(),1)" );
    my $week = $sth->fetchrow_array;

    $sth = $dbi->process( q{select fullname,time_format(sec_to_time(sum(duration)*60),'%k:%i') as duration from training,users where week(date,1)=week(now()-interval 7 day,1) and training.user=users.id and users.invisible != 1 group by fullname order by duration desc limit 3} );
    my $last_week_winners = $sth->fetchall_arrayref({});



    # Get individual statistics for each user
    foreach my $ref (@$data) {
	$sth = $dbi->process( "select activities.activity,time_format(sec_to_time(sum(training.duration)*60),'%k:%i') as duration,sum(training.distance) as distance,activities.distance_units,activities.id as activity_id from training,activities where training.activity=activities.id and training.user=$$ref{id} group by activities.activity" );
	$$ref{rows} = $sth->rows;
	$$ref{statistics} = $sth->fetchall_arrayref({});
    }

    # Get latest and statistics (if logged in)
    my $latest = [];
    my $stats = {};
    if ($u) {

	# Latest
	$sth = $dbi->process( "SELECT training.id,training.date,activities.activity,time_format(sec_to_time(training.duration*60),'%k:%i') AS duration,distance,activities.distance_units,activities.factor,training.comment,time_format(sec_to_time(round(training.duration*activities.factor)*60),'%k:%i') AS real_duration FROM training,users,activities WHERE training.user=users.id AND training.activity=activities.id AND training.user='$uid' ORDER BY date DESC LIMIT 10" );
	$latest = $sth->fetchall_arrayref({});

	# Statistics
	$sth = $dbi->process( "select to_days(max(date))-to_days(min(date)) as days,time_format(sec_to_time(sum(duration)*60),'%k:%i') as total,time_format(sec_to_time(sum(duration)*420/(to_days(max(date))-to_days(min(date)))),'%k:%i') as average,count(date) as count, to_days(now())-to_days(max(date)) as last from training where user=$uid" );
	$stats = $sth->fetchrow_hashref;
	$sth->finish;

	if (defined $$stats{last}) {
	    if ($$stats{last} == 0) {
		$$stats{last_day} = 'idag';
	    } elsif ($$stats{last} == 1) {
		$$stats{last_day} = 'ig�r';
	    } elsif ($$stats{last} == 2) {
		$$stats{last_day} = 'i f�rrg�r';
	    } else {
		$$stats{last_day} = "f�r $$stats{last} dagar sedan";
	    }
	    if ($$stats{last} > 31) {
		$$stats{comment} = 'Du �r ett hoppl�st fall.';
	    } elsif ($$stats{last} > 14) {
		$$stats{comment} = 'Du �r inte bara lat. Du �r fet ocks�.';
	    } elsif ($$stats{last} >= 7) {
		$$stats{comment} = 'Du �r lat.';
	    } elsif ($$stats{last} >= 3) {
		$$stats{comment} = 'Kom igen nu!';
	    } elsif ($$stats{last} == 2) {
		$$stats{comment} = 'Dags att tr�na igen!';
	    } elsif ($$stats{last} == 1) {
		$$stats{comment} = 'Du f�rtj�nar en dags vila.';
	    } else {
		$$stats{comment} = 'Du �r duktig!';
	    }
	}

    }

    # Load template
    my $t = $self->load_tmpl( 'summary.tmpl',
			      global_vars => 1,
			      die_on_bad_params => 0,
			      loop_context_vars => 1 );

    # Set default template parameters
    $self->set_defaults( $t );

    # Set all statistics parameters
    for my $param (keys %$stats) {
	$t->param( $param => $$stats{$param} );
    }

    $t->param( data => $data );
    $t->param( latest => $latest );
    $t->param( week => $week );
    $t->param( week_winners => $week_winners );
    $t->param( last_week_winners => $last_week_winners );
    if ($u) {
	$t->param( logged_in => 1 );
	$t->param( fullname => $u->param( 'fullname' ) );
    }

    return $t->output;
}

#
# Display toplist
#
sub toplist {
    my $self = shift;
    my $dbi = $self->dbi;
    my $q = $self->query;
    my $u = $self->user;

    # Get uid
    my $uid = $u ? $u->param( 'uid' ) : 0;

    # Get toplist
    my $sth = $dbi->process( "SELECT users.id, users.fullname, time_format(sec_to_time(sum(training.duration*activities.factor)*60),'%k:%i') AS duration_totals, if( to_days( now() ) - to_days( min( training.date ) ) >= 7, time_format(sec_to_time(sum(training.duration*activities.factor)*420/(to_days(curdate())-to_days(min(training.date)))),'%k:%i'), '-') AS average FROM training,users,activities WHERE training.date >= CURDATE() - INTERVAL 6 MONTH AND training.user=users.id AND training.activity=activities.id AND (users.invisible != 1 OR users.id=$uid) GROUP BY training.user ORDER BY average DESC" );
    my $toplist = $sth->fetchall_arrayref({});

    # Get year and week
    my ($week,$year) = Week_of_Year( Today() );
    $year = $q->param( 'year' ) if ($q->param( 'year' ));
    $week = $q->param( 'week' ) if ($q->param( 'week' ));

    # Get winner of the week and last week
    $sth = $dbi->process( "SELECT users.id,users.fullname,time_format(sec_to_time(sum(duration)*60),'%k:%i') AS duration FROM training,users WHERE week(date,1)=$week AND year(date)=$year AND training.user=users.id AND (users.invisible != 1 OR users.id=$uid) GROUP BY training.user ORDER BY duration DESC" );
    my $weekwinner = $sth->fetchall_arrayref({});

    # Load template
    my $t = $self->load_tmpl( 'toplist.tmpl',
			      loop_context_vars => 1 );

    # Set default template parameters
    $self->set_defaults( $t );

    # Set template parameters
    $t->param( toplist => $toplist,
	       weekwinner => $weekwinner,
	       year => $year,
	       week => $week );

    return $t->output;

}

#
# Display registered data
#
sub data {
    my $self = shift;
    my $q = $self->query;
    my $u = $self->user;

    # Verify user is logged on
    return $self->errpage( "Du m�ste logga in f�rst!" )
      unless ($u);

    # Get user id
    my $uid = $u->param( 'uid' );

    # Latest
    my $sth = $self->dbi->process( "SELECT training.id,training.date,activities.activity,time_format(sec_to_time(training.duration*60),'%k:%i') AS duration,distance,activities.distance_units,training.comment,time_format(sec_to_time(round(training.duration*activities.factor)*60),'%k:%i') AS real_duration FROM training,users,activities WHERE training.user=users.id AND training.activity=activities.id AND training.user='$uid' ORDER BY date DESC" );
    my $latest = $sth->fetchall_arrayref({});

    # Initialize template
    my $t = $self->load_tmpl( 'data.tmpl' );

    $self->set_defaults( $t );

    $t->param( latest => $latest );

    return $t->output;
}

#
# Display some statistics
#
sub stats {
    my $self = shift;
    my $dbi = $self->dbi;
    my $q = $self->query;
    my $u = $self->user;

    # Get a list of users
    my ($sth, $id);
    if ($u) {
	$id = $q->param( 'uid' ) || $u->param( 'uid' );
	$sth = $dbi->process( "SELECT fullname,id FROM users WHERE id=$id OR invisible=0 ORDER BY fullname" );
    }
    else {
	$id = $q->param( 'uid' );
	$sth = $dbi->process( "SELECT fullname,id FROM users WHERE invisible=0 ORDER BY fullname" );
    }
    my $users = $sth->fetchall_arrayref( {} );
    if ($id) {
	$self->selected( $users, 'id', $id );
    }

    # Get a list of activities
    my $activity = $q->param( 'activity' );
    my $activities = [];
    if ($id) {
	$sth = $dbi->process( "SELECT DISTINCT activities.id, activities.activity FROM activities, training WHERE training.activity=activities.id AND training.user=$id ORDER BY activities.activity" );
	$activities = $sth->fetchall_arrayref({});
	if ($activity) {
	    $self->selected( $activities, 'id', $activity );
	}
    }

    # Get summary
    my $summary = {};
    if ($id) {
	$sth = $dbi->process( "SELECT time_format(sec_to_time(sum(duration)*60),'%k:%i') AS total,time_format(sec_to_time(sum(duration)*420/(to_days(now())-to_days(min(date)))),'%k:%i') AS average,count(date) AS count FROM training WHERE user=$id" );
	$summary = $sth->fetchrow_hashref;
	$sth->finish;
    }


    # Get general statistics
    my $statistics = [];
    if ($id) {
	$sth = $dbi->process( "SELECT activities.activity,time_format(sec_to_time(sum(training.duration)*60),'%k:%i') AS duration,sum(training.distance) AS distance,activities.distance_units FROM training,activities WHERE training.activity=activities.id AND training.user=$id GROUP BY activities.activity" );
	$statistics = $sth->fetchall_arrayref({});
    }

    # Get statistics for a specific activity
    my $activityinfo = {};
    my $activitystats = {};
    if ($id && $activity) {

	# Get activity information
	$sth = $dbi->process( "SELECT * FROM activities WHERE id=$activity" );
	$activityinfo = $sth->fetchrow_hashref;
	$sth->finish;

	# Get activity statistics
	$sth = $dbi->process( "SELECT count(id) as activity_count, time_format(sec_to_time(sum(duration)*60), '%k:%i') AS total_time, time_format(sec_to_time(avg(duration)*60), '%k:%i') AS average_time, sum(distance) AS total_distance, round(avg(distance),1) AS average_distance FROM training WHERE user=$id AND activity=$activity" );
	$activitystats = $sth->fetchrow_hashref;
	$sth->finish;

    }

    # Load template
    my $t = $self->load_tmpl( 'stats.tmpl' );

    # Set default parameters
    $self->set_defaults( $t );

    # Set parameters
    $t->param( $summary );
    $t->param( $activitystats );

    $t->param( users => $users,
	       activities => $activities,
	       statistics => $statistics,
	       uid => $id,
	       activity => $activity,
	       distance_units => $$activityinfo{distance_units} );

    $t->param( activitydiststats => 1 ) if ($$activityinfo{distance_units});

    return $t->output;
}

#
# Display a calendar
#
sub calendar {
    my $self = shift;

    my $q = $self->query;
    my $u = $self->user;
    my $dbi = $self->dbi;
    my $c = new Calendar;

    # Verify that user is logged in
    return $self->errpage( 'Du m�ste logga in f�r att se kalendern.' )
      unless ($u);
    my $uid = $q->param( 'uid' ) || $u->param( 'uid' );
#      my $uid = 1;

    # Get user info for requested user
    my $sth = $dbi->process( "select fullname from users where id=$uid" );
    my $fullname = $sth->fetchrow_array;
    $sth->finish;

    return $self->errpage( 'Ok�nd anv�ndare.' ) unless ($fullname);

    # Get a list of users
    $sth = $dbi->process( "SELECT fullname,id FROM users WHERE id=$uid OR invisible=0 ORDER BY fullname" );
    my $users = $sth->fetchall_arrayref( {} );
    $self->selected( $users, 'id', $uid );


    # Get all CGI parameters
    my $data = $q->Vars;
    my @date = localtime;

    # Set date parameters
    my $year = $$data{year} || $date[5]+1900;
    my $month = $$data{month} || $date[4]+1;

    # Get data from database
    $sth = $dbi->process( "select dayofmonth(date) as day,training.id,concat( activities.activity, if( activities.distance_units = '' or isnull( training.distance ), ' ', concat( ' ', training.distance, ' ', activities.distance_units, ' ' ) ), time_format( sec_to_time( training.duration * 60 ), '%k:%i' ) ) as text from training,activities where training.activity=activities.id and user=$uid and year(date)=$year and month(date)=$month order by day" );

    my $caldata = $sth->fetchall_arrayref;

    # Create month object
    my $m = $c->get_month( "$year-$month-1" );

    # Set current day, if current date is in this month

    if ($month == $date[4]+1 && $year == $date[5]+1900) {
	$m->param( $date[3], 'today' => 1 );
    }

    # Insert data into month object
    my %daydata;
    foreach my $ref (@$caldata) {
	push @{$daydata{$$ref[0]}}, { text => $$ref[2],
				      id => $$ref[1] };
    }
    foreach my $day (keys %daydata) {
	$m->param( $day, 'activities', $daydata{$day} );
    }

    # Load template
    my $t = $self->load_tmpl( 'calendar.tmpl',
			    die_on_bad_params => 0 );

    # Set default parameters
    $self->set_defaults( $t );

    # Set parameters
    $t->param( month => $m->name,
	       year => $m->year,
	       calendar => $m->matrix,
	       fullname => $fullname,
	       uid => $uid,
	       users => $users,
	       next_month => $month==12 ? 1 : $month+1,
	       next_year => $month==12 ? $year+1 : $year,
	       last_month => $month==1 ? 12 : $month-1,
	       last_year => $month==1 ? $year-1 : $year );

    return $t->output;
}

#
# Display weight
#
sub weight {
    my $self = shift;

    # Store internal objects in local variables
    my $q = $self->query;
    my $u = $self->user;
    my $dbi = $self->dbi;

    # Verify that user is logged in
    return $self->errpage( 'Du m�ste logga in f�rst.' ) unless ($u);


    # Get uid
    my $uid = $u->param( 'uid' );

    # Get latest weight information
    my $sth = $dbi->process( "select * from weight where user=$uid order by date desc, time desc" );
    my $data = $sth->fetchall_arrayref({});


    # Initialize template
    my $t = $self->load_tmpl( 'weight.tmpl',
			      die_on_bad_params => 0 );

    # Set default template parameters
    $self->set_defaults( $t );

    # Set paremeters
    $t->param( 'weight' => $data );
    $t->param( 'days' => $q->param( 'days' ) );
    $t->param( 'max' => $q->param( 'max' ) );
    $t->param( 'min' => $q->param( 'min' ) );
    $t->param( 'date' => strftime( '%Y-%m-%d', localtime ) );
    $t->param( 'time' => strftime( '%H:%M:%S', localtime ) );

    # Return template
    return $t->output;
}

#
# Edit weight
#
sub weight_edit {
    my $self = shift;

    # Store instance objects in local variables
    my $q = $self->query;
    my $u = $self->user;
    my $dbi = $self->dbi;

    # Verify that user is logged in
    return $self->errpage( 'Du m�ste logga in f�rst.' ) unless ($u);

    # Get user ID
    my $uid = $u->param( 'uid' );

    # Get ID
    my $id = $q->param( 'id' );

    # Prepare data record
    my $record = {};

    # Save stuff if save button pressed
    if ($q->param( 'save' )) {
	my $data = $q->Vars;
	if ($$data{date} && $$data{time} && $$data{uid} && $$data{weight} ) {

	    # Check date is valid
	    if ($$data{date} !~ m/^\d\d\d\d-\d\d-\d\d$/) {
		$q->param( 'message' => 'Felaktigt datumformat.' );
	    }

	    # Check time is valid
	    if ($$data{time} !~ m/^\d\d(:\d\d)?(:\d\d)?$/) {
		$q->param( 'message' => 'Felaktigt datumformat.' );
	    }
	    unless ($1) {
		$$data{time} .= ':00';
	    }
	    unless ($2) {
		$$data{time} .= ':00';
	    }

	    # Check data validity
	    unless ($$data{weight} =~ m/^[0-9]+(\.[0-9]+)?$/) {
		$q->param( 'message' => 'Ange vikten som ett decimaltal. Anv�nd punkt som kommatecken.' );
	    }

	    # Check there are no errors
	    unless ($q->param( 'message' )) {

	    # Update table if ID is set.
		if ($$data{id}) {
		    my $rows = $self->dbi->update( 'weight',
						   "id=$$data{id}",
						   user => $$data{uid},
						   date => $$data{date},
						   time => $$data{time},
						   weight => $$data{weight},
						   comment => $$data{comment}
						 );

		    # If update succeeded, return browser to start page
		    if ($rows == 1) {
			$self->header_props( -uri => $q->url.'?rm=weight' );
			$self->header_type( 'redirect' );
			return '';
		    } else {
# 			logmsg "Update failed for id=$$data{id}.";
# 			foreach my $key (sort keys %$data) {
# 			    logmsg "$key=$$data{$key}";
# 			}
			$q->param( 'message' => 'Uppdatering misslyckades.' );
		    }
		}

		# Otherwise, insert new record
		else {
		    # No error handling. Sucks, really.
		    $self->dbi->insert( 'weight',
					user => $$data{uid},
					date => $$data{date},
					time => $$data{time},
					weight => $$data{weight},
					comment => $$data{comment} 
				      );

		    # And redirect. Still no error handling.
		    $self->header_props( -uri => $q->url.'?rm=weight' );
		    $self->header_type( 'redirect' );
		    return '';
		}
	    }
	}
	else {
# 	    logmsg "not enough parameters";
# 	    foreach my $key (sort keys %$data) {
# 		logmsg "$key=$$data{$key}";
# 	    }
	    $q->param( 'message' => 'Du m�ste fylla i datum, aktivitet och tid.' );
	}
    }

    # Check if delete button was pressed
    elsif ($q->param( 'delete' )) {
	if ($id) {
	    $self->dbi->delete( 'weight', "id=$id" );
	    $self->header_props( -uri => $q->url.'?rm=weight' );
	    $self->header_type( 'redirect' );
	    return '';
	}
    }

    # Check if cancel
    elsif ($q->param( 'cancel' )) {
	$self->header_props( -uri => $q->url.'?rm=weight' );
	$self->header_type( 'redirect' );
	return '';
    }

    # If this is a known record, fetch its data
    if ($id) {
	my $sth = $self->dbi->process( "select * from weight where id='$id' and user=$uid" );
	$record = $sth->fetchrow_hashref;
	$sth->finish;

	# Set CGI query parameter for each record
	if ($record) {
	    foreach my $key (keys %$record) {
		$q->param( $key => $$record{$key} );
	    }
	}

	# If nothing was found, delete the CGI id parameter.
	else {
	    $q->delete( 'id' );
	}
    }

    # Set user ID
    $q->param( 'uid' => $u->param( 'uid' ) );

    # Set default date and time
    unless ($q->param( 'date' )) {
	$q->param( 'date' => strftime( '%Y-%m-%d', localtime ) );
    }
    unless ($q->param( 'time' )) {
	$q->param( 'time' => strftime( '%H:%M:%S', localtime ) );
    }

    # Set user ID
    $q->param( 'user' => $uid );

    # Load template
    my $t = $self->load_tmpl( 'weightedit.tmpl', associate => $q,
			      die_on_bad_params => 1 );

    # Set default parameters
    $self->set_defaults( $t );

    return $t->output;
}

#
# Plot a weight graph
#
sub weightgraph {
    my $self = shift;

    my $q = $self->query;
    my $u = $self->user;
    my $dbi = $self->dbi;

    # Verify that user is logged in
    return $self->errpage( 'Du m�ste logga in f�rst.' ) unless ($u);

    # Get user id
    my $uid = $u->param( 'uid' );

    # Number of days to display in graph
    my $days = $q->param( 'days' );
    $days = 14 unless ($days =~ m/^\d+$/ && $days >= 4 && $days <= 366);

    # Max and min values for graph
    my ($max, $min) = ($q->param( 'max' ), $q->param( 'min' ));

    if (defined $max && $max !~ m/^\d+(\.\d+)?$/) {
	undef $max;
    }
    if (defined $min && $min !~ m/^\d+(\.\d+)?$/) {
	undef $min;
    }

    # Number of values to use for average count
    my $average_values = 6;

    my $sth = $dbi->process( "select $days + to_days(date) - to_days(curdate()) as day,avg(weight) from weight where user=$uid and to_days(curdate()) - to_days(date) <= $days + $average_values group by date" );

    # Bind columns for later fetch
    my ($day, $weight);
    $sth->bind_columns( \$day, \$weight );

    # Store graph data here
    my $data = [];

    # Variables for statistics
    my $sum = 0;
    my $numval = 0;
    my @lastvalues = ();

    # Get first value
    $sth->fetch;
    for (my $i = -$average_values; $i <= $days; $i++) {

	# Store the x value (date) in graph array
	$$data[0][$i] = join( '/', (Add_Delta_Days( Today(), -$days+$i))[2,1] )
	  if ($i >= 0);

	# Add weight to graph data if we're on its day
	if ($i == $day) {
	    # Add weight to list of last values
	    $lastvalues[$#lastvalues+1] = $weight;

	    # Remove element from list if there are more than
	    # average_values elements in it; this is actually some
	    # kind of FIFO
	    shift @lastvalues if (scalar(@lastvalues) > $average_values);

  	    # Calculate summary of lastvalues
	    my $lastsum = 0;
	    for my $j (0..$#lastvalues) {
		$lastsum += $lastvalues[$j];
	    }

	    # Add weight and average to graph
	    if ($i >= 0) {
		$$data[1][$i] = $weight;
		$$data[2][$i] = $lastsum/scalar(@lastvalues);
	    }

	    # Get next value
	    $sth->fetch;
	}
	else {
	    # Set empty y value
	    $$data[1][$i] = undef
	      if ($i >= 0);
	}
    }

    # Create graph
    my $graph = GD::Graph::linespoints->new( 500, 330 );

    use integer;
    $graph->set(
		dclrs             => [ qw(lred green) ],
		y_label           => 'kg',
		title             => "Vikt de senaste $days dagarna",
		x_plot_values     => 1,
		x_all_ticks       => 0,
		x_label_skip      => ($days / 6),
		y_long_ticks      => 1,
		x_all_ticks       => ($days <= 110 ? 1 : 0),
		show_values       => ($days <= 14 ? 1 : 0),
		values_format     => '%0.1f',
		marker_size       => 1
	       );
    no integer;

    if (defined $max) {
	$graph->set( y_max_value => $max );
    }
    if (defined $min) {
	$graph->set( y_min_value => $min );
    }

    $graph->set_legend( 'Registrerad vikt', "Medelvikt ($average_values senaste m�tningarna)" );

    my $gd = $graph->plot( $data );

    # Set HTTP headers
    $self->header_props( -type => 'image/png' );

    # Return image data to client
    return $gd->png;
}

#
# Plot some statistics
#
sub statgraph {
    my $self = shift;

    my $q = $self->query;
    my $u = $self->user;
    my $dbi = $self->dbi;

    # Verify that user is logged in
#    return $self->errpage( 'Du m�ste logga in f�rst.' ) unless ($u);

    # Get user id
    my $uid = $q->param( 'uid' ); # || $u->param( 'uid' );

    # Get activity
    my $activity = $q->param( 'activity' );

    return $self->errpage( 'Felaktiga parametrar' ) unless ($uid && $activity);


    # Get user information
    my $sth = $dbi->process( "select * from users where id=$uid" );
    my $userinfo = $sth->fetchrow_hashref;
    $sth->finish;

    # Get activity information
    $sth = $dbi->process( "select * from activities where id=$activity" );
    my $activityinfo = $sth->fetchrow_hashref;
    $sth->finish;

    return $self->errpage( "Det finns ingen aktivitet nr $activity" )
      unless ($activityinfo);

    return $self->errpage( "Det g�r inte att ber�kna hastighet utan str�cka." )
      unless ($$activityinfo{distance_units});

    if ($$activityinfo{distance_units} eq 'km') {
	$$activityinfo{timescale} = 60;
	$$activityinfo{speed_units} = 'km/h';
    }
    elsif ($$activityinfo{distance_units} eq 'm') {
	$$activityinfo{timescale} = 1/60;
	$$activityinfo{speed_units} = 'm/s';
    }
    else {
	$$activityinfo{timescale} = 1;
	$$activityinfo{speed_units} = $$activityinfo{distance_units} . '/min';
    }

    # Get data from database
    $sth = $dbi->process( "select concat( dayofmonth(date), '/', month(date) ) as sdate,distance,duration from training where user=$uid and activity=$activity and distance>0 order by date" );

    my ($date, $distance, $duration, $speed);
    $sth->bind_columns( \$date, \$distance, \$duration );

    my $data = [];
    my $i = 0;
    my $maxdist = 0;
    my $maxspeed = 0;

    # Fetch all data
    while ($sth->fetch) {
	$$data[0][$i] = $date;
	$$data[1][$i] = $distance;
	$$data[2][$i] = $distance / $duration * $$activityinfo{timescale};

	# Update max values
	$maxdist = $distance if ($distance > $maxdist);
	$maxspeed = $$data[2][$i] if ($$data[2][$i] > $maxspeed);

	$i++;
    }

    # $i holds the number of data fields
    return $self->errpage( "Inget data att visa" )
      unless ($i > 0);

#    print STDERR "maxspeed: $maxspeed; maxdist: $maxdist\n";
    use integer;
    $maxdist = ($maxdist / 4 + 1) * 5;
    $maxspeed = ($maxspeed / 4 + 1) * 5;
    no integer;
#    print STDERR "maxspeed: $maxspeed; maxdist: $maxdist\n";

    # Create graph
    my $graph = GD::Graph::mixed->new( 500, 330 );

#      use integer;
    $graph->set(
		types             =>[qw(bars linespoints)],
		two_axes          => 1,
		bar_width         => 1,
		bar_spacing       => 10,
  		dclrs             => [ qw(green lred) ],
  		y1_label          => $$activityinfo{distance_units},
		y2_label          => $$activityinfo{speed_units},
#  		y_tick_number     => 5,
#  		y2_min_value      => 0.5,
#  		y2_max_value      => $maxspeed,
#		y1_max_value      => $maxdist,
  		title             => "$$activityinfo{activity} $$userinfo{fullname}",
#  		x_plot_values     => 1,
  		x_all_ticks       => 1,
		x_tick_length     => -4,
		axis_space        => 6,
  		x_label_skip      => ($i / 10),
#		x_labels_vertical => 1,
  		y_long_ticks      => 1,
#  		x_all_ticks       => ($days <= 110 ? 1 : 0),
#  		show_values       => 1,
  		values_format     => '%0.1f',
  		marker_size       => 1
#		y_min_value       => 0,
#		y_max_value       => 20
	       );
#      no integer;

#    $graph->set_legend( 'Registrerad vikt', "Medelvikt ($average_values senaste m�tningarna)" );

    my $gd = $graph->plot( $data );

    # Set HTTP headers
    $self->header_props( -type => 'image/png' );

    # Return picture
    return $gd->png;
}

#
# Edit a record
#
sub edit {
    my $self = shift;

    my $q = $self->query;
    my $u = $self->user;

    # Verify that user is logged in
    return $self->errpage( 'Du m�ste logga in f�rst.' ) unless ($u);

    my $id = $q->param( 'id' );
    my $record = {};

    # Save stuff if save button pressed
    if ($q->param( 'save' )) {
	my $data = $q->Vars;
	if ($$data{date} && $$data{user} && $$data{duration} &&
	    ($$data{activity} || $$data{alt_activ})) {
	    # save stuff

	    # Check if there is a new activity
	    if ($$data{alt_activ} && !$$data{activity}) {

#		logmsg "adding new actvity $$data{alt_activ}";

		# First, set preferrable case of data
		$$data{alt_activ} = ucfirst( lc( $$data{alt_activ} ) );

		# Check that data does not exist
		my $sth = $self->dbi->process( "select * from activities where activity like '%$$data{alt_activ}%'" );
		my $rows = $sth->rows;
		$sth->finish;

		if ($rows == 0) {
		    $self->dbi->insert( 'activities',
					activity => $$data{alt_activ},
					distance_units => $$data{alt_dist} );

		    $sth = $self->dbi->process( "select id from activities where activity='$$data{alt_activ}'" );
		    my $newid = $sth->fetchrow_array;
		    $sth->finish;

		    if ($newid <= 0) {
			$q->param( 'message' => 'Det blev n�got fel vid registreringen av den nya aktiviteten $$data{alt_activ}.' );
		    }
		    $$data{activity} = $newid;
		}
		else {
		    $q->param( 'message' => 'Aktiviteten finns redan. V�lj fr�n listan.' );
		}
	    }

	    # Check date is valid
	    if ($$data{date} lt '2002-05-20') {
		$q->param( 'message' => 'Tr�ning r�knas bara fr�n och med 20 maj. F�rs�k inte fuska!!' );
	    }

	    # Check data validity
	    unless ($$data{duration} =~ m/^[0-9]+$/) {
		$q->param( 'message' => 'Ange tiden i hela minuter. Endast siffror.' );
	    }
	    unless ($$data{duration} >= 20) {
		$q->param( 'message' => 'En aktivitet skall vara minst 20 minuter f�r att r�knas.' );
	    }
	    if ($$data{distance}) {
		$$data{distance} =~ s/,/./;
		unless ($$data{distance} =~ m/^[0-9]+(\.[0-9]+)?$/) {
		    $q->param( 'message' => 'Ange distansen som ett tal, utan enhet. Du kan anv�nda decimaler.' );
		}
	    }

	    # Check there are no errors
	    unless ($q->param( 'message' )) {

	    # Update table if ID is set.
		if ($$data{id}) {
		    my $rows = $self->dbi->update( 'training',
						   "id=$$data{id}",
						   user => $$data{user},
						   date => $$data{date},
						   activity => $$data{activity},
						   duration => $$data{duration},
						   distance => $$data{distance},
						   comment => $$data{comment} );

		    # If update succeeded, return browser to start page
		    if ($rows == 1) {
			$self->header_props( -uri => $q->url.'?rm=data' );
			$self->header_type( 'redirect' );
			return '';
		    } else {
#			logmsg "Update failed for id=$$data{id}.";
			$q->param( 'message' => 'Uppdatering misslyckades.' );
		    }
		}

		# Otherwise, insert new record
		else {
		    # No error handling. Sucks, really.
		    $self->dbi->insert( 'training',
					user => $$data{user},
					date => $$data{date},
					activity => $$data{activity},
					duration => $$data{duration},
					distance => $$data{distance},
					comment => $$data{comment} );
		    # And redirect. Still nor error handling.
		    $self->header_props( -uri => $q->url.'?rm=data' );
		    $self->header_type( 'redirect' );
		    return '';
		}
	    }
	}
	else {
# 	    logmsg "not enough parameters";
# 	    foreach my $key (sort keys %$data) {
# 		logmsg "$key=$$data{$key}";
# 	    }
	    $q->param( 'message' => 'Du m�ste fylla i datum, aktivitet och tid.' );
	}
    }

    # Check if delete button was pressed
    elsif ($q->param( 'delete' )) {
	if ($id) {
	    $self->dbi->delete( 'training', "id=$id" );
	    $self->header_props( -uri => $q->url.'?rm=data' );
	    $self->header_type( 'redirect' );
	    return '';
	}
    }

    # Check if cancel
    elsif ($q->param( 'cancel' )) {
	$self->header_props( -uri => $q->url.'?rm=data' );
	$self->header_type( 'redirect' );
	return '';
    }



    # If this is a known record, fetch its data
    if ($id) {
	my $sth = $self->dbi->process( "select * from training where training.id='$id'" );
	$record = $sth->fetchrow_hashref;
	$sth->finish;

	if ($record) {
	    foreach my $key (keys %$record) {
#		logmsg "$key=$$record{$key}";
		$q->param( $key => $$record{$key} );
	    }
	}
	else {
	    $q->delete( 'id' );
	}
    }

    # Fetch known activities
    my $sth = $self->dbi->process( "select * from activities order by activity" );
    my $activities = $sth->fetchall_arrayref({});
    if ($q->param( 'activity' )) {
	$self->selected( $activities, 'id', $q->param( 'activity' ));
    }

    # Set user ID
    $q->param( 'user' => $u->param( 'uid' ) );

    # Set default date
    unless ($q->param( 'date' )) {
#	logmsg "set default date: ", strftime( '%Y-%m-%d', localtime );
	$q->param( 'date' => strftime( '%Y-%m-%d', localtime ) );
    }

    # Load template
    my $t = $self->load_tmpl( 'edit.tmpl', associate => $q,
			      die_on_bad_params => 0 );

    # Set default parameters
    $self->set_defaults( $t );

    # Set parameters
    $t->param( activities => $activities );

    return $t->output;
}

#
# Logga in
#

sub login {
    my $self = shift;
    my $q = $self->query;

    if ($q->param( 'login' )) {
	# Authenticate user

	my ($user, $password) = ($q->param( 'user' ), $q->param( 'password' ));

	# See if user and password exists in database
	my $sth = $self->dbi->process( "select * from users where user='$user' and password='$password'" );

	my $rows = $sth->rows;
	my $userdata = $sth->fetchrow_hashref;
	$sth->finish;

	if ($rows > 0) {
	    my $u = new CookieAuth;
	    $u->user( $user );
	    $u->param( 'password' => $password );
	    $u->param( 'uid' => $$userdata{id} );

	    # Decide where to send user. If the user name does not
	    # look like an email address, show the userinfo page.
	    my $url = $q->url;
	    if ($user !~ m/^[\w.-]+\@(?:[\w-]+\.)+\w+$/) {
		$url .= '?rm=userinfo';
	    }


	    # Create cookie and redirect browser
	    my $cookie = $q->cookie( -name => 'session_key',
				     -value => $u->create_key );
	    $self->header_props( -uri => $url,
				 -cookie => $cookie );
	    $self->header_type( 'redirect' );
	    return '';
	}
	else {
	    $q->param( 'message' => 'Felaktigt anv�ndarnamn eller l�senord.' );
	}
    }

    # Init template
    my $t = $self->load_tmpl( 'login.tmpl', associate => $q );

    # Set defaults
    $self->set_defaults( $t );

    return $t->output;
}

#
# Logga ut
#

sub logout {
    my $self = shift;
    my $q = $self->query;

    my $cookie = $q->cookie( -name => 'session_key',
			     -value => '',
			     -expire => '-1d' );
    $self->header_props( -uri => $q->url,
			 -cookie => $cookie );
    $self->header_type( 'redirect' );

#    logmsg "logging out; cookie=$cookie";

    return 'mepp';
}


#
# Edit user info, or, sign up a new user.
#
sub userinfo {
    my $self = shift;

    my $q = $self->query;
    my $u = $self->user;


    if ($q->param( 'save' )) {

	# Get all parameters as hash ref
	my $data = $q->Vars;

	# Set 'invisible' to either 1 or 0
	$$data{invisible} = $$data{invisible} ? 1 : 0;

	# Check password stuff
	if ($$data{password}) {
	    unless ($$data{verify} && $$data{password} eq $$data{verify}) {
		$q->param( message => 'Du har inte angett l�senordet r�tt.' );
	    }
	}

	# Check that user name looks like a valid email address
	if ($$data{user} !~  m/^[\w.-]+\@(?:[\w-]+\.)+\w+$/) {
	    $q->param( message => "'$$data{user}' �r inte en giltig epostadress");
#	    logmsg "invalid email $$data{user}";
	}

	# Check for duplicate login name. Bad code. Bad, bad, bad code.
	elsif ($$data{user}) {
	    my $sth = $self->dbi->process( "select id,user from users where user='$$data{user}'" );
	    if ($sth->rows > 0) {
		if ($u) {
		    my $udata = $sth->fetchrow_hashref;
		    unless ($$udata{id} == $u->param( 'uid' )) {
			$q->param( message => "Du kan inte byta till anv�darnamn '$$data{user}' eftersom det �r upptaget." );
		    }
		}
		else {
		    $q->param( message => "Anv�ndarnamnet '$$data{user}' �r redan upptaget. Pr�va n�got annat." );
		}
	    }
	    $sth->finish;
	}

	# Check if user is logged in
	if (!$q->param( 'message' ) && $u) {

	    # Check user data
	    if ($$data{fullname} && $$data{user}) {
		my $rows;
		if ($$data{password}) {
		    $rows = $self->dbi->update( 'users',
						"id=$$data{uid}",
						user => $$data{user},
						fullname => $$data{fullname},
						password => $$data{password},
						invisible => $$data{invisible}
					      );
		}
		else {
		    $rows = $self->dbi->update( 'users',
						"id=$$data{uid}",
						user => $$data{user},
						fullname => $$data{fullname},
						invisible => $$data{invisible},
					      );
		}

		# Redirect client if successfull
		if ($rows == 1) {
#		    logmsg "updated uid $$data{uid}; rows=$rows; password=$$data{password}";
		    return $self->redirect;
		}

		# Not successfull, show error message
		else {
		    $q->param( message => 'Det blev n�got fel vid uppdateringen.' );
		}
	    }

	    # Invalid user data
	    else {
		$q->param( message => 'Du m�ste ange namn och inloggningsnamn' );
	    }
	}

	# This is a new registration; user is not logged in
	elsif (!$q->param( 'message' )) {

	    # Check data
	    if ($$data{fullname} && $$data{user} && $$data{password}) {
		my $rows = $self->dbi->insert( 'users',
					       user => $$data{user},
					       fullname => $$data{fullname},
					       password => $$data{password},
					       invisible => $$data{invisible}
					     );
		if ($rows == 1) {
#		    logmsg "new user";
		    $self->redirect;
		}
		else {
		    $q->param( message => 'Det gick inte att registrera dig. F�rs�k igen.' );
		}
	    }

	    # Not everything filled in
	    else {
		$q->param( message => 'Du m�ste fylla i namn, anv�ndarnamn och l�senord.' );
	    }
	}
    }

    # User cancelled form?
    elsif ($q->param( 'cancel' )) {
	return $self->redirect;
    }

    if ($u) {

	# If the user is logged in, get info (always get uid).
	$q->param( uid => $u->param( 'uid' ) );

	$q->param( user => $u->user ) unless $q->param( 'user' );
	$q->param( fullname => $u->param( 'fullname' ) )
	  unless ($q->param( 'fullname' ));
	$q->param( password => $u->param( 'password' ) )
	  unless ($q->param( 'password' ));
	$q->param( invisible => $u->param( 'invisible' ) )
	  unless ($q->param( 'invisible' ));
    }
    else {
	# Make sure uid is never set unless user is logged in
	$q->delete( 'uid' );
    }

    # Initialize template
    my $t = $self->load_tmpl( 'userinfo.tmpl', associate => $q );

    # Set defaults
    $self->set_defaults( $t );

    return $t->output;
}

#
# Skicka l�senordsp�minnelse
#

sub reminder {
    my $self = shift;
    my $q = $self->query;

    if ($q->param( 'send' )) {

	# Check if user is a valid email address
	my $user = $q->param( 'user' );
	if ($user !~  m/^[\w.-]+\@(?:[\w-]+\.)+\w+$/) {
	    $q->param( message => "Ingen giltig epostadress." )
	}

	# Get user info
	my $sth = $self->dbi->process( "select * from users where user='$user'" );
	my $rows = $sth->rows;
	my $userdata = $sth->fetchrow_hashref;
	$sth->finish;

	# Send mail to user
	if ($rows > 0) {

	    my $msg = new Mail::Send;

	    $msg->to( $$userdata{user} );
	    $msg->subject( 'Ditt l�senord till tr�ningskalendern' );
	    $msg->set( 'From', 'Tr�ningskalendern <traning@raggmunk.nu>' );
	    my $fh = $msg->open;

	    print $fh "Ditt l�senord till tr�ningskalendern �r: $$userdata{password}\n";

	    $fh->close;

	    $q->param( 'message' => "Meddelande om l�senord har skickats till $$userdata{user}." );
	}
	else {
	    $q->param( 'message' => 'Ingen anv�ndare registrerad med den epostadressen.' );
	}
    }

    # Init template
    my $t = $self->load_tmpl( 'reminder.tmpl', associate => $q );

    # Set defaults
    $self->set_defaults( $t );

    return $t->output;
}

#
# Set selected value in an list item. Alter the list and return the
# selected item.
#

sub selected {
    my ($self, $aryref, $key, $value) = @_;

    return undef unless ($aryref && $key && $value);

    foreach my $ref (@$aryref) {
	if ($$ref{$key} eq $value) {
	    $$ref{selected} = 1;
	    return $ref;
	}
    }
    return undef;
}

#
# Return an error page.
#
sub errpage {
    my ($self, $msg) = @_;
    my $q = $self->query;

    my $t = $self->load_tmpl( 'error.tmpl' );

    $self->set_defaults( $t );

    $t->param( error => $msg );

    return $t->output;
}

#
# Set default parameters in template
#
sub set_defaults {
    my ($self, $t) = @_;

    my $u = $self->user;

    if ($u) {
	$t->param( 'logged_in' => 1 );
	$t->param( 'sidebar_username' => $u->param( 'fullname' ) );
    }
}

#
# Redirect client browser and, optionally, set cookie.
#
sub redirect {
    my ($self, $cookie) = @_;
    my $q = $self->query;

    if ($cookie) {
	$self->header_props( -uri => $q->url,
			     -cookie => $cookie );
    } else {
	$self->header_props( -uri => $q->url );
    }
    $self->header_type( 'redirect' );
    return '';
}


#
# Set the internal user thingy
#
sub user {
    my $self = shift;
    $self->{user} = shift if (@_);
    return $self->{user};
}

1;

