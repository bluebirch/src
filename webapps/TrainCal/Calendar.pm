# $Id$
#
# Calendar and date object for WebDiary
#
# $Log$
# Revision 1.4  2002/02/06 17:40:45  stefan
# Lade till en enkel tr�ningskalender
#
# Revision 1.3  2002/01/07 13:32:27  stefan
# Added methods 'today', 'start_of_month', 'end_of_month', 'valid' and
# 'make_valid'.
#
# Revision 1.2  2002/01/07 00:18:08  stefan
# Added a small calendar in the navigation bar.
#
# Revision 1.1  2002/01/06 17:15:25  stefan
# Simple functions that return calendary events for a specific date.
#

package Calendar;
use strict;
use Date::Calc qw(:all);;

# Holiday data
my @calendar_days = ( 'K2,2,7:Kyndelsm�ssodagen',
		      'K6,19,5:Midsommarafton',
		      'K6,20,6:Midsommardagen',
		      'K10,31,6:Alla helgons dag',
		      'K11,27,7:F�rsta advent',
		      'K12,4,7:Andra advent',
		      'K12,11,7:Tredje advent',
		      'K12,18,7:Fj�rde advent',
		      'W1,7:F�rsta s�ndagen efter ny�r',
		      'D1,1:Ny�rsdagen',
		      'D1,5:Trettondedagsafton',
		      'D1,6:Trettondedag jul',
		      'D1,13:Tjugondedag jul',
		      'D2,14:Alla hj�rtans dag',
		      'D3,8:Internationella kvinnodagen',
		      'D3,20:V�rdagj�mningen',
		      'D4,30:Valborsm�ssoafton',
		      'D5,1:F�rsta maj',
		      'D6,6:Sveriges nationaldag',
		      'D11,1:Allhelgonadagen',
		      'D12,10:Nobeldagen',
		      'D12,13:Lucia',
		      'D12,24:Julafton',
		      'D12,25:Juldagen',
		      'D12,26:Annandag jul',
		      'D12,31:Ny�rsafton',
		      'E-63:Septuagesima',
		      'E-49:Fastlagss�ndagen',
		      'E-47:Fettisdagen',
		      'E-46:Askonsdag',
		      'E-7:Palms�ndag',
		      'E-3:Sk�rtorsdag',
		      'E-2:L�ngfredag',
		      'E-1:P�skafton',
		      'E0:P�skdagen',
		      'E1:Annandag p�sk',
		      'E39:Kristi himmelsf�rds dag',
		      'E48:Pingstafton',
		      'E49:Pingstdagen',
		      'E50:Annandag pingst' );

# Calendar events
my %calendar;

# Holds a list of which years are already parsed
my %is_parsed;


# Constructor. Create new instance of calendar object.
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = {};

    bless( $self, $class );
    return $self;
}

######################################################################
# parse - parse calendar data
#
# Parse calendar data and build a hash of all events for the specified
# year.
#
sub parse {
    my ($self, $year) = @_;

    # Step through calendary data
    foreach (@calendar_days) {
	my ($code, $comment) = split /:/, $_;
	if ($code =~ m/^(D|E|W|K)([+-]?\d{1,3})/) {
	    my $cmd = $1;
	    my @param = ($2);
	    while ($code =~ m/,([+-]?\d{1,3})/g) {
		push @param, $1;
	    }
	    my $date;
	    if ($cmd eq 'D') {
		$date = sprintf( '%04d-%02d-%02d', $year, @param );
	    } elsif ($cmd eq 'E') {
		$date = sprintf( '%04d-%02d-%02d',
				 Add_Delta_Days( Easter_Sunday( $year ),
						 $param[0] ) );
	    } elsif ($cmd eq 'W') {
		$date = sprintf( '%04d-%02d-%02d',
				 Business_to_Standard( $year, @param ) );
	    } elsif ($cmd eq 'K') {
		my @date = ($year, @param[0,1]);
		my $searching_dow = $param[2];
		my $dow = Day_of_Week( @date );
		my $delta = ($dow >= $searching_dow) ? $dow - $searching_dow :
		  $searching_dow - $dow;
		$date = sprintf( '%04d-%02d-%02d',
				 Add_Delta_Days( @date, $delta ) );
	    }

	    if ($date) {
		$calendar{ $date } = $comment;
	    }
	}
    }

    $is_parsed{$year} = 1;

}

######################################################################
# event - return calendar event for a specific date
#
sub event {
    my ($self,$date) = @_;

    # Get year (simply assume a correct date format)
    my $year = substr( $date, 0, 4 );

    # Parse all events for this year if this has not already been done
    $self->parse( $year ) unless ($is_parsed{year});

    # Lookup in event hash
    return $calendar{$date};
}

######################################################################
# add_delta - add or subtract days from date
#
sub add_delta {
    my ($self, $date, $delta) = @_;

    return $self->a2s( Add_Delta_Days( $self->s2a( $date ), $delta ) );;
}

######################################################################
# delta - return number of days between two dates
#
sub delta {
    my ($self, $date1, $date2) = @_;

    if ($date1 && $date2) {
	return Delta_Days( $self->s2a( $date1 ), $self->s2a( $date2 ) );
    } else {
	return 999;
    }
}

######################################################################
# prev_month, next_month - add exactly one month
#
sub prev_month {
    my ($self, $date) = @_;

    my @date = $self->s2a( $date );

    if ($date[1] == 1) {
	$date[0]--;
	$date[1] = 12;
    } else {
	$date[1]--;
    }
    my $dim = Days_in_Month( @date[0,1] );
    $date[2] = $dim if ($date[2] > $dim);

    return $self->a2s( @date );
}

sub next_month {
    my ($self, $date) = @_;

    my @date = $self->s2a( $date );

    if ($date[1] == 12) {
	$date[0]++;
	$date[1] = 1;
    } else {
	$date[1]++;
    }
    my $dim = Days_in_Month( @date[0,1] );
    $date[2] = $dim if ($date[2] > $dim);

    return $self->a2s( @date );
}

######################################################################
# today
#
sub today {
    my $self = shift;
    return $self->a2s( Today );
}

######################################################################
# start_of_month
#
sub start_of_month {
    my ($self, $date) = @_;
    my @date = $self->s2a( $date );
    $date[2] = 1;
    return $self->a2s( @date );
}

######################################################################
# end_of_month
#
sub end_of_month {
    my ($self, $date) = @_;
    my @date = $self->s2a( $date );
    $date[2] = Days_in_Month( @date[0,1] );
    return $self->a2s( @date );
}

######################################################################
# valid - check if date is valid
#
sub valid {
    my ($self, $date ) = @_;
    my @date = $self->s2a( $date );
    return @date ? check_date( @date ) : 0;
}

######################################################################
# make_valid - check if date is valid, if not, fix it
#
sub make_valid {
    my ($self, $date) = @_;

    my @date = $self->s2a( $date );

    if (!@date || !check_date( @date )) {
	@date = Today();
    }

    return $self->a2s( @date );
}

######################################################################
# month - return month object
#
sub get_month {
    my ($self, $date) = @_;

    my $m = Calendar::Month->new;

    my @date = $self->s2a( $date );
    $m->set( @date[0,1] );

    return $m;
}

######################################################################
# s2a - convert date string to array
#
sub s2a {
    my ($self, $date) = @_;

    $date =~ m/(\d{4})-0?(\d{1,2})-0?(\d{1,2})/ if ($date);
    return $1 ? ( $1, $2, $3 ) : ();
}

######################################################################
# a2s - convert date array to string
#
sub a2s {
    my $self = shift;
    return sprintf( '%04d-%02d-%02d', @_ );
}

######################################################################
# Sub class to make month matrix
#
{ package Calendar::Month;
  use Date::Calc qw(:all);
  use strict;

  # Create new month object;
  sub new {
      my $proto = shift;
      my $class = ref($proto) || $proto;
      my $self = {};

      $self->{year} = 0;
      $self->{month} = 0;
      $self->{days} = 0;
      $self->{dow_start} = 0;
      $self->{param} = ();

      bless( $self, $class );
      return $self;
  }

  # Set year and month
  sub set {
      my ($self, $year, $month) = @_;

      $self->{year} = $year;
      $self->{month} = $month;
      $self->{days} = Days_in_Month( $year, $month );
      $self->{dow_start} = Day_of_Week( $year, $month, 1 );

      # Initialize parameters
      for my $i (1..$self->{days}) {
	  $self->{param}[$i-1]{day} = $i;
      }
  }

  # Set/get parameter for specific day
  sub param {
      my ($self, $day, $param, $value) = @_;

      if ($value && $self->{param}[$day-1]) {
	  $self->{param}[$day-1]{$param} = $value;
      }
      return $self->{param}[$day-1]{$param};
  }

  # Get the day of week (0-6) for specified day
  sub dow {
      my ($self, $day) = @_;
      return Day_of_Week( $self->{year}, $self->{month}, $day ) - 1;
  }

  # Get the week of the month (0-4) for the specified day
  sub week {
      my ($self, $day) = @_;
      int(($day + Day_of_Week($self->{year},$self->{month},1) - 2) / 7);
  }

  # Return matrix
  sub matrix {
      my $self = shift;

      my @matrix = ();
      for my $day (1..$self->{days}) {
	  $matrix[ $self->week( $day ) ]{dow}[ $self->dow( $day ) ] =
	    $self->{param}[ $day-1 ];
      }
      $matrix[ $#matrix ]{dow}[6] = undef unless ($matrix[ $#matrix ]{dow}[6]);
      return \@matrix;
  }

  # Get month name
  sub name {
      my $self = shift;
      my @name = ( 'Januari',
		   'Februari',
		   'Mars',
		   'April',
		   'Maj',
		   'Juni',
		   'Juli',
		   'Augusti',
		   'September',
		   'Oktober',
		   'November',
		   'December' );

      return $name[ $self->{month} - 1 ];
  }

  # Get year
  sub year {
      my $self = shift;
      return $self->{year};
  }

  # Get month
  sub month {
      my $self = shift;
      return $self->{month};
  }

  # Get days in month
  sub days {
      my $self = shift;
      return $self->{days};
  }

}

1;
