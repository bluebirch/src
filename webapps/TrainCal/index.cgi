#!/usr/bin/perl -w
#
# $Id$
#
# $Log$
# Revision 1.3  2004/01/16 15:37:52  zerblat
# Uppgifter om databasen anges i index.cgi. Inloggning sker med epostadress.
# En ny sida kan anv�ndas f�r att skicka l�senordsp�minnelser.
#
# Revision 1.2  2002/10/14 20:20:29  stefan
# �ndrade s�kv�g f�r HTML::Template.
#
use strict;
use lib qw(/home/stefan/perl/lib/perl5 /home/stefan/perl/lib/perl5/site_perl);
use TrainCal;

my $a = TrainCal->new( TMPL_PATH => 'tmpl/',
		       PARAMS => { dbtype => 'mysql',
				   database => 'stefan',
				   user => 'stefan',
				   password => 'merci' }
		     );

$a->run;
