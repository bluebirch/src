#!/usr/bin/perl -w

use strict;

my $file = shift @ARGV;

print STDERR "Fix file $file...\n";

open FILE, $file;
my @line = <FILE>;
close FILE;

for my $i (0..$#line) {
    $line[$i] =~ s/%3C/\</g;
    $line[$i] =~ s/%20/ /g;
    $line[$i] =~ s/%3E/\>/g;
    $line[$i] =~ s/&lt;/\</g;
    $line[$i] =~ s/&gt;/\>/g;
}

open FILE, ">$file";
print FILE @line;
close FILE;
