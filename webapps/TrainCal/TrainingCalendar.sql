-- $Id$
--
-- $Log$
-- Revision 1.1  2002/09/29 08:45:31  stefan
-- Adding SQL code for creating the 'weight' table.
--

--
-- Table structure for table 'activities'
--

CREATE TABLE activities (
  id int(11) NOT NULL auto_increment,
  activity varchar(30) NOT NULL default '',
  distance_units varchar(5) default NULL,
  factor decimal(4,2) unsigned default '1.00',
  PRIMARY KEY  (id),
  KEY name_idx (activity)
) TYPE=MyISAM;

--
-- Table structure for table 'training'
--

CREATE TABLE training (
  id int(11) NOT NULL auto_increment,
  user int(11) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  activity int(11) NOT NULL default '0',
  duration int(11) NOT NULL default '0',
  distance decimal(11,1) default NULL,
  comment varchar(50) default NULL,
  PRIMARY KEY  (id),
  KEY user_idx (user),
  KEY date_idx (date),
  KEY activity_idx (activity),
  KEY duration_idx (duration),
  KEY distance_idx (distance)
) TYPE=MyISAM;

--
-- Table structure for table 'users'
--

CREATE TABLE users (
  id int(11) NOT NULL auto_increment,
  user varchar(50) NOT NULL default '',
  fullname varchar(50) default NULL,
  password varchar(20) NOT NULL default '*',
  invisible tinyint(4) default NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY name_idx (name)
) TYPE=MyISAM;

--
-- Table structure for table 'weight'
--

CREATE TABLE weight (
  id int(11) NOT NULL auto_increment,
  user int(11) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  time time NOT NULL default '00:00:00',
  comment varchar(50) default NULL,
  PRIMARY KEY  (id),
  KEY idx_user (user),
  KEY idx_date (date),
  KEY idx_time (time)
) TYPE=MyISAM;

