#!/usr/bin/perl -w
#
# $Id$
#
# This perl script is used to create and maintain a catalogue and a
# HTML index of downloadable files. Its main purpose is to create a
# catalogue of program files, patches, updates and other things for
# the Securecuard Support Pages.
#
# Hopefully, the script only needs a standard installation of Perl, in
# order to be able to run under Windows NT on the official web server.
#
# More comments about usage goes here.
#
# $Log$
# Revision 1.4  2001/11/12 12:48:51  stefan
# Layout and bug fixes.
#
# Revision 1.3  2001/11/12 11:18:36  stefan
# Layout changes.
#
# Revision 1.2  2001/11/12 10:49:48  stefan
# Moved database path to /home/www.
#
# Revision 1.1  2001/10/25 15:07:05  stefan
# The first functional version. This version offers the following
# commands:
#
#   update    Update the file database with new files found in the file
#             structure. Changes are written to the file database.
#
#   verify    Verify catalogue and that all files exists. No changes are
#             written to files.txt.
#
#   md5       Calculate MD5 sums on all files in files.txt. Uses
#             external md5sum command.
#
#   index     Create HTML indexes. A main index.html is created, with
#             links to separate HTML pages for each vendor.
#
# It is probably still full of ugly bugs.
#

use strict;
use File::Find;
use Getopt::Std;

my %opt;

######################################################################
# Configurable variables
#

# The name of the HTML catalogue
my $html_index_name = "index.html";

# The name of the catalogue text file, the "raw data"
my $catalogue_filename = "/home/www/files.txt";

# Path to files. Files to be catalogued are stored in this directory
# or subdirectories of it. Must end with a "/"!
my $file_path = "/home/www/";

# File base. This is the path that is appended to the A-reference in
# the HTML catalogue.
my $file_base = "";

# Command line utility used to calculate MD5. %s represents the file
# name.
my $md5_command = "cat %s | md5";

# HTML header
my $html_header = <<'END';
<html>
<header>
<title>Secureguard Download Page</title>
<style>
<!--
body {
    background-color : #48214E;
    color : #E5E6E8;
    font-family : garamond, serif;
    font-size : 12pt;
}
:link {
    color : #e5e6e8;
}
:link:hover {
    color : #9ba0a4
}
:visited {
    color : #e5e6e8;
}
h1 {
    font-family : sans-serif;
    font-size : 20pt;
    font-weight : bold;
}
.product {
    font-family : sans-serif;
    font-size : 14pt;
    font-weight : bold;
    color : #48214e;
    background-color : #E5E6E8;
}
.platform {
    font-family : sans-serif;
    font-size : 10pt;
    color : #E5E6E8;
    padding-top : 2em;
    border-bottom-width : 1px;
    border-bottom-color : #E5E6E8;
    border-bottom-style : solid;
}
.filename {
    font-family : arial narrow, sans-serif;
    font-size : 10pt;
    color : #E5E6E8;
    vertical-align : top;
}
.filedesc {
    font-family : garamond, serif;
    font-size : 12pt;
    color : #E5E6E8;
    vertical-align : top;
}
.fileinfo {
    font-family : arial narrow, sans-serif;
    font-size : 8pt;
    margin-bottom : 2em;
    color : #E5E6E8;
    vertical-align : top;
}
-->
</style>
</header>
<body>
END

# HTML footer
my $html_footer = <<'END';
</body>
</html>
END

# No configurable variables below this line. Don't mess with it!
######################################################################

# The file catalogue is stored in a hash (which means that a very
# large file structure will cause the script to run out of memory;
# just make sure there are not too many files, or enough RAM, right?)
my %catalogue;


######################################################################
# read_key
#
sub key_value {
    my ($id, $key) = @_;

    print sprintf( "%-15s: ", ucfirst( $key ) );
    if ($catalogue{$id}{$key}) {
	print "$catalogue{$id}{$key}\n";
    } else {
	my $tmpval = <STDIN>;
	chomp( $tmpval );
	$catalogue{$id}{$key} = $tmpval if (length($tmpval) > 0);
    }
}
	

######################################################################
# read_catalogue - read the flat file database

sub read_catalogue {

    my $rec = {}; # Temporary variable to hold record

    if (open( CATALOGUE, $catalogue_filename )) {
	while (<CATALOGUE>) {
	    chomp;
	    my ($key, $value) = split /\s*:\s*/, $_, 2;
	    if ($value) {
		$$rec{ lc( $key ) } = $value;
	    } else {
		if ($$rec{filename} && $$rec{size}) {
		    $catalogue{ $$rec{filename}.'-'.$$rec{size} } = $rec;
		    $rec = {};
		} else {
		    warn "All keys not set";
		}
	    }
	}
	if ($$rec{filename} && $$rec{size}) {
	    $catalogue{ $$rec{filename}.'-'.$$rec{size} } = $rec;
	}
	close CATALOGUE;
	return 1; # True
    }
    return 0;
}

######################################################################
# update_catalogue - search for files and update catalogue

sub update_catalogue {
    find( { wanted => \&process_file }, $file_path );
}

sub process_file {
    # Make sure this is a regular file
    return unless (-f);

    # Get filename
    my $filename = $_;
    
    # Get dir
    my $dir = $File::Find::dir;
    $dir =~ s/^$file_path//;

    # Stat file (to get file size)
    my @stat = stat $filename;

    # Create file ID
    my $id = $filename.'-'.$stat[7];

    if ($catalogue{$id}) {
	if ($catalogue{$id}{path} ne $dir) {
	    $catalogue{$id}{path} = $dir;
	    print "Updating path for $catalogue{$id}{filename}\n";
	}
	if (!$catalogue{$id}{time} || $catalogue{$id}{time} ne localtime( $stat[9] )) {
	    $catalogue{$id}{time} = localtime( $stat[9] );
	    print "Updating time stamp for $catalogue{$id}{filename}\n";
	}
    } elsif (!$opt{b}) { # not in batch mode
	print "New file $dir/$filename. Add? ";
	my $answer;
	$answer = <STDIN>;
	chomp $answer;
	if ($answer =~ m/^(y|yes)$/) {

	    $catalogue{$id}{filename} = $filename;
	    $catalogue{$id}{path}     = $dir;
	    $catalogue{$id}{size}     = $stat[7];
	    $catalogue{$id}{time}     = localtime( $stat[9] );


	    key_value( $id, 'filename' );
	    key_value( $id, 'path' );
	    key_value( $id, 'description' );
	    key_value( $id, 'vendor' );
	    key_value( $id, 'product' );
	    key_value( $id, 'version' );
	    key_value( $id, 'platform' );

	    print "\n";

	}
    }
}

######################################################################
# write_catalogue - write catalogue to disk

sub write_catalogue {

    open( CATALOGUE, ">".$catalogue_filename )
	or die "Can not open $catalogue_filename";

    foreach my $id (sort keys %catalogue) {
	foreach my $key (sort keys %{$catalogue{$id}}) {
	    print CATALOGUE ucfirst( $key ), ": $catalogue{$id}{$key}\n";
	}
	print CATALOGUE "\n";
    }
    close CATALOGUE;
    return 1;
}

######################################################################
# verify_catalogue - check that all files in catalogue exists

sub verify_catalogue {
    foreach my $id (sort keys %catalogue) {
	my $file = $file_path . $catalogue{$id}{path} . '/'
	    . $catalogue{$id}{filename};

	if (-f $file) {
	    my @stat = stat $file;

	    if ($stat[7] != $catalogue{$id}{size}) {
		print "File size changed: $file\n";
	    }
	} else {
	    print "File not found: $file; deleting file.\n";
	    delete $catalogue{$id};
	}
    }
}

######################################################################
# verify_md5 - verify or calculate md5 values

sub verify_md5 {
    foreach my $id (sort keys %catalogue) {
	my $file = $file_path . $catalogue{$id}{path} . '/'
	    . $catalogue{$id}{filename};

	if (-f $file) {
	    unless ($catalogue{$id}{md5}) {
		my $cmd = sprintf( $md5_command, $file );
		my $md5 = `$cmd`;
		chomp $md5;
		$catalogue{$id}{md5} = $md5;
		print "$catalogue{$id}{filename}: $md5\n";
	    }
	}
    }
}

######################################################################
# create_index - create the HTML index

sub create_index {

    # We want to display things sorted on vendor (different pages),
    # product and version. Thus, we have to build a new hash.

    my %sorted;
    foreach my $rec (keys %catalogue) {

	# Make sure the vendor, product and version attributes are set
	foreach my $key (('vendor', 'product', 'version', 'platform')) {
	    unless ($catalogue{$rec}{$key}) {
		$catalogue{$rec}{$key} = 'Unknown';
	    }
	}

	$sorted{$catalogue{$rec}{vendor}}{$catalogue{$rec}{product}}{$catalogue{$rec}{version}}{$catalogue{$rec}{platform}}{$rec} = 1;
    }

    my %links;

    foreach my $vendor (sort keys %sorted) {

	my $filename = $vendor.'.html';
	$filename =~ s/\s/_/g;

	$links{$filename} = $vendor;

	open( INDEX, ">".$file_path.$filename )
	    or die "Can not open $file_path$filename";

	print "Writing $filename\n";

	print INDEX $html_header;

	print INDEX "<h1>$vendor</h1>\n";
	print INDEX "<table border=\"0\" cellspacing=\"0\">\n";

	foreach my $product (sort keys %{$sorted{$vendor}}) {
	    foreach my $version (sort keys %{$sorted{$vendor}{$product}}) {

		# A subheader in the table for each product/version

		print INDEX "<tr>\n";
		print INDEX "<td colspan=\"2\" class=\"product\">";
		print INDEX "$product $version</td>\n";
		print INDEX "</tr>\n";

		foreach my $platform (sort keys %{$sorted{$vendor}{$product}{$version}}) {

		    print INDEX "<tr>\n";
		    print INDEX "<td colspan=\"2\" class=\"platform\">";
		    print INDEX "Platform: <b>$platform</b></td>\n";
		    print INDEX "</tr>\n";

		    foreach my $id (sort keys %{$sorted{$vendor}{$product}{$version}{$platform}}) {

			# This is where we print details. Changes here
			# affect layout.

			print INDEX "<tr>\n";

			print INDEX "<td class=\"filename\"><a href=\"$catalogue{$id}{path}/";
			print INDEX "$catalogue{$id}{filename}\">$catalogue{$id}{filename}</a></td>\n";

			print INDEX "<td class=\"filedesc\">$catalogue{$id}{description}</td>\n";
			print INDEX "</tr>\n";

			print INDEX "<tr>\n";
			print INDEX "<td class=\"fileinfo\">&nbsp;</td>\n";
			printf INDEX "<td class=\"fileinfo\">Platform: %s &nbsp; Size: %0.2f MB &nbsp; MD5: %s<br>&nbsp;</td>\n", $catalogue{$id}{platform}, $catalogue{$id}{size} / 1048576, $catalogue{$id}{md5};
			print INDEX "</tr>\n";

		    }

		}

	    }
	}

	print INDEX "</table>\n";
	print INDEX $html_footer;
	close INDEX;

    }

    open( INDEX, ">".$file_path.$html_index_name )
	or die "Can not open $file_path$html_index_name";

    print "Writing $html_index_name\n";

    print INDEX $html_header;

    print INDEX "<h1>Secureguard Download Page</h2>\n";
    print INDEX "<p>Select Product Category:</p>\n";
    print INDEX "<ul>\n";
    foreach my $subindex (sort keys %links) {
	print INDEX "<li><a href=\"$subindex\">$links{$subindex}</a></li>\n";
    }
    print INDEX "</ul>\n";
    print INDEX $html_footer;

    close INDEX;

    return 1;
}

sub print_usage {
    print "Usage: $0 [options] <command>\n\n";
    print "Commands:  update   Update cataloge (find new/moved files)\n";
    print "           verify   Verify catalogue\n";
    print "           md5      Calculate md5 sums\n";
    print "           index    Create HTML index\n";
    print "\n";
    print "Options:   -b       Batch mode. Don't ask questions.\n";
    print "           -f       Force updates.\n";
    print "\n";
}

######################################################################
# MAIN PROGRAM
######################################################################

# Get parameters
getopts( 'fb', \%opt );

if (scalar @ARGV == 0) {
    print_usage;
    exit 2;
}

if (lc( $ARGV[0] ) eq 'update') {
    print "Updating catalogue $catalogue_filename.\n";

    read_catalogue;
    update_catalogue;
    write_catalogue;
} elsif (lc( $ARGV[0] ) eq 'verify') {
    print "Verifying catalogue $catalogue_filename.\n";

    read_catalogue;
    verify_catalogue;

    if ($opt{f}) {
	print "Saving changes\n";
	write_catalogue;
    }

} elsif (lc( $ARGV[0] ) eq 'md5') {
    print "Calculating MD5 message digests, please wait...\n";

    read_catalogue;
    verify_md5;
    write_catalogue;

} elsif (lc( $ARGV[0] ) eq 'index') {
    print "Creating HTML index.\n";

    read_catalogue;
    create_index;

} else {
    print "Unknown command: $ARGV[0]\n\n";
    print_usage;
    exit 2;
}


