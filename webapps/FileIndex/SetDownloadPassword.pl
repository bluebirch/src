#!/usr/bin/perl -w
#
# $Log$
# Revision 1.1  2001/11/12 12:47:49  stefan
# A tool to create passwords for Secureguard Download Page. The scripts
# creates random passwords and writes them to the htpasswd-file, using
# the htpasswd tool. Output is suitable for sending mail, that is,
# piping it to the unix mail tool (mailx). A page with the passwords
# (secret.html) is also created.
#

use strict;

my $CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
my $HTPASSWD = '/home/www/.htpasswd';
my @USERS = ( 'nokia', 'checkpoint' );
my $HTPASSWDBIN = '/usr/apache/bin/htpasswd';
my $SECRET_PAGE = '/home/www/secret.html';
my $URL = 'http://host243-183.skynet.obbit.se/';

sub randpw {
    my $pw;

    for (my $i=0; $i < 8 ; $i++) {
	$pw .= substr( $CHARS, rand length $CHARS, 1 );
    }
    return $pw;
}

open SECRET, ">".$SECRET_PAGE or die "no way";
print SECRET "<html><head><title>Secret page</title></head><body>";
print SECRET "<h1>Current passwords</h1><table>";

print "Reply-To: Stefan Svensson <stefan.svensson\@secureguard.com>\n";
print "Subject: Password for Secureguard Download Page\n\n";
print "Setting passwords for Secureguard Download Page ($URL)\n";
print "\n";

foreach my $user (@USERS) {
    my $pw = randpw;
    system( "$HTPASSWDBIN -b $HTPASSWD $user $pw > /dev/null 2>&1" );
    if ($? == 0) {
	printf "%-12s: %s\n", $user, $pw;
	print SECRET "<tr><td>$user</td><td>$pw</td></tr>";
    } else {
	printf "%-12s: error - no password set\n", $user;
	print SECRET "<tr><td>$user</td><td>error - no password set</td></tr>";
    }
}

print "\n";
print "Current password can be viewed on ",$URL,"secret.html.\n";

print SECRET "</table></body></html>";
close SECRET;
