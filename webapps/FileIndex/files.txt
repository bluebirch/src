Description: Hotfix for Service Pack 2. Fixes issue with counting IP-addressed before defining external interface.
Filename: 41716-bld15f-des.tgz
Md5: 1da1c97eebd5df255b62adafbd5efa30
Path: Nokia/FireWall-1/4.1
Platform: Nokia IPSO
Product: VPN-1/FireWall-1
Size: 2688194
Vendor: Check Point
Version: 4.1

Description: Hotfix for Service Pack 2. Fixes issue with counting IP-addressed before defining external interface. Strong engryption.
Filename: 41716-bld15f-strong.tgz
Md5: a4686b362bfaa7edd3178eda4c00bed8
Path: Nokia/FireWall-1/4.1
Platform: Nokia IPSO
Product: VPN-1/FireWall-1
Size: 2688981
Vendor: Check Point
Version: 4.1

Description: CVPM Release Notes (SP2)
Filename: CVPM_41_sp2_release_notes.pdf
Md5: 7aabd4618121bd6f73c7ce11e2a51b03
Path: CheckPoint/FireWall-1/4.1
Platform: Documentation
Product: VPN-1/FireWall-1
Size: 66641
Vendor: Check Point
Version: 4.1

Description: Remote installation
Filename: RInstall.exe
Md5: f0cdd6d57a9ec9c321285dd4c7889e17
Path: CheckPoint/FireWall-1/4.1
Platform: Windows
Product: VPN-1/FireWall-1
Size: 1661606
Time: Mon Oct  2 19
Vendor: Check Point
Version: 4.1

Description: SP4 Remote Installation
Filename: SP4-Rinstall.tar
Md5: c057a85cab1b728ff64c54791539e24e
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 1454080
Vendor: Check Point
Version: 4.1

Description: Remote installation documentation
Filename: cp_remote_installation.pdf
Md5: a22b10a6c7f24a52b0237d3d2be32599
Path: CheckPoint/FireWall-1/4.1
Product: VPN-1/FireWall-1
Size: 22388
Time: Mon Oct  2 19
Vendor: Check Point
Version: 4.1

Description: Nokia CryptoConsole v3.0
Filename: cryptoconsole-solaris-v3.0-2448.tar.Z
Md5: 25466fa725ae56c9d9e8ed55273f1387
Path: Nokia/VPN
Platform: Nokia CryptoCluster
Product: VPN
Size: 3283507
Time: Thu Jun 28 10
Vendor: Nokia
Version: 3.0

Description: CVPM Download (SP2)
Filename: cvpm_41_sp2_solaris2.tgz
Md5: 6cc9b76e2a14ac3812af1da58a958570
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 365630
Vendor: Check Point
Version: 4.1

Description: CVPM Download (SP2)
Filename: cvpm_41_sp2_winnt.exe.gz
Md5: 6c953ddefede9d4893a1489342e6ab7c
Path: CheckPoint/FireWall-1/4.1
Platform: Windows NT
Product: VPN-1/FireWall-1
Size: 290308
Vendor: Check Point
Version: 4.1

Description: F-Secure SSH for IPSO 3.2.1
Filename: f-secure-ssh.tgz
Md5: 82fca400aa57fddbc05fd2498561df0e
Path: Nokia/IPSO/3.2.1
Platform: Nokia IP
Product: IPSO
Size: 514105
Time: Wed Aug  8 11
Vendor: Nokia
Version: 3.2.1

Description: Service Pack 2 STRONG
Filename: fw-1_41716_1_linux_strong.tgz
Md5: 0177a8e922c4fdb85215d0c42f1583d7
Path: CheckPoint/FireWall-1/4.1
Platform: Linux
Product: VPN-1/FireWall-1
Size: 3503011
Vendor: Check Point
Version: 4.1

Description: Service Pack 2
Filename: fw-1_41716_1_solaris2_des.tgz
Md5: 7cc8f6b8389edc6af25c2ffb50687741
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 10036678
Vendor: Check Point
Version: 4.1

Description: Service Pack 2 STRONG
Filename: fw-1_41716_1_solaris2_strong.tgz
Md5: bc8d48f0cc9386c3b002311ad46e4af5
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 10036219
Vendor: Check Point
Version: 4.1

Description: Service Pack 2 STRONG
Filename: fw-1_41716_1_win32_strong.tgz
Md5: aac3d1924316341d24566f31aafd3913
Path: CheckPoint/FireWall-1/4.1
Platform: Windows NT
Product: VPN-1/FireWall-1
Size: 8642121
Vendor: Check Point
Version: 4.1

Description: Service Pack 3 STRONG
Filename: fw-1_41814_1_solaris2_strong.tgz
Md5: a1713d830b82982528109c5bb2756135
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 8044544
Vendor: Check Point
Version: 4.1

Description: Service Pack 4 STRONG
Filename: fw-1_41862_2_sp4_solaris2_strong.tgz
Md5: bb0c1a9fd160d79a2fb3315e191fbea1
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 10384767
Vendor: Check Point
Version: 4.1

Description: Service Pack 4 STRONG
Filename: fw-1_41862_2_sp4_win32_strong.tgz
Md5: 67f07dc50f20e924e87b168b6f024515
Path: CheckPoint/FireWall-1/4.1
Platform: Windows NT
Product: VPN-1/FireWall-1
Size: 10349288
Vendor: Check Point
Version: 4.1

Description: Service Pack 4 STRONG
Filename: fw-1_41862_5_sp4_linux_strong.tgz
Md5: be326a118ff95737a74fbb24aa9bfefd
Path: CheckPoint/FireWall-1/4.1
Platform: Linux
Product: VPN-1/FireWall-1
Size: 7939721
Vendor: Check Point
Version: 4.1

Description: VPN-1/FireWall-1 Service Pack 4
Filename: fw-1_41864_2_ipso34_strong.tgz
Md5: 025836331f005cd847da5f8ef393e9aa
Path: Nokia/FireWall-1/4.1
Platform: Nokia IP
Product: VPN-1/FireWall-1
Size: 23484757
Time: Thu Oct 18 13
Vendor: Check Point
Version: 4.1

Description: VPN-1/FireWall-1 SP5
Filename: fw-1_41_sp5_ipso3_4_x_strong.tgz
Md5: aad469b71ef898edf839286e117c5b98
Path: Nokia/FireWall-1/4.1
Platform: Nokia IP
Product: VPN-1/FireWall-1
Size: 23514640
Time: Thu Oct 18 13
Vendor: Check Point
Version: 4.1

Description: FW-1 SP5 STRONG
Filename: fw-1_41_sp5_solaris2_strong.tgz
Md5: c39f0f7aaf6a69287442b40b91c7ca5d
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 10409624
Time: Wed Oct 17 16
Vendor: Check Point
Version: 4.1

Description: Service Pack 2 Backward Compatibility Hotfix
Filename: fw-1_4205_1_sp2bc_solaris2.tgz
Md5: ec6089f9cd7b7512f184dd200ad553b0
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 8064498
Vendor: Check Point
Version: 4.1

Description: Service pack 2 Backward Compatibility Hotfix
Filename: fw-1_4205_1_sp2bc_winnt.tgz
Md5: baabf48579c1a6fb370ea76ba4d051b7
Path: CheckPoint/FireWall-1/4.1
Platform: Windows NT
Product: VPN-1/FireWall-1
Size: 4486957
Vendor: Check Point
Version: 4.1

Description: SP7 (Build 4205)
Filename: fw-1_4205_1_win32_des.tgz
Md5: d41d8cd98f00b204e9800998ecf8427e
Path: CheckPoint/FireWall-1/4.0
Platform: Windows NT 4.0
Product: VPN-1/FireWall-1
Size: 7326471
Vendor: Check Point
Version: 4.0

Description: SP7 STRONG (Build 4205)
Filename: fw-1_4205_1_win32_strong.tgz
Md5: 1f23359bed5c0e2c047b8a4418893bfb
Path: CheckPoint/FireWall-1/4.0
Platform: Windows NT 4.0
Product: VPN-1/FireWall-1
Size: 7303442
Vendor: Check Point
Version: 4.0

Description: SP7 (Build 4205)
Filename: fw-1_4205_2_solaris2_des.tgz
Md5: 4091aa37b4339b06b712d9c6fadf1d45
Path: CheckPoint/FireWall-1/4.0
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 10354616
Vendor: Check Point
Version: 4.0

Description: SP7 STRONG (Build 4205)
Filename: fw-1_4205_2_solaris2_strong.tgz
Md5: 763be4b7491edb613fe3644a3623e557
Path: CheckPoint/FireWall-1/4.0
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 10314009
Vendor: Check Point
Version: 4.0

Description: FireWall-1
Filename: fw.solaris2pkgadd.vpndes.tar.gz
Md5: a64b219f5d9433fe5cc87e52d454fc92
Path: CheckPoint/FireWall-1/4.0
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 12707713
Vendor: Check Point
Version: 4.0

Description: FireWall-1 STRONG
Filename: fw.solaris2pkgadd.vpnstrong.tar.gz
Md5: f72cc8c7b4e94fe997bfa3af92c5e936
Path: CheckPoint/FireWall-1/4.0
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 11681792
Vendor: Check Point
Version: 4.0

Description: FW Gui
Filename: fw1_4031win32gui.zip
Md5: 799d8685a958e800379e34d90ddbd696
Path: CheckPoint/FireWall-1/4.0
Platform: Windows
Product: VPN-1/FireWall-1
Size: 2773164
Vendor: Check Point
Version: 4.0

Description: FireWall-1 SP1
Filename: fw1_40win32vpndes.zip
Md5: 9ba47b281bda4acfafe833e54ae76b45
Path: CheckPoint/FireWall-1/4.0
Platform: Windows NT 4.0
Product: VPN-1/FireWall-1
Size: 6739723
Vendor: Check Point
Version: 4.0

Description: FireWall-1 SP1 STRONG
Filename: fw1_40win32vpnstrong.zip
Md5: 7da8f13df0e0a4a79935f5c099c12561
Path: CheckPoint/FireWall-1/4.0
Platform: Windows NT 4.0
Product: VPN-1/FireWall-1
Size: 6710129
Vendor: Check Point
Version: 4.0

Description: SP2
Filename: fw4.1-SP-2-ipso3.2-des.tgz
Md5: 7f7a5975bb321c23dfb973f7c3f09156
Path: Nokia/FireWall-1/4.1
Platform: Nokia IPSO
Product: VPN-1/FireWall-1
Size: 22543933
Vendor: Check Point
Version: 4.1

Description: SP2 STRONG
Filename: fw4.1-SP-2-ipso3.2-strong.tgz
Md5: 09d10d2c6f0061532eabd974728cd546
Path: Nokia/FireWall-1/4.1
Platform: Nokia IPSO
Product: VPN-1/FireWall-1
Size: 22545235
Vendor: Check Point
Version: 4.1

Description: FW Gui
Filename: fwgui.solaris2pkgadd.tar.gz
Md5: 2ea75bc5085d8de782846913618ad1b5
Path: CheckPoint/FireWall-1/4.0
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 6881280
Vendor: Check Point
Version: 4.0

Description: FW Gui SP2
Filename: fwgui_41710_1_win32.tgz
Md5: e4615eb827f1f79fd023ebc531d91aa3
Path: CheckPoint/FireWall-1/4.1
Platform: Windows NT
Product: VPN-1/FireWall-1
Size: 4749276
Vendor: Check Point
Version: 4.1

Description: FW Gui SP2
Filename: fwgui_41712_1_solaris2.tgz
Md5: 9a2a3f26e984e8c9bab885e762e2c48c
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 3026187
Vendor: Check Point
Version: 4.1

Description: FW Gui SP3
Filename: fwgui_41813_2_win32.tgz
Md5: 543bb9e2a2dfb9283dbc6d882b4c6893
Path: CheckPoint/FireWall-1/4.1
Platform: Windows NT
Product: VPN-1/FireWall-1
Size: 4776242
Vendor: Check Point
Version: 4.1

Description: FW Gui SP4
Filename: fwgui_41862_1_sp4_solaris2.tgz
Md5: 8ec2c2c85485f32edae3c083995f289c
Path: CheckPoint/FireWall-1/4.1
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 3055771
Vendor: Check Point
Version: 4.1

Description: FW GUI SP5
Filename: fwgui_41_sp5_win32.tgz
Md5: d518e3fa6a5a24c04645b15f5a44348a
Path: CheckPoint/FireWall-1/4.1
Platform: Windows
Product: VPN-1/FireWall-1
Size: 4836082
Time: Wed Oct 17 16
Vendor: Check Point
Version: 4.1

Description: fwrules - convert rule base to HTML
Filename: fwrules42.zip
Md5: 25620e1df8e97c6b34eefc35c0eec903
Path: Utilities
Platform: Any
Product: fwrules
Size: 24027
Time: Mon Oct  2 19
Vendor: Freeware
Version: 4.2

Description: fwrules - create HTML file from rule base and object database
Filename: fwrules50.zip
Md5: 8d815c2be75f1737643faa90341eb7ff
Path: Utilities
Product: fwrules
Size: 12465
Time: Mon Oct  2 19
Vendor: Freeware
Version: 5.0

Description: Operating system
Filename: ipso.tgz
Md5: 6ab34eab9db226651f9c1873c19268bb
Path: Nokia/IPSO/3.2.1
Platform: IP
Product: IPSO
Size: 11979674
Vendor: Nokia
Version: 3.2.1

Description: IPSO Image
Filename: ipso.tgz
Md5: d94707e593cd8895a290be7d02dfa0cc
Path: Nokia/IPSO/3.3
Platform: IP Series Hardware
Product: IPSO
Size: 19398656
Vendor: Nokia
Version: 3.3 FCS8

Description: Release Notes
Filename: ipso_33_relnotes.pdf
Md5: 8a8868100869bcc6eb6d2864206a593f
Path: Nokia/IPSO/3.3
Platform: IP Series Hardware
Product: IPSO
Size: 203066
Vendor: Nokia
Version: 3.3 FCS8

Description: Load Agent
Filename: load_agent_nt.exe
Md5: f0761e79eab00053fe48399f59db8375
Path: CheckPoint/FireWall-1/4.0
Platform: Windows NT 4.0
Product: VPN-1/FireWall-1
Size: 9216
Vendor: Check Point
Version: 4.0

Description: Load Agent
Filename: loadagent.pkgadd.solaris2.tar.gz
Md5: 6f24fbfa2488239b7f5575c9bff0c5a5
Path: CheckPoint/FireWall-1/4.0
Platform: Solaris
Product: VPN-1/FireWall-1
Size: 7646
Vendor: Check Point
Version: 4.0

Description: Boot Flash for IP300 and IP600 platforms
Filename: nkipflash.bin
Md5: b29a2b0461a1de76d322e7294a5cdc8f
Path: Nokia/IPSO/3.3
Platform: IP Series Hardware
Product: IPSO
Size: 1474560
Vendor: Nokia
Version: 3.3 FCS8

Description: Boot Flash for the VPN 200 Series
Filename: nkvpnflash.bin
Md5: 4f915bdb44ba13313f238683f0f143aa
Path: Nokia/IPSO/3.3
Platform: IP Series Hardware
Product: IPSO
Size: 1474560
Vendor: Nokia
Version: 3.3 FCS8

Description: SP1 Release Notes
Filename: release_notes_40_sp1.pdf
Md5: f7c9d8c5f21a4325fea8027c9d2dc518
Path: CheckPoint/FireWall-1/4.0
Platform: Documentation
Product: VPN-1/FireWall-1
Size: 553351
Vendor: Check Point
Version: 4.0

Description: SP7 Release notes
Filename: release_notes_40_sp7.pdf
Md5: 1258ea3e50c21dbe081c69ff6b1fac64
Path: CheckPoint/FireWall-1/4.0
Platform: Documentation
Product: VPN-1/FireWall-1
Size: 82944
Vendor: Check Point
Version: 4.0

Description: SP2 Release Notes
Filename: release_notes_41_sp2.pdf
Md5: 7b51f02bfa58fb9e6ffdbb6b11433c46
Path: Nokia/FireWall-1/4.1
Platform: Nokia IP
Product: VPN-1/FireWall-1
Size: 145782
Time: Mon Oct  2 19
Vendor: Check Point
Version: 4.1

Description: SP2 Release Notes
Filename: release_notes_41_sp2.pdf
Md5: db0ce84f01bc755a9aa645b6a63ac419
Path: CheckPoint/FireWall-1/4.1
Platform: Documentation
Product: VPN-1/FireWall-1
Size: 272155
Vendor: Check Point
Version: 4.1

Description: SP4 Release Notes
Filename: release_notes_41_sp4.pdf
Md5: d3d84936c6915226add7af8eb00dc0a7
Path: CheckPoint/FireWall-1/4.1
Platform: Documentation
Product: VPN-1/FireWall-1
Size: 141622
Vendor: Check Point
Version: 4.1

Description: SP4 Remote Installation Release Notes
Filename: remote_installation_sp4.pdf
Md5: 3e031c9fd6160b330682a27b9fe28baf
Path: CheckPoint/FireWall-1/4.1
Platform: Documentation
Product: VPN-1/FireWall-1
Size: 41902
Vendor: Check Point
Version: 4.1

Description: VPN-1 SecuRemote 4.1 STRONG (build 4166)
Filename: sr_4166_win2k_strong.zip
Md5: 50a072dc6c48d19f6e06b72077ff628b
Path: CheckPoint/SecuRemote/4.1
Platform: Windows 2000
Product: VPN-1 SecuRemote
Size: 4642463
Vendor: Check Point
Version: 4.1

Description: VPN-1 SecureClient 4.1 STRONG (build 4185)
Filename: srsc_4185_sp4_win2k_strong.zip
Md5: 7ef5a0737daa7a8b26e0a3dd2fece9e3
Path: CheckPoint/SecuRemote/4.1
Platform: Windows 2000
Product: VPN-1 SecuRemote
Size: 5229386
Vendor: Check Point
Version: 4.1

Description: VPN-1 SecureClient 4.1 STRONG (build 4185)
Filename: srsc_4185_sp4_win9x_strong.zip
Md5: 6ce5a750a1cf5eeb51061f424e1d88f2
Path: CheckPoint/SecuRemote/4.1
Platform: Windows 95, 98, ME
Product: VPN-1 SecuRemote
Size: 5063579
Vendor: Check Point
Version: 4.1

Description: VPN-1 SecureClient 4.1 STRONG (build 4185)
Filename: srsc_4185_sp4_winnt_strong.zip
Md5: c46d2b12fee6a212e29e80f60b671564
Path: CheckPoint/SecuRemote/4.1
Platform: Windows NT
Product: VPN-1 SecuRemote
Size: 5204804
Vendor: Check Point
Version: 4.1

