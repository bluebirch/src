#!/usr/bin/perl -w
#
# Simple wrapper for KNForum. Configuration is done here.
#
# $Id$
# $Log$

use strict;
use KNForum;
use KNConfig;

my $config = new KNConfig;

my $app = KNForum->new( TMPL_PATH => 'themes/default/html/',
			PARAMS => { ipc_key => 'BETA',
				    config => $config } );
$app->run();
