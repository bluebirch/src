# $Id$
#
# Configuration package for KNForum.
#
# $Log$
# Revision 1.1  2002/04/02 19:38:15  stefan
# Initial revision. Great stuff!
#
package KNConfig;

# News server configuration

my $NNTPSERVER      = 'localhost';
my $PORT            = 10119;
my $NEWSGROUPS      = [ 'kristnet.*',
			'swnet.*sofi',
			'swnet.*logi',
			'swnet.*tik' ];

# Web interface configuration

my $SESSION_TIMEOUT = 3600; # One hour
my $COOKIE_LIFETIME = '+1y'; # Cookie lives one year... :-)
my $THEME           = 'default';

# Other configuration; don't edit unless you know what you are doing.

my $CACHE_KEY       = 'BETA';
my $CACHE_EXPIRE    = 10;
my $DEBUG           = 1;

# No configurable variables below this line

sub new {
    my $proto  = shift;
    my $user = shift;
    my $class = ref($proto) || $proto;
    my $self = { nntpserver      => $NNTP_SERVER,
		 nntpport        => $PORT,
		 newsgroups      => $NEWSGROUPS,
		 session_timeout => $SESSION_TIMEOUT,
		 cookie_lifetime => $COOKIE_LIFETIME,
		 theme           => $THEME,
		 cache_key       => $CACHE_KEY,
		 cache_expire    => $CACHE_EXPIRE,
		 debug           => $DEBUG
	       };

    bless( $self, $class );
}

sub attr {
    my $self = shift;
    my $attr = shift;
    $self->{$attr} = shift if (@_);
    return $self->{$attr};
}

sub session_timeout {
    my $self = shift;
    $self->{session_timeout} = shift if (@_);
    return $self->{session_timeout};
}
sub theme {
    my $self = shift;
    $self->{theme} = shift if (@_);
    return $self->{theme};
}
sub nntpserver {
    my $self = shift;
    $self->{nntpserver} = shift if (@_);
    return $self->{nntpserver};
}
sub newsgroups {
    my $self = shift;
    $self->{newsgroups} = shift if (@_);
    return $self->{newsgroups}
}
