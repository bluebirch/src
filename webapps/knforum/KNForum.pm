# $Id$
#
# KNForum - webaccess f�r news
#
# $Log$
# Revision 1.17  2002/04/03 19:37:22  stefan
# Soon ready for beta release
#
# Revision 1.16  2002/04/02 19:33:16  stefan
# Adopted new authentication scheme. The web interface uses frames now,
# and I'm halfway through adapting all runmodes for frames.
#
# Revision 1.15  2002/03/11 22:21:30  stefan
# Removed path configuration for HTML::Template, since this is done from
# index.cgi or knforum.cgi. Added IPC key to NNTPCache, which also is
# set from the calling index.cgi.
#
# Revision 1.14  2002/02/28 15:32:05  stefan
# Fixed bug in set_default_template_params.
#
# Revision 1.13  2002/02/28 15:13:33  stefan
# Added a functionality to allways return to the original page when
# replying or going to the setup page. However, this is not yet fully
# implemented and only works for replies.
#
# The idea is to use two CGI parameters, "return" and "params". The
# "return" parameters specifies the run mode to return to, and "params"
# a list of the current parameters (group, article, sort and show).
#
# Example: A specific article is viewed with runmode "article" with
# parameters "group" and "article" identifying which article to
# view. When you reply to this article, run mode "post" is executed,
# with the "return" parameter set to "article" (the run mode to return
# to when the reply is posted) and "params" set to
# "group=<newsgroup>&article=<article>". When the article is posted, the
# browser is redirected to run mode "article" with the parameters
# specified in "params".
#
# The "return" and "params" parameters are automatically set in
# set_default_template_params.
#
# In templates, "return" should usually be set to the current run mode
# (parameter "rm").
#
# Revision 1.12  2002/02/26 22:31:24  stefan
# Added password editing from the user info page (setup run mode).
#
# Revision 1.11  2002/02/26 22:03:10  stefan
# Better selection of valid run modes. Prepare for other authentication
# mechanisms than Apache HTTP authentication. Improved error
# handling. Translated some comments into English.
#
# Revision 1.10  2002/02/26 16:52:58  stefan
# User registration changed completely. The user now enters all
# information on the first page, and an email is sent with a
# verification code. This way, no user can register without a valid
# email address.
#
# Revision 1.9  2002/02/26 09:46:52  stefan
# Moved HTML templates, style sheets and icons to a new subdirectory
# 'themes/default' to prepare for future theme configuration.
#
# From now on, all documentation and comments will be in English, since
# Markus H�rnvi want to give this piece of software international
# recognition.
#

package KNForum;
use base 'CGI::Application';
use strict;
use locale;
use POSIX qw(strftime);
use MIME::Entity;
use HTML::Entities;
use Date::Parse;
use IPC::Cache;
use Mail::Mailer;
use NNTPCache;
use KNUser;
use Donk;

######################################################################
# Global configuration
#

# Path to HTML templates.
#my $TEMPLATE_DIR = 'themes/default/html/';

# Temporary directory, used to store information about newly
# registered users.
my $TMPDIR = $ENV{TMPDIR} || '/tmp';

######################################################################
# setup
#
# This method initiates CGI::Application, creates connections to NNTP
# server, checks user authentication, and more. The setup method is
# always run before any other run mode.
#
sub setup {
    my $self = shift;

    # Set default run mode
    $self->start_mode( 'frameset' );

    # Define run modes                              frames?
    $self->run_modes( 'listgroup' => \&listgroup, # ok
		      'article' => \&article,     # ok
		      'post' => \&post,           # ok
		      'setup' => \&settings,      # ok
		      'newuser' => \&newuser,     # ok
		      'verify' => \&verify,       # ok
		      'password' => \&password,   # ok
		      'index' => \&start,         # ok
		      'frameset' => \&frameset,   # ok
		      'dummy' => \&dummy,         # ok
		      'denyaccess' => \&denyaccess, # ok
		      'login' => \&login,         # ok
		      'logout' => \&logout,       # ok
		      'sidebar' => \&sidebar,     # ok
		      'AUTOLOAD' => \&start );    # ok

    # Path to templates for HTML::Template
#    $self->tmpl_path( $TEMPLATE_DIR );

    # Create cached NNTP connection. Use IPC key as specified in
    # index.cgi.
    $self->nntpcache( NNTPCache->new( $self->param( 'config' ) ) );

    # Enable debugging
    $self->nntpcache->debug(1);

    # Get CGI object
    my $q = $self->query;

    # Get configuration object and store it internally
    my $config = $self->param( 'config' );
    $self->config( $config );

    # Set debugging mode
    $self->debug( $config->attr( 'debug' ) );

    use integer;
    $self->{http_request_id} = 0 + rand( 65535 );
    no integer;

    infomsg "[init $ENV{REMOTE_ADDR}] [mode ",
      ($q->param( 'rm' ) || '(undef)'), "] [req ", $self->{http_request_id},
	"]";

    # Get current runmode.
    my $runmode = $q->param( 'rm' ) || $self->start_mode;

    # Check for session key
    my $session_key = $q->cookie( 'session_key' );

    if ($session_key) {

	# Authenticate user

	my $u = new KNUser;
	if ($u->validate_key( $session_key )) {

	    # Check for session timeout
	    if (time - $u->timestamp <= $self->config->session_timeout) {
		logmsg "session key valid; user = ", $u->user
		  if ($self->debug);

		$self->user( $u );
		$self->remote_user( $u->user );

		$u->set_timestamp;
	    }

	    # Session timeout
	    else {
		logmsg "session timeout; user = ", $u->user
		  if ($self->debug);

		$q->param( 'return' => $self->return_params );
		$q->param( 'rm' => 'logout' );
		$q->param( 'logout_reason', 'Du har loggats ut p� grund av inaktivitet.' );
	    }
	}
	else {
	    logmsg "session key invalid"
	      if ($self->debug);
	    $q->param( 'return' => $self->return_params );
	    $q->param( 'rm' => 'logout' );
	    $q->param( 'logout_reason', 'Du har loggats ut p� grund av att din session inte l�ngre �r giltig.' );
	}
    }
    elsif ($self->debug) {
	logmsg "no session key set";
    }

    # Verify that user is authenticated for certain runmodes
    my $rm = $q->param( 'rm' );
    if (!$self->user && $rm =~ m/^(post|setup)$/) {
	$q->param( 'return' => $self->return_params );
	$q->param( 'rm' => 'denyaccess' );
    }

}


######################################################################
# teardown
#
# Things to do when the application is shut down.
#
sub teardown {
    my $self = shift;
    infomsg "[shutdown] [req ", $self->{http_request_id}, "]";
}

######################################################################
# listgroup
#
# Show all articles in specified newsgroup.
#
sub listgroup {
    my $self = shift;

    # Get CGI query object
    my $q = $self->query;

    # Get NNTPCache object
    my $n = $self->nntpcache;

    # Get user object
    my $u = $self->user;

    # Make sure that sort and view parameters are set
    $self->set_default_view;

    # Get CGI parameters
    my $group = $q->param( 'group' );
    my $article = $q->param( 'article' ); # selected article
    my $sort_order = $q->param( 'sort' );
    my $unread = $q->param( 'show' ) && $q->param( 'show' ) eq 'new';

    # Mark all articles as read if the catchup button is pressed. This
    # is actually quite ineffective, especially having it in the
    # listgroup function, since we have to do some things that is done
    # in sidebar too.
    if ($u && $q->param( 'catchup' )) {
	my $groups = $n->list;
	my $groupinfo = $self->selected_group( $groups, $group );

	if ($groupinfo) {
	    $u->newsrc->mark_range( $group,
				    $$groupinfo{first},
				    $$groupinfo{last} );
	}
    }

    # Get list of articles in selected newsgroup
    my ($list, $index) = $n->listgroup( $group );

    # Mark selected article
    if ($article && defined $$index{$article}) {
	$$list[$$index{$article}]{selected} = 1;
    }

    # Mark or remove all read articles (slow, especially removing read
    # articles)
    if ($u) {
	my $i = 0;
	if ($list) {
	    while ($i < scalar @$list) {
		if ($$list[$i] && $u->newsrc->marked( $group, $$list[$i]{article} )) {
		    if ($unread) {
			splice @$list, $i, 1;
		    } else {
			$$list[$i]{seen} = 1;
			$i++;
		    }
		} else {
		    $i++;
		}
	    }
	}
    }

    # Sort articles
    $list = $n->sort_articles( $list, $sort_order );

    # Initialize HTML::Template and set parameters
    my $t = $self->load_tmpl( 'listgroup.html',
			      die_on_bad_params => 0,
			      loop_context_vars => 1,
			      global_vars => 1 );

#    $self->set_default_template_params( $t );

    $t->param( 'error' => $n->errstr ) unless ($n->ok);
    $t->param( 'articles' => $list );
    $t->param( 'group' => $group );
#    $t->param( 'groups' => $groups );
    $t->param( 'sort' => $sort_order );
    $t->param( 'sort_'.$sort_order => 1 );
    $t->param( 'show' => $q->param( 'show' ) );
    $t->param( 'show_'.$q->param( 'show' ) => 1 );

    return $t->output;
}

######################################################################
# article
#
# Show article.
#
sub article {
    my $self = shift;

    # Get CGI object
    my $q = $self->query;

    # Get NNTPCache object
    my $news = $self->nntpcache;

    # Initiera HTML::Template
    my $t = $self->load_tmpl( 'article.html',
			      die_on_bad_params => 0,
			      global_vars => 1 );

    # Get parameters
    my $group = $q->param( 'group' );
    my $article = $q->param( 'article' );

    # Get article
    my $data = $news->article( $group, $article );

    # If no data was received, there's an error somewhere
    unless ($data) {
	return $self->fatal_error( 'Kunde inte h�mta artikel.' );
    }

    # Mark article as seen
    if ($news->ok && $self->user) {
	$self->user->newsrc->mark( $group, $article );
    }

    # Set parameters
    $t->param( $data );
    $t->param( 'sort' => $q->param( 'sort' ) );
    $t->param( 'show' => $q->param( 'show' ) );

    # Return template
    return $t->output;
}

######################################################################
# post
#
# Redigera och posta newsinl�gg
#
sub post {
    my $self = shift;

    # Get standard objects
    my $q = $self->query;
    my $u = $self->user;
    my $n = $self->nntpcache;
    my $t = $self->load_tmpl( 'post.html',
			      die_on_bad_params => 0 );

    # Get CGI parameters
    my $group = $q->param( 'group' );
    my $article = $q->param( 'article' );
    my $references = $q->param( 'references' );
    my $subject = $q->param( 'subject' );
    my $text = $q->param( 'text' );
    my $return = $q->param( 'return' );
    my $params = $q->param( 'params' );

    # Create suitable return parameters if they are not set.
    unless ($return) {
	if ($group && $article) {
	    $return = 'rm=article,group=$group,article=$article';
	} else {
	    $return = 'rm=dummy';
	}
    }

    # If the form is submitted, try to post it to the NNTP backend.
    if ($q->param( 'submit' )) {

	# Get our own hostname; use the URL we were called with. This
	# is used together with the current time and a random number
	# to create the msgid.
	my $url = $q->url;
	$url =~ m{http://([^/]+)}i;
	my $host = $1;

	# Split, indent and cleanup text
	my @message = split /\r?\n/, $text;
	$self->wrap_or_indent_article( \@message, 0 );
	$text = join "\r\n", @message;
	$text .= "\r\n\r\n";

	# Create MIME entity with headers according to RFC1036.
	my $msg = MIME::Entity->build( From => $u->name.' <'.$u->email.'>',
				       Date => strftime( '%a, %d %b %Y %H:%M:%S %z', localtime ),
				       'Newsgroups:' => $group,
				       'Message-ID' => sprintf( '<%08x%08x@%s>', time, rand 4294967295, $host ),
				       Subject => $subject,
				       Organization => 'KNForum (http://knforum.kristnet.org)',
				       Charset => 'iso-8859-1',
				       Data => $text );

	$msg->head->add( 'References:', $references ) if ($references);
	$msg->sign( Signature => scalar ($u->signature) )
	  if (scalar $u->signature);

	# Create array of MIME entity. Reuse @message variable.
	@message = split /\r?\n/, $msg->as_string;

	# Contact the NNTP server.
	my $server = $n->nntpclient;

	# Post article.
	if ($server->post( @message )) {
	    $n->flush_cache;

	    # Redirect client browser
	    $return =~ s/,/&/g;
	    logmsg "post success; return to ", $q->url, "?$return"
	      if ($self->debug);
	    $self->header_props( -uri => $q->url . "?$return" );
	    $self->header_type( 'redirect' );

	    return '';
	}
    }

    # If form was cancelled, redirect client browser
    elsif ($q->param( 'cancel' )) {
	$return =~ s/,/&/g;
	logmsg "cancel; return to ", $q->url, "?$return"
	  if ($self->debug);
	$self->header_props( -uri => $q->url . "?$return" );
	$self->header_type( 'redirect' );
	return '';
    }

    # If form was not posted, if no text exists and if group and
    # article was specified, get original article and indent it
    # nicely.
    elsif ($group && $article && !$text) {

	# Get original article
	my $original = $n->article( $group, $article );

	# Check if there is any text to work with
	if ($$original{text}) {

	    # Set return parameters; we know that this article exists.
	    $return = "rm=article,group=$group,article=$article";

	    # Get text
	    $text = $$original{text};

	    # Remove signature
	    $text =~ s/^-- [\r\n].*//sm;

	    # Split text in lines
	    my @text = split /\r?\n/, $text;

	    # Wrap lines and indent
	    $self->wrap_or_indent_article( \@text, 1 );

	    # Create a text flow
	    $text = "I ett inl�gg daterat $$original{date} skrev $$original{name}:\r\n\r\n" . join "\r\n", @text;

	    # Set references and subject
	    $references = $$original{references} ?
	      "$$original{references} $$original{msgid}" : $$original{msgid};
	    $subject = ($$original{subject} =~ m/^re:/i) ?
	      $$original{subject} : "Re: $$original{subject}";
	}
    }

    # Get list of newsgroups
    my $groups = $n->list;

    # Set active newsgroup
    $self->selected_group( $groups, $group );

    # Set template paremters
    $t->param( 'text' => $text );
    $t->param( 'subject' => $subject );
    $t->param( 'references' => $references );
#    $t->param( 'group' => $group );
#    $t->param( 'article' => $article );
    $t->param( 'groups' => $groups );
#    $t->param( 'user_login' => $u->user );
    $t->param( 'user_name' => $u->name );
    $t->param( 'user_email' => $u->email );
    $t->param( 'return' => $return );

    return $t->output;
}


######################################################################
# sidebar - display sidebar, handle login, etc
#
sub sidebar {
    my $self = shift;

    # Get CGI query object
    my $q = $self->query;

    # Get user object. The object is undefined if no user is
    # authenticated.
    my $u = $self->user;

    # Get NNTPcache object
    my $n = $self->nntpcache;

    # Load HTML::Template
    my $t = $self->load_tmpl( 'sidebar.html', die_on_bad_params => 0 );

    # Make sure that sort and view parameters are set
    $self->set_default_view;

    # Get selected group
    my $group = $q->param( 'group' );

    logmsg "selected group: ", ($group || '(no group)')
      if ($self->debug);

    # Get list of newsgroups
    my $groups = $n->list;

    # Set number of unread articles in each group
    $self->set_unread_articles( $groups );

    # Select active newsgroup
    my $groupinfo = $self->selected_group( $groups, $group );

    # Set template parameters
    if ($u) {
	$t->param( 'user_login' => $u->user );
	$t->param( 'user_name' => $u->name );
	$t->param( 'user_email' => $u->email );
    }
    $t->param( 'group' => $group );
    $t->param( 'groups' => $groups );
    $t->param( 'sort' => $q->param( 'sort' ) );
    $t->param( 'sort_'.$q->param( 'sort' ) => 1 );
    $t->param( 'show' => $q->param( 'show' ) );
    $t->param( 'show_'.$q->param( 'show' ) => 1 );

    return $t->output;
}

######################################################################
# login - display login screen, authenticate user
#
sub login {
    my $self = shift;

    # Get CGI query object
    my $q = $self->query;

    # Load HTML::template
    my $t = $self->load_tmpl( 'login.html' );

    # Get user name and password
    my $user = $q->param( 'user' );
    my $password = $q->param( 'password' );

    my $msg; # Response message

    # Verify user if login button is pressed
    if ($q->param( 'login' )) {

	# Create user object
	my $u = new KNUser;

	# Check if user exists
	if ($u->exists( $user )) {

	    # Load user info
	    $u->load( $user );

	    # Do password authentication
	    if ($u->verify_password( $password )) {

		# Store user object
		$self->user( $u );

		# Update timestamp
		$u->set_timestamp;

		# Create HTTP cookie
		my $cookie = $q->cookie( -name => 'session_key',
					 -value => $u->create_key,
					 -expires => '+1d',
					 -path => '/' );

		# Store cookie in HTTP headers with URL.
		$self->header_props( -cookie => $cookie,
				     -uri => $q->url . "?return=" .
          				     $q->param( 'return' ) );

		# Set header type to redirect
		$self->header_type( 'redirect' );

		# There should be a mechanism to return to a specific
		# page after login here.

		return '';

	    }
	}
	$msg = "Felaktig inloggning. F�rs�k igen.";
    }

    # Set template parameters
    $t->param( 'user' => $user );
    $t->param( 'message' => $msg );
    $t->param( 'return' => $q->param( 'return' ) );

    return $t->output;
}


######################################################################
# logout - remove session key
#
sub logout {
    my $self = shift;

    # Get CGI object
    my $q = $self->query;

    # Get HTML::Template
    my $t = $self->load_tmpl( 'logout.html',
			      die_on_bad_params => 0 );

    $t->param( 'return' => $q->param( 'return' ) );
    $t->param( 'reason' => $q->param( 'logout_reason' ) );

    # Create a dummy session key cookie
    my $cookie = $q->cookie( -name => 'session_key',
			     -value => '*',
			     -expires => '-1d',
			     -path => '/' );

    # Store cookie in HTTP headers
    $self->header_props( -cookie => $cookie );

    # There should be a mechanism to return to a specific page after
    # login here.

    return $t->output;;
}


######################################################################
# settings
#
# Allow user to edit his profile.
#
sub settings {
    my $self = shift;

    # Messages to user
    my $msg;

    # Standard objects
    my $q = $self->query;
    my $u = $self->user;;

    # Get CGI parameters
    my $name = ($q->param( 'user_name' ) || $u->name);
    my $email = ($q->param( 'user_email' ) || $u->email);
    my $signature = ($q->param( 'user_signature' ) || scalar ($u->signature));
    my $password = $q->param( 'password' );
    my $verify = $q->param( 'verify' );
    my $return = ($q->param( 'return' ) || 'list'); # Run mode to
                                                    # return to

    # Save if we should save
    if ($q->param( 'save' )) {

	# Check validity
	if ($name && $u->valid_email( $email )) {

	    # If password is entered, check it
	    if ($password) {
		if (!$verify) {
		    $msg = "Du m�ste ange l�senordet tv� g�nger.";
		} elsif ($password ne $verify) {
		    $msg = "L�senorden st�mmer inte �verens.";
		} elsif (length( $password ) < 6) {
		    $msg = "L�senordet m�ste vara minst sex tecken l�ngt."
		} else {
		    $u->password( $password );
		}
	    }

	    if (!$msg) {

		# Update user info
		$u->email( $email );
		$u->name( $name );
		$u->signature( split /\r?\n/, $signature );

		$msg = "Anv�ndarinfo �r uppdaterad";
	    }
	}

	# Om vi inte f�tt giltiga v�rden, l�mnar vi ett felmeddelande.
	else {
	    if (!$u->valid_email( $email )) {
		$msg = "Felaktig epostadress. Ange en korrekt epostadress.";
	    } else {
		$msg = "Inget namn angivet. Skriv ditt namn.";
	    }
	}
    }

    # Om formul�ret postades med cancel-knappen, �terg�r vi bara till
    # ursprungs-runmode
#      elsif ($q->param( 'cancel' )) {
#  	return $self->redirect( $return );
#      }

    # Visa ett meddelande om anv�ndaruppgifterna inte st�mmer
    unless ($u->valid) {
	$msg = "Uppge ditt namn och en korrekt epostadress."
    }

    # Initiera HTML::Template
    my $t = $self->load_tmpl( 'setup.html',
			      die_on_bad_params => 0 );

    # S�tt v�rden i template
#    $self->set_default_template_params( $t );
    $t->param( 'user_name' => $name );
    $t->param( 'user_email' => $email );
    $t->param( 'message' => $msg );
    $t->param( 'user_signature' => $signature );

    return $t->output;
}

######################################################################
# newuser - Register a new user.
#
# The user enters a requested login id, a name and an email address. A
# confirmation is sent to the specified email address, and the user is
# requested to verify that the email address is correct before he (or
# she) gets his (or her) password.
#
sub newuser {
    my $self = shift;

    # Variable to store error messages (and to check if an error has
    # occurred).
    my $msg;

    # CGI object
    my $q = $self->query;

    if ($q->param( 'register' )) {
	my $login = $q->param( 'login' );
	my $name = $q->param( 'name' );
	my $email = $q->param( 'email' );
	my $password = $q->param( 'password' );
	my $verify = $q->param( 'verify' );

	# Check data validity
	if (!$login || $login !~ m/^[a-zA-Z0-9]+$/) {
	    $msg = "Ogiltigt anv�ndarnamn. Anv�nd bara bokst�verna A till och med Z, samt siffror.";
	} elsif (!$email || $email !~ m/^[^@]+@([-\w]+\.)+[A-Za-z]{2,4}$/) {
	    $msg = "Ogiltig epostadress.";
	} elsif (!$name) {
	    $msg = "Uppge ditt namn.";
	} elsif (!$password || !$verify) {
	    $msg = "Ange �nskat l�senord tv� g�nger.";
	} elsif ($password ne $verify) {
	    $msg = "L�senordet st�mmer inte. F�rs�k igen.";
	} elsif (length( $password) < 6) {
	    $msg = "L�senordet m�ste vara minst sex tecken l�ngt.";
	} else {

	    # Create user object
	    $self->user( new KNUser ) unless ($self->user);

	    # Check for an existing user name by checking for a
	    # password in the .htpasswd-file.
	    if ($self->user->exists( $login )) {
		$msg = "Anv�ndaren finns redan. Pr�va ett annat anv�ndarnamn.";

		# Yes, I should do something to make this language
		# independent, but not now.
	    } else {

		# Everything is OK. Send email to user.

		my $id = sprintf( '%08X%08X', time, rand 4294967295 );
		if (open TMP, ">$TMPDIR/knforum.$id") {

		    # The user data, including the password, is
		    # written to a file in $TMPDIR. This could be
		    # considered a security issue, but since this
		    # information is not sensitive in any way, it
		    # doesn't really matter.
		    print TMP "$login\n";
		    print TMP "$name\n";
		    print TMP "$email\n";
		    print TMP "$password\n";
		    close TMP;

		    my $mail = new Mail::Mailer 'sendmail';
		    my $fh = $mail->open( { From => 'webmaster@kristnet.org',
					    To => $email,
					    Subject => 'Din registrering p� KNForum',
					    Date => strftime( '%a, %d %b %Y %H:%M:%S %z', localtime )
					  } );
		    print $fh "Hej $name!\n\n";
		    print $fh "Klicka p� l�nken nedan f�r att aktivera ditt anv�ndarkonto p� KNForum:\n\n";
		    print $fh $q->url( -full=>1 ), "?rm=verify&id=$id\n\n";
		    print $fh "(Om det inte fungerar, kan du markera l�nken och kopiera den till din webbl�sare.)\n";
		    $fh->close;

		    my $t = $self->load_tmpl( 'newuser_mailsent.html' );
		    $t->param( 'email' => $email );
		    return $t->output;

		} else {
		    $msg = "Det uppstod ett intern fel. Kontakta systemadministrat�ren.";
		}

	    }
	}
    }

    # If cancel button was pressed, return to main screen
    elsif ($q->param( 'cancel' )) {

	$self->header_props( -uri => $q->url );
	$self->header_type( 'redirect' );
	return '';
    }

    # Initiate HTML::Template and set parameters
    my $t = $self->load_tmpl( 'newuser.html',
			      die_on_bad_params => 0 );

    $t->param( 'login' => $q->param( 'login' ) );
    $t->param( 'name' => $q->param( 'name' ) );
    $t->param( 'email' => $q->param( 'email' ) );
    $t->param( 'message' => $msg );

    return $t->output;
}

######################################################################
# verify - Verify a new user
#
sub verify {
    my $self = shift;

    # Some variables used throughout this method
    my ($login, $name, $email, $password, $msg);

    # Get query object
    my $q = $self->query;

    # Get parameters
    my $id = $q->param( 'id' );

    # Find previous registration
    if (open TMP, "$TMPDIR/knforum.$id") {
	chomp( $login = <TMP> );
	chomp( $name = <TMP> );
	chomp( $email = <TMP> );
	chomp( $password = <TMP> );
	close TMP;

	# Delete temporary file. We don't need it anymore.
	unlink "$TMPDIR/knforum.$id";

	# Create user object
	my $u = new KNUser;

	# Check if user exists
	if ($u->exists( $login )) {
	    $msg = "N�gon annan har hunnit registrera sig med ditt anv�ndarnamn. Du f�r registrera dig p� nytt.";
	} else {

	    # Register new user (piece of cake)
	    $u->user( $login );
	    $u->name( $name );
	    $u->email( $email );
	    $u->password( $password );

	    # Set user object (not necessary, really)
	    $self->user( $u );

	    # Create HTTP cookie
	    my $cookie = $q->cookie( -name => 'session_key',
				     -value => $u->create_key,
				     -expires => '+1d',
				     -path => '/' );

	    # Store cookie in HTTP headers
	    $self->header_props( -cookie => $cookie );
	}

    } else {
	$msg = "Ok�nd verifikationskod. Registrera dig f�rst.";
    }

    # Initialize HTML::Template and set parameters
    my $t = $self->load_tmpl( 'newuser_verify.html',
			      die_on_bad_params => 0 );

    $t->param( 'error' => $msg );
    $t->param( 'name' => $name );

    return $t->output;
}

######################################################################
# password - set new password if user has forgotten it
#
sub password {
    my $self = shift;

    # Message
    my $msg;

    # Get CGI query object
    my $q = $self->query;

    # Get parameters
    my $email = $q->param( 'email' );

    if ($q->param( 'send' )) {

	# Find user
	my $u = new KNUser;
	my $user = $u->find( $email );

	logmsg "look for $email, found $user";

	if ($user) {

	    # Load user info
	    $u->load( $user );

	    # Create random password
	    my $password;
	    for my $i (0..5) {
		$password .= chr( rand( ord( 'z' ) - ord( 'a' ) ) + ord( 'a' ) );
	    }

	    # Send email to user
	    my $mail = new Mail::Mailer 'sendmail';
	    my $fh = $mail->open( { From => 'webmaster@kristnet.org',
				    To => $u->email,
				    Subject => 'Ditt l�senord till KNForum',
				    Date => strftime( '%a, %d %b %Y %H:%M:%S %z', localtime )
				    } );
	    print $fh "Hej ", $u->name, "!\n\n";
	    print $fh "H�r kommer dina inloggningsuppgifter till KNForum:\n\n";
	    print $fh "Login:    ", $u->user, "\n";
	    print $fh "L�senord: $password\n";
	    $fh->close;

	    # Set password
	    $u->password( $password );

	}

	# Show message. Don't bother if a message was really sent or
	# not.
	$msg = "Ett nytt l�senord har skickats till $email." if ($email);
    }

    # Load HTML::Template and set parameters
    my $t = $self->load_tmpl( 'password.html',
			      die_on_bad_params => 0 );
    $t->param( 'email' => $email );
    $t->param( 'message' => $msg );

    return $t->output;

}

######################################################################
# start
#
# Startsida f�r KNForum. Exekveras fr�n index.cgi, som INTE skall ha
# n�gon autenticering. D�remot skall knforum.cgi ha autenticering.
#
sub start {
    my $self = shift;
    my $t = $self->load_tmpl( 'index.html' );
    return $t->output;
}

#
# frameset - Send HTML frameset to client.
#
sub frameset {
    my $self = shift;

    # Get CGI object
    my $q = $self->query;

    # If the return parameter is set, split its contents into single
    # parameters.
    logmsg "return=", $q->param( 'return' ) if ($self->debug);
    if ($q->param( 'return' )) {
	my @params = split ',', $q->param( 'return' );
	foreach (@params) {
	    my ($param, $value) = split '=', $_, 2;
	    logmsg "set $param=$value" if ($self->debug);
	    $q->param( $param, $value );
	}
    }

    # Define URL:s for each frame
    my $sidebar_url = $q->param( 'group' ) ?
      '?rm=sidebar&group=' . $q->param( 'group' ) :
	'?rm=sidebar' ;
    my $list_url = $q->param( 'group' ) ?
      '?rm=listgroup&group=' . $q->param( 'group' ) :
	'?rm=listgroup';
    my $article_url = ($q->param( 'group' ) && $q->param( 'article')) ?
      '?rm=article&group=' . $q->param( 'group' ) . "&article=" .
	$q->param( 'article' ) :
	  '?rm=index';

    # Load HTML::Template
    my $t = $self->load_tmpl( 'frameset.html' );

    # Set parameters
    $t->param( 'sidebar' => $sidebar_url );
    $t->param( 'list' => $list_url );
    $t->param( 'article' => $article_url );

    return $t->output;
}

#
# dummy - display a dummy HTML page
#
sub dummy {
    my $self = shift;
    my $t = $self->load_tmpl( 'dummy.html' );
    return $t->output;
}

######################################################################
# denyaccess
#
sub denyaccess {
    my $self = shift;
    my $t = $self->load_tmpl( 'denyaccess.html' );
    my $return = $self->query->param( 'return' );
    logmsg "return=[$return]" if ($self->debug);
    $t->param( 'return' => $return );
    return $t->output;
}

######################################################################
# set_default_template_params
#
# G�r vad man tror. S�tter template-parametrar som skall finnas
# �verallt.
#
# OBSOLETE: Not used with frames!
#
sub set_default_template_params {
    my ($self, $t) = @_;

    # Get CGI object
    my $q = $self->query;

    # Set user parameters if user is logged in
    if ($self->user) {

	my $u = $self->user;

	# Set user info
	$t->param( 'user_login' => $u->user );
	$t->param( 'user_email' => $u->email );
	$t->param( 'user_name' => $u->name );

	# Set default sort order
	if ($q->param( 'sort' )) {
	    $u->sort( $q->param( 'sort' ) );
	} else {
	    $q->param( 'sort', $u->sort );
	}
	if ($q->param( 'show' )) {
	    $u->show( $q->param( 'show' ));
	} else {
	    $q->param( 'show', $u->show );
	}
    }

    # Set viewing preferences
    $t->param( 'show' => $q->param( 'show' ) );
    $t->param( 'show_'.$q->param( 'show' ) => 1 );
    $t->param( 'sort' => $q->param( 'sort' ) );
    $t->param( 'sort_'.$q->param( 'sort' ) => 1 );

    # Set run mode, return parameters, and more
    $t->param( 'rm' => $q->param( 'rm' ) );
#     $t->param( 'return' => $q->param( 'return' ) );
#     $t->param( 'params' => ($q->param( 'params' ) ||
# 			    join( '/', "group=".($q->param( 'group' ) || ''),
# 				  "article=".($q->param( 'article' ) || ''),
# 				  "sort=".($q->param( 'sort' ) || ''),
# 				  "show=".($q->param( 'show' ) || '' ) ) )
# 	     );
}

######################################################################
# set_default_view - set show/sort to default or predefined values
#
sub set_default_view {
    my $self = shift;

    # Get CGI object and user object
    my ($q, $u) = ($self->query, $self->user);

    # Set default viewing parameters. If user is authenticated, get
    # defaults from user profile, or update user profile if viewing
    # parameters are set.
    if (!$q->param( 'sort' )) {
	$q->param( 'sort', $u ? $u->sort : 'reversedate' );
    } elsif ($u) {
	$u->sort( $q->param( 'sort' ) );
    }
    if (!$q->param( 'show' )) {
	$q->param( 'show', $u ? $u->show : 'new' );
    } elsif ($u) {
	$u->show( $q->param( 'show' ) );
    }
}

######################################################################
# return_params - join runmode, group and article
#
sub return_params {
    my $self = shift;

    # Get query object
    my $q = $self->query;

    return join( ',',
		 "rm=" . ($q->param( 'rm' ) || ''),
		 "group=" . ($q->param( 'group' ) || ''),
		 "article=" . ($q->param( 'article' ) || '') );
}

######################################################################
# redirect - dirigera om klienten till en annan sida
#
# Den enda parameter som kan anges �r vilken runmode som skall g�lla.
#
sub redirect {
    my $self = shift;
    my $runmode = shift;
    my $params = join '&', @_;

    my $q = $self->query;

    # S�tt HTTP-headers och returnera en tom str�ng
    $self->header_type( 'redirect' );
    $self->header_props( -uri=>$q->url() . ($runmode ? "?rm=$runmode" : '')
			 . ($params ? "&$params" : '' ));
    return '';
}

######################################################################
# fatal_error
#
# Visa en helsida med felmeddelande om det uppst�r n�got hemskt,
# fatalt fel som man inte vill ha, t ex att newsservern inte g�r att
# kontakta.
#
sub fatal_error {
    my ($self, $error) = @_;

    $error = "Ett allvarlig fel uppstod."
      unless ($error);

    my $q = $self->query;

    my $result = $q->start_html( 'Allvarligt fel.' ) .
      $q->h1( 'Allvarligt fel' ) . $q->p( $error ) . 
	$q->p( 'Kontakta systemadministrat�ren.' );

    return $result;
}

######################################################################
# selected_group
#
# S�tter parametern "selected" i listan med newsgroups f�r angiven
# newsgroup. Tar tv� parametrar, en arrayref till en lista med
# newsgroups (s� som den returneras fr�n NNTPCache->list) samt ett
# namn p� en newsgroup.
#
sub selected_group {
    my ($self, $groups, $group) = @_;

    return unless ($groups && $group);

    foreach my $ref (@$groups) {
	if ($$ref{group} eq $group) {
	    $$ref{selected} = 1;
	    return $ref;
	}
    }
    return undef;
}

######################################################################
# set_unread_articles
#
# Parse a list of newsgroups, check how many unread articles each
# group has and add this to each record in the list.
#
sub set_unread_articles {
    my ($self, $list) = @_;

    my $u = $self->user;

    return unless ($u);

    my $i=0;
    while ($$list[$i]) {

	# Remove group if it is not a subscribed group (disabled)
#    	if (!$u->newsrc->subscribed( $$list[$i]{group} )) {
#    	    splice @$list, $i, 1;
#    	    next;
#    	}

	# Update list with number of unread articles
	if ($$list[$i]{last} > $$list[$i]{first}) {
	    my @unread = $u->newsrc->unmarked_articles( $$list[$i]{group},
							$$list[$i]{first},
							$$list[$i]{last} );
	    $$list[$i]{unread} = scalar @unread;
	}
	$i++;
    }
}

######################################################################
# wrap_or_indent_article
#
# Radbryt och - om s� �nskas - indentera artikel. Parametrar �r
# arrayref med artikelns rader samt flagga om indentering skall ske.
#
sub wrap_or_indent_article {
    my ($self, $text, $indent) = @_;

    # Indentera varje rad. Om raden blir f�r l�ng, l�t den spilla �ver
    # p� n�sta rad.
    my $overflow;
    my $COLUMNS = 70;
    my $i = -1;
    while (++$i <= $#$text || $overflow) {
	if ($overflow) {
	    if ($$text[$i]) {
		$$text[$i] =~ s/([>\s]*)?(.*)/$1$overflow $2/;
	    } else {
		if ($$text[$i-1]) {
		    if ($$text[$i-1] =~ m/([>\s]*)>/) {
			$$text[$i] = "$1$overflow";
		    } else {
			$$text[$i] = $overflow;
		    }
		} else {
		    $$text[$i] = $overflow;
		}
		splice @$text, $i+1, 0, '';
	    }
	    $overflow = '';
	}
	if ($$text[$i]) {
	    $$text[$i] = '> ' . $$text[$i] if ($indent);
	    if (length( $$text[$i] ) > $COLUMNS) {
		my $j = $COLUMNS+1;
		while (substr( $$text[$i], --$j, 1 ) ne ' ' && $j >= 45) {
		}
		;
		if ($j >= 45) {
		    $overflow = substr( $$text[$i], $j+1 );
		    $$text[$i] = substr( $$text[$i], 0, $j );
		}
	    }
	}
    }
}

######################################################################
# �tkomst till instansdata
#
sub user {
    my $self = shift;
    $self->{user} = shift if (@_);
    return $self->{user};
}
sub nntpcache {
    my $self = shift;
    $self->{nntpcache} = shift if (@_);
    return $self->{nntpcache};
}
sub remote_user {
    my $self = shift;
    $self->{remote_user} = shift if (@_);
    return $self->{remote_user};
}
sub config {
    my $self = shift;
    $self->{config} = shift if (@_);
    return $self->{config};
}
sub debug {
    my $self = shift;
    $self->{debug} = shift if (@_);
    return $self->{debug};
}

# Allt �r sant!
1;
