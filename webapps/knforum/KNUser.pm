# $Id$
#
# KNUser - user information for KNForum
#
# The KNUser package holds all user information and is responsible for
# authentication.
#
# WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING!
#
# I just realised (2002-04-03) that I have created a logical problem
# with this class. Since the new web interface uses frames, there can
# be up to three simultaneous HTTP requests from the same client, all
# of which causes the KNForum scripts to run and the user info to be
# read and rewritten to disk. This could, of course, cause problems if
# one request changes user info, and another request writes back old
# information.
#
# I have some ideas on how to solve this, but I think it will have to
# wait until I redesign KNForum to a true client-server application.
#
# KNUser - anv�ndarinfo f�r KNForum
#
# KNUser ansvarar f�r att h�mta och lagra anv�ndarinformation s�som
# namn, emailadress och sorteringsordning. KNUser h�ller ocks� reda p�
# vilka artiklar anv�ndaren har l�st med hj�lp av ett
# News::Newsrc-objekt. All information lagras i en textbaserad
# konfigurationsfil f�r varje anv�ndare.
#
# Stefan Svensson <stefan@raggmunk.nu>
#
#
# $Log$
# Revision 1.6  2002/04/02 19:31:40  stefan
# Added a new authentication scheme.
#
# Revision 1.5  2002/03/11 22:23:34  stefan
# Added methods for new authentication scheme: create_key, validate_key,
# encrypt and decrypt.
#
# Revision 1.4  2002/02/26 16:54:12  stefan
# Implemented password management with Apache htpasswd files.
#

package KNUser;
use strict;
use News::Newsrc;
use MIME::Base64;
use Crypt::RC4;
use Storable qw(freeze thaw);
use Donk;

######################################################################
# Konfiguration
my $DATADIR = 'data/';

######################################################################
# new - konstruktor
#
sub new {
    my $proto  = shift;
    my $user = shift;
    my $class = ref($proto) || $proto;
    my $self = { userdata => {},
		 newsrc => new News::Newsrc,
		 auth_key => 'q7uiytaso876t4ywg%&yte3ladfg',
		 auth_time => 21600 # seconds!
	       };

    bless( $self, $class );

    # Fetch user information if and only if a user name was specified.
    $self->load( $user ) if ($user);

    return $self;
}

sub DESTROY {
    my $self = shift;

    logmsg "destroy KNUser object";

    if ($self->user) {
	$self->save;
    }
}


######################################################################
# load - h�mta anv�ndardata
#
sub load {
    my ($self, $user) = @_;

    my @newsrc;

    # L�s in anv�ndardata
    if (open USERINFO, "$DATADIR$user") {
	while (<USERINFO>) {
	    next if (m/^#/);
	    chomp;
	    if (s/^@//) {
		my ($key, $value) = split /\s*:\s*/, $_, 2;
		$self->{userdata}{$key} = $value;
	    } else {
		push @newsrc, $_;
	    }
	}
	close USERINFO;
	$self->{newsrc}->import_rc( @newsrc );
    }

    $self->user( $user );
    $self->sort( 'none' ) unless ($self->sort);
    $self->show( 'new' ) unless ($self->show);
}

######################################################################
# save - spara anv�ndardata
#
sub save {
    my $self = shift;

    if ($self->user) {
	my $user = $self->user;
	logmsg "saving to $DATADIR$user";

	if (open USERINFO, ">$DATADIR$user") {
	    print USERINFO "# User data\n";
	    foreach my $key (keys %{$self->{userdata}}) {
		printf USERINFO "@%s:%s\n", $key, $self->{userdata}{$key};
	    }
	    print USERINFO "# Newsrc\n";
	    print USERINFO $_ foreach ($self->{newsrc}->export_rc);
	    close USERINFO;
	} else {
	    warn "Kan inte skriva till $DATADIR$user";
	}
    }
}

######################################################################
# valid - unders�k om anv�ndardata �r korrekt
#
sub valid {
    my $self = shift;

    if ($self->user) {
	if ($self->valid_email && $self->name) {
	    return 1;
	}
    }
    return 0;
}


######################################################################
# valid_email - unders�k om email-adressen �r korrekt
#
sub valid_email {
    my ($self, $email) = @_;

    # Om ingen parameter anges, tar vi adressen fr�n objektinstansen.
    $email = $self->email unless ($email);

    return 1 if ($email && $email =~ m/^[^@]+@([-\w]+\.)+[A-Za-z]{2,4}$/);
    return 0;
}

######################################################################
# exists - check if user exists
#
# Check existance of user by checking for file in data
# directory. There should be a file there with the same name as the
# user.
#
sub exists {
    my ($self, $user) = @_;

    return (-f "$DATADIR/$user");
}

######################################################################
# find - find user based on email address
#
sub find {
    my ($self, $email) = @_;

    return undef unless ($self->valid_email( $email ));

    # Get all regular files under the data directory.
    my @files;
    if (opendir( DIR, $DATADIR )) {
	@files = grep { -f "$DATADIR$_" } readdir( DIR );
	closedir DIR;
    }

    # Search each file until the correct mail is found
    my $user;
    foreach my $file (@files) {
	if (open USERINFO, "$DATADIR$file") {
	    my $donk = grep {m/^\@email:$email$/i} <USERINFO>;
	    close USERINFO;
	    if ($donk) {
		$user = $file;
		last;
	    }
	}
    }
    return $user;
}

######################################################################
# Authentication methods
#

# Create session key, consisting of user name and current time, and
# encrypt it.
sub create_key {
    my $self = shift;

    return undef unless ($self->user);

    my $key = { user => $self->user,
		ip => $ENV{REMOTE_ADDR},
		time => time };

    return $self->encrypt( freeze( $key ) );
}

# Validate session key. If key is valid, load user info and return
# user name (login id). If invalid, return undef.
sub validate_key {
    my ($self, $key) = @_;

    my %key = %{ thaw( $self->decrypt( $key ) ) };

    if ($key{user} && $key{time} && time - $key{time} < $self->{auth_time}) {
	$self->load( $key{user} );
	return $self->user;
    }
    return undef;
}

# Verify password.
sub verify_password {
    my ($self, $password) = @_;

    return 0 unless ($self->user && $password);

    my $encrypted_password = $self->password;

    return (crypt( $password, $encrypted_password ) eq $encrypted_password);
}

# Encrypt with RC4 and encode in Base64. Thanks to Markus H�rnvi.
sub encrypt {
    my ($self, $data) = @_;

    return encode_base64( RC4( $self->{auth_key}, $data ), '' );
}

# Decode Base64 and decrypt with RC4. Thanks to Markus H�rnvi.
sub decrypt {
    my ($self, $data) = @_;

    return $data ?  RC4( $self->{auth_key}, decode_base64( $data ) ) : undef;
}


######################################################################
# Instance data access
#
sub user {
    my $self = shift;
    $self->{userdata}{user} = shift if (@_);
    return $self->{userdata}{user};
}
sub name {
    my $self = shift;
    $self->{userdata}{name} = shift if (@_);
    return $self->{userdata}{name};
}
sub email {
    my $self = shift;
    $self->{userdata}{email} = shift if (@_);
    return $self->{userdata}{email};
}
sub signature {
    my $self = shift;
    $self->{userdata}{signature} = join( '\n', @_ ) if (@_);
    if (wantarray) {
	return split '\n', $self->{userdata}{signature};
    } else {
	my $sig = $self->{userdata}{signature};
	$sig =~ s/\\n/\r\n/g if ($sig);
	return $sig;
    }
}
sub newsrc {
    my $self = shift;
#    $self->{newsrc} = shift if (@_);
    return $self->{newsrc};
}
sub sort {
    my $self = shift;
    $self->{userdata}{sort} = shift if (@_);
    return $self->{userdata}{sort};
}
sub show {
    my $self = shift;
    $self->{userdata}{show} = shift if (@_);
    return $self->{userdata}{show};
}
sub password {
    my $self = shift;
    if (@_) {
	my $salt = join '', ('.', '/', 0..9, 'A'..'Z', 'a'..'z')[rand 64, rand 64];
	$self->{userdata}{password} = crypt( shift, $salt );
    }
    return ($self->{userdata}{password} || '*');
}
sub timestamp {
    my $self = shift;
    $self->{userdata}{timestamp} = shift if (@_);
    return ($self->{userdata}{timestamp} || time);
}
sub set_timestamp {
    my $self = shift;
    $self->timestamp( time );
}



1;

