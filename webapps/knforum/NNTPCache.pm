# $Id$
#
# NNTPCache - cachande API mot NNTP
#
# Denna modul �r i stort sett en sammanslagning av News::NNTPClient
# och IPC::Cache. Kontakten med newsservern sker med News::NNTPClient,
# och alla svar cachas med hj�lp av IPC::Cache. Det �r dock bara
# tr�dade listor och liknande som cachas, inte artiklar. De tr�dade
# listorna tar b�de tid och minne att bygga, plus att de ofta
# efterfr�gas, varf�r man torde tj�na mycket p� att cacha dem.
#
# $Log$
# Revision 1.9  2002/04/02 19:30:19  stefan
# Changed 'newsgroups' attribute to an array reference. When receiving
# the list of active groups with the 'list' method, each record in the
# 'newgroups' array is considered a separate wildmat.
#
# Revision 1.8  2002/03/11 22:22:46  stefan
# Added control of IPC memory segments. The 'new' method should always
# be called with the IPC key as the first parameter.
#
# Revision 1.7  2002/02/28 14:33:26  stefan
# Enabled caching. Problems are hopefully fixed. Cache is purged in
# class destructor if it reaches a maximum size. This will ensure that
# cache will not grow out of control.
#
# Added debugging with "logmsg" in the new Donk module. Thanks to Anders
# Toll for the module name.
#
# Revision 1.6  2002/02/28 07:51:28  stefan
# Disabled caching; there's a problem with that one. Issue NNTP "mode
# reader" on connect.
#
# Revision 1.5  2002/02/26 22:04:58  stefan
# Improved error handling. New data access methods. Debug
# levels. News::NNTPCache sucks.
#
# Revision 1.4  2002/02/26 16:57:01  stefan
# Due to problems with IPC::Cache, I changed the IPC shared memory key
# used and added some heavy debugging output. This will be removed.
#
# I also added some error checking in the article method.
#
# Revision 1.3  2002/02/14 00:17:25  stefan
# Nu kan KNForum ocks� posta newsinl�gg.
#
# Revision 1.2  2002/02/09 09:33:28  stefan
# Tr�dningen omgjord. Jag anv�nder nu en variant av Ulriks ursprungliga
# struktur. Dessutom markeras varje inl�gg med ursprung och
# uppf�ljare. Metod f�r att h�mta newsinl�gg. Och mycket, mycket annat.
#
# Revision 1.1  2002/02/05 22:55:43  stefan
# En f�rsta version som verkar fungera v�l.
#
#

package NNTPCache;
use strict;
use locale;
use POSIX qw(strftime);
use Date::Parse;
use MIME::Parser;
use MIME::WordDecoder;
use News::NNTPClient;
use Donk;

######################################################################
# Package configuration
#
my $MAX_CACHE_SIZE = 300000;

######################################################################
# new
#
# Class constructor. Set default instance values and initialize
# IPC::Cache.
#
sub new {
    my ($proto, $config)  = @_;
    my $class = ref($proto) || $proto;
    my $self = {};

    die "no configuration object" unless ($config);

    # Set default values
    $self->{nntpserver} = $config->attr( 'nntpserver' );
    $self->{nntpport} = $config->attr( 'nntpport' );
    $self->{newsgroups} = $config->attr( 'newsgroups' );

    # Initialize cache. Expire time is five minutes, which means that
    # the expensive operations (such as buildning threads for an
    # entire newsgroup) is run at most every five minutes.
    $self->{cache} =  new IPC::Cache( { namespace => 'KNForum',
					expires_in => $config->attr( 'cache_expire' ),
					cache_key => $config->attr( 'cache_key' ) } );

    # Prepare variable for Neews::NNTPClient. Don't initiate it until
    # necessary.
    $self->{nntpclient} = undef;

    # Bless and return
    bless( $self, $class );
    return $self;
}

######################################################################
# DESTROY
#
# Purge cache if maximum size is reached.
#
sub DESTROY {
    my $self = shift;

    my $c = $self->{cache};
    my $size = $c->_size;

    if ($size > $MAX_CACHE_SIZE) {
	$c->purge;
	my $newsize = $c->_size;
	if ($self->debug) {
	    logmsg( "Purge cache; size=$newsize (saved ", $size-$newsize,
		    ")" );
	}
    }
}

######################################################################
# list - h�mta lista med aktiva diskussionsgrupper
#
# Metoden list �r i stort sett en cachande front-end till
# News::NNTPClient->list, men tar inga parametrar eftersom vi alltid
# f�rv�ntas fr�ga efter samma newsgroups. Detta g�r ocks� cachningen
# smidigare.
#
# Returnerar en referens till array-of-hashes, l�mplig f�r
# HTML::Template.
#
sub list {
    my $self = shift;

    # Pr�va f�rst cache
    my $groups = $self->cache( 'active' );

    # Om inte datat fanns i cachen, h�mta fr�n NNTP-servern
    unless ($groups) {

	# H�mta (och eventuellt skapa) serverobjektet
	my $server = $self->nntpclient;

	# H�mta lista med nyhetsgrupper
	foreach my $wildmat (@{$self->{newsgroups}}) {
	    push @$groups, $server->list( 'active', $wildmat );
	}

	if ($server->ok) {

	    # Bearbetning av resultatet: Varje rad ers�tts med en
	    # hashref med samma data. Detta f�r att HTML::Template
	    # vill ha det s�.
	    my @fields = qw(group last first p);
	    foreach my $i (0..$#$groups) {
		my %data = ();
		@data{@fields} = split /\s+/, $$groups[$i];
		$data{articles} = $data{last} - $data{first} + 1;
		$$groups[$i] = \%data;
	    }

	    # Lagra datat i cacheminnet
	    $self->cache( 'active', $groups );
	}
    }

    return $groups;
}

######################################################################
# newsgroups_array
#
# Returnera en array (eller arrayref) med alla tillg�ngliga
# newsgroups.
#
sub newsgroups_array {
    my $self = shift;

    # Pr�va f�rst cache
    my $newsgroups = $self->cache( 'newsgroups' );

    # Om inte datat finns i cachen, f�r vi rekonstruera det
    unless ($newsgroups) {

	# H�mta lista
	my $list = $self->list;

	# Det finns s�kert n�gon snyggare metod f�r detta
	foreach my $ref (@$newsgroups) {
	    push @$newsgroups, $$ref{group};
	}

	# Spara i cache
	$self->cache( 'newsgroups', $newsgroups );
    }

    return wantarray ? @$newsgroups : $newsgroups;
}

######################################################################
# listgroup
#
# Returnerar en lista �ver inl�gg i angiven newsgroup. Svaret h�mtas
# fr�n IPC::Cache eller fr�n NNTP-servern. Om det h�mtas fr�n
# NNTP-servern, tr�das inl�ggen snyggt.
#
# Returnerar en referens till en array-of-hashes, l�mplig att trycka
# rakt in i HTML::Template.
#
sub listgroup {
    my ($self, $group) = @_;

    # F�rst definierar vi tv� hash-tabeller f�r att dels finna
    # artikelnummer fr�n msgid, dels ett index p� artikelnummer. I
    # array-context returneras, f�rutom referensen till listan, ocks�
    # en referens till offset-hashen. Det kan d� senare anv�ndas f�r
    # att h�mta information om en specifik artikel, eftersom det
    # dessutom cachas.
    my (%article, %offset);

    # V�gra g�ra n�gonting �ver huvud taget om vi inte f�r en riktig
    # newsgroup.
    return undef unless ($group);

    # Pr�va f�rst cache. Det sparar mycket tid.
    my $list = $self->cache( "listgroup_$group" );

    # Om listan fanns i cachen, h�mtar vi ocks� indexet d�rifr�n.
    if ($list) {
	%offset = %{$self->cache( "index_$group" )};
    }

    # Finns inget i cachen, f�r vi h�mta informationen fr�n
    # newsservern i st�llet.
    else {

	# H�mta (och skapa, om s� beh�vs) serverobjektet
	my $server = $self->nntpclient;

	# V�lj newsgroup och h�mta pekare
	my ($first,$last) = $server->group( $group );

	# Felhantering
	if ($server->ok) {

	    # H�mta �versikt; detta �r den i s�rklass effektivaste
	    # metoden, om den st�ds av NNTP-servern. Om den inte
	    # st�ds, tja, d� �r vi r�kta.
	    @$list = $server->xover( $first, $last );

	    # Mera felhantering
	    if ($server->ok) {

		# Bearbeta data; g�r om listan av str�ngar till en
		# lista med hashreferenser.
		my @fields = qw(article subject from date msgid refr char line xref);
		foreach my $i (0.. $#$list) {
		    my %data = ();
		    @data{@fields} = split /\t/, $$list[$i];
		    $data{subject} = unmime( $data{subject} );
		    $data{from} = unmime( $data{from} );
		    $$list[$i] = \%data;

		    # Bygg ocks� en hash p� msgid -> artikelnummer
		    # samt en hash p� artikel -> position i listan med
		    # artiklar. Denna anv�nds bland annat n�r vi
		    # tr�dar alla kommentarer.
		    $article{$data{msgid}} = $data{article};
		    $offset{$data{article}} = $i;

		    # H�r b�r jag ocks� g�ra viss databearbetning, t
		    # ex snygga till datumformatet, splitta namn och
		    # email-adress, etc.
		    ($data{name}, $data{email}) =
		      $self->split_from( $data{from} );
		    ($data{date}, $data{unixtime}) = 
		      $self->format_date( $data{date} );

		    # Skapa tom lista f�r svar
		    $data{replies} = [];
		}

		# En lista som inneh�ller alla ursprungsinl�gg (alla
		# andra inl�gg �r kommentarer till n�got annat
		# inl�gg).
		my @root;

		foreach my $data (@$list) {

		    # H�mta listan p� referenser i newsinl�gget. Det
		    # inl�gg som den h�r artikeln �r en kommentar
		    # till, �r sist i listan, och f�rst i listan �r
		    # det ursprungliga inl�gget som startade hela
		    # kommentarstr�den.
		    my @refr = split /\s+/, $$data{refr};

		    # Endast sista posten i listan �r intressant.
		    my $reply_to = $refr[$#refr];

		    # Om det �r en k�nd artikel, l�ter vi de b�da
		    # artiklarna peka p� varandra...
		    if ($reply_to && $article{$reply_to}) {
			$$data{reply_to} = $article{$reply_to};
			push @{$$list[$offset{$article{$reply_to}}]{replies}},
			  { article => $$data{article} };
		    }

		    # ... annars betraktar vi det som ett
		    # ursprungsinl�gg.
		    else {
			push @root, { article => $$data{article} };
		    }
		}

		# Bl�ddra igenom tr�det och markera varje inl�gg med
		# ett ordningstal och niv� i kommentarstr�det.
  		$self->build_thread( $list, \@root, \%offset );

		# D� �r vi klara med listan, och stoppar in den i
		# cachen f�r �teranv�ndning en annan dag.
		$self->cache( "listgroup_$group", $list );
		$self->cache( "index_$group", \%offset );
	    }
	}
    }

    return wantarray ? ($list, \%offset) :  $list;
}

######################################################################
# article
#
# H�mta artikel fr�n grupp+artikelnummer. Detta cachas INTE, eftersom
# det kan handla om mycket data.
#
# FIXME: H�r beh�vs b�ttre felhantering.
#
sub article {
    my ($self, $group, $article) = @_;

    # Utan parametrar g�r vi ingenting
    return undef unless ($group && $article);

    # F�rst h�mtar vi information om den s�ka artikeln fr�n
    # listgroup. Vi beh�ver till exempel informationen om kommentarer
    # f�r att kunna skapa l�nkar f�r att f�lja kommentarstr�den.
    my ($list, $index) = $self->listgroup( $group );

    # Skapa kontakt med newsservern
    my $server = $self->nntpclient;

    # V�lj newsgroup, s�tt pekare
    my ($first, $last) = $server->group( $group );
    my ($next, $prev);

    # Felhantering
    if ($server->ok) {
	$next = ($article < $last) ? $article + 1 : $last;
	$prev = ($article > $first) ? $article - 1 : $first;
    } else {
	$next = $prev = $first = $last = $article;
    }

    # Eftersom en del newsinl�gg �r i MIME-format, anv�nder vi
    # MIME::Parser f�r att bearbeta newsinl�ggen i st�llet f�r
    # News::Article.
    my $parser = new MIME::Parser;

    # Jag vill inte att MIME::Parser skriver till disk, utan vi h�ller
    # allt i minnet. Dessutom skall inte MIME::Parser vara rekursiv,
    # eftersom vi bara �r intresserade av den f�rsta niv�n. Alla
    # eventuella attachments struntar vi blankt i.
    $parser->output_to_core(1);
    $parser->tmp_to_core(1);
    $parser->extract_nested_messages(0);
    $parser->decode_headers(1);

    # H�mta artikel fr�n NNTP-servern
    my $article_ref = $server->article( $article )
      if ($server->ok);

    # Avkoda MIME-blobban. Det g�r bra �ven om vi inte hittar n�gon
    # artikel.
    my $message = $parser->parse_data( $article_ref );

    # H�mta MIME-huvud
    my $head = $message->head;

    # Plocka ut den intressanta texten
    my $text;
    my $mime_type = $message->mime_type;
    if ($mime_type eq 'text/plain') {
	$text = $message->bodyhandle->as_string;
    } elsif ($mime_type =~ m/^multipart\//) {
	foreach my $part ($message->parts) {
	    if ($part->mime_type eq 'text/plain') {
		$text .= $part->bodyhandle->as_string . "\n\n";
	    } else {
		$text .= "   [" . $part->mime_type 
		  . "]\n";
	    }
	}
    } else {
	$text = '(Inl�gget kan ej visas)';
    }


    # B�rja bygg upp ett svar.
    my $a = { group => $group,
	      article => $article,
	      next => $next,
	      prev => $prev,
	      last => $last,
	      first => $first };

    # Om vi har lyckats h�mta artikeln fr�n newsservern, l�gger vi
    # till den informationen.
    if ($server->ok) {
	$$a{from}                 = $head->get( 'From' );
	($$a{name}, $$a{email})    = $self->split_from( $head->get( 'From' ) );
	($$a{date}, $$a{unixtime}) = $self->format_date( $head->get( 'Date' ) );
	$$a{subject}              = $head->get( 'Subject' );
	chomp( $$a{msgid}         = $head->get( 'Message-Id' ) );
	$$a{newsgroups}           = $head->get( 'Newsgroups' ); # split!
	$$a{references}           = $head->get( 'References' );
	$$a{text}                 = $text;
	if ($list) {
	    $$a{reply_to}             = $$list[$$index{$article}]{reply_to};
	    $$a{replies}              = $$list[$$index{$article}]{replies};
	}
    }

    # I annat fall s�tter vi ett felmeddelande
    else {
	$$a{error} = $self->errstr;
    }

    # S�. Tuta och k�r.
    return $a;
}



######################################################################
# build_thread
#
# G� igenom tr�dhashen rekursivt och markera varje inl�gg med dess
# niv� i kommentarstr�det och dess position i ordningen, om man v�ljer
# att sortera p� kommentarstr�d. Positionen anv�nds f�r att sortera i
# r�tt ordning, och level kan, om man vill, anv�ndas f�r att p� olika
# s�tt markera en indentering i listan.
#
sub build_thread {
    my ($self, $list_ref, $reply_ref, $map_ref, $level, $position) = @_;

    $level = 0 unless ($level);
    $position = 0 unless ($position);

    foreach my $reply_ref (@$reply_ref) {
	my $reply = $$reply_ref{article};
#	print STDERR "looking for article $reply at $$map_ref{$reply}; position = $position; level = $level\n";

	$$list_ref[ $$map_ref{ $reply } ]{level} = $level;
	$$list_ref[ $$map_ref{ $reply } ]{position} = $position;
	if ($$list_ref[$$map_ref{$reply}]{replies}) {
	    $position = $self->build_thread( $list_ref, 
					     $$list_ref[$$map_ref{$reply}]{replies},
					     $map_ref, $level+1, $position+1 );
	} else {
	    $position++;
	}
    }

    return $position;
}

######################################################################
# sort_articles
#
# Sortera listan med artiklar (listgroup).
#
sub sort_articles {
    my ($self, $list_ref, $sort_order) = @_;
    my @sorted;

    $sort_order = 'none' unless ($sort_order);

    if ($list_ref) {
	if ($sort_order eq 'from') {
	    @sorted = sort {$$a{name} cmp $$b{name}} @$list_ref;
	} elsif ($sort_order eq 'thread') {
	    @sorted = sort {$$a{position} <=> $$b{position}} @$list_ref;
	} elsif ($sort_order eq 'date') {
	    @sorted = sort {$$a{unixtime} <=> $$b{unixtime}} @$list_ref;
	} elsif ($sort_order eq 'reversedate') {
	    @sorted = sort {$$b{unixtime} <=> $$a{unixtime}} @$list_ref;
	} elsif ($sort_order eq 'subject') {
	    @sorted = sort {$$a{subject} cmp $$b{subject}} @$list_ref;
	} else {
	    @sorted = @$list_ref;
	}
    }

    return \@sorted;
}

######################################################################
# cache - h�mta eller lagra v�rden i cache
#
sub cache {
    my ($self, $key, $value) = @_;

    if ($value) {
	$self->{cache}->set( $key, $value );
    } else {
	$value = $self->{cache}->get( $key );
    }
    return $value;
}

######################################################################
# flush_cache - t�m cache
#
sub flush_cache {
    my $self = shift;

    logmsg( "Flushing cache" ) if ($self->debug);
    $self->{cache}->clear();
}

######################################################################
# nntpclient - initiera (om n�dv�ndigt) och returnera serverobjekt
#
sub nntpclient {
    my $self = shift;

    unless ($self->{nntpclient}) {
	$self->{nntpclient} = new News::NNTPClient( $self->{nntpserver},
						    $self->{nntpport} );
	$self->{nntpclient}->mode_reader;
    }

    return $self->{nntpclient};
}

######################################################################
# ok
#
# Returnera sant eller falskt beroende p� hur senaste NNTP-operationen
# gick. Om NNTP-objektet inte �r initierat, �r det ett rimligt
# antagande att n�gon NNTP-f�rfr�gan aldrig skett, utan vi kan med
# gott samvete returnera ett sant v�rde.
#
sub ok {
    my $self = shift;
    return ($self->{nntpclient}) ? $self->{nntpclient}->ok : 1;
}

######################################################################
# errstr - returnera felmeddelande
#
sub errstr {
    my $self = shift;
    my $msg = '';

    unless ($self->ok) {
	my $server = $self->nntpclient;
	$msg = "(".$server->code.") ".$server->message;
    }

    return $msg;
}
######################################################################
# split_from
#
# Dela upp avs�ndarf�ltet i namn och email-adress.
#
sub split_from {
    my ($self, $from) = @_;
    my ($name, $email);

    # Ok, jag erk�nner, den h�r programsnutten �r riktig spaghettikod,
    # men jag orkar inte optimera den nu.

    if ($from =~ m/([\w._+-]+@[\w._+-]+)/) {
	$email = $1;
	$name = $from;
	$name =~ s/$email//;
	$name =~ s/["<>()]//g;
	$name =~ s/^\s+//;
	$name =~ s/\s+$//;
	unless ($name) {
	    $email =~ m/([\w._+-]+)@/;
	    $name = $1;
	}
    } else {
	$name = $email = $from;
    }

    return ($name, $email);
}

######################################################################
# format_date
#
# G�r om datumformatet till ett enhetligt format.
#
sub format_date {
    my ($self, $date_string) = @_;

    my $time = str2time( $date_string );
    my $date = strftime( '%Y-%m-%d %H:%M', localtime( $time ) );

    return wantarray ? ($date, $time) : $date;
}

######################################################################
# Data access methods
#
sub debug {
    my $self = shift;
    $self->{debug} = shift if (@_);
    return $self->{debug};
}
sub nntpserver {
    my $self = shift;
    $self->{nntpserver} = shift if (@_);
    return $self->{nntpserver};
}
sub nntpport {
    my $self = shift;
    $self->{nntpport} = shift if (@_);
    return $self->{nntpport};
}
sub newsgroups {
    my $self = shift;
    $self->{newsgroups} = shift if (@_);
    return $self->{newsgroups};
}

1;
