-- $Id$ 

-- Skapa tabeller f�r nya tr�ningskalendern. Vi anv�nder oss av
-- PostgreSQL.

-- $Log$

--
-- Radera existerande tabeller
--
DROP TABLE tkal_aktivitet;
DROP SEQUENCE tkal_aktivitet_id_seq;
DROP TABLE tkal_person;
DROP SEQUENCE tkal_person_id_seq;
DROP TABLE tkal_data;
DROP SEQUENCE tkal_data_id_seq;
DROP TABLE tkal_vikt;
DROP SEQUENCE tkal_vikt_id_seq;

--
-- Skapa nya tabeller
--

CREATE TABLE tkal_aktivitet (
  id serial,
  aktivitet varchar(30),
  enhet varchar(5),
  faktor real DEFAULT 1.00,
  PRIMARY KEY  (id)
);

CREATE INDEX tkal_aktivitet_aktivitet_idx ON tkal_aktivitet (aktivitet);

CREATE TABLE tkal_person (
  id serial,
  epost varchar(50),
  namn varchar(50),
  pass varchar(20) NOT NULL DEFAULT '*',
  osynlig boolean DEFAULT false,
  PRIMARY KEY  (id),
  UNIQUE (epost)
);

CREATE INDEX tkal_person_epost_idx ON tkal_person (epost);

CREATE TABLE tkal_data (
  id serial,
  person integer REFERENCES tkal_person ON DELETE CASCADE ON UPDATE CASCADE,
  datum date,
  aktivitet integer REFERENCES tkal_aktivitet ON UPDATE CASCADE,
  tid integer,
  distans real,
  kommentar text,
  PRIMARY KEY  (id)
);

CREATE INDEX tkal_data_person_idx ON tkal_data (person);
CREATE INDEX tkal_data_datum_idx ON tkal_data (datum);

CREATE TABLE tkal_vikt (
  id serial,
  person integer REFERENCES tkal_person ON DELETE CASCADE ON UPDATE CASCADE,
  datum date,
  tid time,
  vikt real,
  kommentar text,
  PRIMARY KEY  (id)
);

CREATE INDEX tkal_vikt_datum_idx ON tkal_vikt (datum);