package MusicLibrary;

use base 'CGI::Application';
use strict;
use SSSoft::DBI::Application;
use SSSoft::CGI::Logger qw(:all);

#
# Setup CGI::Application
#

sub setup {
    my $self = shift;

    # Run modes
    $self->run_modes( 'list' => \&list,
  		      'edit' => \&edit,
  		      'search' => \&search );

    # Default run mode
    $self->start_mode( 'list' );

    # Initiate database connection
    $self->db( new SSSoft::DBI::Application( 'DBI:mysql:MusicLibrary:localhost',
					     'stefan',
					     'slempropp' ) );

    # Define database relations
    $self->db->define_lookup( 'Media.Artist' => 'Artist:ID:Name' );
    $self->db->define_lookup( 'Media.Label' => 'Label:ID:Name' );
    $self->db->define_lookup( 'Media.Genre' => 'Genre:ID:Genre' );

    # Drop out if we're not connected
    die 'no database connection' unless ($self->db->connected);

}

sub search {
    my $self = shift;

    my $q = $self->query;

    my $t = $self->load_tmpl( 'searchform.html',
			      die_on_bad_params => 0 );

    if ($q->param( 'search' )) {
	my $title = $q->param( 'title' ) ? '%'.$q->param( 'title' ).'%' : '%';
	my $artist = $q->param( 'artist' ) ? '%'.$q->param( 'artist' ).'%' : '%';
	my $result = $self->db->selectall( q{SELECT Media.ID, Artist.Name AS Artist, Media.Title FROM Media,Artist WHERE Media.Artist=Artist.ID AND Media.Title LIKE ? AND Artist.Name LIKE ?}, $title, $artist );
	$t->param( result => $result );
    }


    return $t->output;
}

sub list {
    my $self = shift;

    my $t = $self->load_tmpl( 'list.html',
			      die_on_bad_params => 0 );

    my $table = $self->db->select_table( 'SELECT Artist.Name AS Artist, Media.Title, Media.MediaID, Label.Name AS Label, Media.Year, Genre.Genre FROM Media,Artist,Label,Genre WHERE Media.Artist=Artist.ID AND Media.Label=Label.ID AND Media.Genre=Genre.ID ORDER BY Media.Genre, Artist.SortName, Media.Year' );

    $t->param( 'list' => $table );

    return $t->output;
}

sub edit {
    my $self = shift;

    my $q = $self->query;

    if ($q->param( 'save' )) {
	my @params = grep { !m/^(rm|save)$/ } $q->param;
	my @values = map $q->param( $_ ), @params;

	logmsg "params: @params values: @values";

	my $update = join( ', ', map "$_=?", @params );

	logmsg "update: $update";

    }


    my $t = $self->load_tmpl( 'edit.html',
			      die_on_bad_params => 0 );

    my $query = $q->param( 'query' );

    if ($query) {
	$t->param( 'edit' => $self->db->edit( 'Media', $query ) );
    } else {
	$t->param( 'message' => 'No search query' );
    }

    return $t->output;
}


#
# Instance data
#
sub db {
    my $self = shift;
    $self->{db} = shift if (@_);
    return $self->{db};
}

1;

