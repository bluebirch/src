# Perl package for reading and (perhaps) manipulating PMTxl CSV files.
#
# The following data structure represents the PMTxl data:
#
# $self->{acct} = [ accounthash1, accounthash2, ... ]

# accounthash = { name => accountname,
#                 type => 'Asset|Liability|Income|Expense'
#                 values => [ month_hash, month_hash, ... ]
#                 

# $Log$
# Revision 1.5  2002/08/11 18:01:39  stefan
# Added balance view.
#
# Revision 1.4  2002/08/03 07:23:25  stefan
# Cleaned up code and finished calculation algorithms. There could still
# be some bugs, I guess.
#

package PMTxl;

use strict;
use locale;
use Text::CSV_XS;
use FileHandle;
use CGI::Logger qw(:all);

######################################################################
# new
#
# Class constructor.
#
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = { options => { path => '',
			      dbnum => 0,
			      debug => 0 },
		 acct => [],
		 acct_title => '',
		 acct_index => {},
		 csv => new Text::CSV_XS( { 'sep_char' => ';',
					    'binary' => 1 } ),
		 totals => {}
	       };

    bless( $self, $class );

    $self->options( @_ ) if (@_);

    if (defined $self->dbnum) {
  	$self->read_acct && $self->read_db && $self->summary && $self->format_numbers;
    }

    return $self;
}

######################################################################
# options
#
# Parse options supplied to 'new' method.
#
sub options {
    my $self = shift;

    # Load in options supplied to method
    for (my $x = 0; $x <= $#_; $x += 2) {
	die "Odd number of options" unless (defined($_[($x + 1)]));
	$self->{options}{lc($_[$x])} = $_[($x + 1)];
    }
}

######################################################################
# read_acct
#
# Read PMTxl account info file.
#
sub read_acct {
    my $self = shift;

    my $filename = "$self->{options}{path}PMTxlDB$self->{options}{dbnum}.act.csv";
    my $io = new FileHandle;
    my $csv = $self->csv;

    unless ($io->open( $filename )) {
	logmsg "Could not open $filename";
	return 0;
    }

    logmsg "reading $filename" if ($self->debug);

    # Read database information
    my @dbinfo = @{$csv->getline( $io )};
    $self->{acct_title} = $dbinfo[0];

    # Read column names
    my @head = map( lc($_), @{$csv->getline( $io )} );
    # Cut out budget headers (months names, column 5-16).
    my @months = map { name => $_ }, splice @head, 5, 12;
    $self->{months} = \@months;

    # Read all accounts as an array of hashes
    my $cols;
    while ($cols = $csv->getline( $io )) {
	last if (scalar @$cols != 22);

	# Hash for storing account info
  	my %acct = ();

	# Get yearly budget; store this in a temporary variable for
	# later use in make_summary. They are not used now, since we
	# don't know which months we are interested in yet.
	@{$acct{budget_array}} = $self->numeric( splice @$cols, 5, 12 );

	# Store entire CSV array into hash
  	@acct{@head} = @$cols;

  	if ($acct{isequity}) {
  	    $acct{type} = 'Equity';
  	    logmsg "account $acct{name} changed type to '$acct{type}'";
  	}

	# push, push, push onto array of hashes
  	push @{$self->{acct}}, \%acct;

	# Update index
	$self->{acct_index}{$acct{name}} = $#{$self->{acct}};
    }

    $io->close;

    # Link subcategories
    foreach my $ref (@{$self->{acct}}) {
	if ($$ref{subcatof}) {
	    push @{$self->{acct}[$self->{acct_index}{$$ref{subcatof}}]{subcat}},
	      $$ref{name};
	}
    }

    return 1;
}

######################################################################
# read_db
#
# Read PMTxl data file.
#
sub read_db {
    my $self = shift;

    # Set filename and create CSV object
    my $filename = "$self->{options}{path}PMTxlDB$self->{options}{dbnum}.csv";
    my $io = new FileHandle;
    my $csv = $self->csv;

    # Open data file
    unless ($io->open( $filename )) {
	logmsg "Could not open $filename";
	return 0;
    }

    # Read column names
    my @head = @{$csv->getline( $io )};

    # Set lastmonth to -1 if it is not already set. It is updated when
    # the data is read and increased to the last month that includes
    # data. This assumes fiscal year = calendar year.
    $self->{options}{lastmonth} = -1 unless ($self->{options}{lastmonth});

    # Read data; this data is like one huge table, with every account
    # as a column.
    my $cols;
    while ($cols = $csv->getline( $io )) {

	# Less than 13 columns means EOF
	last if (scalar @$cols < 13);

	# Get date (this format may vary, I think).
	$$cols[0] =~ m/(\d\d\d\d)\.(\d+)\.(\d+)/;
	my ($y, $m, $d) = ($1, $2, $3);

	# Update lastmonth if necessary
	if ($m > $self->{options}{lastmonth}+1) {
	    $self->{options}{lastmonth} = $m-1;
	}

	# Step through the row of data and update all accounts
	# (columns) for which there are values stored.
	for my $i (14..$#$cols) {
	    if ($$cols[$i] && $$cols[$i] =~ m/^[0-9,.-]+$/) {

		# Make sure value is numeric
		$$cols[$i] =~ s/,/./;

		# Add value to the summary of the current month. Make
		# liabilities positive.
		if ($self->{acct}[$i-14]{type} eq 'Liability') {
		    $self->{acct}[$i-14]{values}[$m-1]{actual} -= $$cols[$i];
		} else {
		    $self->{acct}[$i-14]{values}[$m-1]{actual} += $$cols[$i];
		}

#  		# Add the entire transaction
#  		push @{$self->{acct}[$i-14]{trans}}, "$$cols[0] $$cols[$i]";
	    }
	}
    }

    $io->close;

    return 1;
}

######################################################################
# summary
#
# Calculate summaries, diffs, totals, etc.
#
sub summary {
    my $self = shift;

    # Step through array of hashes.
    foreach my $acct (@{$self->{acct}}) {

	# Set flag for reverse negative values in summaries
	my $reverse_summary = ($$acct{type} eq 'Income');

	# Calculate values for each month we are interested in
	for my $i (0..$self->{options}{lastmonth}) {

	    # Add name of month. Sometimes needed.
	    $$acct{values}[$i]{month} = ucfirst( $self->{months}[$i]{name} );

	    # Assign the 'values' array-of-hashes with the temporary
	    # budget array. This can be done now, since we know which
	    # months we are interested in. Budget values for months
	    # outside this scope is simply ignored.
	    $$acct{values}[$i]{budget} = $$acct{budget_array}[$i] || 0;

	    # Make sure 'actual' is defined.
	    $$acct{values}[$i]{actual} = 0
	      unless ($$acct{values}[$i]{actual});

	    # Calculate difference between actual and budget
	    $$acct{values}[$i]{diff} = $reverse_summary ?
	      $$acct{values}[$i]{actual} - $$acct{values}[$i]{budget} :
		$$acct{values}[$i]{budget} - $$acct{values}[$i]{actual};

	    # Set flag if diff is negative; this is for the layout
	    # with HTML::Template. Negative values can, for example,
	    # have different colors.
	    $$acct{values}[$i]{negdiff} = 'y'
	      if ($$acct{values}[$i]{diff} < 0);

	    # Calculate accumulated values for 'actual', 'budget' and
	    # 'diff'.
	    foreach my $key (qw(actual budget diff)) {

		# If this is the first value, no earlier values are
		# available. Set to current value.
		if ($i == 0) {
		    $$acct{values}[$i]{"acc_$key"} = $$acct{values}[$i]{$key};
		}
		else {
		    $$acct{values}[$i]{"acc_$key"} =
		      $$acct{values}[$i]{$key} + $$acct{values}[$i-1]{"acc_$key"};
		}
	    }

	    # Add values to grand totals for each month and category
	    $self->{totals}{$$acct{type}}[$i]{budget}
	      += $$acct{values}[$i]{budget};
	    $self->{totals}{$$acct{type}}[$i]{actual}
	      += $$acct{values}[$i]{actual};
	}

	# Remove the temporary budget array; this saves some memory
	# and results in cleaner programming.
	delete $$acct{budget_array};

	# Calculate budget totals
	$$acct{budget} = $self->sum_aoh( $$acct{values}, 'budget' );

	# Calculate actual totals
	$$acct{actual} = $self->sum_aoh( $$acct{values}, 'actual' );

	# Reverse actual if this is a liability
#	$$acct{actual} = -$$acct{actual} if ($$acct{type} eq 'Liability');

	# Calculate diff totals
	$$acct{diff} = $self->sum_aoh( $$acct{values}, 'diff' );

	# Set flag if diff is negative
	$$acct{negdiff} = 'y' if ($$acct{diff} < 0);
    }

    # Calculate balance between income and expense for each month
    for my $i (0..$self->{options}{lastmonth}) {
	foreach my $key qw(actual budget) {
	    # Calculate difference
	    $self->{totals}{Balance}[$i]{$key} = $self->{totals}{Income}[$i]{$key} - $self->{totals}{Expense}[$i]{$key};

	    # Mark negative if less than zero
	    $self->{totals}{Balance}[$i]{"negative_$key"} = 'y'
	      if ($self->{totals}{Balance}[$i]{$key} < 0);
	}
    }

    # Calculate grand year totals for each category
    while( my ($type, $months) = each %{$self->{totals}}) {

	# Store grand year totals in this hash
	my $grand = {};

	# If this is an income, budget numbers should be subtracted
	# from actual.
	my $reverse_summary = ($type eq 'Income');

	# Step through months. Each month has grand totals that was
	# calculated above.
	foreach my $month (@$months) {

	    # Calculate difference between actual and budget
	    $$month{diff} = $reverse_summary ?
	      $$month{actual} - $$month{budget} :
		$$month{budget} - $$month{actual};

	    # Mark negative difference
	    $$month{negdiff} = 'y' if ($$month{diff} < 0);

	    # Accumulate grand year totals
	    $$grand{actual} += $$month{actual};
	    $$grand{budget} += $$month{budget};
	    $$grand{diff} += $$month{diff};
	}

	# Mark negative values in year grand totals
	foreach my $key qw(actual budget) {
	    $$grand{"negative_$key"} = 'y' if ($$grand{$key} < 0);
	}

	# Store grand totals
	$self->{grand_totals}{$type} = $grand;
    }

    return 1;
}

######################################################################
# format_numbers
#
# Format numbers for output
sub format_numbers {
    my $self = shift;

    $self->formatizer( $self->{acct} );
    $self->formatizer( $self->{totals} );
    $self->formatizer( $self->{grand_totals} );

    return 1;
}

######################################################################
# list
#
# Return an array of hashes containing all accounts of specified
# type. Don't include accounts with a value of 0.
#
sub list {
    my ($self, $type) = @_;

    my $list = [];

    $type = ucfirst( $type );

    foreach my $acctref (@{$self->{acct}}) {
	if ($$acctref{type} eq $type && ($$acctref{actual} != 0 || $$acctref{budget} != 0)) {
	    push @$list, $acctref;
	}
    }
    return $list;
}

######################################################################
# totals
#
# Return a hash of month totals for specified type (expense, income,
# ...) and class (actual, budget, ...).
#
sub totals {
    my ($self, $type) = @_;

    $type = ucfirst( $type );

    logmsg "totals for $type ($$self{totals}{$type})" if ($self->debug);

    my $list = [];
    @$list = @{$self->{totals}{$type}};
    push @$list, $self->{grand_totals}{$type};

    return $list;
}

######################################################################
# assets_total, liabilities_total, equity
#
# Totals of all assets, liabilities, and equity, respectively.
#
sub assets_total {
    my $self = shift;

    return $self->{grand_totals}{Asset}{actual};
}

sub liabilities_total {
    my $self = shift;

    return $self->{grand_totals}{Liability}{actual};
}

sub equity {
    my $self = shift;

    my ($asset, $liability) = ($self->assets_total, $self->liabilities_total );

    return $asset - $liability;
}


######################################################################
# months
#
# Name of months
#
sub months {
    my $self = shift;

    my @months = @{$self->{months}};
    splice @months, $self->lastmonth;
    return \@months;
}

######################################################################
# sum
#
# Sum an array of values.
#
sub sum {
    my $self = shift;
    my $result = 0;
    $result += $_ foreach (@_);
    return $result;
}

######################################################################
# sum_aoh
#
# Sum a reference to an array of hashes on specified key.
#
sub sum_aoh {
    my ($self, $aryref, $key) = @_;
    my $result = 0;
    foreach my $ref (@$aryref) {
	$result += $$ref{$key} || 0;
    }
    return $result;
}

######################################################################
# numeric
#
# Make sure some values are numeric
#
sub numeric {
    my $self = shift;

    s/,/./g foreach (@_);

    return wantarray ? @_ : $_[0];
}

######################################################################
# formatizer
#
# Make nice value format
#
sub formatizer {
    my ($self, $value) = @_;

    if (ref($value) eq 'HASH') {
	foreach my $key (keys %$value) {
	    $$value{$key} = $self->formatizer( $$value{$key} );
	}
    } elsif (ref($value) eq 'ARRAY') {
	for my $i (0..$#$value) {
	    $$value[$i] = $self->formatizer( $$value[$i] );
	}
    } elsif ($value =~ m/^-?\d+(\.\d*)?$/) {
	$value = sprintf( '%0.2f', $value );
    }

    return $value;
}

######################################################################
# acct_printinfo
#
# Print account info on screen.
#
sub acct_printinfo {
    my $self = shift;
    my $acctref = shift;

    printf "Account: %-50.50s%s\n", ($$acctref{subcatof} ? "$$acctref{name} ($$acctref{subcatof})" : $$acctref{name}), $$acctref{type};

    for my $key (qw(budget actual)) {
	print " $key:";
	foreach my $val (@{$$acctref{values}}) {
	    print " $$val{$key}"
	}
	print " = $$acctref{$key}\n";
    }
}

######################################################################
# acct_dump
#
# Dump all accounts on screen using acct_printinfo above.
#
sub acct_dump {
    my $self = shift;

    foreach my $ref (@{$self->{acct}}) {
	print "-------------------- $$ref{name}, $$ref{type}\n";
  	$self->print_hashref( $ref );
#	$self->acct_printinfo( $ref );
    }

}

######################################################################
# dump_totals
#
# Dump all totals. For debugging purposes.
#
sub dump_totals {
    my $self = shift;

    my $arrayref = shift || $self->{totals};

    foreach my $ref (@$arrayref) {
	$self->print_hashref( $ref );
    }
}


######################################################################
# some data access methods
#
sub dbnum {
    my $self = shift;
    $self->{options}{dbnum} = shift if (@_);
    return $self->{options}{dbnum};
}

sub lastmonth {
    my $self = shift;
    $self->{options}{lastmonth} = shift()-1 if (@_);
    return $self->{options}{lastmonth}+1;
}

sub csv {
    my $self = shift;
    return $self->{csv};
}

sub path {
    my $self = shift;
    if (@_) {
	if ($self->{options}{path} = shift) {
	    $self->{options}{path} =~ s/(?<!\/)$/\//; # Add trailing slash '/'
	}
    }
    return defined $self->{options}{path} ? $self->{options}{path} : '';
}

sub debug {
    my $self = shift;
    $self->{options}{debug} = shift if (@_);
    return $self->{options}{debug};
}

######################################################################
# print_array
#
# For debugging purposes.
#
sub print_arrayref {
    my $self = shift;
    my $aryref = shift;
    my $level = shift || 0;
    my $tab = ' ' x (2 * $level);

    foreach (@$aryref) {
	if (ref($_) eq 'HASH') {
	    print $tab, "[\n";
	    $self->print_hashref( $_, $level + 1 );
	    print $tab, "]\n";
	}
	else {
	    printf "%s%s[%s]\n", $tab, $tab, (defined $_ ? $_ : '(undef)' );
	}
    }
}

######################################################################
# print_hashref
#
# For debugging purposes.
#
sub print_hashref {
    my $self = shift;
    my $hashref = shift;
    my $level = shift || 0;
    my $tab = ' ' x (2 * $level);
    print $tab, "$hashref = {\n";
    foreach my $key (sort keys %$hashref) {
	my $value = $$hashref{$key};
	if (ref($value) eq 'ARRAY') {
	    print "$tab  $key =\n";
	    $self->print_arrayref( $value, $level + 1 );
	}
	elsif (ref($value) eq 'HASH') {
	    print "$tab  $key =\n";
	    $self->print_hashref( $value, $level + 1 );
	}
	else {
	    print "$tab  $key = $value\n";
	}
    }
    print $tab, "}\n";
}

1;

