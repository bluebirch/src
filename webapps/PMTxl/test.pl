#!/usr/bin/perl -w

use strict;
use locale;
use PMTxl;

my $pmt = new PMTxl dbnum => 2, path => '/home/stefan/doc/privat/ekonomi/', lastmonth => 11;

#  $pmt->lastmonth(12);
#  $pmt->read_acct;
#  $pmt->read_db;
#  $pmt->summary;
#  $pmt->format_numbers;
#  $pmt->acct_dump;
#  $pmt->print_hashref( $pmt->{totals} );
#  $pmt->print_hashref( $pmt->{grand_totals} );
#  $pmt->dump_totals;
#  $pmt->dump_totals( $pmt->balance );

#my $asset = $pmt->list( 'asset' );

my $list = $pmt->totals( 'balance' );

$pmt->print_arrayref( $list );
