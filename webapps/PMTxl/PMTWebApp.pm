# $Id$
#
# PMTxl web application
#
# $Log$
# Revision 1.2  2002/08/11 18:01:39  stefan
# Added balance view.
#
# Revision 1.1  2002/08/03 07:25:28  stefan
# Converted the PMT application into a CGI::Application.
#
#
package PMTWebApp;
use base 'CGI::Application';
use strict;
use PMTxl;
use CGI::Logger qw(:all);

sub setup {
    my $self = shift;

    $self->run_modes( 'budget' => \&budget,
		      'actual' => \&actual,
		      'balance' => \&balance,
		      'joined' => \&joined );

    $self->start_mode( 'actual' );

    $self->{pmt} = new PMTxl( dbnum => 1,
			      path => '/home/stefan/doc/privat/ekonomi/PMTxl/',
			      lastmonth => 11 );
}

sub categories {
    my $self = shift;
    my $pmt = $self->{pmt};
    my $categories = [ { type => 'Inkomster',
			 list => $pmt->list( 'Income' ),
			 totals => $pmt->totals( 'Income' ),
			 months => $pmt->months },
		       { type => 'Utgifter',
			 list => $pmt->list( 'Expense' ),
			 totals => $pmt->totals( 'Expense' ),
			 months => $pmt->months },
		       { type => 'Balans',
			 list => [],
			 totals => $pmt->totals( 'Balance' ),
			 months => $pmt->months }
		     ];
    return $categories;
}

sub budget {
    my $self = shift;

    my $t = $self->load_tmpl( 'budget.tmpl',
			      die_on_bad_params => 0,
			      global_vars => 1 );

    $t->param( categories => $self->categories );

    return $t->output;
}

sub actual {
    my $self = shift;

    my $t = $self->load_tmpl( 'actual.tmpl',
			      die_on_bad_params => 0,
			      global_vars => 1 );

    $t->param( categories => $self->categories );

    return $t->output;
}

sub balance {
    my $self = shift;

    my $pmt = $self->{pmt};

    my $t = $self->load_tmpl( 'balance.tmpl',
			      die_on_bad_params => 0 );

    $t->param( 'assets' => $pmt->list( 'Asset' ) );
    $t->param( 'assets_total' => $pmt->assets_total );
    $t->param( 'liabilities' => $pmt->list( 'Liability' ) );
    $t->param( 'liabilities_total' => $pmt->liabilities_total );
    $t->param( 'equity' => $pmt->equity );

    return $t->output;
}

sub joined {
    my $self = shift;

    my $t = $self->load_tmpl( 'joined.tmpl',
			      die_on_bad_params => 0,
			      global_vars => 1 );

    $t->param( categories => $self->categories );

    return $t->output;
}

1;
