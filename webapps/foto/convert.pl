#!/usr/bin/perl -w

use strict;
use DBI;

my $infile = "$ENV{HOME}/doc/privat/foto/Filmregister.txt";

open IN, $infile or die;

# H�mta f�ltnamn

my $line = <IN>;
chomp $line;
$line =~ s/[\r"]//g;
my @fieldname = split /\t/, $line;

print STDERR "@fieldname\n";

my @data;

while (<IN>) {
    my %rec;
    chomp;
    s/\r//g;
    my @field = split /\t/;
    for my $i (0..scalar @field) {
	if ($field[$i]) {
	    $field[$i] =~ s/^"//;
	    $field[$i] =~ s/"$//;
	    $rec{$fieldname[$i]} = $field[$i];
	}
    }
    push @data, \%rec;
}
close IN;

my $dbh = DBI->connect( 'DBI:mysql:foto:localhost', 'stefan',
			'slempropp' );

foreach my $ref (@data) {
    print "Processing $$ref{ID}...\n";

    ($$ref{aar}, $$ref{filmtyp}, $$ref{serienummer}) = split /-/, $$ref{ID};

    print "  �r                 : $$ref{aar}\n";
    print "  filmtyp            : $$ref{filmtyp}\n";
    print "  serienummer        : $$ref{serienummer}\n";
    print "  framkallningsdatum : $$ref{Framkallningsdatum}\n"
      if ($$ref{Framkallningsdatum});
    print "  period_start       : $$ref{Period_start}\n";
    print "  period_slut        : $$ref{Period_slut}\n";
    print "  titel              : $$ref{Titel}\n"
      if ($$ref{Titel});
    print "  nyckelord          : $$ref{Nyckelord}\n"
      if ($$ref{Nyckelord});
    print "  bildantal          : $$ref{Antal_bilder}\n"
      if ($$ref{Antal_bilder});
    print "  kommentar          : $$ref{Kommentar}\n"
      if ($$ref{Kommentar});

    print "\n";

    # S�tt in i databas
    $dbh->do( 'insert into filmregister values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
	      undef,
	      $$ref{aar},
	      $$ref{filmtyp},
	      $$ref{serienummer},
	      $$ref{Framkallningsdatum},
	      $$ref{Period_start},
	      $$ref{Period_slut},
	      $$ref{Titel},
	      $$ref{Nyckelord},
	      $$ref{Antal_bilder},
	      $$ref{Kommentar} );
}

$dbh->disconnect;
