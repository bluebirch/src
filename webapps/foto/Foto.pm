# $Id$
#
# $Log$

package Foto;
use base 'CGI::Application';
use strict;
use DBI;

sub setup {
    my $self = shift;

    # S�tt f�rvalt run-mode
    $self->start_mode( 'visafilmer' );

    # Definiera runmodes
    $self->run_modes( 'visafilmer'   => 'visafilmer',
		      'redigera'     => 'redigerafilm',
		      'spara'        => 'spara' );

    # S�tt s�kv�g f�r HTML::Templates
    $self->tmpl_path( '/home/stefan/src/foto/' );

    # Anslut till databas
    $self->{dbh} = DBI->connect( 'DBI:mysql:foto:localhost', 'stefan',
				 'slempropp' );

    # F�rbered n�gra SQL-satser
    $self->{sth_allafilmer} = $self->{dbh}->prepare( q{select * from filmregister order by period_start, period_slut} );
    $self->{sth_film} = $self->{dbh}->prepare( q{select * from filmregister where aar=? and filmtyp=? and serienummer=?} );
    $self->{sth_uppdatera} = $self->{dbh}->prepare( q{update filmregister set framkallningsdatum=?, period_start=?, period_slut=?, titel=?, nyckelord=?, bildantal=?, kommentar=? where aar=? and filmtyp=? and serienummer=?} );
    $self->{sth_insert} = $self->{dbh}->prepare( q{insert into filmregister (framkallningsdatum, period_start, period_slut, titel, nyckelord, bildantal, kommentar, aar, filmtyp, serienummer) values (?,?,?,?,?,?,?,?,?,?)} );

    # Spara undan CGI-object
    $self->{_q} = $self->query;
}

sub teardown {
    my $self = shift;

    $self->{dbh}->disconnect;
}

######################################################################
# visafilmer - visa en lista p� alla filmer i registret
#
sub visafilmer {
    my $self = shift;

    # H�mta data
    $self->{sth_allafilmer}->execute;
    my $ref = $self->{sth_allafilmer}->fetchall_arrayref( {} );

    my $i = 0;
    foreach my $p (@$ref) {
	$$p{id} = join( '-', $$p{aar}, $$p{filmtyp}, $$p{serienummer} );
	$$p{nr} = ++$i;
    }

    # Initiera template
    my $tmpl = $self->load_tmpl( 'visafilmer.html',
				 die_on_bad_params => 0 );

    $tmpl->param( 'filmer' => $ref );
    return $tmpl->output;
}

######################################################################
# redigerafilm
#
sub redigerafilm {
    my $self = shift;

    my $q = $self->query;

    my $aar = $q->param( 'aar' );
    my $filmtyp = $q->param( 'filmtyp' );
    my $serienr = $q->param( 'serienummer' );

    # H�mta data
    $self->{sth_film}->execute( $aar, $filmtyp, $serienr );
    my $ref = $self->{sth_film}->fetchall_arrayref( {} );

    # Initiera template
    my $tmpl = $self->load_tmpl( 'redigerafilm.html',
				 die_on_bad_params => 0 );

    # �verf�r data till template
    my $data = $$ref[0];
    foreach my $field (keys %$data) {
	$tmpl->param( $field => $$data{$field} );
    }

    # Slut
    return $tmpl->output;
}

######################################################################
# spara
#
sub spara {
    my $self = shift;

    my $q = $self->query;
    my $tmpl = $self->load_tmpl( 'spara.html',
				 die_on_bad_params => 0 );

    # Uppdatera databas
#    $self->{sth_uppdatera}->trace(2);
    $self->{sth_uppdatera}->execute( ($q->param( 'framkallningsdatum' ) or undef),
				     ($q->param( 'period_start' ) or undef),
				     ($q->param( 'period_slut' ) or undef),
				     ($q->param( 'titel' ) or undef),
				     ($q->param( 'nyckelord' ) or undef),
				     ($q->param( 'bildantal' ) or undef),
				     ($q->param( 'kommentar' ) or undef),
				     ($q->param( 'aar' ) or undef),
				     ($q->param( 'filmtyp' ) or undef),
				     ($q->param( 'serienummer' ) or undef) );

    my $rows = $self->{sth_uppdatera}->rows;
    my $errstr = $self->{sth_uppdatera}->errstr;

    unless ($rows == 1) {
	$self->{sth_insert}->execute( ($q->param( 'framkallningsdatum' ) or undef),
				      ($q->param( 'period_start' ) or undef),
				      ($q->param( 'period_slut' ) or undef),
				      ($q->param( 'titel' ) or undef),
				      ($q->param( 'nyckelord' ) or undef),
				      ($q->param( 'bildantal' ) or undef),
				      ($q->param( 'kommentar' ) or undef),
				      ($q->param( 'aar' ) or undef),
				      ($q->param( 'filmtyp' ) or undef),
				      ($q->param( 'serienummer' ) or undef) );

	$rows = $self->{sth_insert}->rows;
	$errstr = $self->{sth_insert}->errstr;
    }


    $tmpl->param( 'poster' => $rows );
    $tmpl->param( 'meddelande' => $errstr );
    $tmpl->param( 'id' => $q->param( 'aar' )."-".$q->param( 'filmtyp' )."-".$q->param( 'serienummer' ) );

    return $tmpl->output;
}

1;
