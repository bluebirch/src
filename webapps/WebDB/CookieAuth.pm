# $Id$
#
# CookieAuth - simple authentication with cookies
#

package CookieAuth;
use strict;
use MIME::Base64;
use Crypt::RC4;
use Storable qw(freeze thaw);

#
# Class constructor and destructor
#
sub new {
    my $proto  = shift;
    my $user = shift;
    my $class = ref($proto) || $proto;
    my $self = { userdata => {},
		 auth_key => 'q7uiytaso876t4ywg%&yte3ladfg'
	       };

    bless( $self, $class );

    return $self;
}

#  sub DESTROY {
#      my $self = shift;
#  }


#
# Authentication methods
#

# Create session key, consisting of user name and current time, and
# encrypt it.
sub create_key {
    my $self = shift;

    return undef unless ($self->user);

    my $key = { userdata => $self->{userdata},
		ip => $ENV{REMOTE_ADDR},
		time => time };

    return $self->encrypt( freeze( $key ) );
}

# Validate session key. If key is valid, load user info and return
# user name (login id). If invalid, return undef.
sub validate_key {
    my ($self, $key) = @_;

    my %key = ();
    %key = %{ thaw( $self->decrypt( $key ) ) };

    if ($key{userdata}{user}) {
	$self->{userdata} = $key{userdata};
	print "time: $key{time}\n";
	return 1;
    }
    return 0;
}

# Encrypt with RC4 and encode in Base64. Thanks to Markus H�rnvi.
sub encrypt {
    my ($self, $data) = @_;

    return encode_base64( RC4( $self->{auth_key}, $data ), '' );
}

# Decode Base64 and decrypt with RC4. Thanks to Markus H�rnvi.
sub decrypt {
    my ($self, $data) = @_;

    return $data ?  RC4( $self->{auth_key}, decode_base64( $data ) ) : undef;
}


######################################################################
# Instance data access
#
sub user {
    my $self = shift;
    $self->{userdata}{user} = shift if (@_);
    return $self->{userdata}{user};
}
sub password {
    my $self = shift;
    if (@_) {
	my $salt = join '', ('.', '/', 0..9, 'A'..'Z', 'a'..'z')[rand 64, rand 64];
	$self->{userdata}{password} = crypt( shift, $salt );
    }
    return ($self->{userdata}{password} || '*');
}
sub timestamp {
    my $self = shift;
    $self->{userdata}{timestamp} = shift if (@_);
    return ($self->{userdata}{timestamp} || time);
}
sub set_timestamp {
    my $self = shift;
    $self->timestamp( time );
}
sub param {
    my $self = shift;
    my $param = shift;
    $self->{userdata}{$param} = shift if (@_);
    return $self->{userdata}{$param};
}



1;

