# $Id$
#
# A package for database access through a CGI::Application and
# extensive use of HTML::Template.
#
# Stefan Svensson <stefan@raggmunk.nu>
#
# $Log$

package DBToolKit;

require 5.005_62;
use strict;
use warnings;
use DBI;
use Digest::MD5 qw(md5_hex);
use SSSoft::CGI::Logger qw(:all);
use CGI::Carp;

require Exporter;
#use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);
our @EXPORT_OK = qw();
our @EXPORT = qw();
our $VERSION = '0.01';

#
# Create new object
#
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = { dsn => shift,
		 user => shift,
		 password => shift,
		 connected => 0
	       };

    bless( $self, $class );

    # Try to connect
    $self->connect;

    return $self;
}

#
# Connect to database
#
sub connect {
    my $self = shift;

    if ($self->dsn && $self->user && $self->password && !$self->connected) {
	$self->dbh( DBI->connect( $self->dsn, $self->user, $self->password ) );
	return $self->connected(1) if ($self->dbh);
    }
    return 0;
}


#
# Define database relation. This is, for example, used in the edit
# function below.
#
sub define_lookup {
    my ($self, $source, $relation) = @_;

    my ($table, $field) = split m/\./, $source;

    if ($table && $field && $relation =~ m/^.*:.*:.*$/) {
	$self->{lookup}{$table}{$field} = $relation;
	logmsg "lookup table $table field $field => $relation";
    }
    else {
	carp "wrong parameters";
    }
}
#
# Get list of tables
#
sub tables {
    my $self = shift;

    my @tables;
    @tables = $self->dbh->tables if ($self->connected);

    return wantarray ? @tables : \@tables;
}

#
# Prepare a statement and store it internally in the class for later
# reuse. The statement is uniquely identified with a MD5 hash.
#
sub prepare {
    my ($self, $sql_statement) = @_;

    my $key = md5_hex( $sql_statement );

    logmsg "prepare statement '$key'" if ($self->debug);

    unless ($self->{st}{$key}) {
	$self->{st}{$key} = $self->dbh->prepare( $sql_statement );
    }
    return $self->{st}{$key};
}

#
# Execute a single SQL statement. Call with a SQL statement or a
# statement handle that is anything other than a select
# statement. Returns number of rows affected or undef on error.
#
sub do {
    my ($self, $sql_statement) = splice @_, 0, 2;

    return $self->connected ? $self->dbh->do( $sql_statement, {}, @_ ) : undef;
}

#
# Fetch all values and return as an array of hashes. Call with a SQL
# statement or a statement handle.
#
sub selectall {
    my ($self, $sql_statement) = splice @_, 0, 2;

#    $self->dbh->trace(2) if ($self->debug);

    return $self->dbh->selectall_hashref( $sql_statement, {}, @_ );
}

#
# Get a single record
#
sub selectrow {
    my ($self, $sql_statement) = splice @_, 0, 2;

    my $sth = ref( $sql_statement) ? $sql_statement  :
      $self->prepare( $sql_statement );

    $sth->execute;

    my $data = $sth->fetchrow_hashref;

    $sth->finish;

    return $data;
}

#
# Run a query. Return result as a two-dimensional array-of-hashes
# suitable for iteration with HTML::Template.
#
sub select_table {
    my ($self, $sql_statement) = splice @_, 0, 2;

    my $rows = [];

    # If the SQL statement is not a prepared statement, we must
    # prepare it ourselves.
    unless (ref $sql_statement) {
	$sql_statement = $self->dbh->prepare( $sql_statement );
	return [] unless ($sql_statement); # bail out on error
    }

    # Execute statement
    $sql_statement->execute( @_ );

    # Get number of fields (or columns) in result
    my $fields = $sql_statement->{NUM_OF_FIELDS};
    my $lines = $sql_statement->rows;

    # Add a first header line
    my @$hdr = map { name => $_, value => $_, type => 'header', header => 1 },
      @{$sql_statement->{NAME}};
    push @$rows, { fields => $hdr };

    while (my $data = $sql_statement->fetchrow_arrayref) {
	my $row = [];
	for (my $i = 0; $i < $fields; $i++) {
	    push @$row, { name => $sql_statement->{NAME}->[$i],
			  type => $sql_statement->{TYPE}->[$i],
			  precision => $sql_statement->{PRECISION}->[$i],
			  value => $data->[$i] };
	}
	push @$rows, { fields => $row };
    }

    return $rows;
}

#
# Describe table
#
sub describe {
    my ($self, $table) = @_;

    logmsg "describe $table" if ($self->debug);

    return $self->selectall( "describe $table" );
}

#
# Get fields for editing. Return arrayref of hashes suitable for
# HTML::Template looping.
#
sub edit {
    my $self = shift;

    my $table = shift;
    my $select = shift;

    # Set default options
    my %opt = ( hide => [], read_only => [], link => {} );

    # Get options
    for (my $x = 0; $x <= $#_; $x += 2) {
	unless (defined($_[($x + 1)])) {
	    logmsg "odd number of parameters";
	    next;
	}
	$opt{lc($_[$x])} = $_[($x + 1)];
    }

    # Get table description
    my $tableinfo = $self->describe( $table );

    # Get record to edit
    my $data = $self->selectrow( "select * from $table where $select" );

    unless ($data) {
	logmsg "no default data";
    }

    # Add some extra information to tableinfo hash
    foreach my $ref (@$tableinfo) {

	# Get current value
	$$ref{Value} = $$data{$$ref{Field}} if ($data && $$data{$$ref{Field}});

	# Check if this is a primary key
	if ($$ref{Key} eq 'PRI') {
	    $$ref{Hidden} = 1;
	    $$ref{ReadOnly} = 1;
	    $$ref{Input} = 'Hidden';
	}

	# Check if this is a lookup field. If so, get alternative
	# values from lookup table.
	elsif ($self->{lookup}{$table}{$$ref{Field}}) {
#	elsif ($opt{link}{$$ref{Field}}) {
	    my ($table, $key, $value) = split m/:/,
	      $self->{lookup}{$table}{$$ref{Field}};
	    my $linkdata = $self->selectall( "select $key as Value, $value as Description from $table order by Description" );
	    foreach my $link (@$linkdata) {
		if ($$link{Value} eq $$ref{Value}) {
		    $$link{Selected} = 1;
		    last;
		}
	    }
	    $$ref{Options} = $linkdata;
	}

	# Set HTML form input type
	$$ref{Input} = 'Text' unless ($$ref{Input});
    }

    return $tableinfo;
}

#
# Dump a result hash as a nice, readable table
#
sub print_tabular {
    my ($self, $list) = splice @_, 0, 2;

    # get max width of each column
    my @keys = @_ ? @_ : sort keys %{$$list[0]};
    my %width = map { $_ => length( $_ ) } @keys;
    foreach my $ref (@$list) {
	foreach my $field (@keys) {
	    if (!$width{$field} || length( ($$ref{$field} || '') ) > $width{$field}) {
		$width{$field} = length( $$ref{$field} );
	    }
	}
    }
    print "+", "-" x ($width{$_} + 2) foreach (@keys);
    print "-+\n";
    printf "| %-$width{$_}s ", $_ foreach (@keys);
    print " |\n";
    print "+", "-" x ($width{$_} + 2) foreach (@keys);
    print "-+\n";
    foreach my $ref (@$list) {
	foreach my $field (@keys) {
	    if (!$$ref{$field}) {
		print "|", " " x ($width{$field} + 2);
	    } elsif ($$ref{$field} =~ m/^[+-]?\d+$/) {
		printf "| %$width{$field}d ", $$ref{$field};
	    } else {
		printf "| %-$width{$field}s ", $$ref{$field};
	    }
	}
	print " |\n";
    }
    print "+", "-" x ($width{$_} + 2) foreach (@keys);
    print "-+\n";
}


#
# Data access methods
#
sub dsn {
    my $self = shift;
    $self->{dsn} = shift if (@_);
    return $self->{dsn};
}
sub user {
    my $self = shift;
    $self->{user} = shift if (@_);
    return $self->{user};
}
sub password {
    my $self = shift;
    $self->{password} = shift if (@_);
    return $self->{password};
}
sub dbh {
    my $self = shift;
    $self->{dbh} = shift if (@_);
    return $self->{dbh};
}
sub connected {
    my $self = shift;
    $self->{connected} = shift if (@_);
    return $self->{connected};
}
sub debug {
    my $self = shift;
    $self->{debug} = shift if (@_);
    return $self->{debug};
}

1;
__END__

=head1 NAME

SSSoft::DBI::Application - Perl extension for easy database access from a
CGI::Application.

=head1 SYNOPSIS

  use SSSoft::DBI::Application;

=head1 DESCRIPTION

=head2 Class Methods

See below.

=over 4

=item C<< $db = new SSSoft::DBI::Application >>

Create a new database object and initiate connection with database
server.

=item C<< $db->connect >>

Connect to database server with the current I<DSN>, I<username> and
I<password>. Returns I<1> on success, I<0> otherwise.

=item C<< $db->tables >>

Get a list of available tables. Returns an array if in array context,
or an array reference if in scalar context.

=item C<< $db->do( $sql_statement, @bind_vars ) >>

Execute a single SQL statement. Call with a SQL statement or a
prepared statement handle. Returns number of rows affected.

=back

=head2 Data access methods

The following methods can be used to alter the configuration or
behaviour of each object instance. All methods returns the current
value, and, if called with a parameters, sets the value to that
parameter.

=over 4

=item C<< $db->dsn >>

Database driver DSN.

=item C<< $db->user >>

Database driver username.

=item C<< $db->password >>

Database driver password.

=item C<< $db->dbh >>

Database handle. Should not be altered.

=item C<< $db->connected >>

True if connected to a database server, false otherwise.

=item C<< $db->debug >>

Debug level. Valid options are 0 (false) and non-zero (true).

=back

=head1 AUTHOR

Stefan Svensson <stefan@raggmunk.nu>

=head1 SEE ALSO

perl(1), CGI::Application, HTML::template.

=cut
