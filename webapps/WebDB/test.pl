#!/usr/bin/perl -w
use strict;
use CookieAuth;

sub print_hashref {
    my $hashref = shift;
    foreach my $key (sort keys %$hashref) {
	print "$key: $$hashref{$key}\n";
    }
}

my $u = new CookieAuth;

$u->user( 'stefan' );
$u->param( password => 'tjong' );
$u->param( host => 'localhost' );
$u->param( database => 'webdiary' );

print_hashref $u->{userdata};

my $key = $u->create_key;
print "$key\n";

my $v = new CookieAuth;
if ($v->validate_key( $key )) {
    print_hashref $v->{userdata};
}
