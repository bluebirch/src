package WebDB;
use base 'CGI::Application';
use strict;
use CookieAuth;
use DBToolKit;

sub setup {
    my $self = shift;
    my $q = $self->query;

    $self->run_modes( start => \&start,
		      login => \&login );
    $self->start_mode( 'start' );

    if (!$q->param( 'rm' ) || $q->param( 'rm' ) ne 'login') {
	if (my $session_key = $q->cookie( 'WebDB_session' )) {
	    my $user = new CookieAuth;
	    if ($user->validate_key( $session_key )) {

		# Log in to database
		my $host = $user->param( 'host' );
		my $database = $user->param( 'database' );

		my $db = new DBToolKit;
		$db->dsn( "dbi:mysql:$database:$host" );
		$db->user( $user->user );
		$db->password( $user->password );

		unless ($db->connect) {
		    print STDERR "login failed; host=$host, database=$database, ";
		    print STDERR "user=" . $user->user . ", password=" . $user->password . "\n";
		    $q->param( rm => 'login' );
		    $q->param( message => 'Login to database failed.' );

		    # Delete cookie
#  		    my $cookie = $q->cookie( -name => 'WebDB_session',
#  					     -value => '*',
#  					     -expires => '-1d' );
#  		    $self->header_props( -cookie => $cookie );

		} else {
		    print STDERR "opened database $database\n";
		}

		$self->db( $db );

	    } else {
		$q->param( message => 'Invalid session key.' );
		$q->param( rm => 'login' );
	    }
	} else {
	    print STDERR "no cookie set\n";
	    $q->param( message => 'Please login' );
	    $q->param( rm => 'login' );
	}
    }
}

sub start {
    my $self = shift;
    $self->header_props( -type => 'text/plain' );

    my @tables = $self->db->tables;

    return "start: @tables\n";
}

sub login {
    my $self = shift;
    my $q = $self->query;

    if ($q->param( 'login' )) {
	my $u = new CookieAuth;
	$u->user( $q->param( 'user' ) );
	$u->param( 'password', $q->param( 'password' ) );
	$u->param( 'host', $q->param( 'host' ) );
	$u->param( 'database', $q->param( 'database' ) );

	# Create cookie and redirect browser
	my $cookie = $q->cookie( -name => 'WebDB_session',
				 -value => $u->create_key );
	$self->header_props( -uri => $q->url,
			     -cookie => $cookie );
	$self->header_type( 'redirect' );
	return '';
    }

    # Output template
    my $t = $self->load_tmpl( 'login.html', associate => $q );
    return $t->output;
}

sub db {
    my $self = shift;
    $self->{db} = shift if (@_);
    return $self->{db};
}

1;
