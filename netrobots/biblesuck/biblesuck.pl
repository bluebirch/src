#!/usr/bin/perl -w
#
# $Id$
#
# Suck Bible 2000
#
# $Log$
# Revision 1.1  2001/09/18 21:01:14  stefan
# F�rsta versionen av programmet, som suger texten fr�n www.bibel2000.org.
#

use strict;
use locale;
use LWP::UserAgent;

$|=1;

sub rand_agent {
    my @agents = ( "Mozilla/4.5 [en] (WinNT;I)",
		   "Mozilla/4.71 [en]C-AtHome0407 (Win98; I)",
		   "Mozilla/4.72 [sv] (WinNT;I)",
		   "Mozilla/5.0 [en-US] (Linux; I)",
		   "Mozilla/5.0 [sv-SE] (Windows_NT; I)");
    return $agents[rand scalar @agents];
}

sub geturl {
    my $url = shift;

    # Create user agent object
    my $ua = new LWP::UserAgent;
    $ua->agent( rand_agent );
    $ua->timeout( 30 );

    # Create a request
    my $req = new HTTP::Request GET => $url;

    # Pass request to the user agent and get a response back
    my $res;
    my $i=0;
    do {
	$res = $ua->request( $req );
	$i++;
	unless ($res->is_success) {
	    my $s = $i>10 ? 30 : $i*2;
	    print "X";
	    sleep $s;
	}
    } until ($res->is_success);

    return $res->content;
}

sub parseurl {
    my $url = shift;
    my $output = shift;

    my $retval = 0;

    my $data = geturl( $url );

    while ($data =~ m:<tr>
                      <td.*?>(.*?)</td>
                      <td.*?><p><font.*?>(.*?)</p></td>
                      </tr>:gx) {
	$retval = 1; # returnera sant om vi hittade n�got

	my $versklump = $1;
	my $text = $2;

	$versklump =~ m:<nobr>(.*?)</nobr>:;
	my $vers = $1;

	my $par;
	if ($versklump =~ m:showinfo\.jsp\?par=1\&versid=(\d+):) {
	    my $data = geturl( 'http://bibeln.se/showinfo.jsp?par=1&versid='
				  . $1 );
	    print "p";
	    $data =~ m:<b>(.*?)</b>.*?<a href="showvers.*?>(.*?)</a><br>:s;
	    $par = "[Ref $1 -> $2]";
	    $par =~ tr:\224\226:"-:;
	    $par =~ s:\227:--:g;
	}

	my @not;
	if ($versklump =~ m:showinfo\.jsp\?not=1\&versid=(\d+):) {
	    my $data = geturl( 'http://bibeln.se/showinfo.jsp?not=1&versid='
				  . $1 );
	    print "k";
	    while ($data =~ m:<b>(.*?)</b><br>[\r\n]*(.*?)<br>[\r\n]*:g) {
		my $ref = $1;
		my $txt = $2;
		$txt =~ s:^<i>(.*?)</i>:$1\::;
		$txt =~ s:<b> > </b>::g;
		$txt =~ s:</?[iba].*?>::g;
		$txt =~ s:<(QL|JL).*?>::g;
		$txt =~ s:<EL>:\*:g;
		$txt =~ s: {2,}: :g;
		$txt =~ tr:\224\226�:"-':;
		$txt =~ s:\227:--:g;
		$txt =~ s:\205:...:g;
		push @not, "[Not $ref -> $txt]";
	    }
	}

	#print "vers=$vers\n";
	#print "text=$text\n";
	#print "par=$par\n" if ($par);
	#print "not=$not\n" if ($not);

	# Teckentabellstrubbel
	$text =~ tr:\224\226�:"-':;
	$text =~ s:\227:--:g;
	$text =~ s:\205:...:g;
#	$text =~ s:\224:":g;
#	$text =~ s:\226:-:g;
#	$text =~ s:�:':g;

	# H�mta eventuel rubrik
	if ($text =~ s:<b>(.*?)</b>::g) {
	    my $rubrik = $1;
	    print $output "\n$rubrik\n\n";
	}

	# Ta bort skr�p
	$text =~ s:(&nbsp;)+::g;
	$text =~ s:(?<=[\w\!\?\.\-\,\:\;\"])(<br>)+(?=[\w\-<]): :g;
	#$text =~ s:<br>::g;
	#$text =~ s:<in .*/>::g;
	$text =~ s:<.*?>::g; # alla taggar skall bort
	print $output "[$vers] $text\n";
	print $output "$par\n" if ($par);
	print $output "$_\n" foreach (@not);
    }
    print ".";
    return $retval;
}

#parseurl( 'http://bibeln.se/showvers.jsp?visakap=Luk+1' );
#die 'hadf' unless ($ARGV[0]);

#parseurl( 'http://bibeln.se/showvers.jsp?visakap='.$ARGV[0] ) || die "det gick inte bra";

open( CONF, 'biblesuck.conf' ) or die 'uuuhhg';

while (<CONF>) {
    next if (m/(^#|^\n)/);
    chomp;
    my ($boknamn,$bok) = split /:/;

    my $filnamn = $bok.'.txt';
    $filnamn =~ s/ /_/g;

    unless (-f $filnamn) {
	print "Bok: $boknamn ($bok)\n";
	print "Fil: $filnamn\n";
	print "\n";

  	open( OUTPUT, '>'.$filnamn ) or die 'R�����h!';

	my $bokref = $bok;
	$bokref =~ s/ /%20/g;
	my $kapitel = 1;

	while (parseurl( 'http://bibeln.se/showvers.jsp?visakap='.
			 $bokref.'+'.$kapitel, *OUTPUT{IO} )) {
	    $kapitel++;
	}

	close OUTPUT;
    }

#  	my $url = "bibel.asp?sBok=$book&sKapitel=1";
#  	my $lasturl = '';

#  	while( $url ne $lasturl ) {
#  	    $lasturl = $url;
#  	    $url = parse_page( *OUTPUT{IO}, get_raw_html( $url ));
#  	    unless ($url) {
#  		$url = $lasturl;
#  		$lasturl = '';
#  		print "    fel vid parsning; v�ntar L�NGE och f�rs�ker igen\n";
#  		sleep 300;
#  	    } else {
#  		$lasturl =~ m/sKapitel=(\d+)/i;
#  		print "  - kapitel $1\n";
#  		sleep 2; # l�gg in en paus
#  	    }
#  	}
#  	close OUTPUT;
#      }
}

close CONF;
