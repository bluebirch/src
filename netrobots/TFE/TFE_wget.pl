#!/usr/bin/perl -w
#
# $Id$
#
# Script f�r att h�mta data fr�n
# http://www.tfe.umu.se/weather/fattig.asp och lagra i en databas.
#
# $Log$
# Revision 1.4  2003/04/17 09:19:46  stefan
# Skriv till en annan databas.
#
# Revision 1.3  2002/11/18 06:47:14  stefan
# Skit. Scriptet l�ste bara av plusgrader! :-(
#
# Revision 1.2  2002/11/18 06:40:27  stefan
# Om avl�st tid �r st�rre �n aktuell tid, antar vi att det h�r till
# g�rdagens m�tv�rden och backar datumet en dag. Detta intr�ffar n�r
# scriptet l�ser av det sista m�tv�rdet f�r dagen (23:50).
#
# Revision 1.1  2002/11/17 09:29:29  stefan
# Databasen uppdateras. Dags f�r crontab.
#

use strict;
use LWP::UserAgent;
use DBI;
use POSIX qw(strftime);
use Date::Calc qw(:all);

my $url = 'http://www.tfe.umu.se/weather/fattig.asp';

# Skapa useragent
my $ua = new LWP::UserAgent;

# H�mta TFE-sidan
my $res = $ua->get( $url );

# Om vi inte fick den, hoppa ur med ett felmeddelande
unless ($res->is_success) {
    print STDERR "Kunde inte h�mta $url\n";
    exit 2;
}

# Ta hand om statistiken
my $page = $res->content;

# H�mta data
unless ($page =~ m/V�dret kl\. +(\d\d:\d\d):.*?Temperatur.*?(-?\d+,\d+)� C.*?Solinstr�lning.*?(\d+,\d+) W\/m�.*?Nederb�rd.*?(\d+,\d+) mm.*?Luftfuktighet.*?(\d+,\d+)%.*?Vindhastighet.*?(\d+,\d+) m\/s.*?Vindriktning.*?(\d+)�.*?Tryck.*?(\d+) mBar/s) {
    print STDERR "Kunde inte tolka f�ljande data:\n\n";
    print STDERR $page;
    print STDERR "\n";
    exit 2;
}
my ($tid, $temp, $sol, $regn, $luftfukt, $vindhast, $vindrikt, $tryck) = ($1, $2, $3, $4, $5, $6, $7, $8 );

$tid .= ':00';
$temp =~ s/,/./;
$sol =~ s/,/./;
$regn =~ s/,/./;
$luftfukt =~ s/,/./;
$vindhast =~ s/,/./;
$vindrikt =~ s/,/./;
$tryck =~ s/,/./;

my @datum = Today;
my $nu = strftime( '%H:%M:%S', localtime );

if ($tid gt $nu) {
    @datum = Add_Delta_Days( @datum, -1 );
    print STDERR "$tid > $nu; backar datum\n";
}

my $datum = sprintf( '%04d-%02d-%02d', @datum );

#  print "Datum:         [$datum]\n";
#  print "Tid:           [$tid]\n";
#  print "Temp:          [$temp]\n";
#  print "Sol:           [$sol]\n";
#  print "Regn:          [$regn]\n";
#  print "Luftfuktighet: [$luftfukt]\n";
#  print "Vindhastighet: [$vindhast]\n";
#  print "Vindriktning:  [$vindrikt]\n";
#  print "Tryck:         [$tryck]\n";

my $dbh = DBI->connect( 'DBI:mysql:raggmunk:localhost', 'stefan', 'slempropp' );

# Unders�k om data redan finns i databasen
my $rows = $dbh->do( 'SELECT * FROM ume_vader WHERE datum=? AND tid=?',
		     {},
		     $datum, $tid );
if ($rows == 0) {
    $rows = $dbh->do( 'INSERT INTO ume_vader VALUES (?,?,?,?,?,?,?,?,?)',
			 {},
			 $datum, $tid, $temp, $sol, $regn, $luftfukt, $vindhast, $vindrikt, $tryck );
    if (!$rows || $rows != 1) {
	print STDERR "Fel vid inl�ggning av data: $datum $tid\n";
    }
}
else {
    print STDERR "Dubbeldata: $datum $tid\n";
}

