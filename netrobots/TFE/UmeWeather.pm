# $Id$
#
# $Log$
# Revision 1.5  2002/11/21 17:15:55  stefan
# Grejade lite
#
# Revision 1.4  2002/11/19 20:53:08  stefan
# Lade till aktuell statistik.
#
# Revision 1.3  2002/11/19 18:05:19  stefan
# �ndrade f�rg igen. Jenny bara skrattade �t mig. :-)
#
# Revision 1.2  2002/11/19 18:02:42  stefan
# �ndrade lite utseende p� graferna.
#
# Revision 1.1  2002/11/19 17:46:01  stefan
# S�. Visa lite statistik. Skojigt, skojigt.
#
#
package UmeWeather;
use base 'CGI::Application';
use strict;
use DBI;
use CGI::Logger qw(:all);
use POSIX qw(strftime ceil floor);
use GD::Graph::linespoints;
use GD::Graph::mixed;

sub setup {
    my $self = shift;

    my $q = $self->query;

    $self->run_modes( 'index' => \&index,
		      'graph' => \&graph,
		      'rain' => \&rain );

    $self->start_mode( 'index' );
}

sub db {
    my $self = shift;
    unless ($self->{db}) {
	$self->{db} = DBI->connect( 'DBI:mysql:UmeWeather:localhost',
				    'stefan',
				    'slempropp' );
    }
    return $self->{db};
}

sub index {
    my $self = shift;

    my $q = $self->query;
    my $db = $self->db;

    $q->param( scope => 'month' ) unless ($q->param( 'scope' ));

    my $sth = $db->prepare( 'SELECT * FROM UmeWeather ORDER BY Date DESC,Time DESC LIMIT 1' );
    $sth->execute();
    my $data = $sth->fetchrow_hashref;
    $sth->finish;

    my $t = $self->load_tmpl( 'index.tmpl',
			      associate => $q );

    $t->param( $data );
#    $t->param( scope => 'month' );

    return $t->output;
}

#  sub temp {
#      my $self = shift;

#      my $q = $self->query;

#      my $date = ($q->param( 'date' ) || strftime( '%Y-%m-%d', localtime ));
#      my $scope = ($q->param( 'scope' ) || 'day' );

#      # Set HTTP headers
#      $self->header_props( -type => 'image/png' );

#      # Create graph
#      my $gd = $self->makegraph( date => $date, scope => $scope );

#      return $gd ? $gd->png : '';
#  }

sub graph {
    my $self = shift;

    my $q = $self->query;

    my $date = ($q->param( 'date' ) || strftime( '%Y-%m-%d', localtime ));
    my $scope = ($q->param( 'scope' ) || 'day' );
    my $field = ($q->param( 'field' ) || 'Temperature' );

    # Create graph
    my $gd = $self->makegraph( date => $date,
			       scope => $scope,
			       field => $field );

    if ($gd) {
	$self->header_props( -type => 'image/png' );
	return $gd->png;
    } else {
	logmsg "No graph: date=$date, scope=$scope, field=$field";
	$self->header_props( -type => 'text/plain' );
	return "error";
    }

    return $gd ? $gd->png : '';
}

sub rain {
    my $self = shift;

    my $q = $self->query;

    my $date = ($q->param( 'date' ) || strftime( '%Y-%m-%d', localtime ));

    # Create graph
    my $gd = $self->makegraph( date => $date,
			       scope => 'plainmonth',
			       type => 'bars',
			       query => 'SELECT DAYOFMONTH(Date) AS Offset, SUM(Precipitation) AS Data FROM UmeWeather WHERE MONTH(Date)=MONTH(?) GROUP BY Date' );

    if ($gd) {
	$self->header_props( -type => 'image/png' );
	return $gd->png;
    } else {
	logmsg "No graph: date=$date, scope=month";
	$self->header_props( -type => 'text/plain' );
	return "error";
    }

    return $gd ? $gd->png : '';
}

#  sub solar {
#      my $self = shift;

#      my $q = $self->query;
#      my $db = $self->db;

#      my $date = ($q->param( 'date' ) || strftime( '%Y-%m-%d', localtime ));
#      my $scope = ($q->param( 'scope' ) || 'day' );

#      # Set HTTP headers
#      $self->header_props( -type => 'image/png' );

#      my $gd = $self->makegraph( date => $date, scope => $scope, field => 'Solar' );

#      return $gd ? $gd->png : '';
#  }

#  sub mkgraph {
#      my ($self, $class, @options) = @_;

#      my $graph = new $class (186, 130);

#      $graph->set(
#  		# General
#  		correct_width     => 1,

#  		# Axes
#  		zero_axis_only    => 1,
#  #		zero_axis         => 1,
#  		box_axis          => 0,

#  		# Y axis settings
#  		y_tick_number     => 6,
#  		y_long_ticks      => 1,

#  		# X axis settings
#  		x_label_skip      => 18,

#  		# Other options
#  		@options
#  		);

#      return $graph;
#  }

#  sub mkdaydata {
#      my ($self, $sth, $maxref, $minref) = @_;

#      # Create an empty data structure
#      my $data = [];

#      for (my $i=0; $i <= 143; $i++) {
#  	use integer;
#  	$$data[0][$i] = $i / 6;
#  	no integer;
#  	$$data[1][$i] = undef;
#      }

#      # The statement handle should be executed when mkdaydata is called
#      my ($offset, $value);
#      $sth->bind_columns( \$offset, \$value );

#      # Fetch all data
#      while ($sth->fetch) {
#  	$$maxref = $value if (!$$maxref || $value > $$maxref);
#  	$$minref = $value if (!$$minref || $value < $$minref);
#  	$$data[1][$offset] = $value;
#      }

#      # Return data
#      return $data;
#  }

#  sub mkweekdata {
#      my ($self, $sth, $maxref, $minref) = @_;

#      # Create an empty data structure
#      my $data = [];

#      for (my $i=0; $i <= 167; $i++) {
#  	use integer;
#  	$$data[0][$i] = $i / 6;
#  	no integer;
#  	$$data[1][$i] = undef;
#      }

#      # The statement handle should be executed when mkdaydata is called
#      my ($offset, $value);
#      $sth->bind_columns( \$offset, \$value );

#      # Fetch all data
#      while ($sth->fetch) {
#  	$$maxref = $value if (!$$maxref || $value > $$maxref);
#  	$$minref = $value if (!$$minref || $value < $$minref);
#  	$$data[1][$offset] = $value;
#      }

#      # Return data
#      return $data;
#  }

sub makegraph {
    my $self = shift;

    my $db = $self->db;

    # Set default parameters
    my %opt = ( scope => 'day',
		date => strftime( '%Y-%m-%d', localtime ),
		field => 'Temperature',
		function => 'AVG',
		type => 'lines' );

    # Load in options supplied to method
    for (my $x = 0; $x <= $#_; $x += 2) {
	die "called with wrong number of parameters" unless (defined($_[($x + 1)]));
	$opt{lc($_[$x])} = $_[($x + 1)];
    }

    # Set size of graph
    unless ($opt{width}) {
	if ($opt{scope} eq 'day') {
	    $opt{width} = 170;
	} elsif ($opt{scope} eq 'week') {
	    $opt{width} = 200;
	} else {
	    $opt{width} = 280;
	}
    }
    unless ($opt{height}) {
	$opt{height} = 120;
    }

    # Create query to get data
    unless ($opt{query}) {
	if ($opt{scope} eq 'day') {
	    $opt{query} = "SELECT ROUND( TIME_TO_SEC(Time)/600 ) AS Offset, $opt{field} AS Data FROM UmeWeather WHERE Date=? ORDER BY Offset";
	} elsif ($opt{scope} eq 'week') {
	    $opt{query} = "SELECT WEEKDAY(Date) * 24 + FLOOR(TIME_TO_SEC(Time) / 3600) AS Offset, $opt{function}($opt{field}) AS Data FROM UmeWeather WHERE WEEK(Date,1)=WEEK(?,1) GROUP BY Offset";
	} elsif ($opt{scope} eq 'month') {
	    $opt{query} = "SELECT FLOOR((DAYOFMONTH(Date)-1)*4 + HOUR(Time)/6) AS Offset, $opt{function}($opt{field}) AS Data FROM UmeWeather WHERE MONTH(Date)=MONTH(?) GROUP BY Offset";
	} else {
	    die "Wrong scope";
	}
    }

    my $sth = $db->prepare( $opt{query} );
    $sth->execute( $opt{date} );

    # Create an empty data structure
    my $data = [];

    # Prefill data structure with x values, set x skip
    if ($opt{scope} eq 'day') {
	for (my $i=0; $i <= 143; $i++) {
	    use integer;
	    $$data[0][$i] = $i / 6;
	    $$data[1][$i] = undef;
	}
	$opt{x_label_skip} = 18;
    } elsif ($opt{scope} eq 'week') {
	for (my $i=0; $i <= 167; $i++) {
	    use integer;
	    $$data[0][$i] = qw(M T O T F L S)[$i / 24];
	    $$data[1][$i] = undef;
	}
	$opt{x_label_skip} = 24;
    } elsif ($opt{scope} eq 'month') {
	for (my $i=0; $i <= 123; $i++) {
	    use integer;
	    $$data[0][$i] = $i / 4 + 1;
	    $$data[1][$i] = undef;
	}
	$opt{x_label_skip} = 16;
    } elsif ($opt{scope} eq 'plainmonth') {
	for (my $i=0; $i <= 30; $i++) {
	    $$data[0][$i] = $i+1;
	    $$data[1][$i] = undef;
	}
	$opt{x_label_skip} = 4;
    }

    # The statement handle should be executed when mkdaydata is called
    my ($offset, $value);
    $sth->bind_columns( \$offset, \$value );

    # Fetch all data
    my ($max, $min);
    while ($sth->fetch) {
	$max = $value if (!$max || $value > $max);
	$min = $value if (!$min || $value < $min);
	$$data[1][$offset] = $value;
    }

    $max = ceil( $max );
    $min = floor( $min );

    # Justera max och min-v�rlden
    if ($opt{field} eq 'Pressure') {
	$min = 940;
	$max = 1040;
    }
    elsif ($opt{field} eq 'Humidity') {
	$min = 0;
	$max = 100;
    }
    else {
	$min = 0 if ($min > 0);
	$max = 0 if ($max < 0);
	$max++ if ($min == $max);
    }

    my $diff = $max - $min;

    # Find a reasonable amount of ticks
    my $ticks;
    for ($ticks=4; $ticks <= 11; $ticks++) {
	last if ($diff % $ticks == 0);
    }
    if ($ticks >= 11) {
#	logmsg "no even tick step found; change min and max";
	$ticks = 5;
	$max = $min + ceil( $diff / $ticks ) * $ticks;
    }

#      if ($ticks > 6) {
#  	if ($ticks % 2 == 0) {
#  	    $ticks /= 2;
#  	} elsif ($ticks % 3 == 0) {
#  	    $ticks /= 3;
#  	}
#      }

#    logmsg "max=$max, min=$min, ticks = $ticks",;

    my $class = "GD::Graph::$opt{type}";

    my $graph = new $class ($opt{width}, $opt{height});

    $graph->set(
		# General
		correct_width     => 1,
#		transparent       => 0,
#		bgclr             => 'lgray',
		dclrs             => [ qw(red) ],

		# Axes
		zero_axis_only    => 1,
#		zero_axis         => 1,
		box_axis          => 0,

		# Y axis settings
		y_tick_number     => $ticks,
		y_long_ticks      => 1,
		y_max_value       => $max,
		y_min_value       => $min,

		# X axis settings
		x_label_skip      => $opt{x_label_skip}
		);

    # Plot graph and return PNG data
    return $graph->plot( $data );
}

1;


