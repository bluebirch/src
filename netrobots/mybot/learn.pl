#!/usr/bin/perl -w
#
# $Id$
#
# L�r saker

use strict;
use DBI;

my $dbh = DBI->connect( "DBI:mysql:MyBot:localhost", "mybot", "spam" )
  or die "Can not connect to database";

my $insert = $dbh->prepare( 'INSERT INTO fakta (namn, beskrivning, kategori) VALUES (?, ?, ?)' );


while(<>) {
    chomp;
    next if m/^#/;
    next if ($_ eq '');
    s/\s+$//;
    my ($namn, $beskrivning) = split( /\s+=>\s+/, $_, 2 );
    print "'$namn' -> '$beskrivning'\n";
    $insert->execute( $namn, '"'.$beskrivning.'"', 1 );
}

$dbh->disconnect;
