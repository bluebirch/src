#!/usr/bin/perl -w
#
# $Id$
#
# Det h�r �r f�r tillf�llet v�rldens r�rigaste program.
#

use strict;
use Net::IRC;
use DBI;
use Time::Local;
use LWP::UserAgent;
use LWP::Simple;
use locale;

# En massa konfigurerbara variabler

my $NICK = 'Robot';
my $SERVER = 'irc.kristnet.org';
my $PORT = 6667;
my $IRCNAME = 'Jag vet faktiskt allt.';
my @CHANNELS = ( '#bottar', '#kn_fritt', '#kristnet' );
my %IGNORE = ( 'oraklet' => 1 );

my @whoami = ( "Jag �r bara en dum och f�nig liten robot.",
	       "Bry er inte om mig. Jag �r bara h�r.",
	       "Vad har jag med n�gonting att g�ra?",
	       "Ignorera mig. Jag �r helt oviktig.",
	       "L�t mig vara! Jag har inget gjort!" );

my @hello = ( "Tjena!",
	      "Hejsan!",
	      "Hej p� er!",
	      "Hall�!",
	      "Goddag!",
	      "Tjingeling!" );

my @hellonick = ( 'Hej %s!',
		  'Tjena %s!',
		  'Hall� %s!',
		  'Men hej p� dig, %s!',
		  'Hejsan %s!',
		  'Var h�lsad, �rade %s.',
		  'Hej p� dig du, %s.',
		  'Hej p� dig, %s' );

my @responses = ( 'Det ryktas att %s �r',
		  'Det s�gs att %s �r',
		  'Jag skulle tro att %s kan vara',
		  'N�gon sade att %s �r',
		  'Jag �r helt s�ker p� att %s �r',
		  'Ja, %s �r helt klart' );

my @welcome = ( 'Hej %s! V�lkommen till %s. H�r har inget h�nt p� %s, men skriv n�got s� f�r du se om n�gon svarar.',
		'V�lkommen, %s, till %s. Det har inte h�nt n�got h�r p� %s, s� jag �r glad att du d�k upp!' );


# Mindre konfigurerbara variabler

my %activity = ( '__LATEST_NEWS__' => timelocal( localtime ) );

# Initiera databas

my $dbh = DBI->connect( "DBI:mysql:MyBot:localhost", "mybot", "spam" )
  or die "Can not connect to database";

my $query_thing = $dbh->prepare( q{SELECT namn, beskrivning, kategori
                                  FROM fakta
                                  WHERE namn=?} );

my $insert = $dbh->prepare( q{INSERT INTO fakta
                              (namn, beskrivning, kategori)
                              VALUES (?, ?, 0)} );

my $update = $dbh->prepare( q{UPDATE fakta
                              SET beskrivning=?
                              WHERE namn=? AND kategori=0} );

my $delete = $dbh->prepare( q{DELETE FROM fakta
                              WHERE namn=? AND kategori=0} );

my $search = $dbh->prepare( q{SELECT DISTINCT namn
                              FROM fakta
                              WHERE beskrivning LIKE ?
                              ORDER BY namn} );

# Konfigurera IRC

my $irc = new Net::IRC;
my $knet = $irc->newconn( Nick    => $NICK,
			  Server  => $SERVER,
			  Port    => $PORT,
			  Ircname => $IRCNAME );

#$knet->debug(1);

$knet->add_handler( '376', \&on_connect );
$knet->add_handler( ['public', 'msg'], \&parse_msg );
$knet->add_handler( 'join', \&on_join);
$knet->add_handler( 'quit', \&on_quit );
$knet->add_handler( 'part', \&on_part);
$knet->add_handler( 'topic', \&on_topic);
$knet->add_handler( 'notopic', \&on_topic);
$knet->add_handler( 'kick', \&on_kick );
$knet->add_handler( 'caction', \&on_action);
$knet->add_global_handler([ 251,252,253,254,302,255 ], \&on_init);
#$knet->add_global_handler('disconnect', \&on_disconnect);
$knet->add_global_handler( 433, \&on_nick_taken);
$knet->add_global_handler( 353, \&on_names);
$knet->add_global_handler( 'ping', \&on_ping );
#$knet->add_handler( 'msg', \&on_msg );



print "startar\n";

$irc->start;

$dbh->disconnect;

print "hejd�!";
exit;

######################################################################
# Sm� funktioner

# skriv meddelande om vad jag g�r
sub msg {
    my $msg = shift;
    print " --> $msg\n";
}

# formulera ett svar efter s�kning i databasen
sub create_response {
    my ($sth, $string, $sak) = @_;

    my $result = $sth->fetchrow_hashref
      or die "fetch failed";

    my $response = sprintf( $string, $sak ).' '.
      $$result{beskrivning};
    while ($result = $sth->fetchrow_hashref) {
	$response .= ', eller '.$$result{beskrivning};
    }
    $response .= '.';
    return $response;
}

# r�kna timmar och minuter (sekunder �r parameter)
sub say_time {
    use integer;
    my $seconds = shift;

    my $hours = $seconds / 3600;
    my $minutes = ( $seconds / 60 ) % 60;

    my $say_hour = $hours > 0 ? "$hours timmar" : undef;
    my $say_minutes = $minutes > 0 ? "$minutes minuter" : undef;

    if ($say_hour && $say_minutes) {
	return "$say_hour och $say_minutes";
    } else {
	return $say_hour ? $say_hour : $say_minutes;
    }
}

# returnera en godtycklig agent; f�r nyhetsuppdatering
# stulet fr�n marvi
sub rand_agent {
    my @agents = ( "Mozilla/4.5 [en] (WinNT;I)",
		   "Mozilla/4.71 [en]C-AtHome0407 (Win98; I)",
		   "Mozilla/4.72 [sv] (WinNT;I)",
		   "Mozilla/5.0 [en-US] (Linux; I)",
		   "Mozilla/5.0 [sv-SE] (Windows_NT; I)");
    return $agents[rand scalar @agents];
}

# h�mta artikel fr�n SvD, anropas fr�n "latest_news"
# stulet fr�n marvi
sub get_svd_article {
    my $id = shift;
    my $art;
    # Skapa agentobjektet
    my $ua = new LWP::UserAgent;
    $ua->agent(&rand_agent);
    $ua->timeout( 7 );

    # Skapa en f�rsta f�rfr�gan
    my $req = new HTTP::Request GET => "http://www.svd.se/dynamiskt/senaste_nytt/did_" . $id .
      ".asp";

    # Var sn�ll. Pausa lite.
    #sleep rand(10);

    # Skicka f�rfr�gan, avvakta svar
    my $res = $ua->request($req);

    # Testa om det lyckades
    if ($res->is_success) {
	$art = $res->content;
    } else {
	return undef;
    }
    $art =~ m/<\/span><br><span class=brodtext>(.*?)<\/span><br><\/td>/sm;
    $art = $1;
    # �vers�tt till l�sbar text
    $art =~ s/&ouml;/�/gms;
    $art =~ s/&Ouml;/�/gms;
    $art =~ s/&auml;/�/gms;
    $art =~ s/&Auml;/A/gms;
    $art =~ s/&aring;/�/gms;
    $art =~ s/&Aring;/�/gms;
    $art =~ s/&quot;/"/gms;
    $art =~ s/<br>/\n/gms;
    $art =~ s/\n\n/\n/gms;
    $art =~ s/<.*?>//gms; # ta bort alla taggar (hoppas jag!)

    return split /\n/, $art;
}

# h�mtar senaste nytt fr�n Svenska Dagbladet och dumpar det i alla kanaler
# stulet fr�n marvi och modifierat
sub latest_news {
    my $full_article = shift;
    my @to = @_;
    #sleep rand(60);
    #my $date = sprintf "%04d-%02d-%02d", sub {$_[5]+1900, $_[4]+1, $_[3]}->(localtime);

    # H�mta senaste id (om det finns)
    my $lastid=-1;
    $query_thing->execute( '__LATEST_NEWS__' );
    if ($query_thing->rows >= 1) {
	$query_thing->bind_col( 2, \$lastid );
	$query_thing->fetch;
    }
    if ($full_article) {
	msg( "H�mtar senaste nytt till @to" );
    } else {
	msg( "Kollar senaste nytt; enaste id=$lastid" );
    }

    # Skapa agentobjektet
    my $ua = new LWP::UserAgent;
    $ua->agent(rand_agent);
    $ua->timeout( 7 ); # jag har inte tid att v�nta flera minuter

    # Skapa en f�rsta f�rfr�gan
    my $req = new HTTP::Request GET => "http://www.svd.se/div/news/senastenytt.asp";

    # Skicka f�rfr�gan, avvakta svar
    my $res = $ua->request($req);

    # Testa om det lyckades
    unless ($res->is_success) {
	return 0;
    }

    # parsa resultatet
    my @lines = split "\n", $res->content;
    foreach my $line (@lines) {
	next unless ($line =~ /senaste_nytt/);
	$line =~ m/allnewslist">(\d.*?)&nbsp.*?did_(\d+)\.asp">(.*?)<\/A>/;
	my($time, $id, $title) = ($1, $2, $3);
	next unless $id;
	if ($id != $lastid || $full_article) {
	    if ($full_article) {
		msg( "H�mtar artikel $id fr�n SvD." );
		my @art = get_svd_article($id);

		if (@art) {
		    $knet->privmsg( [ @to ], uc( $title ) );
		    foreach (@art) {
			while ($_) {
			    $knet->privmsg( [@to ], substr( $_, 0, 400 ) );
			    substr( $_, 0, 400, '' );
			    sleep 2; # paus f�r att vi inte skall bli utkastade
			}
		    }
		}
	    } else {
		# Dumpa meddelanden p� mina kanaler
		msg( "Skriver notis om artikel p� @to" );
		$knet->privmsg( [ @to ], "SvD $time: $title [$id]" );
	    }

	    # Uppdatera databasen med lastid
	    if ($lastid == -1) {
		$insert->execute( '__LATEST_NEWS__', $id );
	    } else {
		$update->execute( $id, '__LATEST_NEWS__' );
	    }
	}
	last;
    }
}



######################################################################
# Stora funktioner, "handlers"

# vid anslutning; g� med i ett antal kanaler och s�g "hej"
sub on_connect {
    my $self = shift;

    foreach my $channel (@CHANNELS) {
	msg( "G�r med i kanalen '$channel'" );
	$self->join( $channel );
	$self->privmsg( $channel, $hello[ rand scalar @hello ] );
    }
}

# fet funktion som tolkar alla publika och privata meddelanden
sub parse_msg {
    # initiera lite variabler
    my ($self, $event) = @_;
    my ($nick,$mynick) = ($event->nick, $self->nick);
    my ($arg) = $event->args;

    # variabel f�r att lagra svarstexten
    my @response;

    # privat meddelande?
    my $private = $event->type() eq 'public' ? 0 : 1;

    # vem skall jag svara till?
    my (@to) = $private ? $nick : $event->to;

    # om det �r ett privat meddelande, initiera $to_me som sant
    my $to_me = $private ? 1 : 0;

    # skriv ut vad som h�nder
    if ($private) {
	print "[$nick] $arg\n";
    } else {
	print "<$nick/@to> $arg\n";
    }

    # uppdatera historik
#      if( $activity{ $to[0] } ) {
#  	my $delta = timelocal( localtime ) - $activity{ $to[0] };
#  	msg( "Delta: $delta sekunder" );
#      }
    $activity{ $to[0] } = timelocal( localtime ) if (!$private);

    # om personen �r p� ignore-listan, ignorera
    if ($IGNORE{ lc $nick }) {
	msg( "Ignorerar $nick" );
	return;
    }

    # LOGIK:
    #
    # 1. Har meddelandet n�gon mottagare (privat meddelande eller
    #    inleds med "namn: ...")?
    #
    # 2. Dela upp meddelandet i meningar och bearbeta varje mening f�r
    #    sig.
    #
    # 3. F�r varje meddelande, unders�k om det �r en fr�ga, utrop
    #    eller vanligt p�st�ende.
    #
    # 4. �h, l�s k�llkoden.

    # n�gon mottagare?
    if ($arg =~ m/^(\w+)[:,]\s+(.+)/i) {
	$arg = $2;
	if ($1 =~ m/$mynick/i) {
	    $to_me = 1;
	}
    }

    # dela upp i meningar om det �r flera
    my @sentence;
    while ($arg =~ m/^\s*(.+?[\.\?\!])\s+.+/) {
	push @sentence, $1;
	$arg =~ s/^\s*\Q$1\E\s*//;
    }
    push @sentence, $arg;

    # g� igenom varje mening var f�r sig
    foreach (@sentence) {

	my $question = 0;
	my $shout = 0;

	# n�mns jag?
	my $talking_about_me = $arg =~ m/$mynick/i ? 1 : 0;

	# �r det en fr�ga?
	if (m/\?$/) {
	    $question = 1;
	}

	# �r det n�gon som skriker?
	if (m/\!$/) {
	    $shout = 1;
	}

	# h�r kommer nu en stor if-elsif-konstruktion (som skulle ha
	# gjorts med switch i C), eftersom det bara finns ett t�nkbart
	# alternativ
	#
	# kommandon och korta f�rdefinierade fraser kollas f�rst, s�
	# att det inte uppst�r n�gra tveksamheter eller felaktigheter
	# senare i logiken

	# hj�lp?
	if ($to_me && m/^hj�lp[\!\?\.]?$/i) {
	    msg( "Ger hj�lp. Fast inte mycket." );
	    push @response, 'H�ll i hatten!';
	}

	# debug - sl� p�/av debug
	elsif ($to_me && m/^(debug|avlusa)$/i) {
	    if ($self->debug) {
		$self->debug(0);
		msg( "Inaktiverar debugging" );
		push @response, 'Uppfattat. Inaktiverar avlusning.';
	    } else {
		$self->debug(1);
		msg( "Aktiverar debugging" );
		push @response, 'Uppfattat. Aktiverar avlusning.';
	    }
	}

	# skall jag dra �t skogen?
	elsif ($shout && $to_me && m/(stick|f�rsvinn|dunsta|pys|d�)/i 
	       && $nick =~ m/stefan/i) {
	    msg( "Avslutar" );
	    $query_thing->finish;
	    $dbh->disconnect;
	    $self->quit( "Waaaaaahhhhhhh!!" );
	}

	# skall jag g� med i en kanal?
	elsif ($private && m/^(join|joina|g� till|g� med i)\s+(\#\w+)/i) {
	    my $chan = $2;
	    msg( "G�r till kanalen '$chan'" );
	    $self->join( $chan );
	}

	# l�mna kanal
	elsif ($private && m/^(part|leave|l�mna|g� ur)\s+(\#\w+)/i) {
	    my $chan = $2;
	    msg( "L�mnar kanalen '$chan'" );
	    $self->part( $chan );
	}

	# s�k efter beskrivning
	elsif ($to_me && !$question && m/^s�k\s+(efter\s+)?"?(.+)/i) {
	    # f�rbered pattern
	    my $thing = $2;
	    $thing =~ s/["\!\?\.\s]*$//;
	    $thing = '%'.$thing.'%';

	    # exekvera s�kning
	    $search->execute( $thing );

	    # hur mycket hittade jag?
	    my $rows = $search->rows;
	    msg( "Hittade $rows ord som matchade '$thing'." );

	    # lite variabler
	    my $name;
	    my $result = '';
	    my $i = 0;

	    # s�tt ihop ett svar
	    $search->bind_columns( \$name );
	    while ($search->fetch && length( $result ) < 200 ) {
		$result = $result ? $result.', '.$name : $name;
		$i++;
	    }
	    $result .= " ($i av $rows ord visade)" if ($i < $rows);

	    # s�g svar
	    if ($result) {
		if ($private) {
		    push @response, "T�nkbara ord �r $result."
		} else {
		    push @response, "$nick: T�nkbara ord �r $result."
		}
	    } else {
		push @response, "Hittade inget, $nick."
	    }
	}

	# h�lsa artigt
	elsif (m/^(hej|hejsan|hall�)\s+($mynick|alla|p� er)[\s\!\?\.]*$/i ||
	       (m/^hej[\s\!\?\.]*$/i && $to_me)) {
	    push @response, sprintf( $hellonick[ rand scalar @hellonick ],
				     $nick );
	    msg( "H�lsar artigt." );
	}

	# �r det en fr�ga, f�rs�k besvara den
	elsif (m/^(en|ett|och)?\s*(\S+|".*?")\s+�r\s+(ocks�\s*)?(.+)$/i && !$question) {
	    my ($thing,$desc) = ($2, $4);
	    my $unconditional = $3 ? 1 : 0;

	    # ta bort punkter och annat skr�p
	    $desc =~ s/[\.\?\!\s]*$//;
	    $thing =~ s/^"//;
	    $thing =~ s/"$//;

	    # byt ut "du" och "jag"
	    $thing = $mynick if ($thing =~ m/^du$/i);
	    $thing = $nick if ($thing =~ m/^jag$/i);

	    # kolla om det �r ett ord jag inte vill l�ra mig
	    if ($thing =~ m/^(den|det|vi|oss|de|dem|dom|en|ett|han|hon|allt|sex|vad|h�r|d�r|var|varf�r|saken|vem)$/i) {
		push @response, 'Nej.' if (!@response && $to_me);
		msg( "Ogiltigt ord '$thing'." );
	    } else {
		if ($shout) {
		    # om folk skriker, vill jag inte vara med
		    push @response, "Jag lyssnar inte p� folk som skriker."
		      if ($to_me && !@response);
		    msg( "Folk som skriker ignorerar jag." );
		} else {

		    # kolla om det redan finns i databasen
		    $query_thing->execute( $thing )
		      or die "execute failed";

		    if ($query_thing->rows > 0 && !$unconditional) {
			msg( "'$thing' finns redan i databasen." );

			# Byt ut pronomen
			$thing = 'du' if ($thing eq $nick);
			$thing = 'jag' if ($thing eq $mynick);

			push @response, create_response( $query_thing,
				'Nej, %s �r', $thing )
			  if ($to_me);
		    } else {

			# uppdatera databasen
			$insert->execute( $thing, $desc )
			  or die "insert failed";

			if ($insert->rows == 1) {
			    push @response, "Ok, $nick." if ($to_me);
			    msg( "L�rde mig att '$thing' �r '$desc'" );
			} else {
			    push @response, "Nu blev det blev n�got fel.";
			    msg( "Det gick inte att uppdatera databasen!" );
			}
		    }
		}
	    }
	}

	# �r det n�got jag borde gl�mma?
  	elsif (m/^gl�m\s+(bort\s+)?(.+)/i && !$question && $to_me) {
  	    my $thing = $2;

	    # strippa punkter och andra tecken fr�n sak
  	    $thing =~ s/[\.\?\!\s]*$//;

	    # radera
	    $delete->execute( $thing )
	      or die "execute failed";

	    # hur m�nga rader raderades?
	    my $rows_deleted = $query_thing->rows;

	    # s�k om det finns n�got kvar
	    $query_thing->execute( $thing )
	      or die "search failed";

	    # hur m�nga rader finns kvar?
	    my $rows_left = $query_thing->rows;

	    # skriv vad som h�nt
	    msg( "Raderade $rows_deleted rader med '$thing'; $rows_left kvar." );

	    # tala om vad som h�nt
	    if ($rows_deleted > 0) {
		if ($rows_left == 0) {
		    push @response, "Ok, $nick, jag har gl�mt allt om $thing.";
		} else {
		    push @response, "Ok, $nick, men $rows_left f�rklaringar till $thing �r of�rgl�mliga.";
		}
	    }
	}

	# om det �r en fr�ga, f�rs�k besvara den
	elsif ($question) {
	    my $thing;
	    if (m/^(vad|vem|hur)\s+(var|�r)\s+(.+)/i) {
		$thing = $3;
	    } else {
		$thing = $_;
	    }

	    # ta bort dumma tecken fr�n sak
	    $thing =~ s/[\.\?\!\s]*$//;

	    # byt ut "du" och "jag"
	    $thing = $mynick if ($thing =~ m/^du$/i);
	    $thing = $nick if ($thing =~ m/^jag$/i);

	    # lite speciella ord
	    if ($thing =~ m/^(allt|sex)$/i) {
		push @response, "Allt handlar om sex!";
		msg( "Allt handlar om sex! Oh yeah!" );
	    }
	    elsif ($thing =~ m/^(senaste nytt)$/i) {
		msg( "Kollar senaste nytt" );
		latest_news( 1, @to );
		return;
	    }
	    else {

		# s�k efter ord i databasen
		$query_thing->execute( $thing )
		  or die "Execute failed";

		# hur mycket hittade jag?
		my $rows = $query_thing->rows;

		msg( "Hittade $rows f�rklaringar till '$thing'" );

		# Byt ut pronomen
		$thing = 'du' if ($thing eq $nick);
		$thing = 'jag' if ($thing eq $mynick);

		# Svara med vad jag hittade
		if ($rows > 0) {
		    push @response, create_response( $query_thing,
					 $responses[ rand scalar @responses ],
						     $thing );
		} else {
		    push @response, "Jag vet inte, $nick."
		      if ($to_me);
		}
	    }
	}

	# �r jag omn�mnd eller tilltalad?
	elsif ($to_me) {
	    push @response, $whoami[ rand scalar @whoami ]
	      if (!@response);
	}
    }

    # Om jag har n�got att s�ga, s�g det.
    if (@response) {
	print "<$mynick/@to> @response\n";
	$self->privmsg( [ @to ], join( ' ', @response ) );
    }
}

# stulna rakt av fr�n irctest:

# Change our nick if someone stole it.
sub on_nick_taken {
    my ($self) = shift;

    $self->nick(substr($self->nick, -1) . substr($self->nick, 0, 8));
}

# Reconnect to the server when we die.
sub on_disconnect {
	my ($self, $event) = @_;

	print "F�rlorade kontakten med ", $event->from(), " (",
	      ($event->args())[0], "). D� g�r vi och l�gger oss...\n";

	print "St�nger ned databas.\n";
	$self->connect();
}

# Look at the topic for a channel you join.
sub on_topic {
	my ($self, $event) = @_;
	my @args = $event->args();

	# Note the use of the same handler sub for different events.

	if ($event->type() eq 'notopic') {
	    print "*** No topic set for $args[1].\n";

	# If it's being done _to_ the channel, it's a topic change.
	} elsif ($event->type() eq 'topic' and $event->to()) {
	    print "*** Topic change for ", $event->to(), ": $args[0]\n";

	} else {
	    print "*** The topic for $args[1] is \"$args[2]\".\n";
	}
}

# Prints the names of people in a channel when we enter.
sub on_names {
    my ($self, $event) = @_;
    my (@list, $channel) = ($event->args);    # eat yer heart out, mjd!

    # splice() only works on real arrays. Sigh.
    ($channel, @list) = splice @list, 2;

    print "*** Anv�ndare p� $channel: @list\n";
}

# What to do when someone joins a channel the bot is on.
sub on_join {
    my ($self, $event) = @_;
    my ($channel) = ($event->to)[0];
    my ($nick, $mynick) = ($event->nick, $self->nick);

    print "*** $nick kommer in i $channel\n";

    # registrera tidpunkt
    my $now = timelocal( localtime );

    # om jag vet n�r n�got h�nde sist, kolla om vi skall h�lsa
    if ($activity{ $channel }) {

	# H�lsa om det var l�nge sedan n�got h�nde
	if ($now - $activity{ $channel } > 900 && $mynick ne $nick) {
	    msg( "V�lkomnar $nick till $channel" );
	    $self->privmsg( $channel, 
			    sprintf( $welcome[ rand scalar @welcome ],
				     $nick,
				     $channel,
				     say_time( $now - $activity{ $channel } )
				   )
			  );
	}
    }
    $activity{ $channel } = $now;
}

# N�gon sticker
sub on_quit {
    my ($self, $event) = @_;
    my ($channel) = ($event->to)[0];
    my ($nick, $mynick) = ($event->nick, $self->nick);

    #$event->dump;

    print "*** $nick st�ngde av\n";
    # jag vet inte vilken kanal det var
    #$self->privmsg( $channel, "Hejd�, $nick!" );
}

# What to do when someone leaves a channel the bot is on.
sub on_part {
    my ($self, $event) = @_;
    my ($channel) = ($event->to)[0];
    my ($nick, $mynick) = ($event->nick, $self->nick);

    print "*** $nick l�mnade $channel\n";
    $self->privmsg( $channel, "Hejd�, $nick!" )
	if ($nick ne $mynick);
}

# Handles some messages you get when you connect
sub on_init {
    my ($self, $event) = @_;
    my (@args) = ($event->args);
    shift (@args);

    print "*** @args\n";
}

# n�r n�gon blir kickad
sub on_kick {
    my ($self, $event) = @_;
    my ($nick, $mynick) = (($event->to)[0], $self->nick);
    my ($channel, $kicker) = ($event->args);

    if ($nick eq $mynick) {
	print "*** Jag blev kickad fr�n $channel av $kicker!\n";
	$self->join( $channel );
	$self->privmsg( $channel, "Sluta! $kicker �r dum!" );
    } else {
	print "*** $kicker sparkade ut $nick fr�n $channel\n";
    }
}

# Display formatted CTCP ACTIONs.
sub on_action {
    my ($self, $event) = @_;
    my ($nick, @args) = ($event->nick, $event->args);

    print "* $nick @args\n";
}

# jag tar �ver den f�rdefinierade pinghanteraren; h�r kan jag stoppa
# in skojiga events
sub on_ping {
    my ($self, $event) = @_;
    my $verbose = $self->verbose;

    $self->sl("PONG " . (CORE::join ' ', $event->args));

    print "*** PING->PONG\n";

    # kolla klockslag
    my $now = timelocal( localtime );

    # skriv hur l�ng tid som f�rflutit sedan senaste ping
    if ($activity{__LASTPING__}) {
	msg( sprintf( 'Intervall: %ss', $now - $activity{__LASTPING__} ) );
    }
    $activity{__LASTPING__} = $now;


    # titta efter nyheter varje kvart
    if ($now - $activity{__LATEST_NEWS__} > 420) {
	$activity{__LATEST_NEWS__} = $now;
	latest_news( 0, @CHANNELS );
    }

    return 1;
}
