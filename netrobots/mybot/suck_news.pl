#!/usr/bin/perl
#
# Time-stamp: <2001-03-17 16:34:12 marvi>
#
# Program som h�mtar nyheter fr�n http://www.svd.se
#

use strict;
use LWP::UserAgent;
use LWP::Simple;
use locale;

#$|++;

my $lastid=15212930;

#open LIST, ">nyhet_lista.shtml" or die($!);
#open NEWS, ">nyheter.txt" or die($!);

#&return_urls(&get_list);

&latest_news;

#&print &get_list;

#close LIST;
#close NEWS;

sub rand_agent {
    my @agents = ("Mozilla/4.5 [en] (WinNT;I)", "Mozilla/4.71 [en]C-AtHome0407 (Win98; I)",
		  "Mozilla/4.72 [sv] (WinNT;I)", "Mozilla/5.0 [en-US] (Linux; I)", "Mozilla/5.0 [sv-SE]
(Windows_NT; I)");
    return $agents[rand scalar @agents];
}

sub latest_news {
    #sleep rand(60);
    my $date = sprintf "%04d-%02d-%02d", sub {$_[5]+1900, $_[4]+1, $_[3]}->(localtime);
    # Skapa agentobjektet
    my $ua = new LWP::UserAgent;
    $ua->agent(&rand_agent);
    $ua->timeout( 7 );

    # Skapa en f�rsta f�rfr�gan
    my $req = new HTTP::Request GET => "http://www.svd.se/div/news/senastenytt.asp";

    # Skicka f�rfr�gan, avvakta svar
    my $res = $ua->request($req);

    # Testa om det lyckades
    unless ($res->is_success) {
	return 0;
    }

    my $indata = $res->content;

    my @lines = split "\n", $indata;
    foreach my $line (@lines) {
	next unless ($line =~ /senaste_nytt/);
	$line =~ m/allnewslist">(\d.*?)&nbsp.*?did_(\d+)\.asp">(.*?)<\/A>/;
	my($time, $id, $title) = ($1, $2, $3);
	next unless $id;
	print "id=$id\n";
	if ($id != $lastid) {
	    my @art = get_svd_article($id);

	    print uc( $title ), "\n\n";;
	    foreach (@art) {
		while ($_) {
		    print substr( $_, 0, 240 ), "\n\n";
		    substr( $_, 0, 240, '' );
		}
	    }
	    print "(SvD $time)\n\n";
	}
	last;
    }
}

sub get_svd_article {
    my $id = shift;
    my $art;
    # Skapa agentobjektet
    my $ua = new LWP::UserAgent;
    $ua->agent(&rand_agent);
    $ua->timeout( 7 );

    # Skapa en f�rsta f�rfr�gan
    my $req = new HTTP::Request GET => "http://www.svd.se/dynamiskt/senaste_nytt/did_" . $id .
      ".asp";

    # Var sn�ll. Pausa lite.
    #sleep rand(10);

    # Skicka f�rfr�gan, avvakta svar
    my $res = $ua->request($req);

    # Testa om det lyckades
    if ($res->is_success) {
	$art = $res->content;
    } else {
	return undef;
    }
    open OUT, ">dump.html";
    print OUT $art;
    close OUT;
    $art =~ m/<\/span><br><span class=brodtext>(.*?)<\/span><br><\/td>/sm;
    $art = $1;
    # �vers�tt till l�sbar text
    $art =~ s/&ouml;/�/gms;
    $art =~ s/&Ouml;/�/gms;
    $art =~ s/&auml;/�/gms;
    $art =~ s/&Auml;/A/gms;
    $art =~ s/&aring;/�/gms;
    $art =~ s/&Aring;/�/gms;
    $art =~ s/&quot;/"/gms;
    $art =~ s/<br>/\n/gms;
    $art =~ s/\n\n/\n/gms;
    #$art =~ s/(\/images\/ettan)/http:\/\/www.svd.se$1/sm;
    return split /\n/, $art;
}
