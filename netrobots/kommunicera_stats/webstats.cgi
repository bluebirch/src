#!/usr/bin/perl -w
#
# $Id$
#
# Visa statistik fr�n databasen.
#
# $Log$
# Revision 1.1  2002/12/10 06:09:00  stefan
# Enkelt script f�r att visa statistiken som en webbsida.
#
#
use strict;
use warnings;
use HTML::Template;
use DBI;

# �ppna session mot databasen
my $dbh = DBI->connect( 'DBI:mysql:dumburken_statistics:localhost', 'stefan', 'slempropp' );

my $stats = $dbh->prepare( q{SELECT CONCAT(YEAR(date),MONTH(date)) AS period, SUM(sent/1024) AS sent, SUM(rcvd/1024) AS rcvd, SUM((sent+rcvd)/1024) AS total FROM isp_usage GROUP BY period} );

if ($stats->execute()) {
    my $data = $stats->fetchall_arrayref( {} );

    my $t = HTML::Template->new( filename => 'webstats.tmpl' );

    $t->param( data => $data );

    print "Content-Type: text/html\n\n";
    print $t->output;
}

