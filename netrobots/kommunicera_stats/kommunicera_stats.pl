#!/usr/bin/perl -w
#
# $Id$
#
# Enkelt perlscript f�r att h�mta statistik fr�n
# http://www.kommunicera.umea.se/ lagra det i en databas.
#
# $Log$
use strict;
use warnings;
use LWP::UserAgent;
use DBI;

my $server = 'http://www.kommunicera.umea.se/';

# Skapa UserAgent
my $ua = new LWP::UserAgent;
$ua->agent( 'Mozilla/5.0' );

# Anv�nd kakor. N�dv�ndigt eftersom den dumma servern s�tter en massa
# sessionskakor i klienten.
$ua->cookie_jar( {} );

# F�rst m�ste vi h�mta default.asp och s�tta sessionskakor f�r att
# webservern skall ge oss statistiksidan
my $req = HTTP::Request->new( 'GET', $server . 'default.asp' );
my $res = $ua->request( $req );
unless ($res->is_success) {
    print STDERR "Kunde inte h�mta default.asp\n";
    exit 2;
}

# H�mta statistiksidan
$req = HTTP::Request->new( 'GET', $server . 'manadstrafik.asp' );
$res = $ua->request( $req );
unless ($res->is_success) {
    print STDERR "Kunde inte h�mta manadstrafik.asp\n";
    exit 2;
}

my $page = $res->content;

# �ppna session mot databasen
my $dbh = DBI->connect( 'DBI:mysql:dumburken_statistics:localhost', 'stefan', 'slempropp' );

my $try_date = $dbh->prepare( q{SELECT date FROM isp_usage WHERE date=?} );
my $insert = $dbh->prepare( q{INSERT INTO isp_usage VALUES (?,?,?)} );
my $update = $dbh->prepare( q{UPDATE isp_usage SET sent=?, rcvd=? WHERE date=?} );

# Plocka ut intressanta v�rden fr�n statistiksidan
while ($page =~ m{<TD>(\d{8})</TD>.*?<td align="right">(\d+)</TD>.*?<td align="right">(\d+)</TD>}sg) {
    my ($date, $sent, $rcvd) = ($1, $2, $3);
    $date =~ s/(\d\d\d\d)(\d\d)(\d\d)/$1-$2-$3/;
#    print "$date $sent $rcvd";

    # Try date
    $try_date->execute( $date );

    # Update or insert new values
    if ($try_date->rows == 1) {
#	print "; updating\n";
	$update->execute( $sent, $rcvd, $date );
    }
    else {
#	print "; adding\n";
	$insert->execute( $date, $sent, $rcvd );
    }
    $try_date->finish;
}
