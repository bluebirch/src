#!/usr/bin/perl -w
#
# $Id$
#
# Script f�r att s�ka i jobbdatabaser.
#
# $Log$
# Revision 1.3  2002/02/01 09:10:25  stefan
# Lade till en hel del kommandoradsv�xlar. Styrning om vem som f�r mail
# etc sker nu med hj�lp av kommandoraden.
#
# Revision 1.2  2002/01/28 14:31:50  stefan
# Lade till StepStone.
#
# Revision 1.1  2002/01/28 14:01:04  stefan
# En f�rsta version.
#
#

use strict;
use Netsuck;
use Mail::Send;
use Getopt::Std;

my $last_file = ".jobsuck";

my %opt;

exit 2 unless (getopts( "nd:m:D", \%opt ));

my $net = new Netsuck;

$net->debug( $opt{D} );

# H�mta senaste datum
my %date;
if (open LAST, $last_file) {
    print STDERR "l�ser $last_file\n" if ($opt{D});
    while (<LAST>) {
	chomp;
	my ($source, $date) = split;
	$date{$source} = $date;
    }
    close LAST;
}

# Se till att datum �r satt
print STDERR "s�tter datum\n" if ($opt{D});
my @sources = qw{ams csjobb smartjobb jobline stepstone};
foreach (@sources) {
    if ($opt{d}) {
	$date{$_} = $opt{d};
    } elsif (!$date{$_}) {
	$date{$_} = '2002-01-01';
    }
    print STDERR "  $_ = $date{$_}\n" if ($opt{D});
}

# H�mta jobb fr�n AMS
print STDERR "h�mtar platsannonser fr�n AMS Internet\n" if ($opt{D});
my @ams = $net->ams( 20911, 800000, $date{ams} );
$date{ams} = $ams[0]{date} if (scalar @ams > 0);
print STDERR "  fann ", scalar @ams, " jobb; datum $date{ams}\n" if ($opt{D});

# H�mta jobb fr�n Computer Sweden
print STDERR "h�mtar platsannonser fr�n Computer Sweden\n" if ($opt{D});
my @csjobb = $net->csjobb( 'Norrland', $date{csjobb} );
$date{csjobb} = $csjobb[0]{date} if (scalar @csjobb > 0);
print STDERR "  fann ", scalar @csjobb, " jobb; datum $date{csjobb}\n" if ($opt{D});

# H�mta jobb fr�n Smartjobb
print STDERR "h�mtar platsannonser fr�n Smartjobb\n" if ($opt{D});
my @smartjobb = $net->smartjobb( 109, $date{smartjobb} );
$date{smartjobb} = $smartjobb[0]{date} if (scalar @smartjobb > 0);
print STDERR "  fann ", scalar @smartjobb, " jobb; datum $date{smartjobb}\n" if ($opt{D});

# H�mta jobb fr�n jobline
#  print STDERR "h�mtar platsannonser fr�n Jobline\n" if ($opt{D});
#  my @jobline = $net->jobline( 1577, $date{jobline} );
#  $date{jobline} = $jobline[0]{date} if (scalar @jobline > 0);
#  print STDERR "  fann ", scalar @jobline, " jobb; datum $date{jobline}\n"
#    if ($opt{D});


# H�mta jobb fr�n StepStone
#  print STDERR "h�mtar platsannonser fr�n StepStone\n" if ($opt{D});
#  my @stepstone, $net->stepstone( 211, $date{stepstone} );
#  $date{stepstone} = $stepstone[0]{date} if (scalar @stepstone > 0);
#  print STDERR "  fann ", scalar @stepstone, " jobb; datum $date{stepstone}\n"
#    if ($opt{D});

my @jobs = (@ams, @csjobb, @smartjobb); # @jobline, @stepstone);
print STDERR "fann totalt ", scalar @jobs, " jobb\n" if ($opt{D});

# Sortera listan
@jobs = sort {$$b{date} cmp $$a{date}} @jobs;

# Skriv ut
if (scalar @jobs > 0) {

    if ($opt{m}) {
	my @recipients = split /[:, ]/, $opt{m};

	foreach my $to (@recipients) {

	    print STDERR "skickar mail till $to\n" if ($opt{D});

	    my $msg = new Mail::Send;

	    $msg->to( $to );
	    $msg->subject( 'Nya platsannonser' );

	    my $fh = $msg->open;

	    print $fh scalar @jobs, " nya platsannonser:\n\n";
	    foreach my $ref (@jobs) {
		printf $fh "%-10s %-45s %s\n", $$ref{date}, $$ref{title}, $$ref{src};
		printf $fh "           %s\n\n", $$ref{employer};
	    }

	    $fh->close;
	}
    } else {
	foreach my $ref (@jobs) {
	    printf "%-10s %-45s %s\n", $$ref{date}, $$ref{title}, $$ref{src};
	    printf "           %s\n", $$ref{employer};
	    print "\n";
	}
    }
} elsif ($opt{D}) {
    print STDERR "inga nya platsannonser\n"
}

# Spara senaste datum
unless ($opt{n}) {
    print STDERR "skriver $last_file\n" if ($opt{D});
    open LAST, ">$last_file" or die;
    foreach (keys %date) {
	print LAST "$_ $date{$_}\n";
    }
    close LAST;
}

