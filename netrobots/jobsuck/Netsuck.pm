# $Id$
#
# Klass f�r att h�mta saker fr�n n�tet. Stulen fr�n Robot.
#
# $Log$
# Revision 1.3  2002/02/01 09:12:20  stefan
# Lite b�ttre felhantering om URL:erna �r on�bara.
#
# Revision 1.2  2002/01/28 14:32:04  stefan
# Lade till StepStone. Borde snygga till Jobline.
#
# Revision 1.1  2002/01/28 14:01:04  stefan
# En f�rsta version.
#

package Netsuck;
use strict;
use locale;
use LWP::UserAgent;
use HTML::Entities;

sub new {
    my $proto         = shift;
    my $class         = ref($proto) || $proto;
    my $self          = {};
    $self->{list}     = [];
    $self->{ua}       = new LWP::UserAgent;

    bless( $self, $class );

    # Ange timeout f�r UserAgent
    $self->{ua}->timeout( 7 );

    # Ange agent
    $self->{ua}->agent( $self->rand_agent );

    return $self;
}

######################################################################
# H�mta lediga jobb fr�n AMS Internet
#
sub ams {
    my ($self, $region, $category, $first_date) = @_;

    my @result;

    my $page = 1;

    # H�mta f�rsta sidan
    while (my $data = $self->get_url( "http://platsbanken.ams.se/region/$region/$category,$page.html" )) {

	# Bearbeta alla jobb p� den sidan
	while ($data =~ m:<TD CLASS="PbText" VALIGN="top">(.*?)</TD> <TD CLASS="PbText"><B><A HREF="(.*?)">(.*?)(;\s*(.*?))?</A></B>; (.*?); (.*?)(;.*?)?</TD>:g) {
	    my $date = $1;
	    my $url = $2;
	    my $title = decode_entities( $3 );
	    my ($employer, $city);
	    if ($4) {
		$employer = decode_entities( $5 );
		$city = decode_entities( $6 );
	    } else {
		$employer = decode_entities( $6 );
		$city = decode_entities( $7 );
	    }

	    # Donka till datumet lite
	    $date =~ s/(\d\d)(\d\d)(\d\d)/20$1-$2-$3/;

	    print STDERR "  $date: $title - " if ($self->debug);

	    # L�gg till endast om det matchar s�kt datum
	    if ($date gt $first_date) {
		my $ref = { date => $date,
			    url => $url,
			    title => $title,
			    employer => $employer,
			    city => $city,
			    src => 'AMS' };
		push @result, $ref;
		print STDERR "ok\n" if ($self->debug);
	    } elsif ($self->debug) {
		print STDERR "gammal\n";
	    }
	}

	# G� till n�sta sida
	$page++;
    }

    return @result;
}

######################################################################
# H�mta lediga jobb fr�n ComputerSweden
#
sub csjobb {
    my ($self, $region, $first_date) = @_;

    my $data = $self->get_url( "http://csjobb.idg.se/Arbetsgivaren.nsf/SearchLedigaJobbTraff?OpenForm&karta2=$region&fritextFld=&sorteringFld=PublishStartTime&antalFld=50&x=22&y=11&pageNum=1&sortOrder=1&id=2" );

    my @result;
    while ($data =~ m{<A HREF="(/Arbetsgivaren.nsf/All/.*?)" STYLE="color: #990000;">(.*?)</A></FONT></TD><TD WIDTH="15"></TD><TD NOWRAP><FONT FACE="verdana,arial,helvetica,sans-seriff" SIZE=1 ID="traffTxt">(.*?)</FONT></TD><TD WIDTH="15"></TD><TD NOWRAP><FONT FACE="verdana,arial,helvetica,sans-seriff" SIZE=1 ID="traffTxt">(.*?)</FONT></TD><TD WIDTH="15"></TD><TD NOWRAP><FONT FACE="verdana,arial,helvetica,sans-seriff" SIZE=1 ID="traffTxt">(.*?)</FONT></TD></TR>}g) {

	print STDERR "  $5: $3 - " if ($self->debug);

	if ($5 gt $first_date) {
	    push @result,  { url => "http://csjobb.idg.se/$1",
			     employer => $2,
			     title => $3,
			     city => $4,
			     date => $5,
			     src => 'CSjobb' };
	    print STDERR "ok\n" if ($self->debug);
	} elsif ($self->debug) {
	    print STDERR "gammal\n";
	}
    }

    return @result;
}

######################################################################
# H�mta lediga jobb fr�n VK Smartjobb
#
sub smartjobb {
    my ($self, $region, $first_date) = @_;

    my $data = $self->get_url( "http://smartjobb.vk.se/sokare/sokresultat.asp?yrkeid=-1&ortid=109" );

    my @result;

    while ($data =~ m{<TR[^>]*?>[^>]*?<TD valign=top align=left><FONT[^>]*?>([^>]*?)</TD>[^>]*?<TD valign=top align=left><FONT[^>]*?><a href="([^>]*?)"><img src="/pics/pilfram.gif" width="5" height="7" border="0"> <a href="[^>]*?">([^>]*?)</a></FONT></TD>[^>]*?<TD valign=top align=left><FONT[^>]*?>([^>]*?)</FONT></TD>[^>]*?<TD valign=top align=left><FONT[^>]*?>([^>]*?)</FONT></TD>[^>]*?</TR>}gs) {

	my @date = split m{/}, $1;
	my $date = sprintf( '%04d-%02d-%02d', (localtime)[5]+1900, $date[1], $date[0] );

	print STDERR "  $date: $3 - " if ($self->debug);

	if ($date gt $first_date) {
	    push @result, { date => $date,
			    url => $2,
			    title => $3,
			    employer => $4,
			    city => $5,
			    src => 'Smartjobb' };
	    print STDERR "ok\n" if ($self->debug);
	}
	elsif ($self->debug) {
	    print STDERR "gammal\n";
	}
    }
    return @result;
}

######################################################################
# H�mta lediga jobb fr�n Jobline
#
sub jobline {
    my ($self, $region, $first_date) = @_;

    my @result;

    my $data = $self->get_url( "http://www.jobline.se/R1/Jobopportunities/R1_SearchResult.asp?cboCategory=0&cboAddCategory=0&cboRegionEmpty=$region&cboRegion=1577&UserType=0&strCriteria=&cboPage=1&cboAdsPerPage=100" );

    return @result unless ($data);

    # rensa bort fula script
    $data =~ s/<script.*?\/script>//gsi;
    # ta bort mellanslag och whitespace
    $data =~ s/ {2,}//g;
    $data =~ s/[\t\r]//g;
    # rensa bort all html
    $data =~ s/[ \t\r]*<.*?>[ \t\r]*//gs;
    # splitta p� radbrytning
    my @lines = split /\n+/, $data;

    # s�k data
    my $offset = 0;
    while ($offset <= $#lines) {
	$offset++;
	last if ($lines[$offset-1] eq 'Publicerade');
    }
    # Plocka ut annonserna
    while ($offset <= $#lines) {

	my $title = $lines[$offset++];
	my $employer = $lines[$offset++];
	my $date = $lines[$offset++];

	last unless ($date && $date =~ m/\d\d\d\d-\d\d-\d\d/);

	if ($date gt $first_date) {
	    my $ref = { date => $date,
			title => $title,
			employer => $employer,
			src => 'Jobline' };

	    push @result, $ref;
	}
    }
    return @result;
}

######################################################################
# H�mta lediga jobb fr�n StepStone
#
sub stepstone {
    my ($self, $region, $first_date) = @_;

    my @result;

    # H�mta s�kresultat
    my $data = $self->get_url( "http://www.stepstone.se/sok/liste.html?dummy=dummy&pres=1&sok=avansert&xsok=::EFTS::::geo::,$region," );

    return @result unless ($data);

    while ($data =~ m:<td align=left valign=top bgcolor="#EEEEEE">.*?<font size=2>.*?<a href="(.*?)".*?>\s*(.*?)\s*</a>.*?</font>.*?</td>.*?<td valign=top bgcolor="#EEEEEE">.*?<font size=2>\s*(.*?)\s*</font>.*?</td>.*?<td valign=top bgcolor="#EEEEEE">.*?<font size=2>\s*(.*?)\s*</font>.*?</td>.*?<td valign=top bgcolor="#EEEEEE">.*?<font size=2>\s*(.*?)\s*</font>.*?</td>:gs) {

	my ($url, $title, $employer, $city, $date) = ($1, $2, $3, $4, $5);

	# Snygga till datumet
	$date =~ s/(\d\d)\.(\d\d)\.(\d\d\d\d)/$3-$2-$1/;

	if ($date gt $first_date) {
	    my $ref = { date => $date,
			title => $title,
			employer => $employer,
			city => $city,
			url => $url,
			src => 'StepStone' };

	    push @result, $ref;
	}
    }
    return @result;
}


######################################################################
# Diverse bra funktioner f�r internetkommunikation
#

# S�tt l�mplig agent. Stulet fr�n Markus H�rnvi <marvi@kristnet.org>.
sub rand_agent {
    my $self = shift;
    my @agents = ( "Mozilla/4.5 [en] (WinNT;I)",
		   "Mozilla/4.71 [en]C-AtHome0407 (Win98; I)",
		   "Mozilla/4.72 [sv] (WinNT;I)",
		   "Mozilla/5.0 [en-US] (Linux; I)",
		   "Mozilla/5.0 [sv-SE] (Windows_NT; I)");
    return $agents[rand scalar @agents];
}

# H�mta angiven URL och returnera inneh�llet
sub get_url {
    my $self = shift;
    my $url = shift;

    print STDERR "  h�mtar [$url]\n" if ($self->debug);

    # Skapa f�rfr�gan
    my $req = new HTTP::Request GET => $url;

    # Skicka iv�g f�rfr�gan
    my $res = $self->{ua}->request( $req );

    print STDERR "  misslyckades!\n" if ($self->debug && !$res->is_success);

    # Returnera inneh�ll
    return $res->is_success ? $res->content : undef;
}

# Debug
sub debug {
    my $self = shift;
    $self->{debug} = shift if (@_);
    return $self->{debug};
}

1; # Allt �r sant!
