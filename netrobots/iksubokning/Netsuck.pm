# $Id$
#
# Klass f�r att h�mta saker fr�n n�tet. Stulen fr�n Robot.
#
#

package Netsuck;
use strict;
use locale;
use LWP::UserAgent;
use HTML::Entities;
use Date::Calc qw(:all);

sub new {
    my $proto         = shift;
    my $class         = ref($proto) || $proto;
    my $self          = {};
    $self->{ua}       = new LWP::UserAgent;
    $self->{debug}    = 0;

    bless( $self, $class );

    # Ange timeout f�r UserAgent
    $self->{ua}->timeout( 60 );

    # Ange agent
    $self->{ua}->agent( $self->rand_agent );

    # S�tt spr�k i Date::Calc
    Language( 9 );

    return $self;
}

######################################################################
# h�mta bokningstider
#
sub lediga_tider {
    my $self = shift;

    my $resource = 3473; # badminton
    my $card = 4284; # mitt guldkort

    my $result = [];

    for my $offset (0..8) {
	my ($year, $month, $day) = Add_Delta_Days( Today, $offset );

	print STDERR "S�ker lediga tider f�r ", Date_to_Text_Long( $year, $month, $day ), "..." if ($self->debug);

	my $data = $self->get_url( "http://www.iksu.se/iksubokning/Resources/ClientResources.asp?BookRes_ID=$resource&Comp_ID=13&C=&UL=6&L=1&YNr=$year&MNr=$month&DNr=$day&CU=$card" );

	unless ($data) {
	    print STDERR " misslyckades!\n" if ($self->debug);
	    next;
	}

	print STDERR " (", length( $data ), " bytes)" if ($self->debug);

	while ($data =~ m{<td width='5' bgcolor='green'>&nbsp;</td>.*?<td ><a href='(.*?)'>(.*?)</a></td>}sg) {
	    my $r = { year => $year,
		      month => $month,
		      day => $day,
		      url => $1,
		      time => $2 };

	    $$r{url} =~ s{^\.\.}{http://www.iksu.se/iksubokning};
	    $$r{time} =~ m{(\n\n:\n\n)-(\n\n:\n\n)};
	    $$r{start} = $1 if ($1);
	    $$r{end} = $2 if ($2);

	    push @$result, $r;
	    print STDERR " [", scalar @$result, "]" if ($self->debug);
	}
	print STDERR "\n" if ($self->debug);
    }
    return $result;
}

######################################################################
# boka
#
sub boka {
    my ($self, $url, $password) = @_;

    print STDERR "h�mtar bokningsformul�r\n" if ($self->debug);
    my $data = $self->get_url( $url );

    print STDERR "kolla parametrar\n" if ($self->debug);
    my %param;
    while ($data =~ m/<input.*?type=["'](.*?)["'](.*?name=["'](.*?)['"])?(.*?value=["'](.*?)['"])?/g) {
	my ($type, $name, $value) = ($1, $3, $5);
	if ($name && $type =~ m/(hidden|text|password)/i) {
	    $param{$name} = $value;
	}
    }
    $param{txtPassWord} = $password;
    $param{hidConfirm} = 'Confirm';
    my $params;
    foreach my $param (keys %param) {
	$params .= $params ? "&$param=$param{$param}" : "$param=$param{$param}";
    }
    $url =~ s/(?<=\?).*$/$params/;

    # boka
    print STDERR "bokar\n" if ($self->debug);
    $data = $self->get_url( $url );

    # Kolla att allt gick bra
    if ($data && $data !~ m/Felmeddelande/) {
	return 1;
    }
    return 0;
}


######################################################################
# �tkomst till instansdata
#
sub debug {
    my $self = shift;
    $self->{debug} = shift if (@_);
    return $self->{debug};
}

######################################################################
# Diverse bra funktioner f�r internetkommunikation
#

# S�tt l�mplig agent. Stulet fr�n Markus H�rnvi <marvi@kristnet.org>.
sub rand_agent {
    my $self = shift;
    my @agents = ( "Mozilla/4.5 [en] (WinNT;I)",
		   "Mozilla/4.71 [en]C-AtHome0407 (Win98; I)",
		   "Mozilla/4.72 [sv] (WinNT;I)",
		   "Mozilla/5.0 [en-US] (Linux; I)",
		   "Mozilla/5.0 [sv-SE] (Windows_NT; I)");
    return $agents[rand scalar @agents];
}

# H�mta angiven URL och returnera inneh�llet
sub get_url {
    my $self = shift;
    my $url = shift;

    # Skapa f�rfr�gan
    my $req = new HTTP::Request GET => $url;

    # Skicka iv�g f�rfr�gan
    my $res = $self->{ua}->request( $req );

    # Returnera inneh�ll
    return $res->is_success ? $res->content : undef;
}


1; # Allt �r sant!



