#!/usr/bin/perl -w

use strict;
use Netsuck;
use Date::Calc qw(:all);
use Getopt::Std;


my %opt;
unless( getopts( 'd:s:e:', \%opt ) ) {
    print STDERR "$0: Felaktiga parametrar. S� h�r skall det vara:\n";
    print STDERR "\n";
    print STDERR " -d dag (1=m�ndag, 7=s�ndag)\n";
    exit 2;
}
my $day = ($opt{d} && $opt{d} >= 1 && $opt{d} <= 7) ? $opt{d}+0 : 0;
my $start = ($opt{'s'} || '00:00');
my $end = ($opt{e} || '23:59');



my $internet = new Netsuck;
$internet->debug(1);
my $free = $internet->lediga_tider;

foreach my $ref (@$free) {
    my @date = ($$ref{year}, $$ref{month}, $$ref{day});
    print "Ledig tid: ", Date_to_Text_Long( @date ), " $$ref{time}\n";
    if ($day == Day_of_Week( @date )) {
	print "Vi har en bokning p� s�kt dag kl $$ref{time}!\n";
	if ($$ref{start} ge $start && $$ref{end} le $end) {
	    print "Och den matchar till och med tidsintervallet!\n";
	    if ($internet->boka( $$ref{url} )) {
		print "Bokad!\n";
	    } else {
		print "Bokning misslyckades!\n";
	    }
	}
    }

#    print "URL $$ref{url}\n";
#    print $internet->boka( $$ref{url} );
}

#  if ($internet->boka( 'http://www.iksu.se/iksubokning/Booking/ConfirmBooking.asp?BookRes_ID=3473&StartTime=2002-02-26%2008:00:00&EndTime=2002-02-26%2009:00:00&Name=Badminton%20bana%201&BookResPart_ID=2118&CU=4284&Comp_ID=13&C=13&L=1&UL=6&YNr=2002&MNr=2&DNr=26', 'gul84s76o' )) {
#      print "bokning lyckades\n";
#  } else {
#      print "bokning misslyckades\n";
#  }
