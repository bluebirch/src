#!/usr/bin/perl -w
#
# $Id$
#
# A complete rewrite of Robot, adapted to a true (I think) POE
# model. Everything is events.
#
# $Log$
# Revision 1.12  2003/08/13 15:52:13  stefan
# Some less debugging.
#
# Revision 1.11  2003/07/29 12:37:06  stefan
# 'save all' also saves list of currently loaded plugins.
#
# Revision 1.10  2003/07/29 11:36:49  stefan
# STDERR redirected to a log file specified in the configuration
# file. This will make sure that the curses screen is not garbled.
#
# Revision 1.9  2003/06/26 14:33:34  stefan
# Added event "say" - sends message to specified channel(s) and echoes
# it on screen.
#
# Revision 1.8  2003/06/20 10:48:04  stefan
# Added better configuration save mechanism, and also an autosave
# option.
#
# Revision 1.7  2003/06/20 07:56:12  stefan
# Added a -D (debug) command line switch.
#
# Revision 1.6  2003/06/12 13:41:37  stefan
# Removed handlers for join and other stuff that should be handled by
# the tracker plugin (the state handlers, however, are still left, since
# not all of them have been moved to the plugin).
# Probably other changes too.
#
# Revision 1.5  2003/06/12 11:09:13  stefan
# Reworked plugin interface. Added configuration file with Config::Tiny.
#
# Revision 1.4  2003/06/11 21:52:00  stefan
# Added a wonderful plugin handler.
#
# Revision 1.3  2003/06/11 17:12:27  stefan
# Fixed help command. Change 'attention' events to 'system' events for
# console session. Changed interna structure of joined
# channels. Consider data structures to track channels/users.
#
# Revision 1.2  2003/06/10 18:07:53  stefan
# Added lots of things. More to come.
#
# Revision 1.1  2003/06/10 15:51:57  stefan
# This is the embryo of the new Robot, AKA Robot_MkII. It's much more
# advanced than the old one, with multitasking sessions and completely
# event driven. This will be good.
#

use strict;
#use Curses;
#sub POE::Kernel::ASSERT_DEFAULT () { 1 }
#sub POE::Kernel::TRACE_EVENTS () { 1 }
#sub POE::Kernel::TRACE_RETVALS () { 1 }
#sub POE::Kernel::TRACE_REFCNT () { 1 }
use POE qw(Component::IRC);
use Robot::Debugger;
use Robot::Functions;
use Robot::Console;
use Robot::PluginHandler;
use Getopt::Std;
use Config::Tiny;

# The current name and version of the bot
my $BOTNAME = 'Robot_MkII';
my $VERSION = (split( ' ', q$Revision$ ))[1];

# Hash to store command line options
my %opt;


######################################################################
# Configuration. It shouldn't be here, but in a separate file or
# within commandline switches or whatever. This is a typical FIXME
# situation.
#

#  my %conf = ( pid     => $$,
#  	     nick    => $BOTNAME,
#  	     ircname => "$BOTNAME $VERSION",
#  	     alias   => $BOTNAME,
#  	     server  => 'localhost',
#  	     port    => 6667,
#  	     name    => $BOTNAME,
#  	     channels => [ '#test', '#bot' ],
#  	     plugins => [ qw(Console) ],
#  	   );


######################################################################
# Basic session events, for event functionality, starting up children,
# etc.
#

# Start up main session. Mainly, this consists in firing up a lot of
# child processes and connecting to the irc server.
sub _start {
    my ($kernel, $session, $heap, $conf) = @_[KERNEL, SESSION, HEAP, ARG0];

    # Set alias for current session. This 'deamonizes' the session,
    # and makes it easier to call from other sessions.
    $kernel->alias_set( 'robot' );

    # Store config object
    $heap->{config} = $conf;

    # Set up a hash that will be shared with the session's children.
    my $shared = {};

    # Set up some shared variables
    $shared->{nick} = $conf->{_}->{nick};       # My current nick
    $shared->{channel} = '';                    # My current channel
    $shared->{channels} = {};                   # Hash of all channels
    $shared->{title} = "$BOTNAME $VERSION";     # Main window title
    $shared->{connected} = 0;                   # Connected flag

    # Store the shared hash on the heap.
    $heap->{shared} = $shared;

    # Set up some variables on the heap. Oh, there was none left. :-)

    # Create POE::Component::IRC session.
    POE::Component::IRC->new( 'irc' ) or die "Fatal: no irc";

    # Start console session. Send it configuration and shared memory.
    Robot::Console->new( $conf, $shared ) or die "Fatal: no console";

    # Load plugin handler.
    Robot::PluginHandler->new( $conf, $shared ) or die "Fatal: no plugin handler";

    # Load default plugins
    if ($conf->{_}->{plugins}) {
	my @plugins = split m/\s+/, $conf->{_}->{plugins};
	foreach my $plugin (@plugins) {
	    $kernel->post( console => system => "Loading plugin $plugin" );
	    $kernel->post( pluginhandler => load => $plugin );
	}
    }

    # Register for all events, just for now
    $kernel->post( irc => register => 'all' );

    # Connect to server
    $kernel->yield( 'connect', $conf->{_}{server}, $conf->{_}{port} );

#    $session->option( trace => 1 );
#    $kernel->alias_resolve( 'irc' )->option( trace => 1 );

}

# What to do when session goes down
sub _stop {
    DEBUG "Everything just stopped!";
}

# This event takes care of unknown events
sub _default {
    if ($_[ARG0] !~ m/^irc_/ && $_[HEAP]->{shared}->{debug}) {
	$_[KERNEL]->post( console => debug => "Unknown event [$_[ARG0] @{$_[ARG1]}]" );
    }
    return 0;
}

# Handle events regarding children
sub _child {
    my ($kernel, $action, $session) = @_[KERNEL, ARG0, ARG1];

    my @aliases = $kernel->alias_list( $session );
    my $id = $session->ID();

#    $_[KERNEL]->post( console => debug => "children: $action $id (@aliases)" );
}

# Shut down session. This means: remove alias, and send shutdown to
# all other session.
sub shutdown {
    my($kernel, $session, $heap)=@_[KERNEL, SESSION, HEAP];

    # Save configuration if autosave is on. Do synchronous call, just
    # to be sure.
    $kernel->call( 'robot', 'save_config', 'all' )
      if ($heap->{config}->{_}->{autosave});

    # Remove all aliases
    foreach ($kernel->alias_list( $session )) {
	$kernel->alias_remove( $_ );
    }

    # Clear all alarms
    $kernel->alarm_remove_all();

    # Shut down plugin handler
    $kernel->post( pluginhandler => 'shutdown' );

    # Shut down console (we know what children we have)
    $kernel->post( console => 'shutdown' );

    # Unregister with irc session
    $kernel->post( irc => unregister => 'all' );

    # Shutdown IRC
    $kernel->post( irc => 'quit' => 'Goodbye' );
#    $kernel->post( irc => 'shutdown' => 'Goodbye' );

    return;
}


######################################################################
# Events that actually do something
#

# Command parser
sub parse_cmd {
    my ($kernel, $heap, $cmd, @args) = @_[KERNEL, HEAP, ARG0..$#_];

    if ($cmd eq 'quit') {
	# Shut down and quit
	$kernel->post( robot => 'shutdown' );
    }
    elsif ($cmd eq 'plugin') {
	$kernel->post( pluginhandler => @args );
    }
    elsif ($cmd eq 'post') {
	$kernel->post( @args );
    }
    elsif ($cmd eq 'save') {
	$kernel->yield( 'save_config', @args );
    }
    elsif ($cmd eq 'help') {
	$kernel->post( console => system => "Available commands:" );
	$kernel->post( console => system => "" );
	$kernel->post( console => system =>
		       "  \cBplugin load\cB  Load plugin" );
	$kernel->post( console => system =>
		       "  \cBplugin list\cB  List plugins" );
	$kernel->post( console => system =>
		       "  \cBplugin unload\cB  Unload plugin" );
	$kernel->post( console => system =>
		       "  \cBhelp\cB  Show this help page" );
	$kernel->post( console => system => 
		       "  \cBquit\cB  Shut down $BOTNAME" );
	$kernel->post( console => system => "" );
    }

    # Pass the command directly to the irc server
    else {
	$kernel->post( console => system => "Sending '$cmd' to IRC server" );
	$kernel->post( irc => $cmd => @args );
    }
}

# Connect to server
sub connect {
    if (!$_[HEAP]->{shared}->{connected}) {
	$_[KERNEL]->post( irc => connect => { Nick     => $_[HEAP]->{shared}->{nick},
					      Server   => $_[ARG0],
					      Port     => $_[ARG1],
					      Username => $BOTNAME,
					      Ircname  => $_[HEAP]->{config}->{ircname},
					      Debug    => 0,
					    } );
    }
    else {
	$_[KERNEL]->post( console => error => "Can not connect while connected" );
    }
}

# Save configuration
sub save_config {
    my ($kernel, $heap, @args) = @_[KERNEL, HEAP, ARG0..$#_];

    if ($args[0] eq 'all') {

	# Get list of plugins (synchronous call)
	$kernel->call( pluginhandler => 'export' );

	# Get list of current channels
	$heap->{config}->{_}->{channels} =
	  join( ' ', keys %{ $heap->{shared}->{channels} } );
    }
    $heap->{config}->write( $opt{c} );
#    $kernel->post( console => system => "Configuration saved" );
}

# Cycle through channels
sub next_channel {
    my ($kernel, $heap) = @_[KERNEL, HEAP];

    my @channels = keys %{$heap->{shared}->{channels}};
    my $channel = $heap->{shared}->{channel};

    # Do something only if we have any channels
    if (scalar @channels > 0) {

	# Find current channel
	my $index;
	for ( my $i = 0; $i < @channels; $i++ ) {
	    if ($channels[$i] eq $channel) {
		$index = $i;
		last;
	    }
	}

	# If we did not find anything, select the first channel
	if (! defined $index) {
	    $index = 0;
	}

	# Find next channel
	if (++$index > $#channels) {
	    $index = 0;
	}

	$heap->{shared}->{channel} = $channels[$index];
	$kernel->post( console => system =>
			  "Active channel is now \cB$heap->{shared}->{channel}" );
    }

    # If we're not on any channel, simply delete the current channel
    else {
	$heap->{shared}->{channel} = undef;
    }

    # Refresh status window to reflect changes
    $kernel->post( console => 'refresh_status' );

}

#
# Say something to the IRC server, and echo this on screen.
#

sub say {
    my ($kernel, $heap, $channel, $msg) = 
      @_[KERNEL, HEAP, ARG0, ARG1];

    # Send to IRC server
    $kernel->post( irc => privmsg => $channel => $msg );

    # Echo on screen
    $kernel->post( console => irc_public =>
		   $heap->{shared}->{nick},
		   $channel, $msg );
}


######################################################################
# IRC event handlers
#

sub irc_001 {

    # Join default channels on connect
    if ($_[HEAP]->{config}->{_}->{channels}) {
	my @channels = split m/\s+/, $_[HEAP]->{config}->{_}->{channels};
	foreach my $channel (@channels) {
	    $_[KERNEL]->post( irc => join => $channel );
	}
	$_[KERNEL]->post( console => system => "Joining default channels" );
    }

}

sub irc_disconnected {

    # Mark session disconnected
    $_[HEAP]->{shared}->{connected} = 0;

    # Clear anything we possibly knew about this session
    $_[HEAP]->{shared}->{channel} = undef;
    $_[HEAP]->{shared}->{channels} = {};

    # Try to reconnect
    $_[KERNEL]->post( console => error => "Disconnected from server. Trying again." );
    $_[KERNEL]->delay_set( 'connect', 5, $_[HEAP]->{config}->{_}->{server},
			   $_[HEAP]->{config}->{_}->{port} );
}

sub irc_join {
    my ($kernel, $heap, $nick, $channel) = @_[KERNEL, HEAP, ARG0, ARG1];

    # Strip nick
    $nick = stripnick( $nick );

    # If this is me, remember the channel
    if ($nick eq $heap->{shared}->{nick}) {
	$heap->{shared}->{channel} = $channel;
	$heap->{shared}->{channels}->{$channel} = { join_time => time };
	$kernel->post( console => 'refresh_status' );
	$kernel->post( console => debug => "Registered channel $channel" )
	  if ($heap->{config}->{debug});
    }
}

# Topic information
sub irc_332 {
    my ($channel, $topic) = split m/[ :]+/, $_[ARG1];
    $_[HEAP]->{shared}->{channels}->{$channel}->{topic} = $topic;
    $_[KERNEL]->post( console => 'refresh_status' );
    $_[KERNEL]->post( console => debug => "Registered topic for $channel" )
      if ($_[HEAP]->{config}->{debug});
}

# Topic information
sub irc_topic {
    $_[HEAP]->{shared}->{channels}->{$_[ARG1]}->{topic} = $_[ARG2];
    $_[KERNEL]->post( console => 'refresh_status' );
    $_[KERNEL]->post( console => debug => "Registered topic for $_[ARG1]" )
      if ($_[HEAP]->{config}->{debug});
}

sub irc_433 {
    # Do something with nick to make it unique
    $_[HEAP]->{shared}->{nick} .= '_';
    $_[KERNEL]->post( irc => nick => $_[HEAP]->{shared}->{nick} );
    $_[KERNEL]->post( console => error => "Nick taken. Trying something else." );
}


######################################################################
# Execution starts here

# Parse command line
if (!getopts( 'Dc:', \%opt )) {
    exit 2;
}

# Set default values for some command line switches
$opt{c} = 'Robot.ini';

# Clear debug flag unless asked for it
$Robot::Debugger::DEBUG=0 if (!$opt{D});

# Create configuration object and parse config file
my $conf = Config::Tiny->read( $opt{c} );

# Set some default options
$conf->{_}{nick} = $BOTNAME unless ($conf->{_}{nick});
$conf->{_}{server} = 'localhost' unless ($conf->{_}{server});
$conf->{_}{port} = 6667 unless ($conf->{_}{port});
$conf->{_}{ircname} = "$BOTNAME v$VERSION" unless ($conf->{_}{ircname});
$conf->{_}{debug} = 0 unless ($conf->{_}{debug});
$conf->{_}{logfile} = "error.log" unless ($conf->{_}{logfile});

# Create the main session
POE::Session->create
  ( package_states => [ main => [ '_start',
				  '_stop',
				  '_default',
				  '_child',
				  'irc_disconnected',
				  'irc_001', # connected
				  'irc_433', # nick in use
				  'connect',
				  'parse_cmd',
				  'next_channel',
				  'save_config',
				  'say',
				  'shutdown' ],
		      ],
    args => $conf,
  );

# Send anything that is written to STDERR to the log file.
open STDERR, '>' . $conf->{_}->{logfile};

# Start POE kernel
$poe_kernel->run();
exit 0;
