-- $Id$
--
-- $Log$
--
-- Current Database: robot
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ robot;

USE robot;

--
-- Table structure for table `blocked`
--

DROP TABLE IF EXISTS blocked;
CREATE TABLE blocked (
  thing varchar(100) NOT NULL default '',
  PRIMARY KEY  (thing)
) TYPE=MyISAM;

--
-- Table structure for table `things`
--

DROP TABLE IF EXISTS things;
CREATE TABLE things (
  thing varchar(100) NOT NULL default '',
  descriptor varchar(30) NOT NULL default '',
  description varchar(255) NOT NULL default '',
  source varchar(30) default NULL,
  time timestamp(14) NOT NULL,
  PRIMARY KEY  (thing,descriptor,description),
  KEY thing_idx (thing),
  KEY descriptor_idx (descriptor),
  KEY source_idx (source),
  KEY time_idx (time)
) TYPE=MyISAM;
