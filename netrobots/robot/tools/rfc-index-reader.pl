#!/usr/bin/perl -w
#
# $Id$
#
# Program f�r att l�sa in RFC index i Robots databas. Indexet kan
# h�mtas fr�n ftp://ftp.isi.edu/in-notes/rfc-index.txt
#
# $Log$
# Revision 1.1  2001/09/11 13:05:09  stefan
# Lagrar allt i kategori 2.
#

use strict;
use Brain;

my $brain = new Brain;
$brain->wakeup or die "Ingen hj�rna";

while (<>) {
    chomp;
    if (m/^(\d{4}) (.+)/) {
	my $rfc = "RFC".$1;
	my $text = $2;
	my $more = <>;
	while ($more =~ m/\S/ and not eof) {
	    chomp $more;
	    $more =~ s/\s*(.*)\s*/$1/e;
	    $text .= ' ' . $more;
	    $more = <>;
	}
	$text =~ s/\s*\((Format|Status):.*?\)//g;
	$brain->learn( $rfc, $text, 2 );
    }
}
$brain->sleep;
