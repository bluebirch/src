#!/usr/bin/perl -w
use strict;

use Weather::Underground;

my $weather = Weather::Underground->new(
					place   =>  $ARGV[0],
				     debug           =>      1
				    )
  || die "Error, could not create new weather object: $@\n";

my $arrayref = $weather->getweather()
  || die "Error, calling getweather() failed: $@\n";

foreach (@$arrayref) {
    print "MATCH:\n";
    while (my ($key, $value) = each %{$_}) {
	print "\t$key = $value\n";
    }
}

