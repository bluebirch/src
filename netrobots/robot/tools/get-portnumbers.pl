#!/usr/bin/perl -w
#
# Get port numbers from www.iana.org.
#

use strict;
use warnings;
use LWP::UserAgent;
use DBI;


my $ua = new LWP::UserAgent;

my $req = HTTP::Request->new( GET => 'http://www.iana.org/assignments/port-numbers' );

my $res = $ua->request( $req );

die "No data" unless ($res->is_success);

my $data = $res->content;

my $dbh = DBI->connect( 'DBI:mysql:robot:localhost', 'robot', 'gn3kk' );

my $sth = $dbh->prepare( q{INSERT INTO things (thing, descriptor, description, source) VALUES (?,?,?,?)} );

while ($data =~ m{^([-+\w]+)\s+(\d+)/tcp\s+(.*?)\s*$}gm) {
    my ($service, $port, $desc) = ($1, $2, $3);
    $sth->execute( "port $port", "=", "$service - $desc", "www.iana.org" );
}

$dbh->disconnect;
