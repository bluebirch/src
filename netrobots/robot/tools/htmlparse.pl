#!/usr/bin/perl -w

use strict;
use HTML::Tagset;
use HTML::TreeBuilder;

#  %HTML::Tagset::emptyElement = ( 'tmpl_else' => 1,
#  				%HTML::Tagset::emptyElement );

#  %HTML::Tagset::canTighten = ( 'tmpl_if' => 1,
#  			      'tmpl_else' => 1,
#  			      %HTML::Tagset::canTighten );

foreach my $file_name (@ARGV) {
    my $tree = HTML::TreeBuilder->new; # empty tree
    $tree->ignore_unknown(1);
    $tree->store_comments(0);
    $tree->warn(1);
    $tree->parse_file($file_name);
#    $tree->dump;
    my $muh = $tree->address( '0.1.1.1.4' );
#    $muh->dump;

    my @nodes = $muh->look_down( 'class' => 'smalltable' );

#      print "<!-- cleaned code follows -->\n";
      foreach my $node (@nodes) {
	  print "\n\n\nNODE $node\n\n\n";
	  $node->dump;
      }

#    print $tree->as_XML, "\n";
    
    # Now that we're done with it, we must destroy it.
    $tree = $tree->delete;
  }
