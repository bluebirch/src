# $Id$
#
# Module to handle plugins and dispatch and broadcast events to all
# plugins.
#
# TODO:
#
#  * Use external references for all plugins - ensure plugin sessions
#    won't die.
#
#  * Send events (talk, ask, command, listen) to plugins that have
#    registered for those events.
#
# $Log$
# Revision 1.1  2004/04/22 06:12:00  zerblat
# Robot was restored since the original CVS repository was lost.
#
# Revision 1.17  2003/07/29 13:16:10  stefan
# Responses in _nextplugin can be undefined.
#
# Revision 1.16  2003/07/29 13:04:06  stefan
# Fixed unload bug. Plugins only received the "shutdown" event, not the
# "unload" event.
#
# Revision 1.15  2003/07/29 12:33:47  stefan
# Added 'export' function to export currently loaded modules to global
# configuration.
#
# Revision 1.14  2003/07/29 12:09:37  stefan
# Added 'reload' function. Added 'unregister_all' function. Changed
# plugin interface.
#
# Revision 1.13  2003/07/08 14:32:49  stefan
# Make the pluginhandler less noisy.
#
# Revision 1.12  2003/07/07 16:56:56  stefan
# Consider both ',' and ':' when determining if a message is directed to
# me.
#
# Revision 1.11  2003/07/04 16:07:44  stefan
# irc_msg now generates a 'talk' event instead of a 'listen' event.
#
# Revision 1.10  2003/06/26 14:35:24  stefan
# Implemented use of the new 'say' event of Robot.pl. Removed some
# stupid debug messages.
#
# Revision 1.9  2003/06/20 13:22:49  stefan
# Fixed bug in _nextplugin.
#
# Revision 1.8  2003/06/20 10:58:29  stefan
# Reworked plugin interface. I think I have everything I need now. Well,
# except for this "say" function.
#
# Revision 1.7  2003/06/20 07:59:05  stefan
# Still working on dispatch/callback events. I need some changes, I
# think.
#
# Revision 1.6  2003/06/13 12:47:34  stefan
# Added handlers to handle public irc events, and callbacks.
#
# Revision 1.5  2003/06/13 07:44:47  stefan
# Added states 'register' and 'unregister', which make it possible for
# plugins to register with pluginhandler and receive certain events.
#
# Revision 1.4  2003/06/12 13:42:32  stefan
# Added code to verify that the plugin object really has all state
# handlers it claims to have.
#
# Revision 1.3  2003/06/12 11:23:09  stefan
# Configuration is initialized for each plugin.
#
# Revision 1.2  2003/06/12 11:09:13  stefan
# Reworked plugin interface. Added configuration file with Config::Tiny.
#
# Revision 1.1  2003/06/11 21:49:34  stefan
# This is a very nice plugin handler, actually, and it makes writing of
# plugins very easy. I'm satisfied!
#
#
package Robot::PluginHandler;

use strict;
use POE;
use Robot::Functions;
use Robot::Debugger;

# Tag used for POE refcounts
use constant PH_REFCOUNT_TAG => 'PluginHandler registered';

# Create a new plugin_handler session.
sub new {
    my $package = shift;

    # Create new session
    POE::Session->create
	( package_states => [ $package => [ '_default',
					    '_child',
                                            '_start',
                                            '_stop',
					    'shutdown',
					    'dump',
					    'load',
					    'list',
					    'export',
					    'unload',
					    'register',
					    'reload',
					    'unregister',
					    'unregister_all',
					    '_nextplugin',
					    'irc_public',
					  ],
			      $package => { 'irc_msg' => 'irc_public' },
			    ],
	  args => [ @_ ],
	);
}

######################################################################
# Session handler states
#

# Startup session
sub _start {
    my ($kernel, $heap, $config, $shared) = @_[KERNEL, HEAP, ARG0, ARG1];

    # Register alias
    $kernel->alias_set( 'pluginhandler' );

    # Put config and shared data on the heap
    $heap->{config} = $config;
    $heap->{shared} = $shared;

    # Prepare some stuff on the heap
    $heap->{plugins} = [];  # Array of hashes with information on each
                            # plugin
    $heap->{plugin} = {};   # A hash for quick testing
    $heap->{children} = {}; # List of child sessions
    $heap->{events} = {};   # List of events and sessions that should
                            # receive those events, in order.
    $heap->{sessions} = {}; # Registered sessions.

    $kernel->post( console => plugin => "Plugin handler started" );

    # Register with irc
    $kernel->post( irc => register => qw(public msg) );
}

# End session
sub _stop {
    $_[KERNEL]->alias_remove( 'pluginhandler' );
    DEBUG "pluginhandler stopped; we take no more events";
}

# Keep track of my children
sub _child {
    if ($_[ARG0] eq 'lose') {

	my $id = $_[ARG1]->ID();

#  	$_[KERNEL]->post( console => debug =>
#  			  "Lost a child! It was #$id!" );

	# Remove child from list of childs
	delete $_[HEAP]->{children}{$id};

	# Remove also from list of plugins. First, find plugin in
	# plugin list.
	my $plugins = $_[HEAP]->{plugins};
	my $index;
	for ( my $i = 0; $i < @$plugins; $i++ ) {
	    if ($$plugins[$i]{id} == $id) {
		$index = $i;
		last;
	    }
	}

	if (defined $index) {
	    # Get plugin name

	    my $plugin = $$plugins[$index]{name};

	    # Delete the plugin checklist
	    delete $_[HEAP]->{plugin}{$plugin};

	    # Delete from the plugin stack, including object
	    splice @$plugins, $index, 1;

	    # This is a hack; delete the module from the INC
	    # hash. Hopefully, this means that the module will be
	    # reloaded next time the plugin is loaded.
	    delete $INC{"Plugin/$plugin.pm"};
	    delete $INC{"Plugin/Base.pm"};

	    # Send a message to the console
	    $_[KERNEL]->post( console => plugin =>
			      "Plugin '$plugin' unloaded." );

	}

    }
    else {
	$_[HEAP]->{children}{$_[ARG1]->ID()} = $_[ARG1];
    }
}

# Default state
sub _default {
    $_[KERNEL]->post( console => plugin =>
		      "Unknown event [$_[ARG0] @{$_[ARG1]}]" );
}

# Shut down
sub shutdown {

    DEBUG "initiating shutdown sequence";

    # Remove alias
#    $_[KERNEL]->alias_remove( 'pluginhandler' );

    # Unregister with PoCo::IRC
    $_[KERNEL]->post( irc => unregister => qw(public msg) );

    # Call shutdown on child sessions
    foreach my $child (values %{ $_[HEAP]->{children} }) {
	DEBUG "shut down ", $child->ID();
	$_[KERNEL]->post( $child => 'shutdown' );
	$_[KERNEL]->post( $child => 'unload' );
    }

    DEBUG "pluginhandler shut down";

}

######################################################################
# Plugin states
#

sub load {
    my ($kernel, $heap, $plugin) = @_[KERNEL, HEAP, ARG0];

   # Check that plugin is not already loaded
    if ($heap->{plugin}{$plugin}) {
	$kernel->post( console => plugin => "Plugin '$plugin' already loaded. Unload first." );
	return 0;
    }

    # Try tro create a new object using the desired plugin as class
    # descriptor.
    my $object;
    if (eval "use Plugin::$plugin; \$object = Plugin::$plugin->new();") {

	# Get list of states/events for plugin. The basic states
	# _start, _stop and shutdown, should not be included in this
	# list.
	my @states;
	my $res = eval "\@states = \@Plugin::${plugin}::EVENTS";

	# Add the internal states to the list of handled states. We
	# might as weel test them too, to make sure the plugin is
	# properly written.
	@states = ( qw(_start _stop _default unload), @states );

	# Verify that this plugin really can respond to those events.
	foreach my $state (@states) {
	    if (!$object->can( $state )) {

		# post an error message
		$kernel->post( console => error =>
			       "Plugin '$plugin' claims to handle event '$state', but has no state for it." );

		# Delete plugin from INC hash. This ensures that it
		# will be reloaded from disk next time.
		delete $INC{"Plugin/$plugin.pm"};

		# quit state
		return 0;
	    }
	}

	# Get plugin description
	my $description = 'no description';
	eval "\$description = \$Plugin::${plugin}::DESCRIPTION";

	# Create a section in the configuration for this plugin if it
	# does not exist.
	$heap->{config}->{$plugin} = {}
	  unless (defined $heap->{config}->{$plugin});

	# Create session. All plugin sessions must have a _start,
	# _stop and shutdown state handler. Apart from that, a plugin
	# can be virtually anything. Send the plugin configuration and
	# the shared hash to the plugin also.
	my $newid = POE::Session->create
	  ( object_states => [ $object => [ @states ] ],
	    args => [ $heap->{config}->{$plugin}, $heap->{shared} ]
	  )->ID();

	# Push the plugin information on the plugin stack
	push @{ $heap->{plugins} }, { object => $object,
				      name => $plugin,
				      description => $description,
				      id => $newid };

	# Mark the plugin as loaded
	$heap->{plugin}{$plugin} = 1;

	# Send a message
	$kernel->post( console => plugin => "Loaded plugin $plugin." );


    }
    else {
	my (@err) = split m/\n+/, $@;
	eval "no Plugin::$plugin"; # Unload the module to make sure it
                                   # is still not in memory.
	$kernel->post( console => error => "Load plugin $plugin failed:" );
	$kernel->post( console => error => $_ ) foreach (@err);
    }
}

# list loaded plugins
sub list {
    my ($kernel, $heap) = @_[KERNEL, HEAP];

    $kernel->post( console => plugin => "Plugins currently loaded:" );

    # Iterate over plugins
    foreach my $plugin (@{ $heap->{plugins} }) {

	my @aliases = $kernel->alias_list( $$plugin{id} );
	$aliases[0] = '-' if (scalar @aliases == 0);

	$kernel->post( console => plugin =>
		       sprintf( "  \cB%-15.15s\cB %s" ,$$plugin{name}, $$plugin{description} ) );
    }
}

# Unload plugin
sub unload {
    my $plugin = $_[ARG0];

    if ($_[HEAP]->{plugin}{$plugin}) {

#	$_[KERNEL]->post( console => debug => "Looking for $plugin" );

	# Find plugin in plugin list
	my $plugins = $_[HEAP]->{plugins};
	my $index;
	for ( my $i = 0; $i < @$plugins; $i++ ) {
	    if ($$plugins[$i]{name} eq $plugin) {
		$index = $i;
		last;
	    }
	}

#  	$_[KERNEL]->post( console => debug => "$plugin has index $index in list" );
#  	$_[KERNEL]->post( console => debug => "That is id $$plugins[$index]{id}" );

	if (defined $index) {
	    # Send shutdown and unload to plugin
#	    $_[KERNEL]->post( console => debug => "Sending shutdown to '$plugin'" );
	    $_[KERNEL]->post( $$plugins[$index]{id} => 'shutdown' );
	    $_[KERNEL]->post( $$plugins[$index]{id} => 'unload' );
	}

    }
    else {
	$_[KERNEL]->post( console => plugin =>
			  "There is no plugin named '$plugin'" );
    }
}

# Reload plugin
sub reload {
    my ($kernel, $plugin) = @_[KERNEL, ARG0];

    # Issue a unload immediately
    $kernel->yield( unload => $plugin );

    # Schedule a load somewhere in the near future
    $kernel->delay_set( load => 2 => $plugin );
}

# Export list of plugins to config
sub export {
    my ($kernel, $heap) = @_[KERNEL, HEAP];
    my $config = $heap->{config};

    my @plugins = map { $_->{name} } @{ $heap->{plugins} };

    $config->{_}->{plugins} = join( ' ', @plugins );
}

######################################################################
# States for event registration and pushing events to clients
#

# Ask PluginHandler to send you events, listed in events.
sub register {
    my ($kernel, $heap, $session, $sender, @events) =
      @_[KERNEL, HEAP, SESSION, SENDER, ARG0 .. $#_];

    foreach my $event (@events) {

	$kernel->post( console => plugin =>
		       "register event '$event' with session " . $sender->ID()
		     );

#	DEBUG "register event '$event' with session " . $sender->ID();

	# Check if client already has registered this event
	my $i = scan_array( $heap->{events}->{$event}, $sender );
	if ( !defined $i ) {

	    # Put sender onto event stack
	    push @{ $heap->{events}->{$event} }, $sender;

	    # Remember sender
	    $heap->{sessions}->{$sender} = $sender;

	    # Increase reference count on sender
	    if ($session != $sender) {
		my $refcnt = $kernel->refcount_increment( $sender->ID(),
							  PH_REFCOUNT_TAG );
	    }
	}
    }
}

# Don't send any more events
sub unregister {
    my ($kernel, $heap, $session, $sender, @events) =
      @_[KERNEL, HEAP, SESSION, SENDER, ARG0 .. $#_];

    foreach my $event (@events) {

	$kernel->post( console => plugin =>
		       "unregister event '$event' with session " . $sender->ID() );

#	DEBUG "unregister event '$event' with session " . $sender->ID();

#  	use Data::Dumper;
#  	print STDERR Dumper( $heap->{events}->{$event} );

	# Find index
	my $i = scan_array( $heap->{events}->{$event}, $sender );

	# If it was found, delete it
	if ( defined $i ) {

	    splice @{ $heap->{events}->{$event} }, $i, 1;

	    # Decrease reference count on sender
	    if ($session != $sender) {
		my $refcnt = $kernel->refcount_decrement( $sender->ID(),
							  PH_REFCOUNT_TAG );

#		DEBUG "refcount for session ", $sender->ID(), " now ", $refcnt;

		# Remove sender from list of sessions if there are no
		# more (or less) references
		if ($refcnt <= 0) {
		    delete $heap->{sessions}->{$sender};
		}
	    }
	}
    }
}

# Unregister all events
sub unregister_all {
    my ($kernel, $heap, $session, $sender) =
      @_[KERNEL, HEAP, SESSION, SENDER];

#    DEBUG "unregister all for session ", $sender->ID;

    # Go through all events
    foreach my $event (keys %{ $heap->{events} }) {

	# Find sender in this event
	my $i = scan_array( $heap->{events}{$event}, $sender );

	# If sender was found, delete from list
	if (defined $i) {

#	    DEBUG "removing from event $event";

	    splice @{ $heap->{events}{$event} }, $i, 1;

	    # Decrease reference count on sender
	    if ($session != $sender) {
		my $refcnt = $kernel->refcount_decrement( $sender->ID(),
							  PH_REFCOUNT_TAG );

#		DEBUG "refcount for session ", $sender->ID(), " now ", $refcnt;

		# Remove sender from list of sessions if there are no
		# more (or less) references
		if ($refcnt <= 0) {
		    delete $heap->{sessions}->{$sender};
		}
	    }
	}
    }
}

# Handle callbacks from plugins.
sub _nextplugin {
    my ($kernel, $session, $sender, $heap, $request, $response) =
      @_[KERNEL, SESSION, SENDER, HEAP, ARG0, ARG1];

#      $kernel->post( console => plugin =>
#  		   "Received callback for event '$$request[0]' from session "
#  		   . $sender->ID() )
#        if ($heap->{config}->{_}->{verbose});

    # Structure of response packet
    # [0] response, scalar or arrayref
    # [1] no more plugins, boolean


    # Take care of responses. Public messages are responded to on the
    # channel, and private messages privately.
    if (defined $$response[0]) {

	# Make sure the response is an array reference.
	if ( !ref $$response[0] ) {
	    $$response[0] = [ $$response[0] ];
	}

	# Respond public to public messages, privately to private
	# messages.
	my $to = $$request[5] eq 'irc_public' ? $$request[2] :
	  [ stripnick($$request[1]) ];


	# FIXME: this should be replaced with a "say" function somewhere.
	foreach my $response (@{ $$response[0] }) {
	    $kernel->post( robot => say => $to => $response );
	}
    }


    # Unless the plugin explicitly told us not to pass this further
    # on, we'll send it to the next plugin.
    unless ($$response[1]) {

	# Which plugin in the plugin list for this event did we just
	# receive a postback from?
	my $index = scan_array( $heap->{events}->{$$request[0]}, $sender );

	if (defined $index && defined $heap->{events}->{$$request[0]}[$index+1]) {

	    # Send further to next plugin
	    my $postback = $session->postback( '_nextplugin', @$request );
	    $kernel->post( $heap->{events}->{$$request[0]}[$index+1],
			   @$request,
			   $postback );
	}
#  	else {
#  	    $kernel->post( console => plugin =>
#  			   "No more plugins to send to; discarding event." )
#  	      if ($heap->{config}->{_}->{verbose});
#  	}
    }
#      elsif ($heap->{config}->{_}->{verbose}) {
#  	$kernel->post( console => plugin =>
#  		       "Plugin told us not to pass this request on; discarding event." );
#      }
}

######################################################################
# IRC states to handle events from POE::Component::IRC
#

sub irc_public {
    my ($kernel, $session, $state, $heap, $user, $to, $msg) =
      @_[KERNEL, SESSION, STATE, HEAP, ARG0..ARG2];

#      $kernel->post( console => debug =>
#  		   "Parse line: '$msg' from $user" )
#        if ($heap->{config}->{_}->{debug});

    my $event;
    my $params = [];

    # Check if this is a command
    if ($msg =~ s/^\s*[!:]//) {
	($event, @$params) = split m/\s+/, $msg;

	$event = 'cmd_' . $event;

    }

    # Theck if message is directed to me. This means that this should
    # generate a 'talk' event.
    elsif ($msg =~ s/^\s*$heap->{shared}->{nick}\s*[:,]\s*//i || $state eq 'irc_msg') {
	$event = 'talk';
    }

    # Otherwise, just listen.
    else {
	$event = 'listen';
    }

    # event name (talk, listen, ask, cmd_...)
    # postback ref
    # nick!hostmask
    # destination
    # message
    # params

    # Structure of events sent to plugins (ARG0..ARG2 same as
    # irc_public and irc_msg sent from PoCo::IRC:
    #
    # ARG0: nick!hostmask
    # ARG1: channels/users (array reference)
    # ARG2: text of message (possibly slightly altered)
    # ARG3: parameters (array reference)
    # ARG4: original event (irc_public or irc_msg)
    # ARG5: callback function (code reference)


    # If someone has registered for this event, send it to the
    # appropriate plugin
    if (defined $heap->{events}->{$event}) {

	# Create postback (which, basically, consists of the original
	# event sent to the plugin)
	my $postback = $session->postback( '_nextplugin',
					   $event,
					   $user,
					   $to,
					   $msg,
					   $params,
					   $state );

	# Post event to first plugin. The plugin issues a callback,
	# which is caught by the _extplugin state handler.
	$kernel->post( $heap->{events}->{$event}[0],
		       $event,
		       $user,
		       $to,
		       $msg,
		       $params,
		       $state,
		       $postback );
    } else {
	$kernel->post( console => plugin =>
		       "No registered handler for '$event'." )
	  if ($heap->{config}->{_}->{verbose});
    }
}



sub dump {
    my ($kernel, $heap) = @_[KERNEL, HEAP];

    $kernel->post( console => plugin => "Registered events:" );

    foreach my $event (keys %{ $heap->{events} }) {
	my @plugins;
	foreach my $session (@{ $heap->{events}->{$event} }) {
	    my $alias = ( $kernel->alias_list( $session ) ||
			  $session->ID() );
	    push @plugins, $alias;
	}
	$kernel->post( console => plugin => "  $event: @plugins" );
    }
}

1;

