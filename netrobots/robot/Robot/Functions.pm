# $Id$
#
# Some useful functions used throuout Robot.
#
# $Log$
# Revision 1.3  2003/06/20 08:00:04  stefan
# Added two functions for searching in arrays and arrays-of-hashes.
#
# Revision 1.2  2003/06/10 17:21:18  stefan
# Changed the stripnick function to use split instead.
#
# Revision 1.1  2003/06/10 15:51:57  stefan
# This is the embryo of the new Robot, AKA Robot_MkII. It's much more
# advanced than the old one, with multitasking sessions and completely
# event driven. This will be good.
#
#
package Robot::Functions;

use strict;
use warnings;
use POSIX qw(strftime);
#use Robot::Debugger;

BEGIN {
    use Exporter();
    our (@ISA, @EXPORT, @EXPORT_OK);
    @ISA = qw(Exporter);
    @EXPORT = qw(timestamp stripnick scan_array scan_aoh);
    @EXPORT_OK = qw();
}
our @EXPORT_OK;

# Return current stimestamp. Not an event handler!
sub timestamp {
    return strftime( '%H:%M ', localtime );
}

# Strip host part from nick
sub stripnick {
    my ($nick, $host) = split m/!/, $_[0], 2;
#    DEBUG "split: [$nick] [$host]";
    return wantarray ? ($nick, $host) : $nick;
}

# Search an array of hashes and return the array index where the
# specified hash key matches the supplied value. This is NOT an event!
# Returns undef is value was not found.
sub scan_aoh {
    my ($aryref, $key, $val) = @_;

    return undef unless (ref( $aryref ) eq 'ARRAY');

    my $index;
    for (my $i = 0; $i < @$aryref; $i++) {
	if (@$aryref[$i]->{$key} eq $val) {
	    $index = $i;
	    last;
	}
    }
    return $index;
}

# Similar to scan_aoh, but search a one-dimensional array.
sub scan_array {
    my ($aryref, $val) = @_;

    return undef unless (ref( $aryref ) eq 'ARRAY');

    my $index;
    for (my $i = 0; $i < @$aryref; $i++) {
	if (@$aryref[$i] eq $val) {
	    $index = $i;
	    last;
	}
    }
    return $index;
}

1;

