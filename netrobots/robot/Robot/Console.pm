# $Id$
#
# Console input and output through Curses. This is a POE session.
#
# $Log$
# Revision 1.1  2004/04/22 06:11:59  zerblat
# Robot was restored since the original CVS repository was lost.
#
# Revision 1.14  2003/07/29 12:47:33  stefan
# Bug fix.
#
# Revision 1.13  2003/07/29 11:27:37  stefan
# Turned off mouse events.
#
# Revision 1.12  2003/06/30 12:25:20  stefan
# The console now handles resizing and C-l. There's a known bug in the
# input buffer; if the buffer evaluates to false (undefined, zero length
# or the value '0'), the enter key does not work and the input buffer is
# not redrawn on a reset/clear screen. No big deal.
#
# Revision 1.11  2003/06/27 15:39:37  stefan
# Console now triggers on all backspace keys.
#
# Revision 1.10  2003/06/26 14:34:17  stefan
# Fixed irc_public.
#
# Revision 1.9  2003/06/20 07:55:08  stefan
# Reworked the line wrap functions. The algorithm is ugly, but I guess
# it is necessary since we have hidden characters in the text.
#
# Revision 1.8  2003/06/14 06:27:46  stefan
# Added wrapping of long lines. Much nicer output.
#
# Revision 1.7  2003/06/12 13:43:09  stefan
# Cosmetical changes.
#
# Revision 1.6  2003/06/12 11:58:45  stefan
# Changed plugin message to display plugin name.
#
# Revision 1.5  2003/06/12 11:09:13  stefan
# Reworked plugin interface. Added configuration file with Config::Tiny.
#
# Revision 1.4  2003/06/11 21:53:39  stefan
# Removed some debugging messages. Added a better and more comprehensive
# debug handler.
#
# Revision 1.3  2003/06/11 17:16:35  stefan
# Changed colors and color mapping. Added lots of irc events. Strange, I
# spend lots of time to make things look great on screen, but I have not
# started on the most important thing, the plugin handler and
# plugins. Stripped things down. The console session is supposed to be
# sent a few main events: 'irc' for irc messages, 'system' for system
# messages, 'plugin' for plugin messages, 'error' for error
# messages. Removed some old stuff.
#
# Revision 1.2  2003/06/10 18:07:21  stefan
# Added a lot of things. So much I can't remember.
#
# Revision 1.1  2003/06/10 15:51:57  stefan
# This is the embryo of the new Robot, AKA Robot_MkII. It's much more
# advanced than the old one, with multitasking sessions and completely
# event driven. This will be good.
#
#
package Robot::Console;

use strict;
use Curses;
use Robot::Functions;
use Robot::Debugger;
use POE qw(Wheel::Curses Wheel::FollowTail);
use POSIX qw(strftime);
use Text::Wrap qw(wrap);

# Declare constants
use constant WIN_MAIN             => 1;
use constant WIN_STATUS           => 2;
use constant WIN_STATUS_ALTERNATE => 3;
use constant WIN_INPUT            => 4;
use constant WIN_ERR              => 5;

use constant C_NORMAL             => 6;
use constant C_ALTERNATE          => 7;
use constant C_ERROR              => 8;
use constant C_IRC                => 9;
use constant C_SYSTEM             => 10;
use constant C_PLUGIN             => 11;
use constant C_DEBUG              => 12;

# A map for _colorprint handler
use constant S_ATTR => [ A_NORMAL,     # 0
			 A_BOLD,       # 1
			 A_UNDERLINE,  # 2
			 A_REVERSE,    # 3
			 A_BLINK,      # 4
			 A_DIM,        # 5
			 A_STANDOUT,   # 6
			 A_PROTECT,    # 7
			 A_INVIS,      # 8
			 A_ALTCHARSET, # 9
			 A_CHARTEXT    # 10
		       ];
use constant S_COLOR => [ C_NORMAL,    # 0
			  C_ALTERNATE, # 1
			  C_ERROR,     # 2
			  C_IRC,       # 3
			  C_SYSTEM,    # 4
			  C_PLUGIN,    # 5
			  C_NORMAL,    # 6
			  C_NORMAL,    # 7
			  C_NORMAL,    # 8
			  C_DEBUG      # 9
			];

# Create a new console session. Supply a hash reference, which is
# taken as the heap.
sub new {
    my $package = shift;

    # Create new session
    POE::Session->create
	( package_states => [ $package => [ '_colorprint',
                                            '_default',
                                            '_start',
                                            '_stop',
					    'debug',
                                            'error',
                                            'init',
                                            'input',
                                            'irc',
					    'irc_ctcp_action',
                                            'irc_332',
                                            'irc_333',
					    'irc_353',
                                            'irc_372',
                                            'irc_error',
                                            'irc_join',
					    'irc_mode',
                                            'irc_msg',
					    'irc_nick',
					    'irc_part',
                                            'irc_ping',
                                            'irc_public',
					    'irc_quit',
					    'plugin',
                                            'refresh_status',
                                            'shutdown',
					    'system',
					  ],
			    ],
	  args => [ @_ ],
	);
}

sub _start {
    my ($kernel, $heap, $conf, $shared) = @_[KERNEL, HEAP, ARG0, ARG1];

    # Register alias
    $kernel->alias_set( 'console' );

    # Store config and shared area on heap
    $heap->{config} = $conf;
    $heap->{shared} = $shared;

    # Create Curses wheel; this is for console intput/output
    $heap->{console} =
      POE::Wheel::Curses->new( InputEvent => 'input' );

    # Initialize screen. Synchronous call.
    $kernel->call( console => 'init' );

    # Capture STDERR
#      $heap->{stderr} =
#        POE::Wheel::FollowTail->new( Handle => \*STDERR,
#  				   InputEvent => 'stderr_input',
#  				   ErrorEvent => 'stderr_error' );

    # Register events with irc session
    $kernel->post( irc => register => 'all' );

    # We don't want those events (does'nt work)
#    $kernel->post( irc => unregister => qw(353 366 ping) );
}

sub _stop {
    DEBUG "Everything just stopped!";
}

# Taken from
# http://poe.perl.org/?POE_FAQ/How_do_I_force_a_session_to_shut_down
sub shutdown {
    my($kernel, $session, $heap)=@_[KERNEL, SESSION, HEAP];

    # Unregister with irc session
    $kernel->post( irc => unregister => 'all' );

    # Remove all aliases
    foreach ($kernel->alias_list( $session )) {
	$kernel->alias_remove( $_ );
    }

    # delete all wheels.
    delete $heap->{console} if $heap->{console};

    return 0;
}


# The default handler. This is actually used. All IRC events are
# flooded through here. That's good.
sub _default {
    my ($kernel, $heap, $event, $args) = @_[KERNEL, HEAP, ARG0, ARG1];
    if ($event =~ m/^irc_(.*)/) {

	my $irc = $1;

	# Display numeric event
	if ($irc =~ m/^\d+$/) {
#	    $kernel->yield( irc => "\e01$irc\e00: $$args[1]" );
	}

	# Display anything else
	else {
	    $kernel->yield( irc => "\e11$irc\e00: " . join( ' ', @$args ) );
	}
    } elsif ($heap->{config}->{debug}) {
	$kernel->post( console => debug =>
		       "Unknown event [$_[ARG0] @{$_[ARG1]}]" );
    }
    return 0;
}


######################################################################
# Main handlers for the most important events
#

# Display an error message
sub error {
    $_[KERNEL]->yield( _colorprint => "\e12-\e10!\e12- $_[ARG0]\e00" );
};

# Display an irc control message
sub irc {
    $_[KERNEL]->yield( _colorprint => "\e13-\e10!\e13-\e00 $_[ARG0]\e00", 1 );
};

# Display a system message
sub system {
    $_[KERNEL]->yield( _colorprint => "\e14-*-\e04 $_[ARG0]\e00" );
}

# Display a plugin message
sub plugin {
    my ($alias) = ( $_[KERNEL]->alias_list( $_[SENDER] ) ||
		    $_[SENDER]->ID() );
    $_[KERNEL]->yield( _colorprint => "\e15-!- $alias:\e00 $_[ARG0]" );
}

# Display a debug message. Include som information on its origin.
sub debug {
    my $id = $_[SENDER]->ID();
    my @alias = $_[KERNEL]->alias_list( $_[SENDER] );
    $_[KERNEL]->yield( _colorprint => "\e19-#- @alias/$id ($_[CALLER_FILE]:$_[CALLER_LINE]): $_[ARG0]\e00" );
}

######################################################################
# IRC Events
#

sub irc_ctcp_action {
    my $nick = stripnick( $_[ARG0] );
    my $from = $_[HEAP]->{shared}->{channel} eq $_[ARG1][0] ? $nick :
      "$nick:$_[ARG1][0]";
    $_[KERNEL]->yield( _colorprint => "* \e01$from\e00 $_[ARG2]", 1 );
}

sub irc_error {
    $_[KERNEL]->yield( error => $_[ARG0] );
}

sub irc_join {
    my ($nick, $host) = stripnick( $_[ARG0] );
    $_[KERNEL]->yield( irc => "\e11$nick \e00[\e01$host\e00] has joined \e10$_[ARG1]" );
}

sub irc_mode {
    my $nick = stripnick( $_[ARG0] );
    my $params = $_[ARG3] ? ' ' . join( ' ', @_[ARG3..$#_] ) : '';
    $_[KERNEL]->yield( irc => "mode/\e11$_[ARG1]\e00 [$_[ARG2]$params] by \e01$nick" );
}

sub irc_msg {
    $_[KERNEL]->yield( _colorprint => "\e12[\e01$_[ARG0]\e12]\e00 $_[ARG2]", 1 );
}

sub irc_nick {
    my $nick = stripnick( $_[ARG0] );
    $_[KERNEL]->yield( irc => "\e01$nick\e00 is now known as \e11$_[ARG1]" );
}

sub irc_part {
    my ($nick, $host) = stripnick( $_[ARG0] );
    $_[KERNEL]->yield( irc => "\e01$nick\e00 [\e01$host\e00] has left \e10$_[ARG1]" );
}

sub irc_ping {} # actually, we should not receive this message

sub irc_public {
    my $nick = stripnick( $_[ARG0] );
    my $channel = ref( $_[ARG1] ) ? $_[ARG1][0] : $_[ARG1];
    my $from = $_[HEAP]->{shared}->{channel} eq $channel ? $nick :
      "$nick:$channel";
    $_[KERNEL]->yield( _colorprint => "<\e01$from\e00> $_[ARG2]", 1 );
}

sub irc_quit {
    my ($nick, $host) = stripnick( $_[ARG0] );
    $_[KERNEL]->yield( irc => "\e01$nick\e00 [\e01$host\e00] has quit [$_[ARG1]]" );
}

# Topic
sub irc_332 {
    my ($channel, $topic) = split m/[ :]+/, $_[ARG1];
    $_[KERNEL]->yield( irc => "Topic for \e01$channel\e00: $topic" );
}

# Topic set by
sub irc_333 {
    my ($channel, $user, $time) = split m/\s+/, $_[ARG1];
    my ($nick, $host) = stripnick( $user );
    $_[KERNEL]->yield( irc => "Topic set by \e10$nick\e00[$host] ["
		       . scalar localtime( $time ) . "]"
		     );
}

# namreply
sub irc_353 {
    my (undef, $channel, @names) = split m/[ :]+/, $_[ARG1];
    $_[KERNEL]->yield( irc => "Names on \e01$channel\e00:" );
    $_[KERNEL]->yield( irc => join( ' ', map( "[\e01$_\e00]", @names ) ), 1 );
}

# motd
sub irc_372 {
    $_[KERNEL]->yield( irc => $_[ARG1] );
}


######################################################################
# Other handlers
#

# Initialize console
sub init {
    use integer;
    my ($kernel, $heap) = @_[KERNEL, HEAP];

    unless ($heap->{console}) {
	die "Initialization failed. No Wheel!";
    }

    # Get console size
    my $rows = getmaxy();
    my $cols = getmaxx();

    # Get screen middle
    my $midrow = $rows / 2;

    # Set right margin for Text::Wrap
    $Text::Wrap::columns = $cols;
    $heap->{cols} = $cols;

    # Set newline mode (or whatever it is called)
    nl();

    # Turn of mouse events
    my $oldmousemask;
    mousemask( 0, $oldmousemask );

    # Assign Curses colors
    init_pair( WIN_MAIN, COLOR_WHITE, COLOR_BLACK );
    init_pair( WIN_STATUS, COLOR_WHITE, COLOR_BLUE );
    init_pair( WIN_STATUS_ALTERNATE, COLOR_CYAN, COLOR_BLUE );
    init_pair( WIN_INPUT, COLOR_WHITE, COLOR_BLACK );
    init_pair( WIN_ERR, COLOR_RED, COLOR_BLACK );

    init_pair( C_NORMAL, COLOR_WHITE, COLOR_BLACK );
    init_pair( C_ALTERNATE, COLOR_CYAN, COLOR_BLACK );
    init_pair( C_ERROR, COLOR_RED, COLOR_BLACK );
    init_pair( C_IRC, COLOR_BLUE, COLOR_BLACK );
    init_pair( C_SYSTEM, COLOR_GREEN, COLOR_BLACK );
    init_pair( C_PLUGIN, COLOR_MAGENTA, COLOR_BLACK );
    init_pair( C_DEBUG, COLOR_YELLOW, COLOR_BLACK );

    # Create main window (from top line to middle line)
    delwin( $heap->{win_main} ) if ($heap->{win_main});
    $heap->{win_main} = newwin( $midrow-1, $cols, 1, 0 );
    scrollok( $heap->{win_main}, 1 );
    clear( $heap->{win_main} );
    noutrefresh( $heap->{win_main} );

    # Create message window
    delwin( $heap->{win_msg} ) if ($heap->{win_msg});
    $heap->{win_msg} = newwin( $rows-$midrow-3, $cols, $midrow+1, 0 );
    scrollok( $heap->{win_msg}, 1 );
    clear( $heap->{win_msg} );
    noutrefresh( $heap->{win_msg} );

    # Create top status window
    delwin( $heap->{win_status1} ) if ($heap->{win_status1});
    $heap->{win_status1} = newwin( 1, $cols, 0, 0 );
    bkgd( $heap->{win_status1}, COLOR_PAIR( WIN_STATUS ) );
    clear( $heap->{win_status1} );
    noutrefresh( $heap->{win_status1} );

    # Create middle status window
    delwin( $heap->{win_status2} ) if ($heap->{win_status2});
    $heap->{win_status2} = newwin( 1, $cols, $midrow, 0 );
    bkgd( $heap->{win_status2}, COLOR_PAIR( WIN_STATUS ) );
    clear( $heap->{win_status2} );
    noutrefresh( $heap->{win_status2} );

    # Create bottom status window
    delwin( $heap->{win_status3} ) if ($heap->{win_status3});
    $heap->{win_status3} = newwin( 1, $cols, $rows-2, 0 );
    bkgd( $heap->{win_status3}, COLOR_PAIR( WIN_STATUS ) );
    clear( $heap->{win_status3} );
    noutrefresh( $heap->{win_status3} );

    # Create input window
    delwin( $heap->{win_input} ) if ($heap->{win_input});
    $heap->{win_input} = newwin( 1, $cols, $rows-1, 0 );
#    bkgd( $heap->{win_input}, COLOR_PAIR( WIN_INPUT ) );
    clear( $heap->{win_input} );
    addstr( $heap->{win_input}, $heap->{input_buffer} )
      if ($heap->{input_buffer});
    noutrefresh( $heap->{win_input} );

    # Update screen (should not be necessary here)
    doupdate;

    # Post a status update event
    $kernel->post( console => 'refresh_status' );

    return 1;
}

# Handle input from console
sub input {
    my ($kernel, $heap, $key, $wheel) = @_[KERNEL, HEAP, ARG0,ARG1];

    my $win = $heap->{win_input};

    # Was this the enter key?
    if ($key eq "\n") {
	if ($heap->{input_buffer}) {

	    # Send commands for parsing to main session
	    if ($heap->{input_buffer} =~ m{^/(\w+)?\s*(.*)}) {
		my $cmd = lc( $1 );
		my @params = split m/\s+/, $2;

		$kernel->post( 'robot', 'parse_cmd', $cmd, @params ) if ($cmd);
	    }

	    # Send anything else directly to the current channel.
	    else {
		# Send as a send message to IRC
		if ($heap->{shared}->{channel}) {
		    $kernel->post( robot => say =>
				   $heap->{shared}->{channel} =>
				   $heap->{input_buffer} );
		}
	    }

	    # Clear window and input buffer
	    erase( $win );
	    refresh( $win );
	    $heap->{input_buffer} = '';
	}
    }

    # Backspace
    elsif ($key eq "\b" || $key eq "263") {
	substr( $heap->{input_buffer}, -1, 1, '' );
	addch( $win, "\b" );
	clrtoeol( $win );
	refresh( $win );
    }

    # Resize or redraw
    elsif ($key eq "\cL" || $key eq "410") {
	$kernel->yield( 'init' );
    }

    # C-x
    elsif ($key eq "\cX") {
	$kernel->post( robot => 'next_channel' );
    }

    # Some function key
    elsif ($key =~ /^\d{2,}$/) {
	$kernel->post( console => system => "Unsupported key <" .
		       keyname( $key ) . "> (code $key) pressed." );
    }

    # Add to input buffer if it is not a control or function key
    else {
	if ($key ge ' ' && $key !~ /^\d{2,}$/) {
	    $heap->{input_buffer} .= $key;
	    addch( $win, $key );
	    refresh( $win );
	}
    }
}

# Update status bar
sub refresh_status {
    my $heap = $_[HEAP];
    my $w1 = $heap->{win_status1};
    my $w2 = $heap->{win_status2};
    my $w3 = $heap->{win_status3};
    my $shared = $heap->{shared};

    my ($channel, $topic);

    # Get information for status bar
    if ($shared->{channel}) {
	$channel = $shared->{channel};
	$topic = ($shared->{channels}->{$channel}->{topic}
		  || '(no topic)');
    }
    else {
	$channel = 'no channel';
	$topic = '(no topic)';
    }

    # Print to upper status window
    addstr( $w1, 0, 1, "$shared->{title} - $topic" );
    clrtoeol( $w1 );

    # Print to middle status window
    addch( $w2, 0, 1, ord('[') | COLOR_PAIR( WIN_STATUS_ALTERNATE ) );
    addstr( $w2, $shared->{nick} );
    addch( $w2, ord(']') | COLOR_PAIR( WIN_STATUS_ALTERNATE ) );
    addch( $w2, ' ' );
    addch( $w2, ord('[') | COLOR_PAIR( WIN_STATUS_ALTERNATE ) );
    addstr( $w2, $channel );
    addch( $w2, ord(']') | COLOR_PAIR( WIN_STATUS_ALTERNATE ) );
    clrtoeol( $w2 );

    # Print lower status window
    addstr( $w3, 0, 1, "Plugins: $heap->{config}->{_}->{plugins}" );
    clrtoeol( $w3 );

    # Refresh
    noutrefresh( $w1 );
    noutrefresh( $w2 );
    noutrefresh( $w3 );
    doupdate();
}

# This is ridicolous, but working with curses sucks...
sub _colorprint {
    my ($heap, $text, $mainwindow) = @_[HEAP, ARG0, ARG1];

    # Select window
    my $window = $mainwindow ? $heap->{win_main} : $heap->{win_msg};

    # Get timestamp
    my $timestamp = timestamp;

    # Length of timestamp
    my $timestamplen = length( $timestamp );

    # Maximum length of first line
    my $firstlinelen = $heap->{cols} - $timestamplen;

    # Maximum length of rest of lines
    my $nextlinelen = $firstlinelen - 4;

    # The text split into lines
    my @lines;

    # How many characters to search for break positions at the right
    # margin.
    my $breakzone = 16;

    # Where to stop
    my $stop = length( $text ) - 1;

    # Current position in text string
    my $index = 0;

    # Wrap long lines. This algorithm is ugly, but it's a bit
    # complicated when you have "hidden" characters in the text.

    do {

	# This is where search starts
	my $start = $index;

	# Current calculated length
	my $len = 0;

	# Maxlength; if there already is a line put away, use the
	# 'nextlinelen'.
	my $maxlen = @lines ? $nextlinelen : $firstlinelen;

#	DEBUG "find possible breakpoint: index=$index; stop=$stop";

	# Find first possible breakpoint
	while ($len < $maxlen && $index <= $stop) {

	    # Get current character
	    my $ch = substr( $text, $index, 1 );

#	    DEBUG "ch=", ord($ch), "; len=$len; index=$index; stop=$stop";

	    # If this is an escape character, skip next two chars.
	    if ($ch eq "\e") {
		$index += 2;
#		DEBUG "skipping; index=$index";
	    }

	    # If character is not a bold toggle, increase line length
	    elsif ($ch ne "\cB") {
		$len++;
	    }

	    # Next position
	    $index++;
	}


	# Find possible breakpoint if we're not at end of line. We
	# want to cut the line in the position BEFORE index, so we
	# start with breakpoint=1.
	my $breakpoint = 1;
	if ($index < $stop) {
	    while (substr( $text, $index-$breakpoint, 1 ) !~ m/[ -]/ 
		   && $breakpoint <= $breakzone ) {
		$breakpoint++;
	    }
	}

	# If a breakpoint was found, decrease index
	if ($breakpoint <= $breakzone) {
	    $index = $index - $breakpoint + 1;
	}

#	DEBUG "start=$start; index=$index; breakpoint=$breakpoint; len=", $index-$start;

	# Now, cut out the line and add it to the 'lines' array. If
	# this is the first line, add a timestamp.
	if (@lines) {
#	    DEBUG "  timestamplen = $timestamplen";
	    push @lines, ' ' x ($timestamplen + 4)
	      . substr( $text, $start, $index-$start );
	} else {
	    push @lines, $timestamp . substr( $text, $start, $index-$start );
	}

#	DEBUG "push line: [", substr( $text, $start, $index-$start ), "]";

    } until ($index >= $stop);

    # Set default attributes
    my $attr = COLOR_PAIR( C_NORMAL );
    my $bold = 0;
    attrset( $window, $attr );

    # Print all lines
    foreach my $line (@lines) {

	# Print newline (from previous line) if we're not at the start
	# of a line.
	my ($y,$x);
	getyx( $window, $y, $x );
	addch( $window, "\n" )
	  if ($x != 0);

	while (length( $line )) {
	    my $ch = substr( $line, 0, 1, '' );

	    # Treat an escape character as if there is a new attribute code
	    if ($ch eq "\e") {
		my $newattr = substr( $line, 0, 2, '' );
		if ($newattr =~ m/^(\d)(\d)$/) {
		    $attr = S_ATTR->[$1] | COLOR_PAIR( S_COLOR->[$2] );
		}
	    }

	    # Treat a C-b as a bold attribute toggle
	    elsif ($ch eq "\cB") {
		if ($bold) {
		    $attr = ($attr & ~A_BOLD);
		    $bold=0;
		} else {
		    $attr = ($attr | A_BOLD);
		    $bold=1;
		}
	    }

	    # Add the character and its attribute to the Curses buffer
	    else {
		addch( $window, $attr | ord($ch) );
	    }
	}
    }

    # Refresh screen
    refresh( $window );
}

1;

