# $Id$
#
# This is a simple package to print nicely formatted error messages to
# the CGI error log file.
#
# $Log$
# Revision 1.2  2003/06/20 07:57:22  stefan
# Changed debug message format. Cleaned module.
#
# Revision 1.1  2003/06/10 15:51:57  stefan
# This is the embryo of the new Robot, AKA Robot_MkII. It's much more
# advanced than the old one, with multitasking sessions and completely
# event driven. This will be good.
#
#
package Robot::Debugger;

use strict;
use warnings;

BEGIN {
    use Exporter();
    our (@ISA, @EXPORT, @EXPORT_OK);
    @ISA = qw(Exporter);
    @EXPORT = qw(DEBUG);
    @EXPORT_OK = qw(DEBUG);
}
#our @EXPORT_OK;

our $DEBUG=1;

sub DEBUG {
    return unless ($DEBUG);
    my ($package, $filename, $line) = caller;
    my (undef, undef, undef, $sub) = caller(1);

    print STDERR scalar localtime,  " [$sub/$line] ", @_, "\n";
}

1;

