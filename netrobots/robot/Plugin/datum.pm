# $Id$
#
# En enkel plugin f�r att tala om vadd et �r f�r datum.
#
# $Log$
# Revision 1.1  2004/04/22 06:11:56  zerblat
# Robot was restored since the original CVS repository was lost.
#
# Revision 1.4  2003/07/29 12:19:01  stefan
# Anpassad till nya plugin-interfacet.
#
# Revision 1.3  2003/06/26 14:57:12  stefan
# Lade till kommandot "nu".
#
# Revision 1.2  2003/06/20 10:59:14  stefan
# Plugin-interfacet �ndrat.
#
# Revision 1.1  2003/06/13 12:46:40  stefan
# En enkel plugin f�r att tala om vad det �r f�r datum.
#
#
package Plugin::datum;

use base 'Plugin::Base';
use strict;
use locale;
use POE;
use POSIX qw(strftime);
use POSIX qw(locale_h);

our @EVENTS = qw(init cmd_datum cmd_tid cmd_vecka cmd_nu);
my @CMD = qw(cmd_datum cmd_tid cmd_vecka cmd_nu);

sub init {
    $_[KERNEL]->post( pluginhandler => register => @CMD );
    setlocale( LC_TIME, "sv_SE.iso8859-1" );
}

sub cmd_datum {
    my ($kernel, $postback) = @_[KERNEL, ARG5];

    &$postback( 'I dag �r det ' . strftime( '%Aen den %e %B, %Y.',
					    localtime ), 1 );
}

sub cmd_tid {
    my ($kernel, $postback) = @_[KERNEL, ARG5];

    &$postback( 'Klockan �r nu ' . strftime( '%H:%M.', localtime ), 1 );
}

sub cmd_vecka {
    my ($kernel, $postback) = @_[KERNEL, ARG5];

    &$postback( strftime( 'Det �r vecka %V.', localtime ), 1 );
}

sub cmd_nu {
    my ($kernel, $postback) = @_[KERNEL, ARG5];

    &$postback( strftime( 'Klockan �r %H:%M, %Aen den %e %B, %Y, vecka %V.',
			  localtime) , 1 );
}

1;
