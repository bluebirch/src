# $Id$
#
# Klaga p� alla som s�ger muh.
#
# ARG0: nick!hostmask
# ARG1: channels/users (array reference)
# ARG2: text of message (possibly slightly altered)
# ARG3: parameters (array reference)
# ARG4: original event (irc_public or irc_msg)
# ARG5: callback function (code reference)
#
# $Log$
# Revision 1.3  2003/08/13 14:46:11  stefan
# Variera responsen lite.
#
# Revision 1.2  2003/08/13 14:00:57  stefan
# Lite k�nsligare muh-detektor.
#
# Revision 1.1  2003/08/13 07:49:18  stefan
# Klaga p� alla som s�ger "muh".
#
#
package Plugin::nomuh;

use base 'Plugin::Base';
use strict;
use POE;
use Robot::Functions;

our @EVENTS = qw(init listen);

my @NOMUH = ( "Inget muhande h�r, tack!",
	      "M�ste du muha s� mycket?",
	      "Sn�lla, sluta muha!",
	      "Jag orkar inte h�ra muh muh h�r dagarna i �nda!",
	      "Muh p� dig ocks�, tr�km�ns.",
	      "Nej, tack, vi skall inte ha n�got muh i dag.",
	      "Muh, muh.",
	      "Sluta muha!",
	      "Tyst! Jag vill inte h�ra muh!",
	      "L�t mig slippa h�ra detta st�ndiga muhande!",
	      "S�g inte muh.",
	      "S�g inte muh. Det �r s� tr�kigt.",
	      "Tr�k-muh! H�ll tyst!",
	      "M�ste ni muha j�mt p� den h�r kanalen?",
	      "Jag vill inte h�ra mera muh!",
	      "Jag tycker inte om muh!",
	      "Muh l�ter illa! Sluta!",
	      "Muh luktar illa!",
	      "Inte muh igen!",
	      "Muh igen? Du ger dig visst aldrig?",
	      "Muh var du rolig, trodde du va?",
	      "Muh �r inte roligt.",
	      "Muh �r det s� jobbigt.",
	      "Det �r tr�ttsamt att lyssna p� detta muhande hela tiden.",
	      "Muh hit och muh dit. Har du inte n�got vettigt att s�ga?",
	      "Muh nu igen? Vilket varierat ordf�rr�d du har!",
	      "�r inte muh en svordom?",
	      "Kossor s�ger muh. �r du en kossa?",
	      "Sluta muha, din fula kossa!",
	      "Kossa muh.",
	      "Mamma Muh �r inte hemma. Sluta ropa p� henne.",
	      "Nej, inte muh! Inte muh! Inte muh!",
	      "Nej, nej, nej! Inte muh!",
	      "Jag st�r inte ut med detta eviga muhande!",
	      "Sluta muha! Jag blir tokig!",
	      "Om du inte slutar muha snart blir jag tokig!",
	      "Ta du din muh och stoppa upp n�gonstans!",
	      "Jamen muh d�!"
	    );

sub init {
    $_[KERNEL]->post( pluginhandler => register => qw(listen) );
}

sub listen {
    my ($from, $msg, $postback) = @_[ARG0, ARG2, ARG5];

    if ($msg =~ m/(^|\W)m+u+h*(\W|$)/i) {
	my $nick = stripnick( $from );
	&$postback( "$nick: " . $NOMUH[ rand( $#NOMUH )] );
    }
    else {
	&$postback();
    }
}

1;
