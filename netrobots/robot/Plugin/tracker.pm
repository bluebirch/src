# $Id$
#
# This plugin keeps track of channels and users. Data is saved on the
# shared data area, and is, for example, used by the console session.
#
# This plugin is mandatory. Robot won't function properly if it isn't
# loaded.
#
# $Log$
# Revision 1.4  2003/07/29 11:39:08  stefan
# Tracker plugin adapted to new plugin base.
#
# Revision 1.3  2003/06/21 16:45:49  stefan
# Track topic changes.
#
# Revision 1.2  2003/06/12 14:47:15  stefan
# Forgot to track changes of my own nick.
#
# Revision 1.1  2003/06/12 13:39:35  stefan
# A first version which hopefully does not include too many bugs. It
# seems that the configuration does not work properly.
#
#
package Plugin::tracker;

use base 'Plugin::Base';
use strict;
use POE;

our @EVENTS = qw(init shutdown irc_join irc_part irc_quit irc_kick irc_nick irc_353 irc_332 irc_topic dump);

my @IRC_EVENTS = qw(join part quit kick nick 353 topic 332);

sub init {
    $_[KERNEL]->post( irc => register => @IRC_EVENTS );
}

sub shutdown {
    $_[KERNEL]->post( irc => unregister => @IRC_EVENTS );
}

#
# JOIN
#
# 1. Register user (if not known)
# 2. Register channel on user
# 3. Register channel (if not known)
# 4. Register user on channel
#

sub irc_join {
    my ($kernel, $heap, $user, $channel) = @_[KERNEL, HEAP, ARG0, ARG1];
    my $shared = $heap->{shared};

    # Get nick and host mask
    my ($nick, $hostmask) = split m/!/, $user, 2;

    # Do I know this user? If not, register.
    if (!$shared->{users}->{$nick}) {
	$shared->{users}->{$nick}->{hostmask} = $hostmask;
	$shared->{users}->{$nick}->{detected} = time;
    }

    # Register channel on this user
    $shared->{users}->{$nick}->{channels}->{$channel} = time;

    # If this is me, I can be quite certain I know nothing about this
    # channel. Also, select channel and everything.
    if ($nick eq $shared->{nick}) {

	# Select this as the current channel
	$shared->{channel} = $channel;

	# Add channel to shared list of channels
	$shared->{channels}->{$channel} = { join_time => time };

	# Refresh console
	$kernel->post( console => 'refresh_status' );

	# A tiny message
	$kernel->post( console => plugin => "Registered channel \cB$channel" )
	  if ($heap->{config}->{verbose});
    }

    # Register channel on user
    $shared->{channels}->{$channel}->{users}->{$nick} = time;

    $kernel->post( console => plugin =>
		   "Registered \cB$nick\cB on \cB$channel" )
      if ($heap->{config}->{verbose});
}

#
# PART:
#
# 1. Delete user from channel
# 2. Delete channel form user
# 3. Delete user, if I can't see him on any channels
# 4. If it was me who left the channel, forget the channel
#    completely. Also, switch to next channel and update status bar.
#

sub irc_part {
    my ($kernel, $heap, $nickhost, $channel) = @_[KERNEL, HEAP, ARG0, ARG1];
    my $shared = $heap->{shared};

    # Get nick and host mask
    my ($nick, $hostmask) = split m/!/, $nickhost, 2;

    # Delete this user from channel
    delete $shared->{channels}->{$channel}->{users}->{$nick};

    # Show an informal message
    $kernel->post( console => plugin => "Unregister \cB$nick\cB on $channel" )
      if ($heap->{config}->{verbose});

    # Delete channel from user
    delete $shared->{users}->{$nick}->{channels}->{$channel};

    # Delete user if it has no more channels
    if (scalar %{ $shared->{users}->{$nick}->{channels} } == 0) {
	delete $shared->{users}->{$nick};
	$kernel->post( console => plugin =>
		       "Removed user \cB$nick\cB; not on any known channels" )
	  if ($heap->{config}->{verbose});
    }

    # If this is me, forget anything about the channel
    if ($nick eq $shared->{nick}) {

	# Delete channel from users. Ultimately, delete users if they
	# no longer are on any know channel.
	foreach my $user (keys %{ $shared->{users} }) {
	    delete $shared->{users}->{$user}->{channels}->{$channel};
	    if (scalar %{ $shared->{users}->{$user}->{channels} } == 0) {
		delete $shared->{users}->{$user};
		$kernel->post( console => plugin =>
			       "Removed user \cB$user\cB; not on any known channels" )
		  if ($heap->{config}->{verbose});
	    }
	}

	# Delete channel itself
	delete $shared->{channels}->{$channel};

	# Go to next (remaining) channel (if any)
	$kernel->post( robot => 'next_channel' );

	# A tiny message
	$kernel->post( console => plugin => "Removed channel \cB$channel" )
	  if ($heap->{config}->{verbose});
    }
}

#
# NAMES:
#
# 1. Register all nicks on channel
# 2. Register channel on all nicks
#
sub irc_353 {
    my ($kernel, $heap) = @_[KERNEL, HEAP];
    my $shared = $heap->{shared};
    my (undef, $channel, @names) = split m/[ :\@]+/, $_[ARG1];

    # Register all nicks if they are not known
    foreach my $nick (@names) {
	$shared->{channels}->{$channel}->{users}->{$nick} = time
	  unless ($shared->{channels}->{$channel}->{users}->{$nick});
	$shared->{users}->{$nick}->{channels}->{$channel} = time
	  unless ($shared->{users}->{$nick}->{channels}->{$channel});
	$kernel->post( console => plugin => "Register \cB$nick\cB on \cB$channel" )
	  if ($heap->{config}->{verbose});
    }
}

#
# QUIT:
#
# 1. If it was me, forget anything about anyone; otherwise:
# 2. Remove user from channels
# 3. Remove user
#
sub irc_quit {
    my ($kernel, $heap, $user) = @_[KERNEL, HEAP, ARG0];
    my $shared = $heap->{shared};

    # Get nick and host mask
    my ($nick, $hostmask) = split m/!/, $user, 2;

    # Was this me? In that case, forget everything, since I'm no
    # longer connected to the server.
    if ($nick eq $shared->{nick}) {
	$shared->{channels} = {};
	$shared->{users} = {};
    }

    else {
	# Remove user from channels
	foreach my $channel (keys %{ $shared->{channels} }) {
	    delete $shared->{channels}->{$channel}->{users}->{$nick};
	}
	# Remove user
	delete $shared->{users}->{$nick};

	# Display what is happening
	$kernel->post( console => plugin =>
		       "Removed user \cB$nick" )
	  if ($heap->{config}->{verbose});
    }
}

#
# KICK:
#
# Same as PART. Reschedule message.
#
sub irc_kick {
    $_[KERNEL]->post( console => plugin => "rescheduling event" );
    $_[KERNEL]->yield( 'irc_part', @_[ARG2, ARG1] );
}

#
# NICK
#
# 1. Change nick on channels
# 2. Change nick on user
#
sub irc_nick {
    my ($kernel, $heap, $user, $newnick) = @_[KERNEL, HEAP, ARG0, ARG1];
    my $shared = $heap->{shared};

    # Get nick and host mask
    my ($nick, $hostmask) = split m/!/, $user, 2;

    # Change nick on channels. Is it possible to change the name of a
    # hash key without copying its contents?
    foreach my $channel (keys %{ $shared->{channels} }) {
	$shared->{channels}->{$channel}->{users}->{$newnick} =
	  delete $shared->{channels}->{$channel}->{users}->{$nick};
    }

    # Change nick on user
    $shared->{users}->{$newnick} = delete $shared->{users}->{$nick};

    # Change my nick if it is me
    if ($shared->{nick} eq $nick) {
	$shared->{nick} = $newnick;
	$kernel->post( console => 'refresh_status' );
    }

    # Show a message
    $kernel->post( console => plugin => "Registered nick change \cB$nick\cB -> \cB$newnick" )
      if ($heap->{config}->{verbose});
}

#
# 332 TOPIC
#
# Just rescedule for TOPIC below
#
sub irc_332 {
    my ($kernel, $chantopic) = @_[KERNEL, ARG1];
    my ($channel, $topic) = split m/[ :]+/, $chantopic, 2;
    $kernel->yield( irc_topic => undef, $channel, $topic )
}

#
# TOPIC
#
sub irc_topic {
    my ($heap, $kernel, $channel, $topic) = @_[HEAP, KERNEL, ARG1, ARG2];
    $heap->{shared}->{channels}->{$channel}->{topic} = $topic;
    $kernel->post( console => 'refresh_status' );
    $kernel->post( console => plugin => "Registered topic for \cB$channel" )
      if ($heap->{config}->{verbose});
}

#
# dump (internal function)
#
sub dump {
    my ($kernel, $heap) = @_[KERNEL, HEAP];
    my $shared = $heap->{shared};

    $kernel->post( console => plugin => "Known channels:" );

    foreach my $channel (keys %{ $shared->{channels} }) {
	my @users = keys %{ $shared->{channels}->{$channel}->{users} };
	$kernel->post( console => plugin => "  \cB$channel\cB: @users" );
    }

    $kernel->post( console => plugin => "Known users:" );

    foreach my $user (keys %{ $shared->{users} }) {
	my @channels = keys %{ $shared->{users}->{$user}->{channels} };
	$kernel->post( console => plugin => "  \cB$user\cB: @channels" );
    }
}

1;
