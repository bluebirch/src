# $Id$
#
# This is an example plugin. To write plugins for Robot, follow these
# steps:
#
#  1. Read the POE documentation.
#  2. Read the POE documentation again.
#  3. Modify this example plugin.
#
# $Log$
# Revision 1.2  2003/06/20 13:22:10  stefan
# Gee, I need to explain this stuff.
#
# Revision 1.1  2003/06/11 21:55:12  stefan
# This is a very, very simple example of a plugin. The only thing it
# does is to say "gee, I'm alive", and then dies.
#
#
package Plugin::lookup;

use base 'Plugin::Base';
use strict;
use locale;
use POE;
use Robot::Functions;

our @EVENTS = qw(init cmd_lookup lookup);

sub init {
    my $kernel = $_[KERNEL];
    $kernel->alias_set( 'lookup' );
    $kernel->post( pluginhandler => register => qw(cmd_lookup) );
}

sub shutdown {
    my $kernel = $_[KERNEL];
    $kernel->post( pluginhandler => unregister => qw(cmd_lookup) );
    $kernel->alias_remove( 'lookup' );
}

sub cmd_lookup {
    my ($kernel, $session, $postback, $params) =
      @_[KERNEL, SESSION, ARG5, ARG3];

    my $thing = join( ' ', @$params );

    if ($thing) {
	$kernel->post( brain => whatis => $thing, undef,
		       $session->postback( 'lookup', $thing, $postback ) );
    }
    else {
	&$postback( 'Usage: !lookup <word>' );
    }
}

sub lookup {
    my ($kernel, $request, $response) = @_[KERNEL, ARG0, ARG1];

    my $replies = [];

    if ($$response[0] == 5) {
	for my $ref (@{ $$response[1] }) {
	    $$ref{time} =~ s/(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/$1-$2-$3 $4:$5:$6/;
	    push @$replies, sprintf( '%s, %s: %s %s %s', 
				     @$ref{qw(source time)},
				     ucfirst( $$request[0] ),
				     @$ref{qw(descriptor description)} );
	}
    }
    else {
	push @$replies, 
	  sprintf( '%s: Nothing to report.', uc( $$request[0] ) );
    }

    my $postback = $$request[1];

    if (ref( $postback )) {
	&$postback( $replies );
    }
}

1;
