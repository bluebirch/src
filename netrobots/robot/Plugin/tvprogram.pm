# $Id$
#
# $Log$
# Revision 1.3  2003/07/29 12:23:30  stefan
# Adapted to new plugin interface.
#
# Revision 1.2  2003/07/08 14:45:07  stefan
# Fixed some error handling.
#
# Revision 1.1  2003/07/08 14:30:36  stefan
# Get TV programs from tvprogram.nu. This works on the first attempt!
# I'm stunned!
#
#
package Plugin::tvprogram;

use base 'Plugin::Base';
use strict;
use POE;
use Robot::Functions;

our @EVENTS = qw(init cmd_tv _parse);

sub init {
    my $kernel = $_[KERNEL];
    $kernel->post( pluginhandler => register => qw(cmd_tv) );
}

#08:40 <marvi> Tja. N�got f�r Robot:
#              http://k76.ryd.student.liu.se/tv/docs/developer.html

sub cmd_tv {
    my ($kernel, $session, $params, $postback) =
      @_[KERNEL, SESSION, ARG3, ARG5];

    my $channel_mask = ($$params[0] || 'SVT|TV3 S|TV4|KANAL5');

    $kernel->post( netsuck => geturl =>
		   'http://www.tvprogram.nu/cgi-billigast/tvp9s.cgi?NU-tvprogramnu.htm',
		   $session->postback( '_parse', $channel_mask, $postback )
		 );
}

sub _parse {
    my ($kernel, $request, $response) = @_[KERNEL, ARG0, ARG1];

    my $channels = $$request[0];
    my $text = $$response[0];

    my $result = [];

    if ($text) {
	while ($text =~ m:<TD.*?><font.*?><b>(.*?)</b></font></TD><TD.*?>.*?</TD><TD.*?><font.*?><b>(.*?)\n?</b></font>:gs && scalar @$result <= 4) {

	    my $channel = $1;
	    my $program = $2;

	    push @$result, sprintf( '%-13s %s', $channel, $program )
	      if ($channel =~ m/($channels)/i);
	}
    }
    else {
	push @$result, "Det finns inga TV-program tillg�ngliga just nu.";
    }

    my $postback = $$request[1];

    &$postback( $result );
}


1;
