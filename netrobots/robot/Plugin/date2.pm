# $Id$
#
# Get the current date
#
# $Log$
# Revision 1.3  2003/06/12 11:22:14  stefan
# Still a completely useless plugin.
#
# Revision 1.2  2003/06/12 11:09:13  stefan
# Reworked plugin interface. Added configuration file with Config::Tiny.
#
# Revision 1.1  2003/06/11 22:10:27  stefan
# Completely useless.
#
#
package Plugin::date2;

use base 'Plugin::Base';
use strict;
use POE;

our @EVENTS = qw(init cmd_date _default);

sub init {
    $_[KERNEL]->alias_set( 'date2' );
    $_[KERNEL]->post( pluginhandler => register => qw(cmd_date) );
    $_[HEAP]->{config}->{last} = time;
}

sub shutdown {
    $_[KERNEL]->alias_remove( 'date2' );
    $_[KERNEL]->post( pluginhandler => unregister => qw(cmd_date) );
}

sub _default {
    $_[KERNEL]->post( console => error => "event $_[ARG0] unknown" );
}

sub cmd_date {
    my ($kernel, $postback) = @_[KERNEL, ARG0];

    &$postback( scalar localtime );
}

1;
