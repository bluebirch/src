# $Id$
#
# Simply a POE::Component::Client::HTTP wrapper.
#
# $Log$
# Revision 1.3  2003/07/29 13:05:00  stefan
# Fixed bugs.
#
# Revision 1.2  2003/07/29 12:21:36  stefan
# Adapted to new plugin interface.
#
# Revision 1.1  2003/07/16 13:13:34  stefan
# Simple plugin to fetch web pages.
#
#
package Plugin::netsuck;

use base 'Plugin::Base';
use strict;
use POE qw(Component::Client::UserAgent);
use Robot::Functions;
use HTTP::Request;

our @EVENTS = qw(init shutdown geturl _child _content);

sub init {
    my $kernel = $_[KERNEL];

    # Create PoCo::Client::HTTP
    POE::Component::Client::UserAgent->spawn( agent => 'Robot Netsuck',
					      alias => '_netsuck',
					      max_size => 16384 );

}

sub shutdown {
    my $kernel = $_[KERNEL];

    $kernel->post( _netsuck => 'shutdown' );
}

sub _child {
    $_[KERNEL]->post( console => plugin => "Child: $_[ARG0]" );
}

sub geturl {
    my ($kernel, $session, $url, $main_postback) = 
      @_[KERNEL, SESSION, ARG0, ARG1];

    my $request = HTTP::Request->new( GET => $url );
    my $postback = $session->postback( '_content', $main_postback );

    $kernel->post( console => plugin => "GET $url" );

    $kernel->post( _netsuck => request => { request => $request,
					    response => $postback } );
}

sub _content {
    my ($kernel, $request, $response) = @_[KERNEL, ARG0, ARG1];

    my $postback = $$request[0];
    my ($req, $res, $entry) = @$response;

    my $content;
    $content = $res->content if ($res->is_success);

    if (ref( $postback ) eq 'POE::Session::Postback') {
	&$postback( $content );
    }
    else {
	$kernel->post( console => plugin =>
		       "No postback supplied! Requested web page goes to /dev/null." );
    }
}

1;
