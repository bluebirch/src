# $Id$
#
# Base class for plugins.
#
# $Log$
# Revision 1.1  2004/04/22 06:11:54  zerblat
# Robot was restored since the original CVS repository was lost.
#
# Revision 1.5  2003/07/29 12:11:14  stefan
# Changed plugin interface. Base class sets alias and unregisters
# automatically with pluginhandler. Init and shutdown methods no longer
# mandatory.
#
# Revision 1.4  2003/07/08 14:36:02  stefan
# Changed shutdown method.
#
# Revision 1.3  2003/06/12 11:20:24  stefan
# Removed some ugly debugging.
#
# Revision 1.2  2003/06/12 11:09:13  stefan
# Reworked plugin interface. Added configuration file with Config::Tiny.
#
# Revision 1.1  2003/06/11 21:50:51  stefan
# A simple base class for plugins.
#
#
package Plugin::Base;

use strict;
use POE;

# This is a list of events this module can receive.
our @EVENTS = qw();

# This is a description of the module.
our $DESCRIPTION = 'This module has no description.';

sub new {
    my $this = shift;
    my $class = ref($this) || $this;
    my $self = {};

    # Get session alias from plugin name
    ($self->{alias}) = ($class =~ m/::(.*)/);

    bless $self, $class;
    return $self;
}

sub DESTROY {
    my $self = shift;
}

# POE session start handler
sub _start {

    # Get configuration and shared data
    $_[HEAP]->{config} = $_[ARG0];
    $_[HEAP]->{shared} = $_[ARG1];

    # Set alias
    $_[KERNEL]->alias_set( $_[OBJECT]->{alias} );

    # Post an init event back to myself. It is silently ignored if no
    # init event is handled.
    $_[KERNEL]->yield( 'init' );

    # Say hello.
    $_[KERNEL]->post( console => plugin =>
		      "Plugin $_[OBJECT]->{alias} says hello; initializing." );
}

# POE session stop handler
sub _stop {}

# POE default handler
sub _default {
    $_[KERNEL]->post( console => plugin => "Unknown event: \cB$_[ARG0]\cB" );
}

# POE Unload plugin (e.g. make session die)
sub unload {
    # Remove all aliases
    foreach my $alias ($_[KERNEL]->alias_list( $_[SESSION] )) {
  	$_[KERNEL]->alias_remove( $alias );
    }

    # Unregister all events with plugin handler
    $_[KERNEL]->post( pluginhandler => 'unregister_all' );
}

# Normal class function that returns description
sub description {
    return $DESCRIPTION;
}

1;
