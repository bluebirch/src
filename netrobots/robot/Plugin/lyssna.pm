# $Id$
#
# This is an example plugin. To write plugins for Robot, follow these
# steps:
#
#  1. Read the POE documentation.
#  2. Read the POE documentation again.
#  3. Modify this example plugin.
#
# $Log$
# Revision 1.2  2003/06/20 13:22:10  stefan
# Gee, I need to explain this stuff.
#
# Revision 1.1  2003/06/11 21:55:12  stefan
# This is a very, very simple example of a plugin. The only thing it
# does is to say "gee, I'm alive", and then dies.
#
#
package Plugin::lyssna;

use base 'Plugin::Base';
use strict;
use POE;
use Robot::Functions;

our @EVENTS = qw(init listen);

sub init {
    my $kernel = $_[KERNEL];
    $kernel->alias_set( 'example' );
    $kernel->post( pluginhandler => register => qw(talk) );
}

sub shutdown {
    my $kernel = $_[KERNEL];
    $kernel->post( pluginhandler => unregister => qw(talk) );
    $kernel->alias_remove( 'example' );
}

sub talk {
    my ($postback, $user) = @_[ARG4, ARG0];

    my $nick = stripnick( $user );

    &$postback( "$nick, shut up." );
}

1;
