# $Id$
#
# FIXME: Skriv om hela skiten. Skicka runt en postback i st�llet f�r
# att kr�ngla och greja med att f�rs�ka h�lla reda p� vem som skall ha
# svaret. Det blir MYCKET enklare s�.
#
# $Log$
# Revision 1.10  2003/08/13 15:50:04  stefan
# Allvetaren kan gl�mma bort saker. Antalet svarsformuleringar �r
# ut�kat.
#
# Revision 1.9  2003/08/13 15:22:03  stefan
# Anv�nd postbacks i st�llet.
#
# Revision 1.8  2003/07/29 12:17:40  stefan
# Adapted to new plugin interface.
#
# Revision 1.7  2003/07/07 16:54:21  stefan
# Skrev lite kommentarer. Det finns ett b�ttre s�tt att l�sa hela
# rasket, men jag skall cykla hem nu.
#
# Revision 1.6  2003/07/07 16:07:43  stefan
# Filtrera smileys och annat skr�p i inl�rningsprocessen.
#
# Revision 1.5  2003/07/07 16:01:55  stefan
# Allvetaren ers�tter "jag" med anv�ndarens nick.
#
# Revision 1.4  2003/07/07 15:50:22  stefan
# Robot kan ge vettiga svar p� fr�gor.
#
# Revision 1.3  2003/07/04 16:17:40  stefan
# Robot lyssnar och l�r sig.
#
# Revision 1.2  2003/06/27 15:39:01  stefan
# Nu �r det dags att dricka �l!
#
# Revision 1.1  2003/06/26 16:02:07  stefan
# Allvetaren lyssnar p� trafik p� kanalen och skickar det den snappar
# upp till brain.
#
package Plugin::allvetaren;

use base 'Plugin::Base';
use strict;
use locale;
use POE;
use Robot::Functions;

our @EVENTS = qw(init _respond);

my @ANSWERS = ( '',
		'Jag vet. ',
		'Det vet jag. ',
		'Jag har svar p� din fr�ga. ',
		'Jag kan svaret p� din fr�ga. ',
		'L�s och l�r: ',
		'Se och l�r: ',
		'Jag har h�rt ett rykte: ',
		'Jag har h�rt talas om det d�r. ',
		'En f�gel viskade i mitt �ra: ',
		'Jesus sade: ',
		'Ur Encyclopedia Galactia: ',
		'Utdrag ur Liftarens Guide till Galaxen: ',
		'L�t mig citera v�ltalaren Cicero: ',
		'Paulus skrev: ',
		'P�ven har sagt: ',
		'En g�ng l�ste jag det i en tidning. ',
		'Viktig information: ',
		'Nalle Puh s�ger: ',
		'Lao Tse s�ger: ',
		'G�ran Persson sade en g�ng: ',
		'S� h�r �r det: ',
		'Jag har svaret. ',
		'H�r h�r: ',
		'Lyssna noga till farsan Baloo nu: ',
		'S� s�ger HERREN: ',
		'L�s: ',
		'H�r �r svaret: ',
		'Som det �r sagt: ',
		'Som flickan sa: ',
		'Som pojken sa: ',
		'Ur mitt arkiv: ',
		'Jod�, jag vet. ',
		'Om man s�ger s�h�r: ',
		'S�g s�h�r: ',
		'S�h�r kan det vara: ',
		'S�h�r tror jag: ',
		'Det kan vara s�h�r: ',
		'Direkt ur databasen: ',
		'Ur mitt elektroniska arkiv: ',
		'Citerat fritt ur minnet: ',
		'Ur mitt minne: ',
		'Jag minns allt. ',
		'Jag gl�mmer ingenting. ',
		'Jag gl�mmer det aldrig. ',
		'Ett snabbt svar: ',
		'Snabb respons: ',
		'Ett kort svar: ',
		'F�r att sammanfatta: ',
		'H�r �r svaret du s�ker: ',
	      );


sub init {
    my $kernel = $_[KERNEL];
    $kernel->post( pluginhandler => register => qw(listen talk) );

    # Koppla flera event till samma state
    $kernel->state( 'listen', \&parse );
    $kernel->state( 'talk', \&parse );
}

sub parse {
    my ($kernel, $heap, $session, $state, $postback, $from, $channel, $line) =
      @_[KERNEL, HEAP, SESSION, STATE, ARG5, ARG0, ARG1, ARG2];

    # Nicka till
    my ($nick, $hostmask) = stripnick( $from );

    # Kanalisera
    $channel = $$channel[0] if (ref( $channel ));

    # Unders�k om meddelandet �r till n�gon p� kanalen
    if ($line =~ m/^\s*(\S+)\s*[:,]\s*/i) {

	my $to_nick = $1;

	if (defined $heap->{shared}->{users}->{$to_nick}) {
#  	    $kernel->post( console => plugin =>
#  			   "PARSE: Message is for [\cB$to_nick\cB]" );

	    # Klipp bort namnet i b�rjan p� raden
	    $line =~ s/^\s*$to_nick\s*[:,]\s*//;
	    
	}
    }

    # Dela upp meddelandet i meningar. En mening �r en r�cka tecken
    # som avslutas med punkt, utropstecken eller fr�getecken, f�ljt av
    # mellanslag eller radslut, alternativt endast radslut.
    my @sentence;

    while ($line =~ m{\s*   # Ignorera inledande whitespace
                      (.+?([.!?]+(\s|$)|$)) # punk+space/radslut el radslut
                      }gx) {
  	push @sentence, $1;
    }

    # G� igenom varje mening
    foreach my $msg (@sentence) {
	$kernel->post( console => plugin =>
		       "PARSE: [$msg]" );

	# Om det �r en fr�ga b�r vi f�rs�ka besvara den
	if ($msg =~ m/^((vad|vem|hur|var)\s+(�r|var|har|kan|�ger|bor|f�r|k�r|suger)\s+)?(.*?)[\s.!?]*\?[\s.!]*$/i) {

	    my ($thing, $descriptor) = ($4, $3);

	    # Specialbehandling av "jag".
	    if (lc($thing) eq 'jag') {
		$thing = $nick;
	    }

	    $kernel->post( brain => whatis => $thing, $descriptor,
			   $session->postback( '_respond',
					       $state,
					       $nick,
					       $postback,
					       $thing, $descriptor )
			 );
	}

	# Om det �r n�got jag skall gl�mma, gl�m det.
	#
	elsif ($msg =~ m/^gl�m\s+(bort)?\s*(.*?)[\s\.\!\?]*$/i) {
	    my $thing = $2;

	    $kernel->post( console => plugin => "FORGET: \cB$thing\cB" );

	    $kernel->post( brain => forget => $thing, undef,
			 $session->postback( '_respond',
					     $state,
					     $nick,
					     $postback,
					     $thing,
					     undef )
			 );
	}
	    
	else {

	    # Se om detta �r n�got som robot kan snappa upp och l�ra
	    # sig.
	    $msg =~ m/^(en|ett|och)?\s*([\w\d]+|".*?")\s+(�r|var|har|kan|�ger|bor|f�r|k�r|suger)\s+(ocks�\s*)?(.+?)[\s\!\.\?]*$/;

	    my ($pre, $thing, $descriptor, $also, $description) = 
	      ($1, $2, lc($3), $4, $5);

	    # Om inte regexpen matchade n�got, �r det bara att strunta
	    # i den.
	    if ($thing) {

		# Ta bort eventuella citationstecken
		$thing =~ s/^"\s*(.*?)\s*"$/$1/;

		# Specialbehandling av "jag".
		if (lc($thing) eq 'jag') {
		    $thing = $nick;
		}

		# Filtrera bort fula smileys och skit.
		$description =~ s/[-\s:;_()#]+$//;

#		$kernel->post( console => plugin => "LEARN: L�r '$thing' ($descriptor $description) av $nick" );

		# Skicka det hela till brain
		$kernel->post( brain => learn =>
			       $thing, $descriptor, $description,
			       "$nick/$channel",
			       $also ? 1 : 0,
			       $session->postback( '_respond',
						   $state,
						   $nick,
						   $postback,
						   $thing,
						   $descriptor )  );

	    }
	}
    }

    # Returnera inget. L�t andra lyssna vidare om de vill.
    &$postback( [] );

}

sub _respond {
    my ($kernel, $heap, $request, $response) = @_[KERNEL, HEAP, ARG0, ARG1];

#      $kernel->post( console => plugin =>
#  		   "Respond $$response[0] on ["
#  		   . join( "] [", @$request ) . "]" );

    my $postback = $$request[2];

    # Avg�r vem svaret skall g� till
#      my $to = $$request[2] eq $heap->{shared}->{nick} ? $$request[1] :
#        $$request[2];

    # Avg�r om jag skall prata
    my $talk = $$request[0] eq 'talk';

    # Internt fel
    if ($$response[0] == 0) {
  	$kernel->post( console => error => "Internt fel!" );
#	$kernel->post( robot => say => $to => "Internt fel! Ring Poolia!" )
	&$postback( "Internt fel! Ring Polisen!", 1 )
	  if ($talk);
    }

    # N�gonting �r inl�rt.
    elsif ($$response[0] == 1) {
#	$kernel->post( robot => say => $to => "Ok, $$request[1]." )
	&$postback( "Ok, $$request[1]", 1 )
	  if ($talk);
    }

    # N�gonting jag redan visste.
    elsif ($$response[0] == 2) {
#	$kernel->post( robot => say => $to => "Nej, $$request[1], inte alls." )
	&$postback( "Nej, $$request[1], inte alls.", 1 )
	  if ($talk);
    }

    # Det visste robot redan
    elsif ($$response[0] == 3) {
#	$kernel->post( robot => say => $to => "Det visste jag redan." )
	&$postback( "Det visste jag redan.", 1 )
	  if ($talk);
    }

    # Blockerat ord
    elsif ($$response[0] == 4) {
#  	$kernel->post( robot => say => $to =>
#  		       "Du f�r inte l�ra mig s�dana saker. Det �r f�rbjudet!" )
	&$postback( "Du f�r inte l�ra mig s�dana saker. Det �r f�rbjudet!", 1 )
	  if ($talk);
    }

    # Svar p� fr�ga
    elsif ($$response[0] == 5) {

	# Pussla ihop ett varierat, trevligt svar
	my ($descriptor, $reply);
	my $ref = $$response[1];

	for my $i (0..$#$ref) {

	    # Om $reply �r tom, inleder vi med en fin fras fr�n
	    # anwers-listan.
	    unless ($reply) {
		$reply = $ANSWERS[ rand scalar @ANSWERS ];
		$reply .= ucfirst( $$request[3] ) . ' ' .
		  $$ref[$i]{descriptor} . ' ' . $$ref[$i]{description};
		$descriptor = $$ref[$i]{descriptor};

	    # I annat fall bygger vi p� meningen lite snyggt.
	    } else {

		# Om detta �r sista beskrivningen, sammanfattar vi
		# allt med "samt".
		if ($i == $#$ref) {
		    if ($$ref[$i]{descriptor} eq $descriptor) {
			$reply .= " och $$ref[$i]{description}";
		    } else {
			$reply .= "; samt $$ref[$i]{descriptor} $$ref[$i]{description}";
		    }
		}
		# Annars forts�tter vi bara meningen. Om vi har ett
		# annat verb, upprepar vi det.
		else {
		    if ($$ref[$i]{descriptor} eq $descriptor) {
			$reply .= " och $$ref[$i]{description}";
		    } else {
			$reply .= "; $$ref[$i]{descriptor} $$ref[$i]{description}";
		    }
		    $descriptor = $$ref[$i]{descriptor};
		}
	    }
	}

	$reply .= '.';

#	$kernel->post( robot => say => $to => $reply );
	&$postback( $reply, 1 );

    }

    # Svar, fast Robot vet inget
    elsif ($$response[0] == 6) {

#	$kernel->post( robot => say => $to => "Jag vet inte, $$request[1]." )
	&$postback( "Jag vet inte, $$request[1].", 1 )
	  if ($talk);
    }

    # N�got �r gl�mt
    elsif ($$response[0] == 7) {
	&$postback( "Ok, bortgl�mt!" );
    }

    # Allt �r inte gl�mt
    elsif ($$response[0] == 8) {
	&$postback( "Allt �r inte gl�mt. Men n�stan." );
    }

    # N�got �r gl�mt
    elsif ($$response[0] == 9) {
	&$postback( "Det gl�mmer jag aldrig!" );
    }

    # N�got fel
    else {
	$kernel->post( console => plugin => "Ok�nd svarskod $$response[0]" );
	&$postback( "Ok�nd svarskord $$response[0]", 1 )
    }

}

1;
