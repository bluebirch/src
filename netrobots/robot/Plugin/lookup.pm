# $Id$
#
# Lookup something in the database and display a report.
#
# $Log$
# Revision 1.3  2003/07/29 12:19:54  stefan
# Adapted to new plugin interface.
#
# Revision 1.2  2003/07/07 17:00:06  stefan
# End sentence with a dot. Perfectionism! :-)
#
# Revision 1.1  2003/07/07 16:51:27  stefan
# Lookup something in the database and print a report.
#
#
package Plugin::lookup;

use base 'Plugin::Base';
use strict;
use locale;
use POE;
use Robot::Functions;

our @EVENTS = qw(init cmd_lookup lookup);

sub init {
    my $kernel = $_[KERNEL];
    $kernel->post( pluginhandler => register => qw(cmd_lookup) );
}

sub cmd_lookup {
    my ($kernel, $session, $postback, $params) =
      @_[KERNEL, SESSION, ARG5, ARG3];

    my $thing = join( ' ', @$params );

    if ($thing) {
	$kernel->post( brain => whatis => $thing, undef,
		       $session->postback( 'lookup', $thing, $postback ) );
    }
    else {
	&$postback( 'Usage: !lookup <word>' );
    }
}

sub lookup {
    my ($kernel, $request, $response) = @_[KERNEL, ARG0, ARG1];

    my $replies = [];

    if ($$response[0] == 5) {
	for my $ref (@{ $$response[1] }) {
	    $$ref{time} =~ s/(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)/$1-$2-$3 $4:$5:$6/;
	    push @$replies, sprintf( '%s, %s: %s %s %s.',
				     @$ref{qw(source time)},
				     ucfirst( $$request[0] ),
				     @$ref{qw(descriptor description)} );
	}
    }
    else {
	push @$replies, 
	  sprintf( '%s: Nothing to report.', uc( $$request[0] ) );
    }

    my $postback = $$request[1];

    if (ref( $postback )) {
	&$postback( $replies );
    }
}

1;
