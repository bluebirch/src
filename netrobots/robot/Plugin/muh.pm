# $Id$
#
# Klaga p� alla som s�ger muh.
#
# ARG0: nick!hostmask
# ARG1: channels/users (array reference)
# ARG2: text of message (possibly slightly altered)
# ARG3: parameters (array reference)
# ARG4: original event (irc_public or irc_msg)
# ARG5: callback function (code reference)
#
# $Log$
# Revision 1.4  2003/09/20 09:49:18  stefan
# En helt v�rdel�s plugin.
#
#
package Plugin::muh;

use base 'Plugin::Base';
use strict;
use POE;
use Robot::Functions;

our @EVENTS = qw(init listen);

sub init {
    $_[KERNEL]->post( pluginhandler => register => qw(listen) );
}

sub listen {
    my ($msg, $postback) = @_[ARG2, ARG5];

    if ($msg =~ m/(^|\W)m+u+h*(\W|$)/i) {
	&$postback( "Muh!" );
    }
    else {
	&$postback();
    }
}

1;
