# $Id$
#
# $Log$
# Revision 1.1  2004/04/22 06:11:55  zerblat
# Robot was restored since the original CVS repository was lost.
#
# Revision 1.7  2003/08/13 15:50:25  stefan
# Brain can forget stuff.
#
# Revision 1.6  2003/07/29 12:15:49  stefan
# Adapted to new plugin interface.
#
# Revision 1.5  2003/07/07 16:56:02  stefan
# Return the fields 'source' and 'time'. Still, there's nothing returned
# from the 'learn' state.
#
# Revision 1.4  2003/07/07 15:52:53  stefan
# Added "whatis" function.
#
# Revision 1.3  2003/07/04 13:36:05  stefan
# Removed debugging. Reworked 'learn' state. It now sends a return code
# back to the caller, either with a supplied event name or a postback call.
#
# Revision 1.2  2003/06/26 16:01:26  stefan
# Added a simple, and quite stupid, 'learn' state. I'm not really happy
# about it, but it will do for now.
#
# Revision 1.1  2003/06/20 13:20:37  stefan
# I've got a basic 'select' state that calls the database and returns
# the answer in either an event or a postback.
#
#
package Plugin::brain;

use base 'Plugin::Base';
use strict;
use POE;
use DBI;
use Robot::Functions;
#use Robot::Debugger;

our @EVENTS = qw(init shutdown select learn whatis forget);
our $DESCRIPTION = 'MySQL database interface';

sub init {
    my ($kernel, $heap) = @_[KERNEL,HEAP];
    my $config = $heap->{config};

    # Connect to database
    my $dbh;
    eval { $dbh = DBI->connect( $config->{dsn},
				$config->{user},
				$config->{password},
				{ AutoCommit => 1, PrintError => 1,
				  RaiseError => 0 } )
       };

    # If connection succeeded, set alias to daemonize session, save
    # database connection and register events with plugin handler.
    if ($dbh) {
	$heap->{dbh} = $dbh;
	$kernel->post( console => plugin => "Connected to database" );
    }

    # Otherwise, print an error and remove alias (to kill plugin)
    else {
	$kernel->post( console => plugin => "DBI failed to connect; dsn=$config->{dsn}; user=$config->{user}; password=$config->{password}" );
    }
}

sub shutdown {
    my ($kernel, $heap) = @_[KERNEL, HEAP];

    # Disconnect from database
    $heap->{dbh}->disconnect() if ($_[HEAP]->{dbh});
}

sub select {
    my ($kernel, $heap, $sender, $return_event, $sql, @bind_vars) =
      @_[KERNEL, HEAP, SENDER, ARG0..$#_];

    my $result = [];
    my ($rv, $rc, $errstr);

    if ($heap->{dbh}) {

	# Prepare SQL statement
	my $sth = $heap->{dbh}->prepare_cached( $sql );

	if ($sth) {

	    # Execute statement
	    if ($sth->execute( @bind_vars )) {

		# Get results
		$result = $sth->fetchall_arrayref( {} );
	    }

	}

	$rv = $heap->{dbh}->state;
	$rc = $heap->{dbh}->err;
	$errstr = $heap->{dbh}->errstr;

    }

#    DEBUG "return_event=$return_event; ", ref( $return_event );

    # Post a return event, or, in case of a code ref, call postback.
    if (ref( $return_event )) {
	&$return_event( $result, $rv, $rc, $errstr );
    }
    else {
	$kernel->post( $sender, $return_event, $result, $rv, $rc, $errstr );
    }
}

sub cmd_sql {
    my ($kernel, $session, $heap, $postback, $msg)
      = @_[KERNEL, SESSION, HEAP, ARG5, ARG2];

    $msg =~ s/^\w+\s*//;

    my $response = [ "Result of '$msg':" ];

    $kernel->yield( select => $session->postback( 'show_results', $_[ARG0] ),
		    $msg );

    &$postback( $response, 1 );
}

sub show_results {
    my ($kernel, $request, $response) = @_[KERNEL, ARG0, ARG1];

    my $nick = stripnick( $$request[0] );

#    DEBUG "got event, from $nick";

    if ($$response[0]) {
	foreach my $ref (@{ $$response[0] }) {
	    $kernel->post( irc => privmsg => $nick =>
			   join( ', ', values %$ref ) );
	}
    }
}

#
# A note on return codes for LEARN and WHATIS: They have the same
# return codes (and, of course, different return codes), so they can
# be handled by the same "response" state handler in the calling
# plugin. If you don't understand what I'm saying, I won't blame
# you. I don't understand either.
#

#
# LEARN
#
# Learn something and store it in the database. Call postback (if
# supplied) with return code.
#
# Return codes:
#
#  0  Internal error (SQL)
#  1  Learnt
#  2  Already knew that (use force)
#  3  Duplicate entry
#  4  Blocked
#
sub learn {
    my ($kernel, $heap, $sender, $thing, $descriptor, $description, $source,
	$force, $postback) = @_[KERNEL, HEAP, SENDER, ARG0..ARG5];

    # Display a message
    $kernel->post( console => plugin =>
		   "LEARN \cB$thing\cB $descriptor $description ($source)" );

    # Return code
    my $return_code = 0;

    if ($heap->{dbh}) {

	# Check for a  possibly blocked word
	my $blocked = _get( $heap->{dbh},
			    q{SELECT thing FROM blocked WHERE thing=?},
			    $thing );

	if (defined $blocked) {
	    $return_code = 4; # Blocked
	}
	else {

	    # Check if thing already exists
	    my $time = _get( $heap->{dbh},
			     q{SELECT time FROM things WHERE thing=? AND descriptor=?},
			     $thing, $descriptor );

	    # If we don't know this, or if we force an (additional)
	    # update, insert the the facts into the database.
	    if (!defined $time || $force) {

		# Insert into the database. Duplicate descriptions are
		# prohibited with the primary key.
		my $rows = _sql( $heap->{dbh},
				 q{INSERT INTO things (thing, descriptor, description, source) VALUES (?, ?, ?, ?)},
				 $thing, $descriptor, $description, $source );

		# Exactly one row should have been updated. Otherwise,
		# we assume there was a duplicate.
		if ($rows != 1) {
		    $return_code = 3; # Duplicate
		}
		else {
		    $return_code = 1; # Ok
		}
	    }

	    # We already know this.
	    else {
		$return_code = 2; # Already known; use force
	    }
	}

    }

    # Display result code
    $kernel->post( console => plugin => "Return code: $return_code" );

    # Post return code to caller
    if (defined $postback) {
	$kernel->post( console => plugin => "Postback: " . ref( $postback ) );
	if (ref( $postback ) eq 'POE::Session::Postback') {
	    &$postback( $return_code );
	}
	else {
	    $kernel->post( $sender => $postback => $return_code );
	}
    }
}

#
# WHATIS
#
# Find data in the database. Result codes:
#
#  0  Internal error (SQL)
#  5  Got answer
#  6  Empty answer
#
sub whatis {
    my ($kernel, $heap, $sender, $thing, $descriptor, $postback) =
      @_[KERNEL, HEAP, SENDER, ARG0..ARG2];

    # Display a message
    $kernel->post( console => plugin =>
		   "WHATIS \cB$thing\cB $descriptor" );

    # Return code
    my $return_code = 0;
    my $result = [];

    if ($heap->{dbh}) {

	# Get it.
	if (defined $descriptor) {
	    $result = _select( $heap->{dbh},
			       q{SELECT thing, descriptor, description, source, time FROM things WHERE thing=? AND descriptor=?},
			       $thing, $descriptor );
	}
	else {
	    $result = _select( $heap->{dbh},
			       q{SELECT thing, descriptor, description, source, time FROM things WHERE thing=? ORDER BY descriptor},
			       $thing );
	}
	$return_code = scalar @$result ? 5 : 6;

    }

    # Post return code to caller
    if (defined $postback) {
	if (ref( $postback ) eq 'POE::Session::Postback') {
	    &$postback( $return_code, $result );
	}
	else {
	    $kernel->post( $sender => $postback => $return_code, $result );
	}
    }
}

#
# FORGET
#
# Delete data from the database. Result codes:
#
#  0  Internal error (SQL)
#  7  Deleted
#  8  Partially deleted
#  9  Not deleted
#
sub forget {
    my ($kernel, $heap, $sender, $thing, $descriptor, $postback) =
      @_[KERNEL, HEAP, SENDER, ARG0..ARG2];

    # Display a message
    $kernel->post( console => plugin =>
		   "FORGET \cB$thing\cB" );

    # Return code
    my $return_code = 0;
    my $result = [];
    my $rows;

    if ($heap->{dbh}) {

	# Delete it
	if (defined $descriptor) {
	    $rows = _sql( $heap->{dbh},
			  q{DELETE FROM things WHERE thing=? AND descriptor=?},
			  $thing, $descriptor );
	}
	else {
	    $rows = _sql( $heap->{dbh},
			  q{DELETE FROM things WHERE thing=?},
			  $thing );
	}
	$return_code = $rows ? 7 : 9;

    }

    # Post return code to caller
    if (defined $postback) {
	if (ref( $postback ) eq 'POE::Session::Postback') {
	    &$postback( $return_code, $result );
	}
	else {
	    $kernel->post( $sender => $postback => $return_code, $result );
	}
    }
}


#
# Select data from table and return mutliple values
#
sub _select {
    my ($dbh, $SQL, @bind_vars) = @_;

    my $sth = $dbh->prepare_cached( $SQL );
    $sth->execute( @bind_vars );
    # I should clone the result variable. But I don't care right
    # now. This is a possible bug with newer DBI versions. But not
    # yet. I think. Or hope. You really should not write code when
    # you're tired.
    my $result = $sth->fetchall_arrayref( {} );
    return $result;
}

#
# Select data from table and return a single value
#
sub _get {
    my ($dbh, $SQL, @bind_vars) = @_;

    my $sth = $dbh->prepare_cached( $SQL );
    $sth->execute( @bind_vars );
    my $value = $sth->fetchrow_array;
    $sth->finish;

    return $value;
}

#
# Execute SQL statement and return number of rows affected
#
sub _sql {
    my ($dbh, $SQL, @bind_vars) = @_;
    my $sth = $dbh->prepare_cached( $SQL );
    $sth->execute( @bind_vars );
    my $rows = $sth->rows;
    $sth->finish;
    return $rows;
}

1;
