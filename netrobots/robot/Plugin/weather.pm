# $Id$
#
# $Log$
# Revision 1.3  2003/07/29 12:24:21  stefan
# Adapted to new plugin interface.
#
# Revision 1.2  2003/07/17 14:36:35  stefan
# Fixed a better date representation. Fixed some
# bugs. Fine. Great. Jolly good.
#
# Revision 1.1  2003/07/16 15:43:56  stefan
# A first version that fetches weather information from wunderground.com.
#
#
package Plugin::weather;

use base 'Plugin::Base';
use strict;
use POE;
use Robot::Functions;
use Robot::Debugger;
use Date::Parse;
use POSIX qw(strftime);

our @EVENTS = qw(init cmd_temp _parse);

sub init {
    my $kernel = $_[KERNEL];
    $kernel->post( pluginhandler => register => qw(cmd_temp) );
}

sub cmd_temp {
    my ($kernel, $heap, $session, $postback, $args) = @_[KERNEL, HEAP, SESSION, ARG5, ARG3];

    if (scalar @$args > 0) {

#  	if ($heap->{waiting}) {
#  	    &$postback( "A request is still active. Please wait."

	my $location = join( '+', @$args );

	my $url = 'http://printer.wunderground.com/cgi-bin/findweather/getForecast?query=' . $location;

	$kernel->post( console => plugin => "Get weather for $location." );

#	$heap->{waiting} = 1;
	$kernel->post( netsuck => geturl => $url, 
		       $session->postback( '_parse', $postback ) );

    }
    else {

	&$postback( "You must specify a location, eg !temp stockholm, sweden", 1 );
    }
}

sub _parse {
    my ($kernel, $request, $response) = @_[KERNEL, ARG0, ARG1];

    my $data = $$response[0];
    my $postback = $$request[0];

    $kernel->post( console => plugin => "Parsing weather result" );

    # Rest of this code partly stolen from Weather::Underground.

    # Clean data. The HTML code is really ugly.
    $data =~ s|</?b>||g; # bold tags
    $data =~ s/((?<=\W)[ \t]+)|([ \t]+(?=\W))//g; # single spacing?
    $data =~ s/\r//g; # Remove CR
    $data =~ s/\n{2,}/\n/g; # Remove double newlines

#    DEBUG $data;

    my $list = []; # List of results

    while ($data =~ m|<td><a.*?>([\w,]+)</a></td>\s*<td>\s*([-\d]+)&nbsp;&#176;F\s*/\s*([-\d]+)&nbsp;&#176;C\s*</td>\s*<td>(\d+)%</td>\s*<td>\s*([\d.]+)&nbsp;in\s*/\s*([\d.]+)&nbsp;hPa\s*</td>\s*<td>(.*?)</td>\s*<td>(.*?)</td>|sg) {
	my $place = $1;
	my ($temp_f, $temp_c) = ($2, $3);
	my $humidity = $4;
	my ($pressure_in, $pressure_hpa) = ($5, $6);
	my $conditions = $7;
	my $updated = str2time( $8 );

	push @$list, "$place: $temp_c�C, $humidity%, $pressure_hpa hPa, $conditions (" . strftime( '%H:%M', localtime $updated ) . ").";
    }

    if ($data =~ m/Observed at\s*([\w,. ]+)/) {
	my $location = $1;

	my ($updated) = ($data =~ m/Updated:(.*?)</);
	my ($temp_f, $temp_c) = ($data =~ m|Temperature</td>\s*<td>\s*([-\d]+)&nbsp;&#176;F\s*/\s*([-\d]+)&nbsp;&#176;C|s);
	my ($humidity) = ($data =~ m|Humidity</td>\s*<td>(\d+)%|s);
	my ($dp_f, $dp_c) = ($data =~ m|Dew Point</td>\s*<td>\s*([-\d]+)&nbsp;&#176;F\s*/\s*([-\d]+)&nbsp;&#176;C|s);
	my ($wangle, $w_mph, $w_kmh) = ($data =~ m|Wind</td>\s*<td>\s*(\w+)\s*at\s*([\d.]+)&nbsp;mph\s*/\s*([\d.]+)&nbsp;km/h|s);
	my ($pressure_in, $pressure_hpa) = ($data =~ m|Pressure</td>\s*<td>\s*([\d.]+)&nbsp;in\s*/\s*([\d.]+)&nbsp;hPa|s);
	my ($conditions) = ($data =~ m|Conditions</td>\s*<td>(.*?)<|s);
	my ($visibility_miles, $visibility_km) = ($data =~ m|Visibility</td>\s*<td>\s*([\d.]+)&nbsp;miles\s*/\s*([\d.]+)&nbsp;kilometers|s);
	my ($sunrise) = ($data =~ m|Sunrise</td><td>(.*?)<|s);
	my ($sunset) = ($data =~ m|Sunset</td><td>(.*?)<|s);
	my ($moonrise) = ($data =~ m|Moon Rise</td><td>(.*?)<|s);
	my ($moonset) = ($data =~ m|Moon Set</td><td>(.*?)<|s);

	# A fix
#	$visibility_km = '-' unless ($visibility_km);

	# Another fix
	$updated =~ s/(?<=\s)on(?=\s)//; # remove "on" from date
	my $time = strftime( '%H:%M', localtime str2time( $updated ) );
	my $sunrise_time = strftime( '%H:%M', localtime str2time( $sunrise ) );
	my $sunset_time = strftime( '%H:%M', localtime str2time( $sunset ) );

	# Yet another fix
	my $w_ms = sprintf( '%0.1f', $w_kmh / 3.6 );

	my $response;
	if ($location && $location ne ',') {
	    $response = "$location at $time: Temperature $temp_c�C, humidity $humidity%, pressure $pressure_hpa hPa, dew point $dp_c�C. Wind $wangle at $w_ms m/s. $conditions. ";
	    $response .= "Visibility $visibility_km km. " if ($visibility_km);
	    $response .= "Sunrise $sunrise_time, sunset $sunset_time.";
	}
	else {
	    $response = "Could not parse data. My Creator is lousy and has not fixed me yet. Sorry for the inconvenience.";
	}
	push @$list, $response;
    }

    if (scalar @$list > 0) {
	&$postback( $list, 1 );
    }
    else {
	&$postback( "Sorry, nothing found." );
    }
}

1;
