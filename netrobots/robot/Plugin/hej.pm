# $Id$
#
# S�g hej! :-)
#
# ARG0: nick!hostmask
# ARG1: channels/users (array reference)
# ARG2: text of message (possibly slightly altered)
# ARG3: parameters (array reference)
# ARG4: original event (irc_public or irc_msg)
# ARG5: callback function (code reference)
#
# $Log$
# Revision 1.2  2003/07/29 13:19:19  stefan
# Enkel plugin f�r att h�lsa.
#
# Revision 1.1  2003/06/11 21:51:36  stefan
# Ett exempel p� en plugin. Den s�ger "hej" p� alla rader som b�rjar med
# "hej".
#
package Plugin::hej;

use base 'Plugin::Base';
use strict;
use POE;
use Robot::Functions;

our @EVENTS = qw(init listen talk);

sub init {
    $_[KERNEL]->post( pluginhandler => register => qw(listen talk) );
}

sub listen {
    my ($from, $msg, $postback) = @_[ARG0, ARG2, ARG5];

    if ($msg =~ m/^(hej|hejsan|hej p� er|hej alla)[\s\!]*$/i) {
	my $nick = stripnick( $from );
	&$postback( "Hej $nick!" );
    }
    else {
	&$postback();
    }
}

sub talk {
    my ($from, $msg, $postback) = @_[ARG0, ARG2, ARG5];

    if ($msg =~ m/^(hej|hejsan|hej p� er|hej alla)[\s\!]*$/i) {
	my $nick = stripnick( $from );
	&$postback( "Hej $nick!" );
    }
    else {
	&$postback();
    }
}

1;
