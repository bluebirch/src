# $Id$
#
# Klaga p� alla som s�ger muh.
#
# ARG0: nick!hostmask
# ARG1: channels/users (array reference)
# ARG2: text of message (possibly slightly altered)
# ARG3: parameters (array reference)
# ARG4: original event (irc_public or irc_msg)
# ARG5: callback function (code reference)
#
# $Log$
# Revision 1.1  2003/08/13 14:46:51  stefan
# Klaga p� alla som s�ger "spam" med ett citat fr�n den ber�mda sketchen.
#
#
#
package Plugin::nospam;

use base 'Plugin::Base';
use strict;
use POE;
use Robot::Functions;

our @EVENTS = qw(init listen);

my @NOSPAM = ( "Have you got anything without spam?",
	       "I don't want any spam!",
	       "THAT'S got spam in it!",
	       "Could you do the egg bacon spam and sausage without the spam then?",
	       "I don't like spam!"
	     );
  
sub init {
    $_[KERNEL]->post( pluginhandler => register => qw(listen) );
}

sub listen {
    my ($from, $msg, $postback) = @_[ARG0, ARG2, ARG5];

    if ($msg =~ m/(^|\W)spam(\W|$)/i) {
	my $nick = stripnick( $from );
	&$postback( $NOSPAM[ rand( $#NOSPAM )] );
    }
    else {
	&$postback();
    }
}

1;
