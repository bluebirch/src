# Header

!title Documentation for
!program RASORT
!version v1.1
!author Stefan Svensson
!email 2:205/340
!english

# Document

!begin_document
!maketitle
!tableofcontents

!node Synopsis

rasort <-mf> [-spi] [-ln] 

!node Description

This program sorts the message and/or file areas of RemoteAccess version
2.50 or later. Areas are first sorted on group number, in the same order
as in  MGROUPS.RA or  FGROUPS.RA. Within  every group  areas are  sorted
alphabetically on their name.

Optionally RASORT can pack MESSAGES.RA and FILES.RA, i.e. remove  unused
entries. This can be very useful if MESSAGES.RA is created by a  program
like the Fmail Echomail Processor.

!node Command syntax

RASORT <commands and switches>

!subnode Specifying target files

!begin_xlist [-S-]
!item [-F]  Process FILES.RA.
!item [-M]  Process MESSAGES.RA.
!end_xlist

Specifying both -F and -M will cause RASORT to process both FILES.RA and
MESSAGES.RA.

RASORT looks for FILES.RA and MESSAGES.RA  in the path specified by  the
environment variable "RA".  If this environment  variable is  undefined,
RASORT searches the current directory.

!subnode Commands

!begin_xlist [-S-]
!item [-S]  Sort area file.
!item [-P]  Pack area file.
!item [-I]  Reindex area file. This command is automatically enabled is 
            -S or -P is specified.
!end_xlist

!subnode Switches

!begin_xlist [-S-]
!item [-L]  Enable logging. RASORT will 
!item [-N]  Don't query before processing files.
!end_xlist

!node Examples

RASORT -M -S

Sort MESSAGES.RA.

