(*
**  undial.inc
**
**  Structures for NODIAL.FD
**
**  Copyright 1991-1993 Joaquim Homrighausen; All rights reserved.
**
**  Last revised: 93-06-18                         FrontDoor 2.11+
**
**  -------------------------------------------------------------------------
**  This information is not necessarily final and is subject to change at any
**  given time without further notice
**  -------------------------------------------------------------------------
*)
CONST
  MAXUNDIAL       =200;                 {Maximum number of undialable systems}
  UNDIAL_TROUBLE  =1;                                        {Got one failure}
  UNDIAL_WORSE    =2;                                       {Got two failures}
  UNDIAL_NOMORE   =3;                         {Got three failures, undialable}

(*
**  When the Mailer reaches the maximum number of resend retries for a system
**  during an event (session handshake failures, bad sends, etc.) it bumps
**  the 'Badness' counter one. Once it reaches three (i.e. the Mailer has
**  reached the resend limit in three separate events), the system is flagged
**  as 'hard' undialable and the Mailer will no longer attempt to call it.
*)
TYPE
  NoDialRec = RECORD
    Zone,                                       {Address of undialable system}
    Net,
    Node,
    Point    : WORD;
    PhoneCRC : LONGINT;                       {CRC-32 of raw telephone number}
    Badness  : WORD;                            {How undialable the system is}
  END;{NoDialRec}

(* end of file "undial.inc" *)
