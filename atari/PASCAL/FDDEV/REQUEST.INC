(*
**  request.inc
**
**  Structures for REQUEST.FD
**
**  Copyright 1991-1993 Joaquim Homrighausen; All rights reserved.
**
**  Last revised: 93-06-21                         FrontDoor 2.11+
**
**  -------------------------------------------------------------------------
**  This information is not necessarily final and is subject to change at any
**  given time without further notice
**  -------------------------------------------------------------------------
*)

  { Miscellaneous flags (STATUS) }

CONST
  ACTIVE                =$01;
  DELETED               =$02;                          {Never written to disk}

  { All strings are NUL terminated, a'la C ASCIIZ strings }

TYPE
  REQUEST = RECORD
    Filename  :ARRAY[1..41] of CHAR;      {Filename or unique portion thereof}
    password  :ARRAY[1..17] of CHAR;                                {Password}
    Status    :BYTE;                                              {0=Inactive}
  END;{REQUEST}

(* end of file "request.inc" *)
