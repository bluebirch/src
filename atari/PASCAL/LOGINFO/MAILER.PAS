UNIT Mailer;

{$i defines.inc}

INTERFACE

uses errHndle, Mem, Strngs, Global, Output;

const
  MAILER_CONNECTION_ALL       = 0;
  MAILER_CONNECTION_INCOMING  = 1;
  MAILER_CONNECTION_OUTGOING  = 2;

procedure compress_all_ftrans;
procedure new_mailer_session( var p : mailer_session_ptr; time : longint );
procedure new_mailer_ftrans( var first, p : mailer_ftrans_ptr );
procedure store_ftrans( direction : word;
                        m : mailer_session_ptr;
                        var p : mailer_ftrans_ptr;
                        time : longint;
                        filename : filename_string;
                        size : longint;
                        cps : word;
                        code : word );
procedure write_mailer_sessions( out : integer; selection : word );
procedure write_mailer_flist( out : integer; selection : word );

IMPLEMENTATION

procedure new_mailer_ftrans( var first, p : mailer_ftrans_ptr );
  begin
    if first = nil then
      begin
        first := malloc( sizeof( mailer_ftrans_rec ) );
        p := first;
      end
    else
      begin
        p := first;
        while p^.next <> nil do p := p^.next;
        p^.next := malloc( sizeof( mailer_ftrans_rec ) );
        p := p^.next;
      end;

    if noError then p^.next := nil;
  end; (* new_mailer_ftrans *)

function is_ARCmail( filename : filename_string ) : boolean;
  var
    s : string[3];
  begin
    s := copy( filename, lastpos( '.', filename ) + 1, 3 );
    if length( s ) = 3 then
      if s[3] in ['0'..'9'] then
        begin
          delete( s, 3, 1 );
          if (s = 'MO') or (s = 'TU') or (s='WE') or (s='TH') or (s='FR') or (s='SA') or (s='SU') then
            is_ARCmail := true
          else
            is_ARCmail := false;
        end
      else is_ARCmail := false
    else
      is_ARCmail := false;
  end; (* is_ARCmail *)

(*
 * procedure compress_ftrans
 *
 * S�ker igenom listan med �verf�rda filer och f�r samman alla PKT-
 * filer och ARCmail-filer till en gemensam post.
 *)

procedure compress_ftrans( var f_first : mailer_ftrans_ptr );
  var
    pkt_size      : longint;
    pkt_cps       : word;
    pkt_count     : word;
    arcmail_size  : longint;
    arcmail_cps   : word;
    arcmail_count : word;
    tic_size      : longint;
    tic_cps       : word;
    tic_count     : word;
    req_size      : longint;
    req_cps       : word;
    req_count     : word;
    f, f_prev     : mailer_ftrans_ptr;
    remove        : boolean;

  begin
    pkt_size := 0;
    pkt_cps := 0;
    pkt_count := 0;
    arcmail_size := 0;
    arcmail_cps := 0;
    arcmail_count := 0;
    tic_size := 0;
    tic_cps := 0;
    tic_count := 0;
    req_size := 0;
    req_cps := 0;
    req_count := 0;
    f := f_first;
    f_prev := nil;
    remove := false;

    while f <> nil do
      begin
        if copy( f^.filename, lastpos( '.', f^.filename ) + 1, 3 ) = 'PKT' then
          begin
            pkt_size := pkt_size + f^.size;
            pkt_cps := (pkt_cps * pkt_count + f^.cps) div (pkt_count + 1);
            inc( pkt_count );
            remove := true;
          end
        else if is_ARCmail( f^.filename ) then
          begin
            arcmail_size := arcmail_size + f^.size;
            arcmail_cps := (arcmail_cps * arcmail_count + f^.cps) div (arcmail_count + 1);
            inc( arcmail_count );
            remove := true;
          end
        else if copy( f^.filename, lastpos( '.', f^.filename ) + 1, 3 ) = 'TIC' then
          begin
            tic_size := tic_size + f^.size;
            tic_cps := (tic_cps * tic_count + f^.cps) div (tic_count + 1);
            inc( tic_count );
            remove := true;
          end
        else if copy( f^.filename, lastpos( '.', f^.filename ) + 1, 3 ) = 'REQ' then
          begin
            req_size := req_size + f^.size;
            req_cps := (req_cps * req_count + f^.cps) div (req_count + 1);
            inc( req_count );
            remove := true;
          end;

        if remove then
          begin
            if f_prev = nil then
              begin
                f_first := f^.next;
                dispose( f );
                f := f_first;
              end
            else
              begin
                f_prev^.next := f^.next;
                dispose( f );
                f := f_prev^.next;
              end;
            remove := false;
          end
        else
          begin
            f_prev := f;
            f := f^.next;
          end;
      end;

    if (req_count > 0) and noError then
      begin
        f := malloc( sizeof( mailer_ftrans_rec ) );
        if noError then
          begin
            f^.filename := Config.name_REQ;
            f^.size := req_size;
            f^.cps := req_cps;
            f^.code := 0;
            f^.next := f_first;
            f_first := f;
          end;
      end;

    if (tic_count > 0) and noError then
      begin
        f := malloc( sizeof( mailer_ftrans_rec ) );
        if noError then
          begin
            f^.filename := Config.name_TIC;
            f^.size := tic_size;
            f^.cps := tic_cps;
            f^.code := 0;
            f^.next := f_first;
            f_first := f;
          end;
      end;
    if (arcmail_count > 0) and noError then
      begin
        f := malloc( sizeof( mailer_ftrans_rec ) );
        if noError then
          begin
            f^.filename := Config.name_ARCmail;
            f^.size := arcmail_size;
            f^.cps := arcmail_cps;
            f^.code := 0;
            f^.next := f_first;
            f_first := f;
          end;
      end;
    if (pkt_count > 0) and (noError) then
        begin
          f := malloc( sizeof( mailer_ftrans_rec ) );
          if noError then
            begin
              f^.filename := Config.name_PKT;
              f^.size := pkt_size;
              f^.cps := pkt_cps;
              f^.code := 0;
              f^.next := f_first;
              f_first := f;
            end;
        end;
  end; (* compress_ftrans *)

(* procedure compress_all_ftrans
 *
 * Calls 'compress_ftrans' for each file list in the mailer_session tree
*)
procedure compress_all_ftrans;
  var
    m : mailer_session_ptr;
  begin
    if Config.Verbose then writeln( 'Checking transferred files' );

    m := _mailer_session_first;
    while m <> nil do
      begin
        compress_ftrans( m^.rcvdlist );
        compress_ftrans( m^.sentlist );
        m := m^.next;
      end;
 end; (* compress_all_ftrans *)




procedure new_mailer_session( var p : mailer_session_ptr; time : longint );
  var
    tmp : mailer_session_ptr;
  begin
    (* Allocate memory for new session info structure. *)

    if _mailer_session_first = nil then
      begin
        (* If no sessions is allocated, allocate the first element *)

        _mailer_session_first := malloc( sizeof( mailer_session_rec ) );
        p := _mailer_session_first;
        p^.next := nil;
      end
    else
      begin
        (* Search list and insert session at the proper place *)

        p := _mailer_session_first;
        while (p^.next <> nil) and (time > p^.time) do p := p^.next;
        tmp := p^.next;
        p^.next := malloc( sizeof( mailer_session_rec ) );
        if noError then
          begin
            p := p^.next;
            p^.next := tmp;
          end;

        {while (time > p^.time) and (p^.next <> nil) do p := p^.next;}
      end;


    if noError then
      begin
        p^.time := time;
        p^.rcvdlist := nil;
        p^.sentlist := nil;
      end;
  end; (* new_mailer_session *)

(* procedure store_mailer_ftrans
 *
 * Store file transfer information in memory.
 *
*)
procedure store_ftrans( direction : word;
                        m : mailer_session_ptr;
                        var p : mailer_ftrans_ptr;
                        time : longint;
                        filename : filename_string;
                        size : longint;
                        cps : word;
                        code : word );
  begin
    (* Allocate memory for the file transfer record *)
    case direction of
      SENT  : new_mailer_ftrans( m^.sentlist, p );
      RCVD  : new_mailer_ftrans( m^.rcvdlist, p );
      end;

    (* Store the file transfer *)
    if noError then
      begin
        p^.time := time;
        p^.address := m^.address;
        p^.filename := filename;
        p^.size := size;
        p^.cps := cps;
        p^.code := code;

        (* If the file was incomplete, also update mailer session *)
        if (code and FTRANS_INCOMPLETE) <> 0 then
          m^.code := m^.code or SESSION_FTRANSERR;

        (* Update file and byte counters for the session *)
        case direction of
          SENT  : begin
                    inc( m^.sentfiles );
                    m^.sentbytes := m^.sentbytes + size;
                  end;
          RCVD  : begin
                    inc( m^.rcvdfiles );
                    m^.rcvdbytes := m^.rcvdbytes + size;
                  end;
          end;
      end;
  end; (* store_mailer_ftrans *)




(*
 * procedure write_mailer_sessions
 *
 * Prints the mailer sessions that fits the argument in 'selection'
 * using patterns. If 'ftrans_pattern' is non-zero length, all files
 * transferred during the session is also written (using patterns).
 * The output is written to 'main_output'.
*)
procedure write_mailer_sessions( out : integer; selection : word );
  var
    p         : mailer_session_ptr;
    f         : mailer_ftrans_ptr;
  begin
    p := _mailer_session_first;
    _last := 0;

    case selection of
      MAILER_CONNECTION_ALL      : begin
                                     if Config.Verbose then writeln( 'Writing all mailer connections' );
                                     write_header( out, Config.session_header_all );
                                   end;
      MAILER_CONNECTION_INCOMING : begin
                                     if Config.Verbose then writeln( 'Writing incoming mailer connections' );
                                     write_header( out, Config.session_header_incoming );
                                   end;
      MAILER_CONNECTION_OUTGOING : begin
                                     if Config.Verbose then writeln( 'Writing outgoing mailer connections' );
                                     write_header( out, Config.session_header_outgoing );
                                   end;
      end;

    while p <> nil do
      begin
        if (selection = MAILER_CONNECTION_ALL) or ((selection = MAILER_CONNECTION_INCOMING) and (p^.code and 1 = 0))
        or ((selection = MAILER_CONNECTION_OUTGOING) and (p^.code and 1 = 1)) then
          begin

            (* Write date header and title bar if date has changed. *)
            if Config.UseDateHeader then write_date_separator( out, p^.time );

            (* Write session line *)

            putText( out, patternstr( SessionList, @Config.session_pattern, p ) );

            if length( Config.ftrans_pattern ) > 0 then
              begin
                f := p^.sentlist;
                while (f <> nil) do
                  begin
                    if f = p^.sentlist then
                      putText( out, patternstr( FileTransList, @Config.ftrans_pattern_sent, f ) )
                    else
                      putText( out, patternstr( FileTransList, @Config.ftrans_pattern, f ) );
                    f := f^.next;
                  end;
                f := p^.rcvdlist;
                while (f <> nil) do
                  begin
                    if f = p^.rcvdlist then
                      putText( out, patternstr( FileTransList, @Config.ftrans_pattern_rcvd, f ) )
                    else
                      putText( out, patternstr( FileTransList, @Config.ftrans_pattern, f ) );
                    f := f^.next;
                  end;
                if length( Config.session_pattern2 ) = 0 then
                  putText( out, '' );
              end;
            if length( Config.session_pattern2 ) > 0 then
              putText( out, patternstr( SessionList, @Config.session_pattern2, p ) );
          end;
      p := p^.next;
    end;
  end; (* write_mailer_sessions *)

(* procedure write_mailer_flist
 *
 * Write a list of all files
 *)

procedure write_mailer_flist( out: integer; selection : word );
  var
    m         : mailer_session_ptr;
    f         : mailer_ftrans_ptr;
  begin
    m := _mailer_session_first;
    _last := 0;

    if selection = RCVD then
      begin
        if Config.Verbose then writeln( 'Writing list of received files' );
        write_header( out, Config.mailer_flist_hdr_rcvd )
      end
    else
      begin
        if Config.Verbose then writeln( 'Writing list of sent files' );
        write_header( out, Config.mailer_flist_hdr_sent );
      end;

    while m <> nil do
      begin
        if selection = RCVD then
          f := m^.rcvdlist
        else
          f := m^.sentlist;
        while f <> nil do
          begin

            with Config do
              if (f^.filename <> name_ARCmail) and (f^.filename <> name_PKT)
              and (f^.filename <> name_TIC) and (f^.filename <> name_REQ) then
                begin
                  (* Write date separator *)
                  if Config.UseDateHeader then write_date_separator( out, m^.time );

                  putText( out, patternstr( FileTransList, @mailer_flist_pattern, f ) );
                end;

            f := f^.next;
          end;
      m := m^.next;
    end;
  end; (* write_mailer_flist *)

end.
