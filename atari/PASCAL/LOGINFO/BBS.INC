(*
 * new_bbs_user
 *
 * Allocate a new element in BBS user chain. The global variable '_bbs_user_first'
 * points to the first element in the list. The new element is initialized with
 * zeroes.
*)
procedure new_bbs_user( var p : bbs_user_ptr );
  begin
    if sizeof( bbs_user_rec ) < maxavail then
      begin
        if _bbs_user_first = nil then
          begin
            new( _bbs_user_first );
            p := _bbs_user_first;
          end
        else
          begin
            p := _bbs_user_first;
            while p^.next <> nil do p := p^.next;
            new( p^.next );
            p := p^.next;
          end;
        fillchar( p^, sizeof( p^ ), 0 );
        p^.next := nil;
      end
    else
      _error := ERR_MEM;
  end; (* new_bbs_user *)


(*
 * write_bbs_users
 *
 * Writes the BBS-users to the output file using patterns.
 *
*)
procedure write_bbs_users;
  var
    p : bbs_user_ptr;
  begin
    if _cfg.opt_verbose then writeln( 'Writing list of BBS users' );

    write_header( _cfg.bbs_user_header );

    p := _bbs_user_first;
    _last := 0;
    while p <> nil do
      begin
        if _cfg.use_date_separator then write_date_separator( p^.time );

        putText( patternstr( _cfg.bbs_user_pattern, p ) );
        p := p^.next;
      end;
  end; (* write_bbs_users *)


(*
 * procedure check_bbs_users
 *
 * Perform some probability checks on the BBS users.
*)
procedure check_bbs_users;
  var
    p : bbs_user_ptr;
  begin
    if _cfg.opt_verbose then writeln( 'Checking BBS users' );
    p := _bbs_user_first;
    while p <> nil do
      begin
        if p^.time > p^.offline then
          begin
            p^.offline := p^.time;
            p^.exitcode := USER_LOST;
          end;
        p := p^.next;
      end;
  end;