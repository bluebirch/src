UNIT ErrHndle;

(*
    LOGINFO - Log Analyzer for FidoNet Software
    (C)Copyright 1996,1997 Stefan Svensson. All rights reserved.

    Error handler.

    This unit handles all errors that may occur when LogInfo is running.
*)

{$i defines.inc}

INTERFACE

uses
  DOS, Global;

type
  string255 = string[255];
  string255ptr = ^string255;

const
  (* I/O errors *)
  errIO                         = 500;
  errUnableToCreateMailPacket   = 501;
  errMailPacketAlreadyExists    = 502;
  errWhileWritingMessage        = 503;
  errUnableToOpenTextFile       = 504;

  (* Configuration errors *)
  errConfiguration              = 200;
  errUnknownMacro               = 201;
  errUnknownSubMacro            = 202;
  errTooManyNodeInfoPatterns    = 203;
  errInvalidQuotes              = 204;
  errUnknownVariable            = 205;
  errUnknownOutputType          = 206;
  errInvalidEscapeCode          = 207;
  errInvalidWDMSGArray          = 208;
  errInvalidWDPRIArray          = 209;
  errInvalidPriority            = 210;

  (* Memory management errors *)
  errMemoryHandling             = 300;
  errNoMemory                   = 301;
  errNoMemoryForMessage         = 302;
  errNoMemoryForMailPacket      = 303;

  (* Command line parameter errors *)
  errInvalidCommand             = 400;
  errInvalidSwitch              = 401;
  errNoCommand                  = 402;

  (* Process errors that may occur when analyzing log files *)
  errInvalidAddress             = 101;
  errUnableToParseDate          = 102;
  errNumValueExpected           = 103;
  errUnableToParseLine          = 104;

  (* Some error codes that never should occur *)
  errWhileCreatingMessage       = 32765;
  errInternalError              = 32767;
  errInternalConfiguration      = 32766;

  (* File system errors (DOS) *)
  errFileNotFound        = 2;
  errPathNotFound        = 3;
  errTooManyFiles        = 4;
  errAccessDenied        = 5;
  errInvalidFileHandle   = 6;
  errInvalidFileAccess   = 12;
  errInvalidDriveNumber  = 13;
  errDiskReadError       = 100;
  errDiskWriteError      = 101;
  errFileNotAssigned     = 102;
  errFileNotOpen         = 103;
  errFileNotOpenForInput = 104;
  errFileNotOpenForOutput= 105;
  errInvalidNumericFormat= 106;
  errWriteProtected      = 150;
  errDiskSeekError       = 156;
  errSectorNotFound      = 158;
  errHardWareFailure     = 162;

  (* Severity levels *)
  sevWarning             = 1;
  sevError               = 2;
  sevFatal               = 3;

var
  errCode   : integer;    (* Last error code *)
  noError   : boolean;    (* TRUE if no error has occured *)
  IOsuccess : boolean;    (* TRUE if the last I/O operation was successful *)


procedure Error( code : integer; data : pointer );
function  IO( ioCode : integer; filename : string255ptr ) : boolean;

IMPLEMENTATION

var
  errorCode : integer;

procedure error( code : integer; data : pointer );

  (*
      This procedure is called whenever an error occurs. The error code is
      passed in the 'code' variable and a pointer to some descriptive data
      is passed in the 'data' pointer.

      The descriptive data expected depends on the error code. See below for
      each error code what type of data is expected.

      Each error code has also a severity level. Severity 1 means that the
      error is harmless and program execution should continue. Severity 2
      means that LogInfo should quit nice and cleanly and abort program
      execution. Severity 3 is a fatal error and program execution is
      immediately halted.
  *)

  var
    severity : integer;
  begin
    if code <> 0 then
      begin
        write( 'ERROR: ' );
        case code of

         (* I/O errors *)

          errIO:
            begin
              writeln( 'General I/O error.' );
              severity := sevError;
            end;

          errUnableToCreateMailPacket:
            begin
              writeln( 'Unable to create mail packet ', string255( data^ ) );
              severity := sevError;
            end;

          errMailPacketAlreadyExists:
            begin
              writeln( 'Mail packet ', string255( data^ ), ' does already exist.' );
              severity := sevError;
            end;

          errWhileWritingMessage:
            begin
              writeln( 'Error while writing message or mail packet' );
              writeln( '       Please contact Stefan Svensson at 2:205/340' );
              severity := sevError;
            end;

          errUnableToOpenTextFile:
            begin
              writeln( 'Unable to open text file ', string255( data^ ) );
              severity := sevError;
            end;

          (* Configuration errors *)

          errConfiguration:
            begin
              writeln( 'Configuration error: ', string255( data^ ) );
              severity := sevError;
            end;

          errUnknownMacro:
            begin
              writeln( 'Unknown macro: ', char( data^ ) );
              severity := sevError;
            end;

          errUnknownSubMacro:
            begin
              writeln( 'Unknown sub-macro: ', char( data^ ) );
              severity := sevError;
            end;

          errTooManyNodeInfoPatterns:
            begin
              writeln( 'Too many ''NodeInfoPattern''-variables.' );
              severity := sevWarning;
            end;

          errInvalidQuotes:
            begin
              writeln( 'Missing or invalid quote ("): ', string255( data^ ) );
              severity := sevWarning;
            end;

          errUnknownVariable:
            begin
              writeln( 'Unknown configuration variable: ', string255( data^ ) );
              severity := sevWarning;
            end;

          errUnknownOutputType:
            begin
              writeln( 'Unknown type: ', string255( data^ ) );
              writeln( '       Use ''TextFile'', ''Netmail'' or ''MailPacket''' );
              severity := sevError;
            end;

          errInvalidEscapeCode:
            begin
              writeln( 'Invalid escape code in this line:' );
              writeln( '       ', string255( data^ ) );
              severity := sevWarning;
            end;

          errInvalidWDMSGArray:
            begin
              writeln( 'Invalid element: WatchdogMessage[ ', integer( data^ ), ' ]' );
              severity := sevWarning;
            end;

          errInvalidWDPRIArray:
            begin
              writeln( 'Invalid element: WatchdogPriority[ ', integer( data^ ), ' ]' );
              severity := sevWarning;
            end;

          errInvalidPriority:
            begin
              writeln( 'Invalid watchdog priority: ', byte( data^ ) );
              severity := sevWarning;
            end;


          (* Memory handling *)

          errMemoryHandling:
            begin
              writeln( 'General memory allocation error.' );
              writeln( '       Please contact Stefan Svensson at 2:205/340' );
              severity := sevFatal;
            end;

          errNoMemory:
            begin
              writeln( 'Insufficient memory; ', memavail, ' bytes available, ', longint( data^ ), ' bytes needed.' );
              severity := sevFatal;
            end;

          errNoMemoryForMessage:
            begin
              writeln( 'Insufficient memory to create message.' );
              severity := sevError;
            end;

          errNoMemoryForMailPacket:
            begin
              writeln( 'Insufficient memory to create mail packet.' );
              severity := sevError;
            end;

          errInvalidCommand:
            begin
              writeln( 'Invalid command - ', string255( data^ ) );
              severity := sevError;
            end;

          errInvalidSwitch:
            begin
              writeln( 'Invalid switch - ', string255( data^ ) );
              severity := sevError;
            end;

          errNoCommand:
            begin
              writeln( 'No command specified.' );
              severity := sevError;
            end;

          (* Processing errors *)

          errInvalidAddress:
            begin
              writeln( 'Invalid address - ', string255( data^ ) );
              severity := sevError;
            end;

          errUnableToParsedate:
            begin
              writeln( 'Unable to parse date from this line:' );
              writeln( '       ', string255( data^ ) );
              severity := sevWarning;
            end;

          errNumValueExpected:
            begin
              writeln( 'Numerical value expected in this line:' );
              writeln( '       ', string255( data^ ) );
              severity := sevWarning;
            end;

          errUnableToParseLine:
            begin
              writeln( 'Unable to parse this line:' );
              writeln( '       ', string255( data^ ) );
              severity := sevWarning;
            end;

          errWhileCreatingMessage:
            begin
              writeln( 'Error while creating message or mail packet.' );
              writeln( '       Please contact Stefan Svensson at 2:205/340' );
              severity := sevError;
            end;

          errInternalError:
            begin
              writeln( 'Internal error.' );
              writeln( '       Please contact Stefan Svensson at 2:205/340'  );
              severity := sevFatal;
            end;

          errInternalConfiguration:
            begin
              writeln( 'Error in internal configuration file.' );
              writeln( '       Please delete file ', string255( data^ ), ' and try again.' );
              writeln( '       If this doesn''t help, please contact Stefan Svensson at 2:205/340' );
              severity := sevFatal;
            end;


          else
            begin
              writeln( 'Unknown error' );
              severity := 3;
            end;

          end;

        (* Store error code in global public errCode variable *)
        errCode := code;

        (* Determine how severe this error was *)
        if severity > 1 then noError := false;
        if severity > 2 then
          begin
            writeln( 'Panic! Aborting program!' );
            halt( errCode div 100 );
          end;
      end;
  end; (* Error *)

function IO( ioCode : integer; filename : string255ptr ) : boolean;

  (*
      This function is called after any I/O operation that has the compiler
      I/O checking turned off. IOresult is passed in 'ioCode', and a pointer
      to the name of the file currently being accessed is passed in
      'filename'.

      The function gives the user the possibility to skip this file or
      change the file name.
  *)

  var
    answer   : string;
    TryAgain : boolean;
    IsPath   : boolean;
  begin
    if ioCode <> 0 then
      begin
        IOsuccess := FALSE;

        if filename^[ length( filename^ ) ] = '\' then IsPath := TRUE else IsPath := FALSE;

        write( 'ERROR: ' );
        if IsPath then write( 'Path ' ) else write( 'File ' );
        write( filename^, ' - ' );

        case ioCode of
          errFileNotFound         : write( 'File not found' );
          errPathNotFound         : write( 'Path not found' );
          errTooManyFiles         : write( 'Too many files' );
          errAccessDenied         : write( 'Access denied' );
          errInvalidFileHandle    : write( 'Invalid file handle' );
          errInvalidFileAccess    : write( 'Invalid file access' );
          errInvalidDriveNumber   : write( 'Invalid drive number' );
          errDiskReadError        : write( 'Disk read error' );
          errDiskWriteError       : write( 'Disk write error' );
          errFileNotAssigned      : write( 'File not assigned' );
          errFileNotOpen          : write( 'File not open' );
          errFileNotOpenForInput  : write( 'File not open for input' );
          errFileNotOpenForOutput : write( 'File not open for output' );
          errInvalidNumericFormat : write( 'Invalid numeric format' );
          errWriteProtected       : write( 'Disk is write-protected' );
          errDiskSeekError        : write( 'Disk seek error' );
          errSectorNotFound       : write( 'Sector not found' );
          errHardWareFailure      : write( 'Hardware failure' );
          else                      write( 'Unknown I/O error' );
          end;

        writeln( ' (', ioCode, ')' );

        if not Config.Unattended then
          begin
            repeat
              if IsPath then write( '[R]etry, [N]ew path or [S]kip? ' )
              else write( '[R]etry, [N]ew file or [S]kip? ' );
              readln( answer );
            until (length( answer ) = 1) and (upcase( answer[1] ) in ['R', 'N', 'S']);

            if upcase( answer[1] ) = 'R' then
              IO := FALSE
            else if upcase( answer[1] ) = 'N' then
              begin
                if IsPath then write( 'New path: ' )
                else write( 'New file: ' );
                readln( answer );
                if answer <> '' then
                  begin
                    filename^ := fexpand( answer );
                    if IsPath and (filename^[ length( filename^ ) ] <> '\') then
                      filename^ := filename^ + '\';
                  end;
                IO := FALSE;
              end
            else
              IO := TRUE;
          end
        else
          IO := TRUE;
      end
    else
      begin
        IOsuccess := TRUE;
        IO := TRUE;
      end;

  end; (* IOsuccess *)


begin
  errCode := 0;
  noError := true;
end.
