unit FidoNet;

{$i defines.inc}

INTERFACE

uses dos, strConv, unixTime;

const
  Revision          = 2;
  USERNAME_MAXLEN   = 36;
  SUBJECT_MAXLEN    = 72;
  PASSWORD_MAXLEN   = 8;
  ORIGIN_MAXLEN     = 70;
  MSGTEXT_BLOCKSIZE = 1024;

  (* Message attribute flags *)
  msgPrivate        = $0001;
  msgCrash          = $0002;
  msgRcvd           = $0004;
  msgSent           = $0008;
  msgFileAttached   = $0010;
  msgInTransit      = $0020;
  msgOrphan         = $0040;
  msgKillSent       = $0080;
  msgLocal          = $0100;
  msgHold           = $0200;
  msgFileRequest    = $0800;
  msgReturnReceipt  = $1000;
  msgIsReceipt      = $2000;
  msgAuditRequest   = $4000;
  msgFileUpdateReq  = $8000;

  (* Error codes returned by functions *)
  EIADDR          = -1;      (* Invalid address *)
  ENHNDL          = -35;     (* No handle available *)
  EIHNDL          = -37;     (* Invalid handle *)
  ENSMEM          = -39;     (* Insufficent memory *)
  EFILXS          = -31;     (* File already exists *)
  EFILCR          = -30;     (* File creation error *)

type
  addressRec      = record
                      zone, net, node, point : integer;
                    end;

  kludgeStr       = string[36];
  userNameStr     = string[USERNAME_MAXLEN];
  subjectStr      = string[SUBJECT_MAXLEN];
  passwordStr     = string[PASSWORD_MAXLEN];
  originStr       = string[ORIGIN_MAXLEN];
  echomailAreaStr = string[46];

  pktHandle       = integer;
  msgHandle       = integer;

  msgHeaderTypes     = (PackedMsgType2, OpusMSG);

(* Address maintenance *)

function addressStr( src : string; var address : addressRec ) : integer;
function strAddress( address : addressRec ) : string;
function validAddress( src : string ) : boolean;
function equalAddress( a1, a2 : addressRec ) : boolean;
function greaterAddress( a1, a2 : addressRec ) : boolean;

(* Mail packet *)

function pktNew( var handle : pktHandle ) : integer;
function pktClr( var handle : pktHandle ) : integer;

function pktAddress( handle : pktHandle; orgAddress, dstAddress : addressRec ) : integer;
function pktDate( handle : pktHandle; date : longint ) : integer;
function pktProduct( handle : pktHandle; code, rev : word ) : integer;
function pktPassword( handle : pktHandle; password : passwordStr ) : integer;
function pktCapability( handle : pktHandle; cw : word ) : integer;
function pktProductData( handle : pktHandle; data : longint ) : integer;

function pktAddMsg( handle : pktHandle; msg : msgHandle ) : integer;

function pktWrite( handle : pktHandle; path : dirStr ) : integer;

(* Messages *)

function msgNew( var handle : msgHandle ) : integer;
function msgClr( var handle : msgHandle ) : integer;

function msgAddKludge( handle : msgHandle; kludge : kludgeStr ) : integer;
function msgAddress( handle : msgHandle; orgAddress, dstAddress : addressRec ) : integer;
function msgAttributeSet( handle : msgHandle; attribute : word ) : integer;
function msgAttributeClr( handle : msgHandle; attribute : word ) : integer;
function msgDate( handle : msgHandle; time : longint ) : integer;
function msgFromUser( handle : msgHandle; userName : userNameStr ) : integer;
function msgToUser( handle : msgHandle; userName : userNameStr ) : integer;
function msgSubject( handle : msgHandle; subject : subjectStr ) : integer;
function msgMakeMSGID( handle : msgHandle; address : addressRec ) : integer;
function msgAddSeenBy( handle : msgHandle; address : addressRec ) : integer;
function msgOrigin( handle : msgHandle; origin : originStr; address : addressRec ) : integer;

function msgText( handle : msgHandle; txt : string ) : integer;
function msgTextLn( handle : msgHandle; txt : string ) : integer;

function msgOpusMsgWrite( handle : msgHandle; path : dirStr ) : integer;

(**************************************************************************)

IMPLEMENTATION

const
  MAXPKTHANDLES   = 5;
  MAXMSGHANDLES   = 20;

type
  monthStr  = string[3];
  kludgePtr = ^kludgeRec;
  kludgeRec = record
                kludge        : kludgeStr;
                next          : kludgePtr;
              end;

  dateTimeType  = packed array [1..20] of char;

  msgTxtPtr = ^msgTxtRec;
  msgTxtRec = packed record
                block : packed array [1..MSGTEXT_BLOCKSIZE] of byte;
                size  : word;
                next  : msgTxtPtr;
              end;

  msgPtr    = ^msgRec;

  msgPackedHeader   = packed record    (* Message header type 2, FTS-0001 *)
                        msgType       : word; (* 02h 00h *)
                        origNode      : word; (* lo-hi *)
                        destNode      : word; (* lo-hi *)
                        origNet       : word; (* lo-hi *)
                        destNet       : word; (* lo-hi *)
                        attribute     : word; (* lo-hi *)
                        cost          : word; (* lo-hi *)
                        dateTime      : dateTimeType;
                        msgInfo       : packed array [1..144] of char;
                      end;

  msgOpusHeader     = packed record     (* Opus/FD MSG header *)
                        fromUser      : packed array [1..USERNAME_MAXLEN] of char;
                        toUser        : packed array [1..USERNAME_MAXLEN] of char;
                        subject       : packed array [1..SUBJECT_MAXLEN] of char;
                        dateTime      : dateTimeType;
                        timesRead     : word;
                        destNode      : word;
                        origNode      : word;
                        cost          : word;
                        origNet       : word;
                        destNet       : word;
                        dateWritten   : longint;
                        dateArrived   : longint;
                        replyTo       : word;
                        attribute     : word;
                        replyNext     : word;
                      end;

  msgRec    = record
                orgAddress    : addressRec;
                dstAddress    : addressRec;
                toUserName    : userNameStr;
                fromUserName  : userNameStr;
                subject       : subjectStr;
                kludges       : kludgePtr;
                seenBy        : kludgeStr;
                echomailArea  : echomailAreaStr;
                date          : longint;
                attribute     : word;
                cost          : word;
                msgTxt        : msgTxtPtr;
              end;

  msgLstPtr = ^msgLstRec;
  msgLstRec = record
                msg           : msgHandle;
                next          : msgLstPtr;
              end;

  pktPtr    = ^pktRec;
  pktHeader = packed record     (* Pkt header type 2.2 *)
                origNode      : word;
                destNode      : word;
                year          : integer;
                month         : integer;
                day           : integer;
                hour          : integer;
                min           : integer;
                sec           : integer;
                baud          : integer;
                pktVer        : integer; (* Always 2 *)
                origNet       : word;
                destNet       : word;
                prdCodL       : byte; (* FTSC Product Code (lo) *)
                prdRevMajor   : byte; (* FTSC Product Rev  (major) *)
                password      : packed array [1..8] of char;
                qOrigZone     : integer;
                qDestZone     : integer;
                filler        : packed array [1..2] of byte;
                capValid      : word; (* CW byte-swapped valied copy *)
                prdCodH       : byte; (* FTSC Product Code (hi) *)
                prdRevMinor   : byte; (* FTSC Product Rev (minor) *)
                capWord       : word; (* Capability Word *)
                origZone      : integer;
                destZone      : integer;
                origPoint     : integer;
                destPoint     : integer;
                prodData      : longint;

                (* Packet terminator 00h 00h *)
              end;

  pktRec    = record
                hdr           : pktHeader;
                msgs          : msgLstPtr;
              end;

  {$IFDEF Atari}
  rawFile   = file of byte;
  {$ELSE}
  rawFile   = file;
  {$ENDIF}

var
  pktTbl : array [1..MAXPKTHANDLES] of pktPtr;
  msgTbl : array [1..MAXMSGHANDLES] of msgPtr;

(* --- Packet maintenance ----------------------------------------------- *)

function pktValidHandle( handle : pktHandle ) : boolean;
  begin
    if (handle > 0) and (handle <= MAXPKTHANDLES) then
      begin
        if pktTbl[ handle ] <> nil then pktValidHandle := true
        else pktValidHandle := false;
      end
    else
      pktValidHandle := false;
  end; (* pktValidHandle *)

function msgValidHandle( handle : msgHandle ) : boolean;
  begin
    if (handle > 0) and (handle <= MAXMSGHANDLES) then
      begin
        if msgTbl[ handle ] <> nil then msgValidHandle := true
        else msgValidHandle := false;
      end
    else
      msgValidHandle := false;
  end; (* msgValidHandle *)

(* Allocate memory for a new mail packet. 'path' is a complete path and
 * filename of a mail packet.
*)
function pktNew( var handle : pktHandle ) : integer;
  begin
    (* Find an emtpy handle *)
    handle := MAXPKTHANDLES;
    while (pktTbl[handle] <> nil) and (handle > 0) do dec( handle );

    (* If handle is found allocate a packet record *)
    if handle > 0 then
      begin
        if sizeof( pktRec ) < maxavail then
          begin
            new( pktTbl[ handle ] );
            fillChar( pktTbl[ handle ]^, sizeof( pktTbl[ handle ]^ ), 0 );

            pktTbl[ handle ]^.msgs := nil;
            {$ifdef Atari}
            pktTbl[ handle ]^.hdr.pktVer := $0200;
            {$else}
            pktTbl[ handle ]^.hdr.pktVer := $0002;
            {$endif}

            pktNew := 0;
          end
        else
          pktNew := ENSMEM;
      end
    else
      pktNew := ENHNDL;
  end; (* pktNew *)

function msgNew( var handle : msgHandle ) : integer;
  begin
    (* Find an emtpy handle *)
    handle := MAXMSGHANDLES;
    while (msgTbl[handle] <> nil) and (handle > 0) do dec( handle );

    (* If handle is found allocate a message record *)
    if handle > 0 then
      begin
        if sizeof( msgRec ) < maxavail then
          begin
            new( msgTbl[ handle ] );
            fillChar( msgTbl[ handle ]^, sizeof( msgTbl[ handle ]^ ), 0 );

            with msgTbl[ handle ]^ do
              begin
                kludges := nil;
                msgTxt := nil;
              end;

            msgNew := 0;
          end
        else
          msgNew := ENSMEM;
      end
    else
      msgNew := ENHNDL;
  end; (* msgNew *)

(* Delete mail packet header and list of messages. However, the messages
 * is NOT deleted; they must be deleted using the 'msgClr' function.
*)
function pktClr( var handle : pktHandle ) : integer;
  var
    p : pointer;
  begin
    if pktValidHandle( handle ) then
      begin
        with pktTbl[ handle ]^ do while msgs <> nil do
          begin
            p := msgs^.next;
            dispose( msgs );
            msgs := p;
          end;

        dispose( pktTbl[ handle ] );
        pktTbl[ handle ] := nil;
        handle := 0;
        pktClr := 0;
      end
    else
      pktClr := EIHNDL;
  end; (* pktClr *)

function msgClr( var handle : msgHandle ) : integer;
  var
    k : pointer;
  begin
    if msgValidHandle( handle ) then
      begin
        with msgTbl[ handle ]^ do
          begin
            while kludges <> nil do
              begin
                k := kludges^.next;
                dispose( kludges );
                kludges := k;
              end;
            while msgTxt <> nil do
              begin
                k := msgTxt^.next;
                dispose( msgTxt );
                msgTxt := k;
              end;
          end;

        dispose( msgTbl[ handle ] );
        msgTbl[ handle ] := nil;
        handle := 0;
        msgClr := 0;
      end
    else
      msgClr := EIHNDL;
  end; (* msgClr *)

function pktAddress( handle : pktHandle; orgAddress, dstAddress : addressRec ) : integer;
  begin
    if pktValidHandle( handle ) then
      begin
        with pktTbl[ handle ]^.hdr do
          begin
            {$ifdef Atari}
            origNode  := swap( orgAddress.node );
            destNode  := swap( dstAddress.node );
            origNet   := swap( orgAddress.net );
            destNet   := swap( dstAddress.net );
            qOrigZone := swap( orgAddress.zone );
            qDestZone := swap( dstAddress.zone );
            origZone  := swap( orgAddress.zone );
            destZone  := swap( dstAddress.zone );
            origPoint := swap( orgAddress.point );
            destPoint := swap( dstAddress.point );
            {$else}
            origNode  := orgAddress.node;
            destNode  := dstAddress.node;
            origNet   := orgAddress.net;
            destNet   := dstAddress.net;
            qOrigZone := orgAddress.zone;
            qDestZone := dstAddress.zone;
            origZone  := orgAddress.zone;
            destZone  := dstAddress.zone;
            origPoint := orgAddress.point;
            destPoint := dstAddress.point;
            {$endif}
          end;
        pktAddress := 0;
      end
    else
      pktAddress := EIHNDL;
  end; (* pktAddress *)

function pktDate( handle : pktHandle; date : longint ) : integer;
  var
    year, month, day, hour, min, sec, wday, yday : integer;
  begin
    if pktValidHandle( handle ) then
      begin
        with pktTbl[ handle ]^.hdr do
          begin
            unix2time( date, year, month, day, hour, min, sec, wday, yday );
            {$ifdef Atari}
            year := swap( year );
            month := swap( month );
            day := swap( day );
            hour := swap( hour );
            min := swap( min );
            sec := swap( sec );
            {$else}
            year := year;
            month := month;
            day := day;
            hour := hour;
            min := min;
            sec := sec;
            {$endif}
          end;
        pktDate := 0;
      end
    else
      pktDate := EIHNDL;
  end; (* pktDate *)

function pktProduct( handle : pktHandle; code, rev : word ) : integer;
  begin
    if pktValidHandle( handle ) then
      begin
        with pktTbl[ handle ]^.hdr do
          begin
            prdCodL := lo( code );
            prdCodH := hi( code );
            prdRevMajor := hi( rev );
            prdRevMinor := lo ( rev );
          end;
        pktProduct := 0;
      end
    else
      pktProduct := EIHNDL;
  end; (* pktProduct *)

function pktPassword( handle : pktHandle; password : passwordStr ) : integer;
  begin
    if pktValidHandle( handle ) then
      begin
        move( password[1], pktTbl[ handle ]^.hdr.password, length( password ) );
        pktPassword := 0;
      end
    else
      pktPassword := EIHNDL;
  end; (* pktPassword *)

function pktCapability( handle : pktHandle; cw : word ) : integer;
  begin
    if pktValidHandle( handle ) then
      begin
        with pktTbl[ handle ]^.hdr do
          begin
            {$ifdef Atari}
            capWord := swap( cw );
            capValid := cw;
            {$else}
            capWord := cw;
            capValid := swap( cw );
            {$endif}
          end;
        pktCapability := 0;
      end
    else
      pktCapability := EIHNDL;
  end; (* pktCapability *)

function pktProductData( handle : pktHandle; data : longint ) : integer;
  begin
    if pktValidHandle( handle ) then
      begin
        pktTbl[ handle ]^.hdr.prodData := data;
        pktProductData := 0;
      end
    else
      pktProductData := EIHNDL;
  end; (* pktProductData *)


(* Add a message to the packet. A pointer to the message structure is
 * stored in the 'msgs' list in the packet structure.
*)
function pktAddMsg( handle : pktHandle; msg : msgHandle ) : integer;
  var
    p : msgLstPtr;
  begin
    if pktValidHandle( handle ) and msgValidHandle( msg ) then
      begin
        if sizeof( msgLstPtr ) < maxavail then
          begin
            with pktTbl[ handle ]^ do
              begin
                if msgs = nil then
                  begin
                    new( msgs );
                    p := msgs;
                  end
                else
                  begin
                    p := msgs;
                    while p^.next <> nil do p := p^.next;
                    new( p^.next );
                    p := p^.next;
                  end;
                p^.msg := msg;
                p^.next := nil;
              end;
          end
        else
          pktAddMsg := ENSMEM;
      end
    else
      pktAddMsg := EIHNDL;
  end; (* pktAddMsg *)

function getOpusMsgNumber( path : dirStr ) : integer;
  var
    f     : searchRec;
    fdir  : dirStr;
    fname : nameStr;
    fext  : extStr;
    num   : integer;
    max   : integer;
    {$ifdef VirtualPascal}
      code : Longint;
    {$else}
      code : integer;
    {$endif}

  begin
    max := -1;

    findFirst( path + '*.MSG', Archive, f );

    while dosError = 0 do
      begin
        fsplit( f.name, fdir, fname, fext );
        val( fname, num, code );
        if code = 0 then if num > max then max := num;
        findNext( f );
      end;

    getOpusMsgNumber := max + 1;
  end; (* getOpusMsgNumber *)

(* Write a string raw to the output pktFile. No I/O checking. Local
 * procedure.
*)
procedure fputTxt( var f : rawFile; txt : string );
  begin
    (* Write a string very raw to the output file *)
    {$I-}
    blockwrite( f, txt[1], length( txt ) );
    {$I+}
  end; (* fputTxt *)

function monthName( month : integer ) : monthStr;
  begin
    case month of
      1 : monthName := 'Jan';
      2 : monthName := 'Feb';
      3 : monthName := 'Mar';
      4 : monthName := 'Apr';
      5 : monthName := 'May';
      6 : monthName := 'Jun';
      7 : monthName := 'Jul';
      8 : monthName := 'Aug';
      9 : monthName := 'Sep';
      10 : monthName := 'Oct';
      11 : monthName := 'Nov';
      12 : monthName := 'Dec';
      end;
  end; (* monthName *)

(* Create a date string
*)
procedure makeDateTime( time : longint; var dateTime : dateTimeType );
  var
    i     : byte;
    date  : string[20];
    year, month, day, hour, min, sec, wday, yday : integer;
  begin
    unix2time( time, year, month, day, hour, min, sec, wday, yday );

    date := strlongz( day, 2 ) + ' ' + monthName( month ) + ' ' + strlong( year mod 100, 2 )
            + '  ' + strlongz( hour, 2 ) + ':' + strlongz( min, 2 ) + ':' + strlongz( sec, 2 );
    move( date[1], dateTime, length( date ) );
  end; (* makeDateTime *)


(* Write a message to file
*)
function msgWrite( var f : rawFile; handle : msgHandle; headerType : msgHeaderTypes ) : integer;
  var
    hdr         : pointer;
    hdrSize     : integer;
    maxHdrSize  : integer;
    retv        : integer;
    i           : integer;
    k           : kludgePtr;
    t           : msgTxtPtr;
  begin
    retv := 0;

    if msgValidHandle( handle ) then with msgTbl[ handle ]^ do
      begin
        case headerType of
          OpusMSG         : maxHdrSize := sizeof( msgOpusHeader );
          PackedMsgType2  : maxHdrSize := sizeof( msgPackedHeader );
          end;

        hdrSize := maxHdrSize;

        if hdrSize < maxavail then
          begin
            getmem( hdr, maxHdrSize );
            fillchar( hdr^, hdrSize, 0 );

            case headerType of
              PackedMsgType2: begin
                                {$ifdef Atari}
                                msgPackedHeader( hdr^ ).msgType   := $0200;
                                msgPackedHeader( hdr^ ).origNode  := swap( orgAddress.node );
                                msgPackedHeader( hdr^ ).destNode  := swap( dstAddress.node );
                                msgPackedHeader( hdr^ ).origNet   := swap( orgAddress.net );
                                msgPackedHeader( hdr^ ).destNet   := swap( dstAddress.net );
                                msgPackedHeader( hdr^ ).attribute := swap( attribute );
                                msgPackedHeader( hdr^ ).cost      := swap( cost );
                                {$else}
                                msgPackedHeader( hdr^ ).msgType   := $0002;
                                msgPackedHeader( hdr^ ).origNode  := orgAddress.node;
                                msgPackedHeader( hdr^ ).destNode  := dstAddress.node;
                                msgPackedHeader( hdr^ ).origNet   := orgAddress.net;
                                msgPackedHeader( hdr^ ).destNet   := dstAddress.net;
                                msgPackedHeader( hdr^ ).attribute := attribute;
                                msgPackedHeader( hdr^ ).cost      := cost;
                                {$endif}
                                makeDateTime( date, msgPackedHeader( hdr^ ).dateTime );
                                move( toUserName[1], msgPackedHeader( hdr^ ).msgInfo, length( toUserName ) );
                                i := length( toUserName ) + 1;
                                move( fromUserName[1], msgPackedHeader( hdr^ ).msgInfo[i+1], length( fromUserName ) );
                                i := i + length( fromUserName ) + 1;
                                move( subject[1], msgPackedHeader( hdr^ ).msgInfo[i+1], length( subject ) );
                                i := i + length( subject ) + 1;
                                hdrSize := hdrSize - (144 - i);

                                (* Add INTL kludge in netmail or AREA kludge in echomail *)
                                if (echomailArea <> '') then
                                  retv := msgAddKludge( handle, 'AREA: ' + echomailArea )
                                else if orgAddress.zone <> dstAddress.zone then
                                  retv := msgAddKludge( handle, 'INTL ' + strAddress( orgAddress )+' '+strAddress(dstAddress));

                                (* Add FMPT and TOPT kludges *)
                                if (orgAddress.point <> 0) and (retv = 0) then
                                  retv := msgAddKludge( handle, 'FMPT ' + strLong( orgAddress.point, 0 ) );
                                if (dstAddress.point <> 0) and (retv = 0) then
                                  retv := msgAddKludge( handle, 'TOPT ' + strLong( dstAddress.point, 0 ) );
                              end;
              OpusMSG:        begin
                                move( fromUserName[1], msgOpusHeader( hdr^ ).fromUser, length( fromUserName ) );
                                move( toUserName[1], msgOpusHeader( hdr^ ).toUser, length( toUserName ) );
                                move( subject[1], msgOpusHeader( hdr^ ).subject, length( subject ) );
                                makeDateTime( date, msgOpusHeader( hdr^ ).dateTime );
                                msgOpusHeader( hdr^ ).destNode  := dstAddress.node;
                                msgOpusHeader( hdr^ ).origNode  := orgAddress.node;
                                msgOpusHeader( hdr^ ).cost      := cost;
                                msgOpusHeader( hdr^ ).origNet   := orgAddress.net;
                                msgOpusHeader( hdr^ ).destNet   := dstAddress.net;
                                msgOpusHeader( hdr^ ).attribute := attribute;
                                (* dateWritten, dateArrived, replyTo and replyNext not supported *)

                                (* Add INTL kludge in netmail or AREA kludge in echomail *)
                                if (echomailArea <> '') then
                                  retv := msgAddKludge( handle, 'AREA: ' + echomailArea )
                                else if orgAddress.zone <> dstAddress.zone then
                                  retv := msgAddKludge( handle, 'INTL ' + strAddress( dstAddress )+' '+strAddress(orgAddress));

                                (* Add FMPT and TOPT kludges *)
                                if (orgAddress.point <> 0) and (retv = 0) then
                                  retv := msgAddKludge( handle, 'FMPT ' + strLong( orgAddress.point, 0 ) );
                                if (dstAddress.point <> 0) and (retv = 0) then
                                  retv := msgAddKludge( handle, 'TOPT ' + strLong( dstAddress.point, 0 ) );

                              end;
              end;

            (* Write message header *)
            blockWrite( f, hdr^, hdrSize );

            (* Write kludges *)
            k := kludges;
            while k <> nil do
              begin
                fputTxt( f, #1 + k^.kludge + #13 );
                k := k^.next;
              end;

            (* Write message text *)
            t := msgTxt;
            while t <> nil do
              begin
                blockwrite( f, t^.block, t^.size );
                t := t^.next;
              end;

            (* Write SEEN-BY-lines *)
            if seenBy <> '' then
              fputTxt( f, 'SEEN-BY: ' + seenBy + #13 );

            (* Terminate message *)
            fputTxt( f, #0 );

            (* Clear message header *)
            freemem( hdr, maxHdrSize );

            msgWrite := retv;
          end
        else
          begin
            msgWrite := ENSMEM;
          end;
      end
    else
      msgWrite := EIHNDL;
  end; (* msgWrite *)

(* Write Opus *.MSG to disk. Search the highest message number in
 * 'path' and write a message.
*)
function msgOpusMsgWrite( handle : msgHandle; path : dirStr ) : integer;
  var
    msgNum : integer;
    f      : rawFile;
  begin
    if msgValidHandle( handle ) then
      begin
        msgNum := getOpusMsgNumber( path );

        assign( f, path + strLong( msgNum, 0 ) + '.MSG' );
        {$i-}
        rewrite( f, 1 );
        {$i+}
        if ioresult = 0 then
          begin
           msgOpusMsgWrite := msgWrite( f, handle, OpusMSG );
           close( f );
          end
        else
          msgOpusMsgWrite := EFILCR;
      end
    else
      msgOpusMsgWrite := EIHNDL;
  end; (* msgOpusMsgWrite *)


(* Write complete mail packet to disk.
*)
function pktWrite( handle : pktHandle; path : dirStr ) : integer;
  var
    f : rawFile;
    p : msgLstPtr;
    k : kludgePtr;
    t : msgTxtPtr;
  begin
    if pktValidHandle( handle ) then with pktTbl[ handle ]^ do
      begin
        assign( f, path + strBaseN( today, 16, 8 ) + '.PKT' );

        (* Test if file exists *)
        {$i-}
        reset( f );
        {$i+}
        if ioresult <> 0 then
          begin
            {$i-}
            rewrite( f, 1 );
            {$i+}
            if ioresult = 0 then
              begin
                {$i-}
                (* Write packet header *)
                blockwrite( f, hdr, sizeof( hdr ) );

                (* Write messages *)
                p := msgs;

                while p <> nil do
                  begin
                    msgWrite( f, p^.msg, PackedMsgType2 );

                    p := p^.next;
                  end;

                (* Terminate packet *)
                fputTxt( f, #0 );
                close( f );
                {$i+}
                pktWrite := 0;
              end
            else
              pktWrite := EFILCR;
          end
        else
          begin
            close( f );
            pktWrite := EFILXS;
          end;
      end
    else
      pktWrite := EIHNDL;
  end;

(* Message Header ------------------------------------------------------------- *)

function msgAddKludge( handle : msgHandle; kludge : kludgeStr ) : integer;
  var
    k : kludgePtr;
  begin
    if msgValidHandle( handle ) then with msgTbl[ handle ]^ do
      begin
        if sizeof( kludgeRec ) < maxavail then
          begin
            if kludges = nil then
              begin
                new( kludges );
                k := kludges;
              end
            else
              begin
                k := kludges;
                while k^.next <> nil do k := k^.next;
                new( k^.next );
                k := k^.next;
              end;
            k^.next := nil;
            k^.kludge := kludge;
            msgAddKludge := 0;
          end
        else
          msgAddKludge := ENSMEM;
      end
    else
      msgAddKludge := EIHNDL;
  end; (* addKludge *)

function msgAddress( handle : msgHandle; orgAddress, dstAddress : addressRec ) : integer;
  begin
    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.orgAddress := orgAddress;
        msgTbl[ handle ]^.dstAddress := dstAddress;

        msgAddress := 0;
      end
    else
      msgAddress := EIHNDL;
  end; (* msgAdress *)

function msgAttributeSet( handle : msgHandle; attribute : word ) : integer;
  begin
    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.attribute := msgTbl[ handle ]^.attribute or attribute;

        msgAttributeSet := 0;
      end
    else
      msgAttributeSet := EIHNDL;
  end; (* msgAttributeSet *)

function msgAttributeClr( handle : msgHandle; attribute : word ) : integer;
  begin
    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.attribute := msgTbl[ handle ]^.attribute and (not attribute);

        msgAttributeClr := 0;
      end
    else
      msgAttributeClr := EIHNDL;
  end; (* msgAttributeSet *)

function msgDate( handle : msgHandle; time : longint ) : integer;
  begin
    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.date := time;

        msgDate := 0;
      end
    else
      msgDate := EIHNDL;
  end; (* msgDate *)

function msgFromUser( handle : msgHandle; userName : userNameStr ) : integer;
  begin
    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.fromUserName := userName;
        msgFromUser := 0;
      end
    else
      msgFromUser := EIHNDL;
  end; (* msgFromUser *)

function msgToUser( handle : msgHandle; userName : userNameStr ) : integer;
  begin
    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.toUserName := userName;
        msgToUser := 0;
      end
    else
      msgToUser := EIHNDL;
  end; (* msgToUser *)

function msgSubject( handle : msgHandle; subject : subjectStr ) : integer;
  begin
    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.subject := subject;
        msgSubject := 0;
      end
    else
      msgSubject := EIHNDL;
  end; (* msgSubject *)

function msgMakeMSGID( handle : msgHandle; address : addressRec ) : integer;
  begin
    msgMakeMSGID := msgAddKludge( handle, 'MSGID: ' + strAddress( address ) + ' ' + strBaseN( today, 16, 8 ) );
  end; (* msgMakeMSGID *)

function msgAddSeenBy( handle : msgHandle; address : addressRec ) : integer;
  begin
    (* This should be better! *)

    if msgValidHandle( handle ) then
      begin
        msgTbl[ handle ]^.seenBy := strLong( address.net, 0 ) + '/' + strLong( address.node, 0 );
        msgAddSeenBy := 0;
      end
    else
      msgAddSeenBy := EIHNDL;
  end; (* msgAddSeenBy *)

function msgOrigin( handle : msgHandle; origin : originStr; address : addressRec ) : integer;
  var
    retv : integer;
  begin
    (* Add origin line. *)
    (* NOTE: Origin line must be added AFTER all message text *)

    if msgValidHandle( handle ) then
      msgOrigin := msgTextLn( handle, ' * Origin: ' + origin + ' (' + strAddress( address ) + ')' )
    else
      msgOrigin := EIHNDL;
  end; (* msgOrigin *)

function msgNewTextBlock : msgTxtPtr;
  var
    p : msgTxtPtr;
  begin
    (* Allocate new text block *)
    if sizeof( p^ ) < maxavail then
      begin
        getmem( p, sizeof( p^ ) );
        fillchar( p^, sizeof( p^ ), 0 );
        p^.next := nil;
        msgNewTextBlock := p;
      end
    else
      msgNewTextBlock := nil;
  end;

function msgText( handle : msgHandle; txt : string ) : integer;
  var
    p   : msgTxtPtr;
    len : word;
  begin
    (* Adds messages text to dynamic text blocks *)

    if msgValidHandle( handle ) then with msgTbl[ handle ]^ do
      begin
        if msgTxt = nil then
          begin
            (* Allocate new block *)
            msgTxt := msgNewTextBlock;
            p := msgTxt;
          end
        else
          begin
            (* Find current block *)
            p := msgTxt;
            while (p^.size >= MSGTEXT_BLOCKSIZE) and (p^.next <> nil) do p := p^.next;
            if p^.size >= MSGTEXT_BLOCKSIZE then
              begin
                (* Allocate new block if last block is full *)
                p^.next := msgNewTextBlock;
                p := p^.next;
              end;
          end;
        if p <> nil then
          begin
            if length( txt ) <= (MSGTEXT_BLOCKSIZE - p^.size) then
              begin
                (* Move text string to block *)
                move( txt[1], p^.block[p^.size+1], length( txt ) );
                p^.size := p^.size + length( txt );
              end
            else
              begin
                (* Move partial text string to block *)
                len := MSGTEXT_BLOCKSIZE - p^.size;
                move( txt[1], p^.block[p^.size+1], len );
                p^.size := MSGTEXT_BLOCKSIZE;

                (* Allocate a new block *)
                p^.next := msgNewTextBlock;
                if p^.next <> nil then
                  begin
                    (* Move rest of string to new block *)
                    p := p^.next;
                    move( txt[len+1], p^.block[1], length( txt ) - len );
                    p^.size := length( txt ) - len;
                  end;
              end;
            msgText := 0;
          end
        else
          msgText := ENSMEM;
      end
    else
      msgText := EIHNDL;
  end; (* msgText *)

function msgTextLn( handle : msgHandle; txt : string ) : integer;
  begin
    msgTextLn := msgText( handle, txt + #13 );
  end; (* msgTextLn *)

(* Address maintenance *)

function addressStr( src : string; var address : addressRec ) : integer;
  var
    pos1, pos2, pos3 : byte;
    retv             : integer;
    {$ifdef VirtualPascal}
      errcode : Longint;
    {$else}
      errcode : integer;
    {$endif}

  begin
    (* Convert string to a FidoNet address *)
    retv := 0;
    pos1 := pos( ':', src );
    pos2 := pos( '/', src );
    pos3 := pos( '.', src );

    with address do
      begin
        if pos1 = 0 then
          begin
            zone := 0;
            val( copy( src, 1, pos2 - 1 ), net, errcode );
            if errcode <> 0 then retv := EIADDR;
          end
        else
          begin
            val( copy( src, 1, pos1 - 1 ), zone, errcode );
            if errcode <> 0 then retv := EIADDR;
            val( copy( src, pos1 + 1, pos2 - pos1 - 1 ), net, errcode );
            if errcode <> 0 then retv := EIADDR;
          end;
        if pos3 = 0 then
          begin
            val( copy( src, pos2 + 1, length( src ) - pos2 ), node, errcode );
            if errcode <> 0 then retv := EIADDR;
            point := 0;
          end
        else
          begin
            val( copy( src, pos2 + 1, pos3 - pos2 - 1 ), node, errcode );
            if errcode <> 0 then retv := EIADDR;
            val( copy( src, pos3 + 1, length( src ) - pos3 ), point, errcode );
            if errcode <> 0 then retv := EIADDR;
          end;
      end;
    addressStr := retv;
  end; (* addressStr *)

function strAddress( address : addressRec ) : string;
  begin
    with address do
      begin
        if (zone = 0) and (net = 0) and (node = 0) and (point = 0) then
          begin
            strAddress := '';
          end
        else
          begin
            if zone = 0 then
              strAddress := strlong( net, 0 ) + '/' + strlong( node, 0 )
            else if point = 0 then
              strAddress := strlong( zone, 0 ) + ':' + strlong( net, 0 ) + '/' + strlong( node, 0 )
            else
              strAddress := strlong( zone, 0 ) + ':' + strlong( net, 0 ) + '/' + strlong( node, 0 )
                            + '.' + strlong( point, 0 );
          end;
      end;
  end; (* strAddress *)

function validAddress( src : string ) : boolean;
  var
    valid : boolean;
    p : byte;
    len : byte;
  begin
    p := 1;
    len := length( src );
    valid := true;
    while (src[p] in ['0'..'9']) and (p < len) do inc( p );
    if (p = 1) or (src[p] <> ':' ) then valid := false
    else
      begin
        inc( p );
        while (src[p] in ['0'..'9']) and (p < len) do inc( p );
        if src[p] <> '/' then valid := false
        else
          begin
            inc( p );
            while (src[p] in ['0'..'9']) and (p < len) do inc( p );
            if src[p] <> '.' then
              begin
                if not( (p = len) and (src[p] in ['0'..'9']) ) then valid := false;
              end
            else
              begin
                inc( p );
                while (src[p] in ['0'..'9']) and (p < len) do inc( p );
                if not( (p = len) and (src[p] in ['0'..'9']) ) then valid := false;
              end;
          end;
      end;
    validAddress := valid;
  end; (* validAddress *)

function equalAddress( a1, a2 : addressRec ) : boolean;
  begin
    if (a1.zone = a2.zone) and (a1.net = a2.net) and (a1.node = a2.node) and (a1.point = a2.point) then
      equalAddress := true
    else
      equalAddress := false;
  end; (* equalAddress *)

function greaterAddress( a1, a2 : addressRec ) : boolean;
  begin
    if a1.zone > a2.zone then
      greaterAddress := true
    else
      begin
        if (a1.zone = a2.zone) and (a1.net > a2.net) then
          greaterAddress := true
        else
          begin
            if (a1.zone = a2.zone) and (a1.net = a2.net) and (a1.node > a2.node) then
              greaterAddress := true
            else
              begin
                if (a1.zone = a2.zone) and (a1.net = a2.net) and (a1.node = a2.node) and (a1.point > a2.point) then
                  greaterAddress := true
                else
                  greaterAddress := false;
              end
          end
      end
  end;

procedure init;
  var
    i : integer;
  begin
    for i := 1 to MAXPKTHANDLES do pktTbl[ i ] := nil;
    for i := 1 to MAXMSGHANDLES do msgTbl[ i ] := nil;
  end;

begin
  init;
end.
