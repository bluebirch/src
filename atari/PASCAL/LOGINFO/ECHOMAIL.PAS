UNIT EchoMail;

{$i defines.inc}

INTERFACE

uses dos, errHndle, mem, strconv, global, misc, output;

const
  (* Echomail area update codes *)
  ECHOMAIL_AREANAME   = 1;
  ECHOMAIL_MSGS       = 2;
  ECHOMAIL_DUPES      = 3;
  ECHOMAIL_TOSSTIME   = 4;

(* procedure new_echomail_area( var p : echomail_ptr ); *)
procedure update_echomail_area( areaname : areaname_string; field : word; p : pointer );
procedure sort_echomail_areas( sort_key : word );
procedure write_echomail_areas( out : integer );
procedure save_echomail_areas;
procedure read_echomail_areas;

IMPLEMENTATION

var
  EchoMailFileName : PathStr;

procedure new_echomail_area( var p : echomail_ptr );
  begin
    p := malloc( sizeof( echomail_rec ) );
  end; (* new_echomail_area *)

procedure update_echomail_area( areaname : areaname_string; field : word; p : pointer );
  var
    i : word;
    {$IFDEF Atari}
    found : boolean;
    {$ENDIF}

  begin
    (* Find area to update; this might be a binary search in the future *)

    i := _echomail_counter;

    {$IFDEF Atari}
    found := false;
    while (i > 0) and not found do
      begin
        if _echomail[i]^.areaname = areaname then found := true
        else dec( i );
      end;
    {$ELSE}
    while (i > 0) and (_echomail[i]^.areaname <> areaname) do dec( i );
    {$ENDIF}
    if i = 0 then
      begin
        inc( _echomail_counter );
        i := _echomail_counter;
        new_echomail_area( _echomail[i] );
        if noError then
          begin
            _echomail[i]^.areaname := areaname;
            _echomail[i]^.firsttoss := $7FFFFFFF;
          end;
      end;

    (* Update echomail area *)

    {$IFDEF DEBUGALL}
    writeln( '>Update echomail area "', areaname, '" (', i , ')' );
    {$ENDIF}

    if noError then
      begin
        case field of
          ECHOMAIL_MSGS     : _echomail[i]^.msgs := _echomail[i]^.msgs + longint( p^ );
          ECHOMAIL_DUPES    : _echomail[i]^.dupes := _echomail[i]^.dupes + longint( p^ );
          ECHOMAIL_TOSSTIME : begin
                                if longint( p^ ) > _echomail[i]^.lasttoss then _echomail[i]^.lasttoss := longint( p^ );
                                if longint( p^ ) < _echomail[i]^.firsttoss then _echomail[i]^.firsttoss := longint( p^ );
                              end;
          end;
      end;
  end; (* update_echomail_area *)

procedure sort_echomail_areas( sort_key : word );

  procedure sort( leftend, rightend : integer );
    var
      i, j : integer;
      x    : echomail_ptr;
      tmp  : pointer;
    begin
      i := leftend;
      j := rightend;

      (* Partition list *)
      x := _echomail[ (leftend + rightend) div 2 ];
      while i <= j do
        begin
          case sort_key of
            ECHOMAIL_AREANAME : begin
                                  while x^.areaname > _echomail[i]^.areaname do inc( i );
                                  while x^.areaname < _echomail[j]^.areaname do dec( j );
                                end;
            ECHOMAIL_MSGS     : begin
                                  while x^.msgs < _echomail[i]^.msgs do inc( i );
                                  while x^.msgs > _echomail[j]^.msgs do dec( j );
                                end;
            else begin
                   error( errInternalError, nil );
                 end;
            end;
          if i <= j then
            begin
              tmp := _echomail[i];
              _echomail[i] := _echomail[j];
              _echomail[j] := tmp;
              inc( i );
              dec( j );
            end;
        end;
      if leftend < j then
        sort( leftend, j );
      if i < rightend then
        sort( i, rightend );
    end; (* sort_echomail_areas.sort *)

  begin
    if _echomail_counter > 0 then
      begin
        if Config.Verbose then writeln( 'Sorting echomail areas' );
        sort( 1, _echomail_counter );
      end;
  end; (* sort_echomail_areas *)


procedure write_echomail_areas( out : integer );
  var
    i : word;
  begin
    if Config.Verbose then writeln( 'Writing echomail areas' );
    
    write_header( out, Config.echomail_header );

    for i := 1 to _echomail_counter do
      putText( out, patternstr( EchomailList, @Config.echomail_pattern, _echomail[i] ) );
  end; (* write_echomail_areas *)


procedure save_echomail_areas;
  var
    i : word;
    f : text;
  begin
    if Config.Verbose then writeln( 'Writing ECHOMAIL.CFG' );
    
    if EchoMailFileName = '' then EchoMailFileName := Config.SystemPath + 'ECHOMAIL.CFG';

    repeat
      assign( f, EchoMailFileName );
      {$i-}
      rewrite( f );
      {$i+}
    until IO( ioresult, @EchoMailFileName );

    if IOsuccess then
      begin
        writeln( f, '; This file is maintained by ', progname_str, ' and must not be altered' );
        for i := 1 to _echomail_counter do with _echomail[i]^ do
          begin
            writeln( f, ';' );
            writeln( f, 'BEGIN' );
            writeln( f, 'N=', areaname );
            writeln( f, 'M=', msgs );
            writeln( f, 'D=', dupes );
            writeln( f, 'F=', timestamp( firsttoss ) );
            writeln( f, 'L=', timestamp( lasttoss ) );
            writeln( f, 'END' );
          end;
        close( f );
      end;
  end; (* save_echomail_areas *)

procedure read_echomail_areas;
  var
    f         : text;
    s         : string;
    variable  : string;
    value     : string;
  begin
    if EchoMailFileName = '' then EchoMailFileName := Config.SystemPath + 'ECHOMAIL.CFG';

    repeat
      assign( f, EchoMailFileName );
      {$I-}
      reset( f );
      {$I+}
    until IO( ioresult, @EchoMailFileName );

    if IOsuccess then
      begin
        if Config.Verbose then writeln( 'Reading ECHOMAIL.CFG' );

        {$IFDEF Atari}
        variable := '';
        {$ENDIF}
        
        while not eof( f ) and noError {$IFDEF Atari} and (variable <> 'EOF') {$ENDIF} do
          begin
            readln( f, s );

            if (length( s ) > 0) and (s[1] <> ';') then
              begin
                split_variable( s, variable, value );
                if variable = 'BEGIN' then
                  begin
                    inc( _echomail_counter );
                    new_echomail_area( _echomail[ _echomail_counter ] );
                  end
                else if variable = 'END' then
                  begin
                  end
                {$IFDEF Atari}
                else if variable = 'EOF' then
                  begin
                  end
                {$ENDIF}
                else if length( variable ) = 1 then
                  begin
                    with _echomail[ _echomail_counter ]^ do case variable[1] of
                      'N' : areaname := value;
                      'M' : msgs := longStr( value );
                      'D' : dupes := longStr( value );
                      'F' : firsttoss := timestamp2unix( value );
                      'L' : begin
                              lasttoss := timestamp2unix( value );
                            end;
                      else error( errInternalConfiguration, @EchoMailFileName);
                      end;
                  end
                else
                  error( errInternalConfiguration, @EchoMailFileName );
              end;
          end;
        close( f );

        if not noError then writeln( 'Error in ECHOMAIL.CFG: ', s );
      end
    else
      writeln( 'ECHOMAIL.CFG not found' );
  end;

begin
  EchoMailFileName := '';
end.
