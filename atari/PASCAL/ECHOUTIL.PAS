program echoutil;

(************************************************************************)
(* Skrivet av Stefan Svensson, 70:717/1.1                               *)
(************************************************************************)

{$IFDEF CPU86}
{$R-}
{$ENDIF}


uses dos;

const
	MAX_RECORDS = 4096;

type
  line_string = string[255];
  name_string = string[30];
  desc_string = string[100];

  list_record = record
                  name : name_string;
                  desc : desc_string;
                end;
	list_array  = array [0..MAX_RECORDS] of ^list_record;
  list_ptr    = ^list_array;
  list_recnum = longint;

  list_ofs_ptr    = ^list_ofs_record;
  list_ofs_record = record
                      ofs   : list_recnum;
                      next  : list_ofs_ptr;
                    end;

const
	PROG_VERSION  = $010B;
  FREE_MEM      = 65536;

  ERR_IO        = 2;
  ERR_PARAM     = 3;
  ERR_MEM       = 4;
  ERR_INTERNAL  = 5;

var
  error       : integer;  (* Intern felkod *)
  mem_free    : longint;  (* Fritt minne *)

  opt_verbose : boolean;

  report_filename   : pathstr;

  old_list          : list_ptr;
  old_list_records  : list_recnum;
  old_list_filename : pathstr;
  old_ofs_first,
  old_ofs_current   : list_ofs_ptr;

  new_list          : list_ptr;
  new_list_records  : list_recnum;
  new_list_filename : pathstr;
  new_ofs_first,
  new_ofs_current   : list_ofs_ptr;

procedure err( errcode : integer );
  begin
    error := errcode;
    write( 'Error ', errcode, ': ' );
    case errcode of
      ERR_IO : writeln( 'File not found' );
      ERR_MEM : writeln( 'Not enough memory' );
      ERR_PARAM : begin
            writeln( 'Invalid parameters' );
            writeln;
            writeln( 'Usage: ECHOUTIL -O<old list> -N<new list> -R<report file> [-V]' );
            writeln;
          end;
      else writeln( 'Unknown error' );
      end;
  end;

function fix_len( s : string; len : byte ) : string;
  begin
    if len <> 0 then
      begin
        if length( s ) > len then s := copy( s, 1, len )
        else while length( s ) < len do s := s + ' ';
      end;
    fix_len := s;
  end;


procedure split( src : line_string; var name : name_string; var desc : desc_string );

  (*
  Plockar ut det f�rsta ordet i en str�ng och returnerar detta i variabeln
  "name". Resten av str�ngen returneras i "desc".
  *)
  
  var
    i, j, src_len : byte;
  begin
    i := 1;
    src_len := length( src );
    while (src[i] in [' ', #9]) and (i <= src_len) do inc( i );
    j := i;
    while not (src[j] in [' ', #9]) and (j <= src_len) do inc( j );
    name := copy( src, i, j-i );
    desc := copy( src, j+1, src_len - j );
  end; (* split *)

procedure read_list( list : list_ptr; filename : pathstr; var records : list_recnum );
  var
    infile    : text;
    textline  : line_string;
    i         : longint;
  begin
    i := 0;

    assign( infile, filename );
    {$I-}
    reset( infile );
    {$I+}
    if ioresult = 0 then
      begin
        while (not eof( infile )) and (i < MAX_RECORDS) and (error = 0) do
          begin
            readln( infile, textline );
            if sizeof( list_record ) < maxavail then
              begin
                new( list^[i] );
                split( textline, list^[i]^.name, list^[i]^.desc );
                inc( i );
              end
            else
              err( ERR_MEM );
          end;
        if i >= MAX_RECORDS then err( ERR_INTERNAL );
        close( infile );
        records := i;
      end
    else
      err( ERR_IO );
  end; (* read_list *)

procedure sort_list( list : list_ptr; first, last : list_recnum );
  var
    start, stop : list_recnum;
    tmp    : pointer;
    middle : name_string;

  begin
    start := first;
    stop := last;
    middle := list^[(first+last) div 2]^.name;

    while start <= stop do
      begin
        while list^[start]^.name < middle do inc( start );
        while list^[stop]^.name > middle do dec( stop );
        if start <= stop then
          begin
            tmp := list^[start];
            list^[start] := list^[stop];
            list^[stop] := tmp;
            inc( start );
            dec( stop );
          end;
      end;
    if first < stop then 
        sort_list( list, first, stop );
    if start < last then
        sort_list( list, start, last );
  end; (* sort_list *)

procedure store_list_ofs( var ofs_first, ofs_current : list_ofs_ptr; ofs : list_recnum );
  begin
    if sizeof( list_ofs_record ) < maxavail then
      begin
        if ofs_first = nil then
          begin
            new( ofs_first );
            ofs_current := ofs_first;
          end
        else
          begin
            new( ofs_current^.next );
            ofs_current := ofs_current^.next;
          end;
        ofs_current^.next := nil;
        ofs_current^.ofs := ofs;
      end
    else
      err( ERR_MEM );
  end; (* store_list_ofs *)

procedure clear_list_ofs( var ofs_first, ofs_current : list_ofs_ptr );
  begin
    while ofs_first <> nil do
      begin
        ofs_current := ofs_first^.next;
        dispose( ofs_first );
        ofs_first := ofs_current;
      end;
  end; (* clear_list_ofs *)

procedure compare_lists;
  var
    old_ofs, new_ofs : list_recnum;
  begin
    new_ofs := 0;
    old_ofs := 0;
    while (old_ofs < old_list_records) or (new_ofs < new_list_records) do
      begin
        if old_list^[old_ofs]^.name = new_list^[new_ofs]^.name then
          begin
            if old_ofs < old_list_records then inc( old_ofs );
            if new_ofs < new_list_records then inc( new_ofs );
          end
        else if ((old_list^[old_ofs]^.name < new_list^[new_ofs]^.name) and (old_ofs < old_list_records))
             or (new_ofs >= new_list_records) then
          begin
            if opt_verbose then writeln( 'Removed area : ', old_list^[old_ofs]^.name );
            store_list_ofs( old_ofs_first, old_ofs_current, old_ofs );
            if old_ofs < old_list_records then inc( old_ofs )
            else inc( new_ofs );
          end
        else
          begin
            if opt_verbose then writeln( 'Added area   : ', new_list^[new_ofs]^.name );
            store_list_ofs( new_ofs_first, new_ofs_current, new_ofs );
            if new_ofs < new_list_records then inc( new_ofs )
            else inc( old_ofs );
          end;
      end;
  end; (* compare_lists *)

procedure write_differences;
  var
    output : text;
  begin
    if (new_ofs_first = nil) and (old_ofs_first = nil) then
      writeln( 'No changes' )
    else
      begin
        writeln( 'Writing ', report_filename );
        assign( output, report_filename );
        {$I-}
        rewrite( output );
				{$I+}
        writeln( output, #1, 'PID: EchoUtil ', hi( PROG_VERSION ), '.', lo( PROG_VERSION ) );
        if ioresult = 0 then
          begin
            if new_ofs_first <> nil then
              begin
                writeln( output, '--[ Nya konferenser ]-------------------------------------------------' );
                writeln( output );
                new_ofs_current := new_ofs_first;
                while new_ofs_current <> nil do
                  begin
                    with new_list^[ new_ofs_current^.ofs ]^ do
                      writeln( output, fix_len( name, 20 ), ' ', desc );
                    new_ofs_current := new_ofs_current^.next;
                  end;
                writeln( output );
                writeln( output );
               end;
            if old_ofs_first <> nil then
              begin
                writeln( output, '--[ Nedlagda konferenser ]--------------------------------------------' );
                writeln( output );
                old_ofs_current := old_ofs_first;
                while old_ofs_current <> nil do
                  begin
                    with old_list^[ old_ofs_current^.ofs ]^ do
                      writeln( output, fix_len( name, 20 ), ' ', desc );
                    old_ofs_current := old_ofs_current^.next;
                  end;
                writeln( output );
                writeln( output );
              end;
            writeln( output, '--- EchoUtil ', hi( PROG_VERSION ), '.', lo( PROG_VERSION ));
            writeln( output, ' * Origin: Ljuslyktan BBS (2:205/340)' );
            close( output );
          end
        else
          err( ERR_IO );
      end;
  end; (* write_differences *)

procedure clear_list( var list : list_ptr; var recs : longint );
  var
    i : longint;
  begin
    for i := recs-1 downto 0 do dispose( list^[i] );
    dispose( list );
  end;

procedure initialize;
  var
    i : byte;
    parameter : string;
  begin
    error := 0;
    opt_verbose := false;
    report_filename := '';

    old_list_filename := '';
    old_list_records := 0;
    old_ofs_first := nil;
    old_ofs_current := nil;

    new_list_filename := '';
    new_list_records := 0;
    new_ofs_first := nil;
    new_ofs_current := nil;
  

    for i := 1 to paramcount do
      begin
        parameter := paramstr( i );
        if (length( parameter ) >= 2) and (parameter[1] in ['-','/']) then
          begin
            case upcase( parameter[2] ) of
              'N' : new_list_filename := fexpand( copy( parameter, 3, length( parameter ) - 2 ) );
              'O' : old_list_filename := fexpand( copy( parameter, 3, length( parameter ) - 2 ) );
              'R' : report_filename := fexpand( copy( parameter, 3, length( parameter ) - 2 ) );
              'V' : opt_verbose := true;
              else err( ERR_PARAM );
              end;
          end
        else
          err( ERR_PARAM );
      end;

    if (old_list_filename = '') or (new_list_filename = '') or (report_filename = '') then err( ERR_PARAM );
  end; (* initialize *)
  
begin
	writeln( 'ECHOUTIL v', hi( PROG_VERSION ), '.', lo( PROG_VERSION ) {$IFDEF MSDOS} ,' (MS-DOS)' {$ENDIF} );
  writeln;

  mem_free := memavail;

  initialize;

	if opt_verbose and (error = 0) then
    begin
      writeln( 'Free memory : ', mem_free, ' bytes' );
      writeln;
    end;

  (* Allokera minne *)


  if 2*sizeof( list_array ) < maxavail then
    begin
      new( old_list );
      new( new_list );
    end
  else
    err( ERR_MEM );

  (* L�s in listor *)

  if error = 0 then
    begin
      writeln( 'Reading ', old_list_filename );
      read_list( old_list, old_list_filename, old_list_records );
      sort_list( old_list, 0, old_list_records - 1 );
    end;

  if error = 0 then
    begin
      writeln( 'Reading ', new_list_filename );
      read_list( new_list, new_list_filename, new_list_records );
      sort_list( new_list, 0, new_list_records - 1 );
    end;

  if error = 0 then
    begin
      if opt_verbose then writeln( 'Comparing lists' );
      compare_lists;
    end;

  if error = 0 then
    begin
      write_differences;
    end;

  (* Frig�r minne *)

  clear_list_ofs( old_ofs_first, old_ofs_current );
  clear_list_ofs( new_ofs_first, old_ofs_current );
  clear_list( new_list, new_list_records );
  clear_list( old_list, old_list_records );
  if mem_free <> memavail then err( err_mem );
end.
