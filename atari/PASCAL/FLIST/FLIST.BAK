program flist;

uses dos;

type
  header_type =   record
                    version_string  : array [1..32] of byte;
                    rev_number      : word;
                    data_type       : word;
                    header_size     : word;
                    creation_date   : longint;
                    last_modified   : longint;
                    total_records   : word;
                    record_size     : word;
                  end;

  nodenum_type =  record
                    zone        : word;
                    net         : word;
                    node        : word;
                    point       : word;
                  end;

  raw_echo_type = record
                    signature       : word;
                    writelevel      : word;
                    {$IFDEF HSpascal}
                    area_name       : array [1..50] of byte;
                    _junk1          : byte;
                    {$ELSE}
                    area_name       : array [1..51] of byte;
                    {$ENDIF}
                    comment         : array [1..51] of byte;
                    options         : word; (* Bitmask *)
                    RA_board_number : word;
                    msgbase_type    : byte;
                    msgbase_path    : array [1..61] of byte;
                    board           : word;
                    origin_line     : array [1..59] of byte;
                    address         : word;
                    group           : longint;
                    _also_seen_by   : word;
                    msgs            : word;
                    days            : word;
                    days_rcvd       : word;
                    export          : array [1..64] of nodenum_type;
                    RA_rd_sec       : word;
                    RA_rd_flags     : array [1..4] of byte;
                    RA_rd_notflags  : array [1..4] of byte;
                    RA_wr_sec       : word;
                    RA_wr_flags     : array [1..4] of byte;
                    RA_wr_notflags  : array [1..4] of byte;
                    RA_sys_sec      : word;
                    RA_sys_flags    : array [1..4] of byte;
                    RA_sys_notflags : array [1..4] of byte;
                    QBBS_tmplat_sec : word;
                    QBBS_tmplat_flg : array [1..4] of byte;
                    _internal_use   : byte;
                    RA_netreply_brd : word;
                    RA_boardtype    : byte;
                    RA_attr         : byte;
                    RA_attr2        : byte;
                    RA_group        : word;
                    RA_altgroup     : array [1..3] of word;
                    RA_msgkinds     : byte;
                    qwk_name        : array [1..13] of byte;
                    SBBS_min_age    : word;
                    SBBS_attr       : word;
                    SBBS_replystat  : byte;
                    QBBS_groups     : byte;
                    QBBS_aliases    : byte;
                    last_toss_date  : longint;
                    last_scan_date  : longint;
                    also_seen_by    : longint;
                    stat            : word; (* bitmask *)
                    _reserved       : array [1..180] of byte;
                  end;

  {$IFDEF HSPASCAL}
  byte_array_type = array [0..254] of byte;
  {$ENDIF}

const
  program_version = 91;


var
  {$IFDEF HSPascal}
  areafile        : file of byte;
  {$ELSE}
  areafile        : file;
  {$ENDIF}
  bytesread       : word;
  header          : header_type;
  area            : raw_echo_type;
  area_file_pos   : word;
  list_file       : text;

  group_mask      : longint;
  fmail_path      : pathstr;
  flist_path      : dirstr;
  output_filename : pathstr;
  format_filename : pathstr;

  format          : array[1..1024] of char;
  format_size     : integer;
  format_pos      : word;
  format_tmp      : word;
  format_len      : word;

  errcode         : integer;

  verbose         : boolean;

{$IFDEF HSPascal}
procedure swapint( var int : word );
  begin
    int := ((int and $00FF) shl 8) or ((int and $FF00) shr 8);
  end;
{$ENDIF}

function asciiz_to_pascal( asciiz_string : pointer ) : string;

  var
    {$IFDEF HSPascal}
    ch : ^byte_array_type;
    {$ELSE}
    ch : ^char;
    {$ENDIF}
    s : string;
    i : byte;

  begin
    ch := asciiz_string;
    s := '';
    
    {$IFDEF HSPascal}
    i := 0;
    
    while ch^[i] <> 0 do
      begin
        s := s + chr( ch^[i] );
        inc( i );
      end;

    {$ELSE}

    while ch^ <> #0 do
      begin
        s := s + ch^;
        inc( ch );
      end;

    {$ENDIF}

    asciiz_to_pascal := s;
  end;

function upcasestr( s : string ) : string;
  var
    i : byte;
  begin
    for i := 1 to length( s ) do s[i] := upcase( s[i] );
    upcasestr := s;
  end;

function strlong( value : longint  ) : string;
  var
    s : string;
  begin
    str( value, s );
    strlong := s;
  end;

function groups( mask : longint ) : string;
  var
    s : string;
    i : byte;
  begin
    s := '';
    for i := 1 to 26 do
      begin
        if mask and 1 = 1 then s := s + chr( 64+i );
        mask := mask shr 1;
      end;
    groups := s;
  end;


function option( mask : word; nr : byte ) : string;
  begin
    mask := mask shr nr;
    if mask and 1 = 1 then option := 'Yes'
    else option := 'No';
  end;


function get_number( pos1, pos2 : word ) : byte;
  var
    value : byte;
    multiplyer : word;
    pos : word;
  begin
    value := 0;
    multiplyer := 1;
    for pos := pos2 downto pos1 do
      begin
        value := (ord( format[pos] ) - ord( '0' )) * multiplyer;
        multiplyer := multiplyer * 10;
      end;
    get_number := value;
  end;

function fix_len( s : string; len : byte ) : string;
  begin
    if len <> 0 then
      begin
        if length( s ) > len then s := copy( s, 1, len )
        else while length( s ) < len do s := s + ' ';
      end;
    fix_len := s;
  end;

procedure initialize;

  var
    i, j        : word;
    parameter   : string;
    format_file : file;
    dir         : dirstr;
    name        : namestr;
    ext         : extstr;

  begin
    errcode := 0;
    group_mask := 0;
    fmail_path := '';
    output_filename := '';
    format_filename := '';
    verbose := false;

    fsplit( upcasestr( paramstr( 0 ) ), dir, name, ext );
    flist_path := dir;

    for i := 1 to paramcount do
      begin
        parameter := upcasestr( paramstr( i ) );
        if parameter[1] in ['-','/'] then
          begin
            case parameter[2] of
              'P' : fmail_path := copy( parameter, 3, length( parameter ) - 2 );
              'O' : output_filename := copy( parameter, 3, length( parameter ) - 2 );
              'F' : format_filename := copy( parameter, 3, length( parameter ) - 2 );
              'V' : verbose := TRUE;
              end;
          end
        else
          for j := 1 to length( parameter ) do
            group_mask := group_mask OR  ( 1 shl ( ord( parameter[j] ) - ord( 'A' ) ) );
      end;

    if group_mask = 0 then group_mask := $FFFFFFFF;

    if length( fmail_path ) = 0 then fmail_path := getenv( 'FMAIL' );

    if length( fmail_path ) > 0 then
      begin
        if fmail_path[ length( fmail_path ) ] <> '\' then fmail_path := fmail_path + '\';
        fmail_path := fexpand( fmail_path );
      end
    else
      fmail_path := '.\';

    if length( output_filename ) = 0 then
      output_filename := 'FLIST.LST';

    if length( format_filename ) = 0 then format_filename := 'FLIST.FMT';
    fsplit( format_filename, dir, name, ext );
    format_filename := name + ext;

    writeln( 'Groups      : ', groups( group_mask ) );
    writeln( 'Format file : ', format_filename );
    writeln( 'List file   : ', output_filename );
    writeln( 'Fmail path  : ', fmail_path );
    writeln;

    (* Search for format file in current directory *)

    assign( format_file, format_filename );

    {$I-}

    {$IFDEF HSPascal}
    reset( format_file );
    {$ELSE}
    reset( format_file, 1 );
    {$ENDIF}

    {$I+}

    if ioresult <> 0 then
      begin

        (* Search for format file in FLIST directory *)

        assign( format_file, flist_path + format_filename );

        {$I-}

        {$IFDEF HSPascal}
        reset( format_file );
        {$ELSE}
        reset( format_file, 1 }
        {$ENDIF}

        {$I+}

        if ioresult <> 0 then
          begin

            (* Search for format file i FMAIL directory *)

            assign( format_file, fmail_path + format_filename );

            {$I-}

            {$IFDEF HSPascal}
            reset( format_file );
            {$ELSE}
            reset( format_file, 1 }
            {$ENDIF}

            {$I+}

            if ioresult <> 0 then
              begin
                writeln( format_filename, ' not found!' );
                errcode := 2;
              end;

          end;
      end;


    if errcode = 0 then
      begin
        write( 'Reading format file ', format_filename );

        blockread( format_file, format, 1024, format_size );
        close( format_file );

        writeln( ' (', format_size, ' bytes)' );
      end;
  end;


begin
  writeln( 'FLIST v', hi( program_version ), '.', lo( program_version ), ': Create area listings from Fmail Area File' );
  writeln( 'This program is FREEWARE. Use it on your own risk!' );
  writeln;

  initialize;

  if errcode = 0 then
    begin
      assign( areafile, fmail_path+'FMAIL.AR' );
      {$I-}

      {$IFDEF HSPascal}
      reset( areafile );
      {$ELSE}
      reset( areafile, 1 );
      {$ENDIF}

      {$I+}
      if ioresult <> 0 then
        begin
          writeln( fmail_path, 'FMAIL.AR not found!' );
          errcode := 2;
        end;
    end;

  if errcode = 0 then
    begin
      assign( list_file, output_filename );
      {$I-}
      rewrite( list_file );
      {$I+}
      if ioresult <> 0 then
        begin
          close( areafile );
          writeln( 'Unable to create ', output_filename );
          errcode := 3;
        end;
    end;

  if errcode = 0 then
    begin
      write( 'Processing FMAIL.AR' );

      blockread( areafile, header, sizeof( header_type ) );

      {$IFDEF HSPascal}
      with header do
        begin
          swapint( rev_number );
          swapint( data_type );
          swapint( header_size );
          swapint( total_records );
          swapint( record_size );
        end;
      {$ENDIF}
      
      if verbose then
        with header do
          begin
            writeln;
            writeln( '  Identification : ', asciiz_to_pascal( addr( version_string ) ) );
            writeln( '  Revision       : ', rev_number );
            writeln( '  Data type      : ', data_type );
            writeln( '  Header size    : ', header_size );
            writeln( '  Total records  : ', total_records );
            writeln( '  Record size    : ', record_size );
            writeln;
          end
      else
        writeln( ' (', header.total_records, ' records)' );

      if header.record_size <> sizeof( raw_echo_type ) then
        begin
          writeln( 'Invalid record size; internal structure is ', sizeof( raw_echo_type ), ' bytes' );
          close( areafile );
          errcode := 4;
        end;
    end;

  if errcode = 0 then
    begin
      for area_file_pos := 1 to header.total_records do
        begin
          blockread( areafile, area, sizeof( area ) );
          with area do
            begin
              if group AND group_mask <> 0 then
                for format_pos := 1 to format_size do
                  case format[format_pos] of
                    '%' : begin
                            inc( format_pos );

                            if format[format_pos] in ['0'..'9'] then
                              begin
                                format_tmp := format_pos;
                                while format[format_pos] in ['0'..'9'] do inc( format_pos );
                                format_len := get_number( format_tmp, format_pos-1 );
                              end
                            else
                              format_len := 0;

                            case format[format_pos] of
                              'b' : write( list_file, fix_len( strlong( board ), format_len ) );
                              'B' : write( list_file, fix_len( strlong( RA_board_number ), format_len ) );
                              'c' : write( list_file, fix_len( asciiz_to_pascal( addr( comment ) ), format_len ) );
                              'g' : write( list_file, fix_len( groups( group ), format_len ) );
                              'n' : write( list_file, fix_len( asciiz_to_pascal( addr( area_name ) ), format_len ) );
                              'O' : write( list_file, fix_len( asciiz_to_pascal( addr( origin_line ) ), format_len ) );
                              'p' : write( list_file, fix_len( asciiz_to_pascal( addr( msgbase_path ) ), format_len ) );
                              't' : write( list_file, fix_len( strlong( msgbase_type ), format_len ) );
                              'w' : write( list_file, fix_len( strlong( writelevel ), format_len ) );
                              's' : write( list_file, fix_len( chr(lo(signature))+chr(hi(signature)), format_len ) );
                              'o' : begin
                                      inc( format_pos );
                                      case format[format_pos] of
                                        'a' : write( list_file, fix_len( option( options, 0 ), format_len ) );
                                        'c' : write( list_file, fix_len( option( options, 6 ), format_len ) );
                                        'd' : write( list_file, fix_len( option( options, 14 ), format_len ) );
                                        'i' : write( list_file, fix_len( option( options, 5 ), format_len ) );
                                        'l' : write( list_file, fix_len( option( options, 8 ), format_len ) );
                                        'p' : write( list_file, fix_len( option( options, 4 ), format_len ) );
                                        't' : write( list_file, fix_len( option( options, 1 ), format_len ) );
                                        'x' : write( list_file, fix_len( option( options, 11 ), format_len ) );
                                        's' : write( list_file, fix_len( option( options, 2 ), format_len ) );
                                        'S' : write( list_file, fix_len( option( options, 15 ), format_len ) );
                                        end;
                                    end;
                              end;
                          end;
                    ^Z : ;
                    else write( list_file, format[format_pos] );
                    end;
            end;
        end;
    end;

  if errcode = 0 then
    begin
      close( areafile );
      close( list_file );
      writeln;
      writeln( output_filename, ' created' );
    end
  else
    writeln( 'Exiting with errorlevel ', errcode );

  halt( errcode );
end.
