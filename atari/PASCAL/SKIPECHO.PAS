program skipecho;

(************************************************************************)
(* Det h�r programmet l�ser in uplink-listor f�r echomail och plockar   *)
(* bort valda m�ten. Fr�msta syftet �r att anv�ndas p� KristNets BBS:er *)
(* f�r att plocka bort icke �nskv�rda m�ten fr�n FidoNets uplink-listor *)
(*                                                                      *)
(* Skrivet av Stefan Svensson, 70:717/1.1                               *)
(************************************************************************)

uses dos;

type
  line_string = string[255];
  name_string = string[30];
  desc_string = string[100];

  skiplist_ptr    = ^skiplist_record;
  skiplist_record = record
                      name : name_string;
                      next : skiplist_ptr;
                    end;

const
  PROG_VERSION = $0100;

var
  error               : integer;  (* Intern felkod *)

  uplink_file         : text;     (* Uplink-lista *)
  uplink_filename     : string;

  skip_file           : text;     (* Lista p� m�ten som skall tas bort *)
  skip_filename       : string;

  new_uplink_file     : text;     (* Den nya, reviderade uplink-listan *)
  new_uplink_filename : string;

  skiplist_first      : skiplist_ptr;
  skiplist_current    : skiplist_ptr;

procedure split( src : line_string; var name : name_string; var desc : desc_string );

  (*
  Plockar ut det f�rsta ordet i en str�ng och returnerar detta i variabeln
  "name". Resten av str�ngen returneras i "desc".
  *)
  
  var
    i, j, src_len : byte;
  begin
    i := 1;
    src_len := length( src );
    while (src[i] in [' ', #9]) and (i <= src_len) do inc( i );
    j := i;
    while not (src[j] in [' ', #9]) and (j <= src_len) do inc( j );
    name := copy( src, i, j-i );
    desc := copy( src, j+1, src_len - j );
  end; (* split *)
  
procedure test;
  var s : string;
    name, desc : string;
  begin
    readln( s );
    split( s, name, desc );
    writeln( '"', name, '" "', desc, '"' );
  end;

procedure read_skiplist;
  var
    textline  : line_string;
    name      : name_string;
    desc      : desc_string;
  begin
    assign( skip_file, skip_filename );
    {$I-}
    reset( skip_file );
    {$I+}
    if ioresult = 0 then
      begin
        while not eof( skip_file ) do
          begin
            readln( skip_file, textline );
            split( textline, name, desc );
            if skiplist_first = nil then
              begin
                new( skiplist_first );
                skiplist_current := skiplist_first;
              end
            else
              begin
                new( skiplist_current^.next );
                skiplist_current := skiplist_current^.next;
              end;
            skiplist_current^.next := nil;
            skiplist_current^.name := name;
          end;
        close( skip_file );
      end
    else
      error := 2;
  end; (* readskip_list *)

procedure clear_skiplist;
  begin
    while skiplist_first <> nil do
      begin
        skiplist_current := skiplist_first;
        skiplist_first := skiplist_first^.next;
        dispose( skiplist_current );
      end;
  end; (* clear_skiplist *)

function skip_name( name : name_string ) : boolean;
  var
    skip_it : boolean;
  begin
    skip_it := false;
    skiplist_current := skiplist_first;
    while (skiplist_current <> nil) and not skip_it do
      begin
        if skiplist_current^.name = name then skip_it := true;
        skiplist_current := skiplist_current^.next;
      end;
    skip_name := skip_it;
  end; (* skip_name *)

procedure process_uplink_file;
  var
    textline  : line_string;
    name      : name_string;
    desc      : desc_string;
  begin
    while not eof( uplink_file ) do
      begin
        readln( uplink_file, textline );
        split( textline, name, desc );
        if skip_name( name ) then
          writeln( '  - skipping ', name )
        else
          writeln( new_uplink_file, textline );
      end;
  end; (* process_uplink_file *)

procedure initialize;
  var 
    i             : byte;
    parameter     : string;

  begin
    error := 0;
    uplink_filename := '';
    new_uplink_filename := '';
    skip_filename := '';
    skiplist_first := nil;
    skiplist_current := nil;

    for i := 1 to paramcount do
      begin
        parameter := paramstr( i );
        case parameter[1] of
          '-','/' : case upcase( parameter[2] ) of
                      'S' : skip_filename := fexpand( copy( parameter, 3, length( parameter ) - 2 ) );
                      else error := 3;
                      end;
          else      begin
                      if uplink_filename = '' then
                        uplink_filename := fexpand( parameter )
                      else if new_uplink_filename = '' then
                        new_uplink_filename := fexpand( parameter )
                      else
                        error := 3;
                    end;
          end;
      end;

    if (uplink_filename = '') or (new_uplink_filename = '') then error := 3;

    if error = 3 then
      begin
        writeln( 'usage: skipecho <source file> <destination file> [-S<skip list file>]' );
      end;
    if error = 0 then
      begin
        if skip_filename = '' then skip_filename := fexpand( 'skipecho.lst' );
      end;
  end;

begin
  writeln( 'SKIPLIST v', hi( PROG_VERSION ), '.', lo( PROG_VERSION ) );
  writeln;

  initialize;

  if error = 0 then 
    begin
      writeln( 'Reading ', skip_filename );
      read_skiplist;
    end;

  if error = 0 then
    begin
      assign( uplink_file, uplink_filename );
      {$I-}
      reset( uplink_file );
      {$I+}
      if ioresult <> 0 then
        begin
          writeln( 'Can''t open source file' );
          error := 2;
        end;
    end;

  if error = 0 then
    begin
      assign( new_uplink_file, new_uplink_filename );
      {$I-}
      rewrite( new_uplink_file );
      {$I+}
      if ioresult <> 0 then
        begin
          writeln( 'Can''t open destination file' );
          error := 2;
        end;
    end;

  if error = 0 then
    begin
      writeln( 'Processing ', uplink_filename );
      process_uplink_file;
      writeln( 'Changes written to ', new_uplink_filename );
      close( uplink_file );
      close( new_uplink_file );
    end;

  clear_skiplist;
  
  writeln;
  if error <> 0 then writeln( 'Exiting with errorlevel ', error )
  else writeln( 'Have a nice day.' );

  halt( error );
end.
