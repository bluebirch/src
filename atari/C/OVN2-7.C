/* �vning 2.7 */

#include <stdio.h>

void main()
{
	int total_sec=0, min, sec;
	
	printf( "Ange antal sekunder: " );
	scanf( "%u", &total_sec );
	
	min = total_sec / 60;
	sec = total_sec % 60;
	
	printf( "%d sekunder �r %d minuter och %d sekunder.", total_sec, min, sec );
}

	