/*	LAB 5 project 9.4, 96-02-22
	Stefan Svensson, D1b
	
	This program reads a sample text file with exam scores of students,
	calculates the average score for each student and writes the result
	to a new text file (which has the same name as the input file except
	for the extention '.avg'). The input file has the following format:
	
	FirstName LastName Exam1 Exam2 Exam3
	
	It is required that the input file strictly follows this format.
	Otherwise the program will probably crash!
	The output file name has the following format:
	
	LastName, FirstInitial <tab> ExamAverage
*/	
	
#include <stdio.h>
#include <string.h>

void convert_and_print( char *, FILE * );
void get_a_nice_output_filename( char *, char * );

void main( void )
{
	char str[255];
	char infilename[25], outfilename[25];
	FILE *infile;
	FILE *outfile;
	
	puts( "LAB 9.5.3 - Professor Diskus' average counter / Stefan Svensson, D1b, 960222" );
	printf( "\nEnter input filename: " );
	gets( infilename );
	get_a_nice_output_filename( outfilename, infilename );
	
	if ((infile = fopen( infilename, "r" )) == NULL)
	{
		puts( "Could not open input file!" );
		return;
	}
	if ((outfile = fopen( outfilename, "w" )) == NULL)
	{
		puts( "Could not open output file!" );
		fclose( infile );
		return;
	};
	
	printf( "Skriver till %s...", outfilename );

	while (!feof( infile ) && (fgets( str, 255, infile ) != NULL))
	{
		convert_and_print( str, outfile );
	}
	
	fclose( infile );
	fclose( outfile );
	
	printf( " Klart!\n" );
}

/*	get_a_nice_output_filename
	This function creates a new file name from the input file name by
	replacing (or adding) the extention (with) '.avg'.
*/

void get_a_nice_output_filename( char *newfilename, char *filename )
{
	int i=0;
	long size;
	
	size=strlen( filename );
	while ((i < size) && (filename[i] != '.')) i++;
	strncpy( newfilename, filename, i );
	newfilename[i]='\0';
	strcat( newfilename, ".avg" );
}

/*	isalphanum
	This function returns true (1) if the character is a valid letter.
	NOTE: This function knows nothing about languages other than English.
*/

int isalphanum( char ch )
{
	if (((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z')) || ((ch >= '0') && (ch <= '9')))
		return 1;
	else
		return 0;
}

/*	getword
	Searches the first word of the string src and copies it to dest.
	The function returns a pointer to the first character after the
	found word.
*/

char *getword( char *dest, char *src )
{
	long len;
	int n;
	
	n=0;	
	while (!isalphanum( *src )) src++;
	len = strlen( src );
	while ((isalphanum( src[n] )) && (n < len)) (n)++;
	strncpy( dest, src, n );
	dest[n]='\0';
	return (src+n);
}
	
/*	convert_and_print
	This function converts the input string (described above), converts
	it and writes it to the output file.
*/

void convert_and_print( char *src, FILE *output )
{
	int i;
	char word[5][25];
	long sum=0;
	float average;
	char *ptr;
	
	for( i=0; i<5; i++ )
	{
		src = getword( word[i], src );
	}
	for( i=2; i<5; i++ )
	{
		sum += strtol( word[i], &ptr, 0 );
	}
	average = (float) sum / 3;
	
	fprintf( output, "%s, %c\t%.1f\n", word[1], word[0][0], average );
}

/* LAB5-9-3.C end */
