/* vim:ai ts=8 sw=4
 *
 * $Id$
 *
 * tn-gw-nav:
 *
 * Navigate the TIS gauntlet telnet gateway (or any telnet gateway based on
 * the TIS firewall toolkit) to establish an SSH session tunneling through
 * the telnet gateway.
 *
 * This code is hereby granted to the public domain.
 *
 * John Saunders <john.saunders@nlc.net.au>
 * Charlie Brady <charlie.brady@nlc.net.au>
 *
 * Proxy password support added by Stefan Svensson <stefan@kristnet.org>
 *
 */

#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#ifndef INADDR_NONE
# define INADDR_NONE -1
#endif

/* Define these only if you need them */
#ifdef NEED_UCHAR_TYPE
typedef unsigned char	uchar_t;
#endif
#ifdef NEED_ULONG_TYPE
typedef unsigned long	ulong_t;
#endif

/* Wait for the "Connected" string from the gateway, otherwise we */
/* wait for the SSH version string which is more robust. Some */
/* versions of tn-gw don't send a "Connected" so it's undefined */
/* by default to work with the widest range of telnet gateways. */
#undef WAIT_FOR_CONNECTED_STRING

/* Define this to enable the keepalive socket option. Hopefully this */
/* will prevent dead connections from remaining active forever. */
#define USE_KEEPALIVE

/* Function prototypes */
int main(int argc, char **argv);
static void usage(char *progname);
static int do_read_write(int from, int to, int add_escapes, int remove_escapes);
static int do_write(uchar_t *buffer, int size, int to);
static void wait_for(char *pattern, int from);
static void sig_catch(int sig);
static void connect_to_server(char *host, char *port);
static ulong_t getipaddress(char *host);
static short getportnum(char *portnum);
static void add_iac_escape(uchar_t *from, uchar_t *to, int *sizep);
static void strip_iac_escape(uchar_t *from, uchar_t *to, int *sizep);
static void safe_strncpy(char *to, char *from, int len);

/* Global variables */
static int sock_fd;
static int escape_in_mode = 0;
static int escape_out_mode = 0;
static int visible_chat = 0;
static int gateway_mode = 0;
static int proxy_with_password = 0;

/* Main function of course */
int main(int argc, char **argv)
{
    int opt;
    char gw_host[256];
    char gw_port[256];
    char gw_prompt[256];
    char host[256];
    char port[256];
    fd_set readfds;
    int max_fd;
    int selret;
    char command[1024];

    char gw_user[256];
    char gw_pass[256];
    char gw_userprompt[256];
    char gw_passprompt[256];

    /* Process the command line */
    gw_host[0] = gw_port[0] = gw_prompt[0] = host[0] = port[0] = '\0';
    gw_user[0] = gw_pass[0] = gw_userprompt[0] = gw_passprompt[0] = '\0';
    while ((opt = getopt(argc, argv, "coih:p:P:l:s:L:S:")) != EOF)
    {
	switch (opt)
	{
	case 'c':
	    visible_chat = 1;
	    break;
	case 'i':
	    escape_in_mode = 1;
	    break;
	case 'o':
	    escape_out_mode = 1;
	    break;
	case 'h':
	    gateway_mode = 1;
	    safe_strncpy(gw_host, optarg, sizeof(gw_host));
	    safe_strncpy(gw_port, "23", sizeof(gw_port));
	    break;
	case 'p':
	    safe_strncpy(gw_port, optarg, sizeof(gw_port));
	    break;
	case 'P':
	    safe_strncpy(gw_prompt, optarg, sizeof(gw_prompt));
	    break;
	case 'l':
  	    safe_strncpy(gw_user, optarg, sizeof(gw_user));
	    proxy_with_password = 1;
	    break;
	case 's':
	    safe_strncpy(gw_pass, optarg, sizeof(gw_pass));
	    break;
	case 'L':
  	    safe_strncpy(gw_userprompt, optarg, sizeof(gw_userprompt));
	    break;
	case 'S':
	    safe_strncpy(gw_passprompt, optarg, sizeof(gw_passprompt));
  	    break;
	default:
	    usage(argv[0]);
	    exit(1);
	}
    }
    for (; optind < argc; optind++)
    {
	if (host[0] == '\0')
	{
	    safe_strncpy(host, argv[optind], sizeof(host));
	}
	else if (port[0] == '\0')
	{
	    safe_strncpy(port, argv[optind], sizeof(port));
	}
    }

    /* Validate arguments */
    if ((host[0] == '\0') || (port[0] == '\0'))
    {
	usage(argv[0]);
	exit(1);
    }

    /* Catch signals */
    signal(SIGALRM, SIG_IGN);
    signal(SIGPIPE, sig_catch);
    signal(SIGCHLD, sig_catch);

    /* Determine which mode to use */
    if (gateway_mode)
    {
	/* Supply a default telnet gateway prompt */
	if (gw_prompt[0] == '\0')
	{
	    safe_strncpy(gw_prompt, "tn-gw-> ", sizeof(gw_prompt));
	}

	/* Connect to the gateway host */
	connect_to_server(gw_host, gw_port);

	/* Login sequence */
	if (proxy_with_password) 
	{
	  /* Supply a default login prompt */
	  if (gw_userprompt[0] == '\0')
	  {
	    safe_strncpy(gw_userprompt, "Username: ", sizeof(gw_userprompt));
	  }

	  /* Supply a default password prompt */
	  if (gw_passprompt[0] == '\0')
	  {
	    safe_strncpy(gw_passprompt, "Password: ", sizeof(gw_passprompt));
	  }

	  wait_for( gw_userprompt, sock_fd );
	  sprintf( command, "%s\n", gw_user );
	  write( sock_fd, command, strlen( command ));
	  wait_for( gw_passprompt, sock_fd );
	  sprintf( command, "%s\n", gw_pass );
	  write( sock_fd, command, strlen( command ));
	}

	/* Wait here for the telnet gateway prompt */
	wait_for(gw_prompt, sock_fd);

	/* Send the command to cause the gateway to connect */
	sprintf(command, "connect %s %s\n", host, port);
	write(sock_fd, command, strlen(command));

#ifdef WAIT_FOR_CONNECTED_STRING
	/* Wait here for the msg 'Connected to %h.\r\n'. */
	sprintf(command, "Connected to %s.\r\n", argv[2]);
	wait_for(command, sock_fd);
#else
	wait_for("SSH-", sock_fd);
	write(STDOUT_FILENO, "SSH-", 4);
#endif
    }
    else
    {
#ifdef USE_KEEPALIVE
	/* Enable keepalive processing */
	{
	    int on = 1;
	    setsockopt(STDIN_FILENO, SOL_SOCKET, SO_KEEPALIVE, 
		       (const char *)&on, sizeof(on));
	}
#endif

	/* Connect to the host */
	connect_to_server(host, port);
    }

    /* Start the select processing loop */
    max_fd = sock_fd + 1;
    for (;;)
    {
	FD_ZERO(&readfds);
	FD_SET(STDIN_FILENO, &readfds);
	FD_SET(sock_fd, &readfds);

	/* Wait for read activity, handle interrupt system call */
	do
	{
	    selret = select(max_fd, &readfds, NULL, NULL, NULL);
	    if (selret < 0 && errno != EINTR)
	    {
	    	perror("select");
		exit(1);
	    }
	} while (selret <= 0);

	/* Process read from stdin */
	if (FD_ISSET(STDIN_FILENO, &readfds))
	{
	    if (do_read_write(STDIN_FILENO, sock_fd,
		gateway_mode && escape_in_mode,
		!gateway_mode && escape_out_mode) < 0)
	    {
		break;
	    }
	}

	/* Process read from the gateway */
	if (FD_ISSET(sock_fd, &readfds))
	{
	    if (do_read_write(sock_fd, STDOUT_FILENO,
		!gateway_mode && escape_in_mode,
		gateway_mode && escape_out_mode) < 0)
	    {
		break;
	    }
	}
    }

    return (0);
}

/* Display command line usage info */
static void usage(char *progname)
{
    fprintf(stderr, "usage: %s [-c] [-i] [-o] [-h gw-host [-p gw-port]]\n"
                    "       [-l gw-user [-s gw-password]] [-P gw-prompt]\n"
                    "       [-L prompt] [-S prompt] host port\n\n", progname);
    fprintf(stderr, "-c              Display chat activity.\n");
    fprintf(stderr, "-i              Escape telnet characters going into the "
    		    "network.\n");
    fprintf(stderr, "-o              Unescape telnet characters coming out of "
    		    "network.\n");
    fprintf(stderr, "-h gw-host      Specify client mode, and host to "
    		    "connect to.\n");
    fprintf(stderr, "-p gw-port      Use a port other than the telnet port\n");
    fprintf(stderr, "-l gw-user      Login into telnet proxy with username "
	            "\"gw-user\"\n");
    fprintf(stderr, "-s gw-password  Login into telnet proxy with password "
	            "\"gw-password\"\n");
    fprintf(stderr, "-P gw-prompt    Expect a prompt other than \"tn-gw-> "
	            "\"\n");
    fprintf(stderr, "-L prompt       Expect a login prompt other than "
                    "\"Username: \"\n");
    fprintf(stderr, "-S prompt       Expect a password prompt other than "
                    "\"Password: \"\n");
}

/* Read from "from", escape nasty characters in the buffer */
int do_read_write(int from, int to, int add_escapes, int remove_escapes)
{
    int size;
    uchar_t input_buffer[BUFSIZ];
    uchar_t processed_buffer[BUFSIZ * 2];

    /* Read some data */
    if ((size = read(from, input_buffer, BUFSIZ)) < 0)
    {
	perror("read");
	return -1;
    }
    if (size == 0)		/* end-of-file condition */
    {
	return -1;
    }

    /* Escape or un-escape the data */
    if (add_escapes)
    {
	add_iac_escape(input_buffer, processed_buffer, &size);
    }
    else if (remove_escapes)
    {
	strip_iac_escape(input_buffer, processed_buffer, &size);
    }
    else
    {
	return do_write(input_buffer, size, to);
    }
    return do_write(processed_buffer, size, to);
}

/* Write the buffer, in successive pieces if necessary */
int do_write(uchar_t *buffer, int size, int to)
{
    int written;

    /* Loop until all data is written */
    while (size > 0)
    {
	written = write(to, buffer, size);
	if (written < 0)
	{
	    /* this should not happen */
	    perror("write");
	    return -1;
	}
	size -= written;
    }
    return 1;
}

/* Wait for the specified string */
void wait_for(char *pattern, int from)
{
    char c;
    int i;
    int col;

    if (visible_chat)
    {
	/* Display the string we are waiting for */
	fprintf(stderr, "Waiting for : \"");
	for (col = i = 0; (c = pattern[i]) != '\0'; i++)
	{
	    if (++col > 42)
	    {
		fprintf(stderr, "\"\n              \"");
		col = 1;
	    }
	    if (isprint(c))
	    {
		fprintf(stderr, "%c", (uchar_t)c);
	    }
	    else switch (c)
	    {
	    case '\r':
		fprintf(stderr, "\\r");
		++col;
		break;
	    case '\n':
		fprintf(stderr, "\\n");
		++col;
		break;
	    default:
		fprintf(stderr, "\\x%02x", (uchar_t)c);
		col += 3;
	    }
	}
	fprintf(stderr, "\"\nGot         : \"");
    }

    /* Wait alarm */
    signal(SIGALRM, sig_catch);
    alarm(60);

    /* Process each character */
    for (col = i = 0; pattern[i] != '\0'; )
    {
	/* Get a character */
	if (read(from, &c, 1) != 1)
	{
	    fprintf(stderr, "Wait for failed\n");
	    exit(1);
	}

	if (visible_chat)
	{
	    /* Display the character received */
	    if (++col > 42)
	    {
		fprintf(stderr, "\"\n              \"");
		col = 1;
	    }
	    if (isprint(c))
	    {
		fprintf(stderr, "%c", (uchar_t)c);
	    }
	    else switch (c)
	    {
	    case '\r':
		fprintf(stderr, "\\r");
		col++;
		break;
	    case '\n':
		fprintf(stderr, "\\n");
		col++;
		break;
	    default:
		fprintf(stderr, "\\x%02x", (uchar_t)c);
		col += 3;
	    }
	    fflush(stderr);
	}

	/* Check for a patch against the expected pattern */
	if (pattern[i] == c)
	{
	    i++;
	}
	else
	{
	    i = 0;
	}
    }

    /* Cancel the alarm as it worked */
    alarm(0);
    signal(SIGALRM, SIG_IGN);

    if (visible_chat)
    {
	fprintf(stderr, "\" [Got it]\n");
    }
}

/* Catch signals */
void sig_catch(int sig)
{
    if (sig == SIGALRM)
    {
	fprintf(stderr, "\nWait for pattern timeout - exiting\n");
    }
    else
    {
	fprintf(stderr, "Signal %d caught - exiting\n", sig);
    }
    exit(1);
}

/* Establish a connect to the telnet gateway host. */
void connect_to_server(char *host, char *port)
{
    struct sockaddr_in	addr;

    /* Make a socket */
    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
	perror("socket");
	exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = getportnum(port);
    addr.sin_addr.s_addr = getipaddress(host);

    /* Connect it */
    if (connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
	perror("connect");
	exit(1);
    }

#ifdef USE_KEEPALIVE
    /* Enable keepalive processing */
    {
	int on = 1;
	if (setsockopt(sock_fd, SOL_SOCKET, SO_KEEPALIVE,
		       (const char *)&on, sizeof(on)) < 0)
	{
	    perror("setsockopt");
	    exit(1);
	}
    }
#endif
}

/* Convert a host name to an IP address. */
ulong_t getipaddress(char *host)
{
    struct hostent *hent;
    ulong_t ip;

    /* Nasty code to check for dotted decimal IP addresses */
    if (((ip = inet_addr(host)) == INADDR_NONE)
	&&
	(strcmp(host, "255.255.255.255") != 0))
    {
	/* Regular hostname was specified */
	if ((hent = gethostbyname(host)) == NULL)
	{
	    fprintf(stderr, "%s: Hostname not found\n", host);
	    exit(1);
	}
	memcpy(&ip, hent->h_addr, sizeof(ip));
	endhostent();
    }

    return (ip);
}

/* Return the port number, in network order, of the specified service  */
static short getportnum(char *portnum)
{
    char *digits = portnum;
    struct servent *serv;
    short port;

    /* Assume it's a numerical port to start with */
    for (port = 0; isdigit(*digits); ++digits)
    {
	port = (port * 10) + (*digits - '0');
    }

    /* If there are chars left over, or the number is bad, then lookup */
    if ((*digits != '\0') || (port <= 0))
    {
	if ((serv = getservbyname(portnum, "tcp")) == NULL)
	{
	    fprintf(stderr, "%s: Invalid port\n", portnum);
	    exit(1);
	}
	port = serv->s_port;
	endservent();
    }
    else
    {
    	port = htons(port);
    }

    return (port);
}

/* Expand NUL, IAC and DONT characters to escape sequences and adjust *sizep */
void add_iac_escape(uchar_t *from, uchar_t *to, int *sizep)
{
    int countdown;		/* counter */

    for (countdown = *sizep; countdown; countdown--, from++)
    {
	switch (*from)
	{
	case '\0':
	    *to++ = DONT;
	    *to++ = 'a';
	    (*sizep)++;
	    break;
	case IAC:
	    *to++ = DONT;
	    *to++ = 'b';
	    (*sizep)++;
	    break;
	case DONT:
	    *to++ = DONT;
	    *to++ = 'c';
	    (*sizep)++;
	    break;
	default:
	    *to++ = *from;
	    break;
	}
    }
}

/* Strip escape sequences from buffer and adjust *sizep */
void strip_iac_escape(uchar_t *from, uchar_t *to, int *sizep)
{
    static int state;		/* static garantees 0 initial value */
    int countdown;		/* counter */

    for (countdown = *sizep; countdown; countdown--, from++)
    {
	if (state == 0)
	{
	    if (*from == DONT)
	    {
	    	(*sizep)--;
		state = 1;
	    }
	    else
	    {
		*to++ = *from;
	    }
	}
	else /* state == 1 */
	{
	    switch (*from)
	    {
		case 'a':
		    *to++ = '\0';
		    break;
		case 'b':
		    *to++ = IAC;
		    break;
		case 'c':
		    *to++ = DONT;
		    break;
		default:
		    *to++ = *from;
		    break;
	    }
	    state = 0;
	}
    }
}

/* Do a strncpy but always 0 terminate the string */
static void safe_strncpy(char *to, char *from, int len)
{
    strncpy(to, from, len - 1);
    to[len - 1] = '\0';
}
