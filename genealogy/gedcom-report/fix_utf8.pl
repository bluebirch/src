#!/usr/bin/perl -w
use strict;

my $file = shift @ARGV || die "No GEDCOM file specified";

local $/ = undef;

open GEDCOM, "<$file" || die "Error opening $file for reading";
my $data = <GEDCOM>;
close GEDCOM;

$data =~ s/([\xc0-\xdf])(\r?\n.*?CONC\s+)(.)/$2$1$3/g;

open GEDCOM, ">$file" || die "Error opening $file for writing";
print GEDCOM $data;
close GEDCOM;

