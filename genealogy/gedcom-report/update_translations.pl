#!/bin/perl -w

use strict;

open MANIFEST, "<MANIFEST";
my @files = grep m/(^bin|\.pm$)/, <MANIFEST>;
close MANIFEST;

my %string;
foreach my $file (@files) {
    chomp $file;
    local $/ = undef;
    print "scanning $file\n";
    open FILE, "<$file";
    my $data = <FILE>;
    close FILE;
    while ($data =~ m/Ts?\(\s*"(.*?)".*?\)/gs) {
	$string{$1} = $1;
    }
}

my @translations = grep m:Locale/.*\.pm:, @files;

my %old;
foreach my $file (@translations) {
    chomp $file;
    local $/ = undef;
    open FILE, "<$file";
    my $data = <FILE>;
    close FILE;
    while ($data =~ m/^\s*"(.*?)"\s*=>\s*"(.*?)"\s*,?\s*$/mg) {
	if ($string{$1}) {
	    $string{$1} = $2;
	}
	else {
	    $old{$1} = $2;
	}
    }
    $file =~ m/([a-zA-Z_]+)\.pm/;
    my $lang = $1;
    open FILE, ">$file";
    print FILE "# -*- coding: utf-8 -*-\n";
    print FILE "# Translation file, automatically updated.\n";
    print FILE "# \$Id\$\n";
    print FILE "package Gedcom::Report::Locale::$lang;\n";
    print FILE "use strict;\n";
    print FILE "use utf8;\n";
    print FILE "\n";
    print FILE "our \%T = (\n";
    foreach my $key (sort keys %string) {
	print FILE "    \"$key\" => \"$string{$key}\",\n";
    }
    if (%old) {
	print FILE "# Obsolete:\n";
	foreach my $key (sort keys %old) {
	    print FILE "#   \"$key\" => \"$old{$key}\",\n";
	}
    }
    print FILE ");\n";
    print FILE "\n";
    print FILE "1;\n";
    close FILE;
    print "wrote $file\n";
}

