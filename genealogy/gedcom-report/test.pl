# -*- coding: utf-8 -*-
use strict;
use locale;
use utf8;
use Gedcom::Report;
use Gedcom::Report::LaTeX;
use Data::Dumper;
use open IO => ':utf8';

my $ged = new Gedcom::Report "example.ged";

# open OUT, ">test.ged";
# #binmode( OUT, ":encoding(iso-8859-1)" ) || die "binmode failed";
# print OUT $ged->as_source;
# close OUT;

local $| = 1;
print "Pattern: ";

#my $pattern = <>;
#chomp $pattern;
my $pattern = "Alice Charlotta";# unless ($pattern);

my $indi = $ged->get_individual( $pattern );

if ($indi) {
    $indi->refn( 1 );
    open TEX, ">test.tex";
    print TEX begin_document;
    print TEX maketitle( "Släktforskning", "Stefan Björk" );
    print TEX tableofcontents;
    print TEX $indi->ancestors_report( generations => 6 );
    print TEX $indi->descendants_report( generations => 6 );
    print TEX end_document;
    close TEX;
}
else {
    print "No individual found with pattern $pattern.\n";
}
