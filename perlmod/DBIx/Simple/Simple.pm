package DBIx::Simple;

use 5.006;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use DBIx::Simple ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);
our $VERSION = '0.01';


# Preloaded methods go here.

# Object constructor
sub new {
    my $this = shift;
    my $class = ref($this) || $this;
    my $self = {};
    bless $self, $class;
    $self->initialize();
    return $self;
}

# Object initialization
sub initialize {
    print STDERR "spam";
}

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

DBIx::Simple - Simplified database interface, optimized for use with
HTML::Template.

=head1 SYNOPSIS

  use DBIx::Simple;

=head1 DESCRIPTION

This module is supposed to become a simplified user interface to DBI
modules. It is especially suitable for web applications based on
HTML::Template, since all data is returned as arrays-of-hashes
suitable for template loops.

The module will keep track of some database characteristics by itself,
and will also be possible to automatically detect relations between
databases. I guess this is something that can be specified in the
database somehow, with schemas or something, but since MySQL doesn't
support that anyway, we do it the hard way round.

=head2 PUBLIC INTERFACE

=over

=item C<relation>

Manually add or change a relation that the module can not guess.

=item C<PREPARE>

Internal function. Used to expand lists of tables, fields and
conditions to a working SQL query. Don't know how to do this yet. :-)

=item C<select( table, selection )>

This is the question. Don't really know how to handle this. A single
table is easy, but how to handle several tables? It must somehow
reflect the underlying SQL query. Maybe I could select fields only,
and the module takes care of underlying relations by itself? For
example, say that I specify only 'Table1.FieldA' and 'Table2.FieldB',
then DBIx::Simple figures the rest out by itself.

=item C<update( table, selection, data )>

Update selected table. Selection is either a scalar search string or
an hash with field/value pairs. Same for data.  How to handle update
of multiple tables?

=item C<insert( table, data )>

Insert new data into table. Primary key will be automatically
detected.

=item C<add( table, data )>

Insert new field or update table. How to determine primary key fields?
This must be done automatically. How to handle multiple tables?

=item C<list( table, field, selection )>

Return a list with primary key and field from a table.

=item C<lasterror>

Return detils of the last error message.

=back

=head2 EXPORT

None by default.


=head1 AUTHOR

Stefan Svensson E<lt>stefan@raggmunk.nuE<gt>

=head1 SEE ALSO

L<perl>, L<spam>.

=cut
