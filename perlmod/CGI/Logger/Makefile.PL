use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    'NAME'		=> 'CGI::Logger',
    'VERSION_FROM'	=> 'Logger.pm',
    'PREREQ_PM'		=> { CGI => 1.0 },
);

# $Id$
# $Log$
