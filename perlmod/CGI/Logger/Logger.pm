package CGI::Logger;

# $Id$
#
# A simple module for nice debug messages under Apache.
#
# (C)Copyright 2002 Stefan Svensson <stefan@raggmunk.nu>
#
# $Log$

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( 'all' => [ qw(logmsg) ] );
our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );
our @EXPORT = qw();
our $VERSION = '0.01';


# Create prefix for the log message
sub prefix {
    my $time = scalar(localtime);
    my (undef, undef, undef, $sub) = caller(2);
    $sub = 'main' unless (defined $sub);
    return "[$time] $sub: ";
}

# Print a message to STDERR.
sub logmsg {
    my $prefix = prefix;
    print STDERR $prefix, @_, "\n";
}

1;
__END__

=head1 NAME

CGI::Logger - Perl extension for logging nicely formatted messages to
              the Apache log file (error_log).

=head1 SYNOPSIS

  use CGI::Logger qw(:all);

  logmsg "This is a log message."

=head1 DESCRIPTION

CGI::Logger prints nicely formatted messages to STDERR, which, when
running as a CGI module, is printed to the error log of the Apache web
server. Each message is prefixed with a timestamp and the calling
function, which helps in debugging.

=head2 EXPORT

None by default.

=head1 AUTHOR

Stefan Svensson <stefan@raggmunk.nu>

=head1 SEE ALSO

perl(1).

=cut
