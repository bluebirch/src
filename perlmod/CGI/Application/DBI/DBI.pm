# $Id$
#
# A package for database access through a CGI::Application and
# extensive use of HTML::Template.
#
# Stefan Svensson <stefan@raggmunk.nu>
#
# $Log$

package CGI::Application::DBI;

require 5.005_62;
use strict;
use warnings;
use DBI;
use CGI::Logger qw(:all);

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);
our @EXPORT_OK = qw();
our @EXPORT = qw();
our $VERSION = '0.01';

#
# Create new object
#
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = { dsn => shift,
		 user => shift,
		 password => shift,
		 connected => 0
	       };

    bless( $self, $class );

    # Try to connect
    $self->connect;

    return $self;
}

#
# Connect to database
#
sub connect {
    my $self = shift;

    if ($self->dsn && $self->user && $self->password && !$self->connected) {
	$self->dbh( DBI->connect( $self->dsn, $self->user, $self->password ) );
	return $self->connected(1) if ($self->dbh);
    }
    return 0;
}

#
# Get list of tables
#
sub tables {
    my $self = shift;

    my @tables;
    @tables = $self->dbh->tables if ($self->connected);

    return wantarray ? @tables : \@tables;
}

#
# Execute a single SQL statement. Call with a SQL statement or a
# statement handle that is anything other than a select
# statement. Returns number of rows affected or undef on error.
#
sub do {
    my ($self, $sql_statement) = splice @_, 0, 2;

    return $self->connected ? $self->dbh->do( $sql_statement, {}, @_ ) : undef;
}

#
# Data access methods
#
sub dsn {
    my $self = shift;
    $self->{dsn} = shift if (@_);
    return $self->{dsn};
}
sub user {
    my $self = shift;
    $self->{user} = shift if (@_);
    return $self->{user};
}
sub password {
    my $self = shift;
    $self->{password} = shift if (@_);
    return $self->{password};
}
sub dbh {
    my $self = shift;
    $self->{dbh} = shift if (@_);
    return $self->{dbh};
}
sub connected {
    my $self = shift;
    $self->{connected} = shift if (@_);
    return $self->{connected};
}
sub debug {
    my $self = shift;
    $self->{debug} = shift if (@_);
    return $self->{debug};
}

1;
__END__

=head1 NAME

CGI::Application::DBI - Perl extension for easy database access from a
CGI::Application.

=head1 SYNOPSIS

  use CGI::Application::DBI;

=head1 DESCRIPTION

=head2 Class Methods

See below.

=over 4

=item C<< $db = new CGI::Application::DBI >>

Create a new database object and initiate connection with database
server.

=item C<< $db->connect >>

Connect to database server with the current I<DSN>, I<username> and
I<password>. Returns I<1> on success, I<0> otherwise.

=item C<< $db->tables >>

Get a list of available tables. Returns an array if in array context,
or an array reference if in scalar context.

=item C<< $db->do( $sql_statement, @bind_vars ) >>

Execute a single SQL statement. Call with a SQL statement or a
prepared statement handle. Returns number of rows affected.

=back

=head2 Data access methods

The following methods can be used to alter the configuration or
behaviour of each object instance. All methods returns the current
value, and, if called with a parameters, sets the value to that
parameter.

=over 4

=item C<< $db->dsn >>

Database driver DSN.

=item C<< $db->user >>

Database driver username.

=item C<< $db->password >>

Database driver password.

=item C<< $db->dbh >>

Database handle. Should not be altered.

=item C<< $db->connected >>

True if connected to a database server, false otherwise.

=item C<< $db->debug >>

Debug level. Valid options are 0 (false) and non-zero (true).

=back

=head1 AUTHOR

Stefan Svensson <stefan@raggmunk.nu>

=head1 SEE ALSO

perl(1), CGI::Application, HTML::template.

=cut
