# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

######################### We start with some black magic to print on failure.

# Change 1..1 below to 1..last_test_to_print .
# (It may become useful if the test is moved to ./t subdirectory.)

BEGIN { $| = 1; print "1..3\n"; }
END {print "not ok 1\n" unless $loaded;}
use CGI::Application::DBI;
$loaded = 1;
print "ok 1\n";

######################### End of black magic.

# Insert your test code below (better if it prints "ok 13"
# (correspondingly "not ok 13") depending on the success of chunk 13
# of the test code):

use strict;
use warnings;

# test 2 - create object
my $db = new CGI::Application::DBI;
if ($db) {
    print "ok 2\n";
}
else {
    print "not ok 2\n";
}

# test 3 - connect to database
$db->debug(1);
$db->dsn( 'DBI:mysql:webdiary:localhost' );
$db->user( 'stefan' );
$db->password( 'slempropp' );

if ($db->connect) {
    print "# connect to ", $db->dsn, " succeeded\n";
    print "ok 3\n";
}
else {
    print "not ok 3 # connect failed\n";
}

# test 4 - get list of tables
my @tables = $db->tables;
if (@tables) {
    print "# tables:\n";
    print "#   $_\n" foreach (@tables);
    print "ok 4\n";
}
else {
    print "not ok 4 # no list of tables\n";
}

# test 5 - try to create a new table
my $lines = $db->do( q{CREATE TABLE test (field1 varchar(100) DEFAULT '' NOT NULL, field2 int(100) DEFAULT '' NOT NULL, PRIMARY KEY (field2))}  );
if (defined $lines) {
    print "ok 5 # create table\n";
}
else {
    print "not ok 5\n";
}

# test 6 - insert values
$lines = $db->do( q{INSERT INTO test VALUES ('spam', 234234)} );
if ($lines == 1) {
    print "ok 6 # insert values\n";
}
else {
    print "not ok 6\n";
}

# test 6 - drop table
$lines = $db->do( q{DROP TABLE test} );
if (defined $lines) {
    print "ok 6 # drop table\n";
}
else {
    print "not ok 6\n";
}
