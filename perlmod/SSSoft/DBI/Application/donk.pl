use strict;
use String::CRC32;
use String::CRC;
use Digest::MD2 qw(md2_hex);
use Digest::MD5 qw(md5_hex);
use Digest::SHA1 qw(sha1_hex);
use Digest::Tiger;

my $data;

sub measure {
    my ($coderef) = shift;

    # time calculation
    require 'sys/syscall.ph';
    my $TIMEVAL_T = "LL";
    my $done = my $start = pack($TIMEVAL_T, ());
    syscall(&SYS_gettimeofday, $start, 0) != -1  or die "gettimeofday: $!";

    my ($result) = &$coderef (@_);

    # more time calculation
    syscall( &SYS_gettimeofday, $done, 0) != -1 or die "gettimeofday: $!";
    my @start = unpack($TIMEVAL_T, $start);
    my @done  = unpack($TIMEVAL_T, $done);
    my $MSB = $done[0] - $start[0];
    my $LSB = $done[1] - $start[1];
    my $time;
    if ($MSB > 0) {
	$time = 1000000 - $start[1] + $done[1];
    } else {
	$time = $done[1] - $start[1];
    }

    return ($result, $time);
}

$data = "brynanuppafjässasponken";

my %func = ( crc32 => [],
	     crc => [ 64 ],
	     md5_hex => [],
	     sha1_hex => [],
	     Digest::Tiger::hexhash => []
	   );

foreach my $func (sort keys %func) {

    printf "%s %s %d\n", $func, measure( \&$func, $data, @{$func{$func}} );

}
