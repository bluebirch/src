#!/usr/bin/perl -w
#
# $Id$
#
# G�r fint kontoutdrag fr�n data kopierat fr�n handelsbankens hemsksida
#

my @list;
my ($start, $slut);
while (<>) {
    chomp;
    next if (length() <= 1);
    m/(\d\d\d\d-\d\d-\d\d)\s{2,}(.*?)\s{2,}(-?[ \d]+,\d+)\s{2,}(-?[ \d]+,\d+)/;
#    print "$1: $2 ($3)\n";
    my $hsh = { date => $1,
		text => $2,
		amount => $3 };

    my $saldo = $4;
    $saldo =~ s/ //g;
    $saldo =~ tr/,/./;

    if (not defined $slut) {
	$slut = $saldo;
    }

    $$hsh{amount} =~ s/ //g;
    $$hsh{amount} =~ tr/,/./;

    $start = $saldo - $$hsh{amount};

    push @list, $hsh;
}

@list = sort { $$a{date} cmp $$b{date} } @list;

printf "           ING�ENDE BALANS                 %10.2f\n", $start;
my $saldo = $start;
foreach my $hsh (@list) {
    $saldo += $$hsh{amount};
    printf "%-10s %-20s %10.2f %10.2f\n", $$hsh{date}, $$hsh{text}, $$hsh{amount}, $saldo;
}
printf "           SALDO                           %10.2f\n", $slut;
