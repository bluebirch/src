;; $Id$

;; Load this file with
;;
;;   (load "~/emacs/common")
;;
;; in ~/.emacs

;; Add ~/emacs to load-path.
(setq load-path (cons (expand-file-name "~/emacs") load-path))

;; Enable reftex-mode in LaTeX mode
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

;; Enable pmx-mode
(autoload 'pmx-mode "pmx-mode" "PMX editing mode." t)
(setq auto-mode-alist
      (append auto-mode-alist
	      '(("\\.pmx$" . pmx-mode))))

;; Set default font to fixed (probably won't work on other flavors of Emacs)
;(set-face-font 'default "-misc-fixed-medium-r-normal-*-10-*-*-*-*-*-iso8859-15")

;; $Log$
;; Revision 1.2  2010-07-20 17:22:28  skalman
;; Added font selection.
;;
;; Revision 1.1  2010-06-20 11:33:12  skalman
;; Added `common.el'
;;
