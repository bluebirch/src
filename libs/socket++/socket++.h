// $Id$
//
// C++ wrapper for socket communication
//
// $Log$
// Revision 0.2  1998/11/29 19:45:34  stefan
// It compiles... amazing!
//
// Revision 0.1  1998/11/29 19:16:17  stefan
// *** empty log message ***
//

#ifndef SOCKET_PLUSPLUS_H
#define SOCKET_PLUSPLUS_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

class Socket
{
  int sockfd;   // Socket file descriptor

public:
  // Default constructor
  Socket( void );

  // Bind socket to a specified port on the host machine
  int bind( const int port );

  // Connect to a host, either by address or by name, on specified port
  int connect( const char *host, const int port );
  int connect( const struct in_addr addr, const int port );

  // Disconnect socket
  int disconnect( void );
  
  // Get socket file descriptor; return -1 if no descriptor is
  // initialized.
  int get_filedescriptor( void ) { return sockfd; }

  // Default destructor
  ~Socket( void );
};
#endif
