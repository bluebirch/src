// $Id$
//
// $Log$
// Revision 0.2  1998/11/29 19:46:01  stefan
// It compiles... amazing!
//
// Revision 0.1  1998/11/29 19:28:57  stefan
// *** empty log message ***
//
//

#include <iostream.h>
#include "socket++.h"

Socket::Socket( void )
{
  // Constructor; just about nothing to do yet
  sockfd = 0;
}

int Socket::bind( const int port )
{
  cout << "So, ye want to bind to port " << port << "?" << endl;
}

int Socket::connect( const char *host, const int port )
{
  cout << "Ah, I should connect to " << host << " on port " << port << "?"
       << endl;
}

int Socket::connect( const struct in_addr addr, const int port )
{
  cout << "Some strange addres, huh?" << endl;
}

int Socket::disconnect( void )
{
  cout << "Disconnect. Bye bye." << endl;
}

Socket::~Socket( void )
{
  cout << "Bye, bye" << endl;
}
