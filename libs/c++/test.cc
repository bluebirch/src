// sends strings to tsinread.cc and gets back their length
// usage: tsinwrite hostname portno
//        see the output of tsinread for what hostname and portno to use

#include        <sockinet.h>
#include        <stdlib.h>

int main(int argc, char** argv)
{
  iosockinet remote_host (sockbuf::sock_stream);

  if (argc != 3) {
    cerr << "usage: " << argv[0] << " <host> <port>" << endl;
    return 2;
  }

  //  cout << "connect to " << argv[1] << ":" << argv[2] << endl;
  
  remote_host->connect( argv[1], atoi( argv[2] ) );

  cout << "connected" << endl;

  remote_host->recvtimeout( 2 );

  char buf[512];
  remote_host.getline( buf, sizeof( buf ) );
  cout << buf << endl;
  remote_host << "mode reader\r\n" << flush;
  remote_host << "list active\r\n" << flush;

  try {
    while ( int rval = remote_host->read( buf, sizeof( buf ) ) ) {
      cout << "rval=" << rval << endl;
      //      cout << buf << endl;
    }
  }
  catch (int *donk) {
    cout << "donk" << endl;
  }

  cout << "quitting" << endl;

  remote_host << "quit\r\n" << flush;

  remote_host->shutdown( sockbuf::shut_readwrite );

//    sio << "Hello! This is a test\n" << flush;

//    // terminate the while loop in tsinread.cc
//    //  sio.shutdown(sockbuf::shut_write);

//    int len;
//    while (sio >> len) cout << len << ' ';
//    cout << endl;

  cout << "spam" << endl;

  return 0;
}
