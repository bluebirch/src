//#define SUN 1

#ifndef _SOCKET_H
#define _SOCKET_H

#include <F1types.h>
//#include <error.h>

#define HIDE_BSTRING 1
#ifndef HIDE_BSTRING
 #include <bstring.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>   
#include <signal.h>
#include <string.h>                  
#include <unistd.h>           

#include <sys/param.h>
#include <sys/socket.h>
//#include <sys/systeminfo.h>    
#include <sys/time.h>
#include <sys/types.h>
#include <sys/uio.h>           
#include <sys/wait.h>     

#ifndef F1_WINDOIDS
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#endif

enum SocketStatus                   {
  SocketStatusOk                    ,SocketStatusFailed                ,
  SocketStatusInitializationFailed  ,SocketStatusPermissionDenied      ,
  SocketStatusAddressInUse          ,SocketStatusUnknownHost           ,
  SocketStatusAlreadyConnected      ,SocketStatusTimedOut              ,
  SocketStatusConnectionRefused     ,SocketStatusNetworkUnreachable    ,
  SocketStatusOutOfResources        ,SocketStatusNoMoreData            ,
  SocketStatusNoHandle              ,SocketStatusCount                 };

#define SELECT_TIMEOUT 1

#define SOH   1
#define STX   2
#define ETX   3
#define EOT   4
#define ENQ   5
#define ACK   6
#define BEL   7
#define BS    8
#define HT    9
#define LF    10
#define VT    11
#define FF    12
#define CR    13
#define SO    14
#define SI    15
#define DLE   16


typedef void (*BUFFPRINT) (void *, int);

extern void SockPrintBuffer(void *BUFFER,int LEN);

typedef class Socket C_SOCKET;

#define dHOST_NAME "localhost"

class	Socket	{
	F1_int	iSHandle			;
	F1_int	iSStatus			;
	F1_int	iSPortnumber			;
	F1_int	IN_HEADERS			;
	F1_int	iEcho				;
	F1_int	Hit_Cnt				;
	F1_char	szINBUFFER	[1024]		;
	F1_char	szSHostname	[50]		;
	struct  sockaddr_in	sa_in		;
	struct  sockaddr_in	sa_client	;
	struct  timeval		TV_TIME		;//Used In Select Function.
	F1_int     SetStatus()			;
	F1_char szHOST_NAME[64]			;
public:
	F1_pChar setHostname(F1_pChar szHost)
		{strcpy(szHOST_NAME,szHost);return szHOST_NAME;};
	F1_long	Write_Total	;//Total bytes written by this socket;
	F1_int	Write_Last	;//Number of bytes sent on last write.
	F1_long	Read_Total	;
	F1_int	Read_Last	;
	F1_int  iW_sec		,iW_Msec	;
	F1_int  iSW_sec		,iSW_Msec	;
	F1_int  iR_sec		,iR_Msec	,
		iSR_sec		,iSR_Msec	;
	F1_int	COUNTS			(				);
	void	((*RHDRF	)	(void *,int	)		);
	void	((*RDF		)	(void *,int	)		);
	F1_int  ((*RHF		)	(		)		);     
	F1_int		RDF_PRINT	(F1_int		W_Sec	=SELECT_TIMEOUT	,
					 F1_int		W_MSec	=0	,
					 F1_int		SW_Sec	=0	,
					 F1_int		SW_MSec	=900	); 
virtual void		SockPrintBuffer	(F1_pVoid	BUFFER		,
					 F1_int		LEN		);
	F1_int		ECHO		(				)
					{return(iEcho);			};
	F1_int		ECHO		(F1_int i			)
					{iEcho=i;return(iEcho);		};
	F1_int		HAND		(				)
					{return(iSHandle);		};
	F1_int		HITS		(				)
					{return Hit_Cnt;		};
	F1_long		WTOTAL		(				)
					{return Write_Total;		};
	F1_long		RTOTAL		(				)
					{return Read_Total ;		};
	F1_int		WLAST		(				)
					{return Write_Last ;		};
	F1_int		RLAST		(				)
					{return Read_Last  ;		};
	F1_int		OPEN		(				);
	F1_int		ISOPEN		(				)
					{return(iSHandle>-1);		};
	F1_int		CLOSE		()				;
	F1_int		CONNECT		(F1_pChar	Hostname	,
					 F1_int		Portnumber	);
	F1_int		SERVER		(F1_int		Portnumber	,
					 F1_int		Max_Connect=10	);
	F1_pChar	HOSTNAME	(F1_pChar	szHostname	);
	F1_pChar	HOSTNAME	(				)
					{return(szSHostname);		};
	F1_int		IS_READABLE	(F1_int		W_Sec	=SELECT_TIMEOUT,
					 F1_int		W_uSec	=0	);
	F1_int		IS_WRITABLE	(F1_int		W_Sec	=SELECT_TIMEOUT,
					 F1_int		W_uSec	=0	);
	F1_int		READ		(F1_pVoid	Buffer		,
					 F1_int		len		,
					 F1_int		W_Sec	=-1	,
					 F1_int		W_MSec	=-1	,
					 F1_int		SW_Sec	=-1	,
					 F1_int		SW_MSec	=-1	);


/*					 F1_int		W_Sec	=SELECT_TIMEOUT,
					 F1_int		W_MSec	=0	,
					 F1_int		SW_Sec	=0	,
					 F1_int		SW_MSec	=500	
);*/


	F1_int		WRITE		(F1_pVoid	Buffer		,
					 F1_int		len		);
	F1_int		SEND_FILE	(F1_pChar	filename	);
	C_SOCKET &operator<<		(F1_pChar	outstring	);
	C_SOCKET &operator<<		(F1_char	outchar		);
	 Socket();
	~Socket();
	};
#endif
