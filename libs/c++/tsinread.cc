

// receives strings from tsinwrite.cc and sends the strlen
// of each string back to tsinwrite.cc
#include        <sockinet.h>

int main()
{
        sockinetbuf     si(sockbuf::sock_stream);
        si.bind();

        cout << si.localhost() << ' ' << si.localport() << endl;
        si.listen();

        iosockinet s = si.accept();
        char          buf[1024];

        while (s >> buf) {
                cout << buf << ' ';
                s << ::strlen(buf) << endl;
        }
        cout << endl;
        
        return 0;
}
