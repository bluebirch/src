// reads data sent by tdinwrite.cc
#include <sockinet.h>

int main(int ac, char** av)
{
        isockinet  is (sockbuf::sock_dgram);
        is->bind();

        cout << "localhost = " << is.localhost() << endl
             << "localport = " << is.localport() << endl;

        char         buf[256];
        int          n;

        is >> n;
        cout << av[0] << ": ";
        while(n--) {
                is >> buf;
                cout << buf << ' ';
        }
        cout << endl;

        return 0;
}
