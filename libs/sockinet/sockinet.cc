// $Id$
//
// C++ socket communications wrapper, source file.
// Written by Stefan Svensson <stefan@kristnet.org>
//
// This project goal is mainly to learn C++ programming
// and socket communications.
//

// System include files
#include <iostream.h>
#include <string.h>
#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// Header file
#include "sockinet.h"

// netdb.h error variable
extern int h_errno;

// Constructor
Socket::Socket ( int type, int domain ) 
{
  cout << "Initializing socket of type " << type << " in domain " 
       << domain << endl;

  sockfd = socket( domain, type, 0 );
}

// Bind socket to a specific port
int Socket::bind( int port )
{
  cout << "Bind to port " << port << endl;
  return 0;
}

// Connect socket to port on named host
int Socket::connect( const char *host, int port )
{
  struct hostent *he;
  struct sockaddr_in sa;
  struct in_addr *addr;

  cout << "Connect to " << host << ", port " << port << endl;

  // Lookup name
  if ((he = gethostbyname( host )) != NULL) {

    // I should check all addresses, but this will do for now

    sa.sin_family = AF_INET; // Should be selectable
    sa.sin_port = port;
    sa.sin_addr = *(struct in_addr *) he->h_addr_list[0];
    bzero( &sa.sin_zero, sizeof( sa.sin_zero ));

    cout << "Connect " << inet_ntoa( sa.sin_addr ) << ", port "
	 << sa.sin_port << endl;

    // That's it - try to connect

    return ::connect( sockfd, (sockaddr *) &sa, sizeof( sockaddr ));

  } else {
    // gethostbyname failed
    herror( "junk funk bunk" );
    return h_errno;
  }

  return 0;
}

// Disconnect socket
int Socket::shutdown( int how )
{
  cout << "Disconnect socket" << endl;
  return ::shutdown( sockfd, how );
}

