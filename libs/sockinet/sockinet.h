// $Id$
//
// C++ socket communications wrapper, header file.
// Written by Stefan Svensson <stefan@kristnet.org>
//
// This project goal is mainly to learn C++ programming
// and socket communications.
//

#include <sys/types.h>
#include <sys/socket.h>

class Socket {

  // Socket file descriptor
  int sockfd;

public:

  // Constructor
  Socket( int type = SOCK_STREAM, int domain = AF_INET );

  // Return the socket file descriptor
  int fd( void ) { return sockfd; };

  // Bind socket to a specific port - or whatever
  int bind( int port );

  // Connect socket to port on named host
  int connect( const char *host, int port );

  // Disconnect socket
  int shutdown( int how = 2 );
};
