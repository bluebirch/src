// Test file

#include <stdio.h>
#include <unistd.h>
#include <iostream.h>
#include "sockinet.h"

int main( int argc, char **argv )
{
  class Socket remote_host;
  char host[255];
  char port[255];
  int portnum;

  host[0] = port[0] = '\0';

  // Process the command line
  int opt;
  while ((opt = getopt( argc, argv, "v" )) != EOF) {
    switch (opt) {
    case 'v':
      cout << "Enabled verbose mode does not work, really." << endl;
      break;
    default:
      cout << "Invalid option." << endl;
      exit( 1 );
    }
  }
  for( ; optind < argc; optind++ ) {
    if (host[0] == '\0') {
      strcpy(host, argv[optind] );
    } else if (port[0] == '\0') {
      strcpy(port, argv[optind] );
    }
  }

  // Validate arguments
  if ((host[0] == '\0') || (port[0] == '\0')) {
    cout << "Specify host and port number, pease" << endl;
    exit( 1 );
  }

  if (sscanf( port, "%u", &portnum ) == 0) {
    cout << "Invalid port number" << endl;
  }


  // Run some tests
  cout << "Socket file descriptor: " << remote_host.fd() << endl;
  remote_host.bind( 3987 );
  if (remote_host.connect( host, portnum ) == 0) {
    cout << "yer connected!" << endl;
  } else {
    perror( "connect" );
  }
  remote_host.shutdown();

  return 0;
}
