#!/usr/bin/perl -w
#
# This is a script written for one particular purpose only: to scan
# the load of report mails from Bombardier Tramsportation in Stockholm
# and print all information in a single report.
#
# Written by Stefan Svensson <stefan@raggmunk.nu>
# (C)Copyright CSC Sverige AB
#
# $Log$
# Revision 1.2  2003/07/28 09:38:26  stefan
# Added detection of Continuus backup problem mails. Fixed some bugs.
#
# Revision 1.1  2003/07/23 12:26:40  stefan
# This version scans for completed savegroups, staging messages and
# cloning. It reports any errors (that passes the mask of uninteresting
# error messages), and also adds some logic and error messages on its
# own.
#
# Command line switches are:
#
#  -v             Verbose operation. Print fun stuff on standard error
#                 output.
#  -f <mailfile>  Scan specified mailbox.
#  -d <days>      Report for 'days' backwards in time (default: 0).
#  -D <date>      Report for a particular date.
#
#
use strict;
use Mail::Box::Manager;
use Date::Parse;
use Date::Calc qw(:all);
use Getopt::Std;
use POSIX qw(strftime);

# Error messages to mask
my @ERRMASK = ( q{cannot open \(unknown error},
		q{cannot open \(The process.*being used},
		q{changed during save$},
		q{^SYSTEM DB:\\\\ Disk Quota Databases},
		q{^SYSTEM DB:\\\\ Removable Storage Database},
		q{preclntsave: The tmp file exists},
		q{Preclntsave exiting},
		q{(Starting|Finished) export of Informix part of},
		q{savegrp: suppressed.*lines of output},
		q{snap_backup [Cc]reating snapshot},
		q{snap_backup drwxr-xr-x},
		q{Warning -.*size grew during save},
		q{Expected -?\d+ bytes.*got \d+ bytes},
		q{Resource temporarily unavailable$},
		q{No such file or directory$},
		q{The NTFS Change Journal will be used for this save},
		q{file access conflict$}
	      );

# Mail subject mask (uninteresting mails)
my $MAILMASK = q{^(Legato FRC Completion|Legato nsrwatch output|rsync av ebiconfig|Ny anv�ndare|System Notification|Bombardier_Transportation_Signal_AB EK00007341)};

# Events to check every day
my @EVENTS = qw(rsync_hlmfs03 bootstrap kill_baan virscan baan_run clean_arch checkbatch);

# Get options
my %opt;
getopts( 'D:d:f:v', \%opt )
  or die "invalid options, read the f*cking source code";

# Parse options
my $MAILFOLDER = $opt{f} ? $opt{f} : '/home/stefan/mail/jobb/bombardier';

my $days = defined $opt{d} ? $opt{d} : 0;

my ($start, $end, $report_date);
if ($opt{D}) {
    die "wrong date '$opt{D}'" unless ($opt{D}  =~ m/^\d\d\d\d-\d\d-\d\d$/);
    $report_date = $start = $end = $opt{D};
}
else {
    $start = sprintf( '%04d-%02d-%02d', Add_Delta_Days( Today, -$days ) );
    $end = sprintf( '%04d-%02d-%02d', Today );
    $report_date = $start;
}



# Get a unix time value and return the date in YYYY-MM-DD
# format. Midnight is considered 23:00, not 00:00!
sub makedate {
    my $time = shift;
    if ($time) {
	my ($sec, $min, $hour, $day, $mon, $year) = localtime $time;
	$mon++;
	$year += 1900;
	if ($hour >= 23) {
	    ($year, $mon, $day) = Add_Delta_Days( $year, $mon, $day, 1 );
	}
	return sprintf( '%4d-%02d-%02d', $year, $mon, $day );
    }
    return undef;
}

my $manager = new Mail::Box::Manager;

print STDERR "Opening mail folder (this can take a while)...\n"
  if ($opt{v});

my $folder = $manager->open( folder => $MAILFOLDER );
my $mailcount = $folder->messages;

print STDERR "Folder: ", $folder->name, ", $mailcount messages.\n"
  if ($opt{v});

my (%host, %stage, %error, %clone, %event, %baan_batch);


my $counter = 0;
foreach my $msg ($folder->messages) {

    print STDERR "\r", sprintf( "\r%3d", ++$counter / $mailcount * 100 ), "%"
      if ($opt{v});

    # Get subjeect
    my $subject = $msg->subject;

    # Get sender
    my $sender = $msg->sender->user;

    # Get date and time of mail
    my $mail_unixtime = str2time( $msg->date );
    my $mail_date = &makedate( $mail_unixtime );
    my $mail_time = strftime( '%H:%M:%S', localtime $mail_unixtime );

    # Get message body in readable format
    my $body = $msg->decoded->string;

    ##################################################################
    # Legato savegroup completion
    if ($subject =~ m{savegroup completion}) {

	# Get header info
	$body =~ m{NetWorker Savegroup: \((.*?)\) (.*?) completed, (\d+) client\(s\) \((.*?)\)};
	my ($importance, $savegroup, $clients, $summary) = ($1,$2, $3, $4);

	next unless ($savegroup);

	# Get times
	$body =~ m{Start time:\s*(.*?)$}m;
	my $start = str2time( $1 );
	$body =~ m{End time:\s*(.*?)$}m;
	my $end = str2time( $1 );

	# Bail out on error
	if (!$end) {
	    print $body;
	    die "wrong here";
	}

	# Determine date of this report. If time is after 23:00,
	# advance date one day.
	my $date = &makedate( $end );

	# Get failed clients (if any)
	my @failed = ();
	if ($summary =~ m{Failed$}) {
	    @failed = split m/\s+/, $summary;
	    $#failed--; # Remove last array value; it only reads
                        # "Failed" anyway.
	}

	# Store failed client as an error message
	if (@failed) {
	    push @{ $error{$date} }, "Failed clients: " . join( ' ', @failed );
	}

	# Get all saved partitions
	while ($body =~ m{^\s*(.*?):\s+(.*?)\s+level=(.*?),\s+(\d+)\s+(.*?)\s+(\d\d:\d\d:\d\d)\s+(\d+)\s+files\s*$}gm) {
	    my ($host, $partition, $level, $size, $size_unit, $files) = ($1, $2, $3, $4, $5, $6);

	    # Recalculate size to MB
	    if ($size_unit eq 'KB') {
		$size = $size / 1024;
	    } elsif ($size_unit eq 'GB') {
		$size = $size * 1024;
	    }

	    # Store values
	    $host{$date}{$host}{partitions}{$partition} = 
	      { level => $level, size => $size, unit => $size_unit, files => $files };

	    # Collect statistics for the host
	    $host{$date}{$host}{size} = 0 unless (defined $host{$date}{$host}{size});
	    $host{$date}{$host}{size} += $size;

	    # Guess what backup level was used for this host
	    if ($partition !~ m{^SYSTEM (DB|FILES|STATE):} && $partition !~ m{^bootstrap} && $partition !~ m{^index:} && !defined $host{$date}{$host}{level}) {
		$host{$date}{$host}{level} = $level;
	    }
	}

	# Get error messages
 	while ($body =~ m{^\s*\*\s+(.*?):\s*(.*?)$}gm) {
	    my ($host, $message) = ($1, $2);

	    # Mask unwanted and uniteresting error messages
	    my $masked = 0;
	    foreach my $mask (@ERRMASK) {
		if ($message =~ m/$mask/) {
		    $masked = 1;
		    last;
		}
	    }

	    if (!$masked) {

		# Store error message if it was not masked
#		push @{ $host{$date}{$host}{errors} }, $message;

		# Store in global list of errors also
		push @{ $error{$date} }, "$host: $message";
	    }
	}

    }

    ##################################################################
    # legato staging
    elsif ($subject =~ m{^Legato staging}) {

	# Split message body in parts
	my (@part) = split m/Listing savesets for group/, $body;

	# Throw away the first part; it's junk anyway
	shift @part;

	# Check each part
	for (my $i=0; $i< scalar @part; $i++) {

	    # Get group name, pool and start date
	    $part[$i] =~ m/Group: (.*?) with pool\(s\): (.*?)  Started staging at: (.*?)$/m;
	    my ($group, $pool) = ($1, $2);
	    my $start = str2time( $3 );

	    # Decide which date this message belongs to
	    my $date = &makedate( $start );

	    unless ($date) {
		print STDERR $part[$i];
		die "no date";
	    }

	    # Get results
	    my ($total, $correct, $incorrect) = (0,0,0);
	    if ($part[$i] =~ m/Total # savesets: (\d+), Correct # savesets: (\d+), Incorrect # savesets: (\d+)/) {
		($total, $correct, $incorrect) = ($1, $2, $3);
	    }

	    # Verify that stage has completed
	    if ($part[$i] !~ m/Completed Staging/) {
		push @{ $error{$date} }, "Group $group never completed staging.";
	    }

	    # Store information in stage hash
	    if (not defined $stage{$date}{$group}) {
		$stage{$date}{$group}{total} = $total;
		$stage{$date}{$group}{correct} = $correct;
		$stage{$date}{$group}{incorrect} = $incorrect;
		$stage{$date}{$group}{pool} = $pool;
	    }
#  	    else {
#  		print STDERR "stage group '$group' has already been parsed for $date\n";
#  	    }
	}
    }

    ##################################################################
    # legato cloning
    elsif ($subject =~ m{^Start cloning in Sto}) {
	$body =~ m/Starting Cloning in Stockholm at (.*?)$/m;
	my $start = str2time( $1 );
	my $date = &makedate( $start );
	$clone{$date}{start} = $start;
    }
    elsif ($subject =~ m{^Legato cloning STO}) {
	$body =~ m/Clone Complete at: (.*?)$/m;
	my $end = str2time( $1 );

	if ($end) {
	    my $date = &makedate( $end );

	    # There could be more than one cloning session ending on
	    # this day.
	    if (defined $clone{$date}{end}) {
		push @{ $error{$date} }, "A clone session ended at " .
		  strftime( '%H:%M:%S', localtime $clone{$date}{end} ) .
		    " and another one at " .
		  strftime( '%H:%M:%S', localtime $end ) .
		    "; much to clone yesterday?";
	    }
	    $clone{$date}{end} = $end;
	}
	else {
	    my $failtime = str2time( $msg->date );
	    my $date = &makedate( $failtime );
	    push @{ $error{$date} }, "Cloning failed at " . 
	      strftime( '%H:%M:%S', localtime $failtime );
	}
    }

    ##################################################################
    # continuus backup problems
    elsif ($subject =~ m{^Continuus backup problems}) {

	# Get error message (the database name, I guess)
	$body =~ m/^(.*?) failed on (.*?)\./;
	my ($procedure, $db) = ($1, $2);

	unless ($db) {
	    print STDERR $body;
	    print STDERR "\n\nno error message found above!!!!\n\n";
	}

	# Get time
	my $failtime = str2time( $msg->date );
	my $date = &makedate( $failtime );
	die "no date" unless ($date);
	push @{ $error{$date} }, "Continuus backup: $procedure failed on $db";
    }

    ##################################################################
    # rsync till hlmfs03
    elsif ($subject =~ m{^rsync till hlmfs03}) {

	# Just note that this event happened. I don't care about error
	# messages.
	$event{$mail_date}{rsync_hlmfs03} = 1;
    }

    ##################################################################
    # legato bootstrap report
    elsif ($subject =~ m{^Bootstrap Report}) {

	# We've got a bootstrap. Good.
	$event{$mail_date}{bootstrap} = 1;
    }

    ##################################################################
    # kill baan
    elsif ($subject =~ m{^Killing BaaN user processes}) {

	# Baan processed killed. I don't really know how to get useful
	# information from this mail, so I just note that this event
	# happened.
	$event{$mail_date}{kill_baan} = 1;
    }

    ##################################################################
    # NAI update
    elsif ($subject =~ m{^NAI update}) {

	if ($body =~ m/^New datfile/m) {
	    $event{$mail_date}{nai_update} = 1;
	}
	else {
	    $event{$mail_date}{nai_update} = 0;
	}
    }

    ##################################################################
    # baan batches
    elsif ($subject =~ m{^Baan batches from yesterday}) {

	while ($body =~ m/([-_A-Z0-9]+).*?\d\d-[A-Z]{3}-\d\d\s+\d+\s+\d\d-[A-Z]{3}-\d\d\s+\d+\s+(\d+)/g) {
	    my ($job, $code) = ($1,$2);

	    if ($code != 2) {
		push @{ $error{$mail_date} }, "Baan batch $job exited with code $code";
	    }
	    $baan_batch{$mail_date}{$job} = $code;
	}
    }

    ##################################################################
    # virus scanning
    elsif ($body =~ m{Your "cron" job.*?/export/appl/uvscan/sesig_uvscan.sh}s) {

	# Just note this event happened. I assume reporting is done
	# some other way.
	$event{$mail_date}{virscan} = 1;
    }

    ##################################################################
    # baan batches
    elsif ($body =~ m{Your "cron" job.*?/export/home/job100/cronjob.sh}s) {
	$event{$mail_date}{baan_run} = 1;
    }

    ##################################################################
    # clean_arch
    elsif ($body =~ m{Your "cron" job.*?/usr/local/admin/bin/clean_arch.sh}s) {
	$event{$mail_date}{clean_arch} = 1;
    }

    ##################################################################
    # checkbatch
    elsif ($body =~ m{Your "cron" job.*?/export/home/job100/checkbatch}s) {
	$event{$mail_date}{checkbatch} = 1;
    }

    ##################################################################
    # discard uninteresting messages
    elsif ($subject =~ m{$MAILMASK}) {
    }

    ##################################################################
    # discard boring cron job notifications
    elsif ($body =~ m{Your "cron" job.*?(rsync_ebiconfig.sh|df -lk|printerstat.cfg)}s) {
    }


    ##################################################################
    # unknown mail
    else {
	push @{ $error{$mail_date} }, "Mail from $sender at $mail_time: $subject";
    }



}

######################################################################
# write reports for specified days
#

do {

    # First, check if cloning has both started and stopped
    if (!defined $clone{$report_date}) {
	push @{ $error{$report_date} }, "Cloning neither started nor ended.";
    }
    elsif (defined $clone{$report_date}{start} && !defined $clone{$report_date}{end}) {
	push @{ $error{$report_date} }, "Cloning started, but never ended.";
    }
    elsif (!defined $clone{$report_date}{start} && defined $clone{$report_date}{end}) {
	push @{ $error{$report_date} }, "A clone session ended, but was never started.";
    }

    # Then see if all expected events has occured
    foreach my $event (@EVENTS) {
	if (!defined $event{$report_date} || !defined $event{$report_date}{$event}) {
	    push @{ $error{$report_date} }, "Expected event '$event' never occured.";
	} elsif ($event{$report_date}{$event} == 0) {
	    push @{ $error{$report_date} }, "Expected event '$event' failed.";
	}
    }


    # Print report header
    my $header =  "SYSTEM REPORT " .
      uc( Date_to_Text_Long( split( m/-/, $report_date) ) );
    print $header, "\n";
    print "=" x length( $header ), "\n\n";

    # Any errors?
    if (defined $error{$report_date}) {
	print "Error messages:\n";
	print "------------------------------------------------------------\n";

	foreach my $err (@{ $error{$report_date} }) {
	    print "* $err\n";
	}
	print "\n";
    }
    else {
	print "No error messages recorded today! Congratulations!\n\n";
    }

    # Any host information for this day?
    if (defined $host{$report_date}) {

	print "Host                 Levl      Size\n";
	print "-------------------- ---- ---------\n";

	foreach my $host (sort keys %{ $host{$report_date} } ) {

	    # Print host information
  	    if ($host{$report_date}{$host}{level}) {
  		printf "%-20.20s %4s %6d MB\n",  $host, $host{$report_date}{$host}{level}, $host{$report_date}{$host}{size};
  	    } else {
  		print "$host\n";
  	    }

#	    # Print error messages
#  	    foreach my $err (@{ $host{$report_date}{$host}{errors} }) {
#  		print "  * $err\n";
#  	    }
	}
	print "\n";
    }

    # Any stage information?
    if (defined $stage{$report_date}) {

	print "Stage group          Pool  Tot  Ok Err\n";
	print "-------------------- ----- --- --- ---\n";

	foreach my $group (sort keys %{ $stage{$report_date} }) {
	    printf "%-20.20s %-5.5s %3d %3d %3d %s\n", $group, $stage{$report_date}{$group}{pool}, $stage{$report_date}{$group}{total}, $stage{$report_date}{$group}{correct}, $stage{$report_date}{$group}{incorrect};
	}
	print "\n";
    }


    # Cloning
    if (defined $clone{$report_date}) {

	print "Cloning\n";
	print "---------------\n";

#	if (defined $clone{$report_date}{start} && defined $clone{$report_date}{end}) {};

	if (defined $clone{$report_date}{start}) {
	    printf "Start: %s\n", strftime( '%H:%M:%S',
					    localtime $clone{$report_date}{start} );
	}

	if (defined $clone{$report_date}{end}) {
	    printf "End:   %s\n", strftime( '%H:%M:%S',
					    localtime $clone{$report_date}{end} );
	}
	print "\n";

    }

    # Events
    if (defined $event{$report_date}) {

	print "Events (cron jobs etc)  Status\n";
	print "----------------------- -------\n";

	foreach my $event (sort keys %{ $event{$report_date} }) {
	    printf "%-23s %s\n", $event, ($event{$report_date}{$event} ? "ok" : "failed");
	}
	print "\n";
    }


    # Report Baan batches
    if (defined $baan_batch{$report_date}) {

	print "Baan batch job   Code\n";
	print "---------------- ----\n";

	foreach my $job (sort keys %{ $baan_batch{$report_date} }) {
	    printf "%-16s %4d\n", $job, $baan_batch{$report_date}{$job};
	}
	print "\n";
    }

    print "\n\n";

    # Calculate next date
    $report_date = sprintf( '%04d-%02d-%02d', Add_Delta_Days( split( m/-/, $report_date ), 1 ));

} until ($report_date gt $end);

# END
