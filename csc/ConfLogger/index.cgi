#!/usr/bin/perl -w
#
# $Id$
#
# This application is used for documentation of configuration
# changes. It's a simple web interface. Authentication relies on
# http-auth of the web server and uses the CGI REMOTE_USER environment
# variables.
#
# It was developed for CSC to manage documentation at one of their
# customer sites.
#
# Written by Stefan Svensson <stefan@raggmunk.nu>
# (C)Copyright 2002
#

use strict;
use ConfLogger;

# Set permissions on files created by this script
umask 0007;

my $app = new ConfLogger( TMPL_PATH => 'templates/' );
$app->run;

#
# Revision history
#
# $Log$
# Revision 1.5  2002/07/25 08:53:34  stefan
# Added 'umask' to set permissions on created files.
#
# Revision 1.4  2002/07/19 11:35:14  stefan
# Configured for the target host at Saab Avionics. Added new field
# 'hardware' to list of fields.
#
# Revision 1.3  2002/07/16 12:40:28  stefan
# Added support for htpasswd-files with Apache::Htpasswd. The users
# full name can be specified in the htpasswd file, and is displayed
# on the web interface. Full name is not used in the log files. In
# the future, there could be a web based user administration tool.
#
# Revision 1.2  2002/07/16 12:06:41  stefan
# DBPATH must be absolute (should be fixed in future versions).
#
# Revision 1.1  2002/07/12 11:36:37  stefan
# Initial version with default configuration.
#
