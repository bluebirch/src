#!/usr/bin/perl -w
#
# $Id$
#
# This program is used to convert the old flat textfiles to the new
# database.
#
# $Log$
# Revision 1.2  2003/02/27 14:01:44  stefan
# Some changes. Added time field.
#
# Revision 1.1  2002/11/06 19:25:43  stefan
# This is an extremly ugly fast hack, but I think it works.
#
use strict;
use DBI;
use Date::Parse;
use POSIX qw(strftime);

my $DSN = 'DBI:mysql:ConfLogger:localhost';
my $USER = 'ConfLogger';
my $PASSWORD = 'murvel';
my $DEFAULTTYPE = 1;
my $DEFAULTHISTORYTYPE = 5;
my $DEFAULTIMPORTANCE = 3;

my $db = DBI->connect( $DSN, $USER, $PASSWORD );


foreach my $file (@ARGV) {
    print "processing file $file\n";

    open IN, $file;

    my $line;

    my %hostdata = ();

    # Read header
    while ($line = <IN>) {
	last if ($line =~ m/^###/);
	chomp $line;
	my ($key,$val) = split m/\s*:\s*/, $line, 2;
	if ($key) {
	    $key =~ s/\s//g;
	    $key = lc $key;
	    $hostdata{$key} = $val;
	}
    }

    # Try to find host in database
    my $result = $db->selectall_arrayref( 'SELECT * FROM Inventory WHERE Name=?',
					 { Slice => {} }, $hostdata{host} );

    # Update database if found
    if (scalar @$result > 0) {
	my $rows = $db->do( 'UPDATE Inventory SET Name=?, Hardware=?, OperatingSystem=?, IPAddress=?, Description=? WHERE ID=?',
			    {},
			    $hostdata{host}, $hostdata{hardware}, 
			    $hostdata{operatingsystem}, $hostdata{ipaddress},
			    $hostdata{comment}, $$result[0]{ID} );
#  	if ($rows != 1) {
#  	    print "WARNING: database update for host $hostdata{host} failed!\n";
#  	}
    }

    # Add new to database
    else {
	my $rows = $db->do( 'INSERT INTO Inventory (Name, Hardware, OperatingSystem, IPAddress, Description, Type) VALUES (?,?,?,?,?,?)',
			    {},
			    $hostdata{host}, $hostdata{hardware}, 
			    $hostdata{operatingsystem}, $hostdata{ipaddress},
			    $hostdata{comment}, $DEFAULTTYPE );
	if ($rows != 1) {
	    print "WARNING: database insert for host $hostdata{host} failed\n";
	}
    }

    # Get ID again; now it should be there
    $result = $db->selectall_arrayref( 'SELECT * FROM Inventory WHERE Name=?',
					 { Slice => {} }, $hostdata{host} );

    # Bail out if this fails
    if (scalar @$result != 1) {
	die "can't find $hostdata{host} in database";
    }

    my $equipment = $$result[0]{ID};

    print "equipment id for $hostdata{host} = $equipment\n";

    # Process log messages
    do {
	my ($date, $user) = ($line =~ m/^###(.*?)\/(.*?)$/);
	my $time = str2time( $date );
	my $longline = '';
	my @para;
	$line = <IN>;
	while ($line !~ m/^###/ && !eof IN) {
	    chomp $line;
	    if ($line) {
		if ($longline) {
		    $longline = join( ' ', $longline, $line);
		}
		else {
		    $longline = $line;
		}
	    }
	    elsif ($longline) {
		push @para, $longline;
		$longline = '';
	    }
	    $line = <IN>;
	}
	push @para, $longline if ($longline);
	if (scalar @para > 0) {

	    # Get user id
	    my $uid = $db->selectrow_array( 'SELECT ID FROM User WHERE Name=?',
					    {},
					    $user );

	    unless ($uid) {
		print "WARNING: Unknown user '$user'\n";
		$db->do( q{INSERT INTO User (Name,Fullname) VALUES (?,'(Automatically created user)')},
			 {},
			 $user );
		$uid = $db->selectrow_array( 'SELECT ID FROM User WHERE Name=?',
						{},
						$user );
		die "could not add user '$user'" unless ($uid);
	    }

	    my $text = join( "\n\n", @para );
	    my $logdate = strftime( '%Y-%m-%d', localtime $time );
	    my $logtime = strftime( '%H:%M:%S', localtime $time );

	    # Check if message exists
	    my $id = $db->selectrow_array( 'SELECT ID FROM History WHERE User=? AND Date=? AND Time=? AND Text=?',
					   {},
					   $uid, $logdate, $logtime, $text );

	    unless ($id) {
		my $rows = $db->do( q{INSERT INTO History (Equipment,Type,Importance,Date,Time,User,Text) VALUES (?,?,?,?,?,?,?)},
				    {},
				    $equipment, $DEFAULTHISTORYTYPE, $DEFAULTIMPORTANCE, $logdate, $logtime, $uid, $text );

		unless ($rows == 1) {
		    print "WARNING: update failed\n";
		}
	    }
	    else {
		print "log message already exists: $logdate $logtime\n";
	    }
	}
    } while (!eof IN);
}

$db->disconnect;
