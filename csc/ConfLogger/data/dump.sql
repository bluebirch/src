-- MySQL dump 8.22
--
-- Host: localhost    Database: ConfLogger
---------------------------------------------------------
-- Server version	3.23.54-log

--
-- Dumping data for table 'History'
--


INSERT INTO History VALUES (1,1,5,3,NULL,'2002-08-30','15:07:57',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (2,2,5,3,NULL,'2002-08-30','14:55:04',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (3,3,5,3,NULL,'2002-07-26','14:30:24',2,NULL,NULL,NULL,NULL,'R�ttat till tiden, ett fel som har varit sedan str�mavbrottet i maj. H�rdvaran verkar ej vara Y2K-s�ker. Datorn visade �r 1934, vilket inte blev s� bra vid backup av datorn. Nu har datorn r�tt �r.');
INSERT INTO History VALUES (4,3,5,3,NULL,'2002-08-15','16:59:16',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (5,4,5,3,NULL,'2002-10-08','09:53:01',2,NULL,NULL,NULL,NULL,'Installerat rekommenderat rekommenderade patchar f�r Solaris 2.6. Denna server har aldrig tidigare patchats.');
INSERT INTO History VALUES (6,4,5,3,NULL,'2002-10-08','13:22:18',2,NULL,NULL,NULL,NULL,'Patchning klar, bootar nu om efter att ha meddelat Thomas Lawenius.');
INSERT INTO History VALUES (7,4,5,3,NULL,'2002-10-08','17:46:33',2,NULL,NULL,NULL,NULL,'Efter patchning vart det v�ldigt fullt p� /usr partitionen.');
INSERT INTO History VALUES (8,4,5,3,NULL,'2002-10-10','08:29:56',2,NULL,NULL,NULL,NULL,'Installerat bind 8.3.1 (h�mtat fr�n sunfreeware) , men ej aktiverat.');
INSERT INTO History VALUES (9,5,5,3,NULL,'2002-08-09','16:18:52',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (10,6,5,3,NULL,'2002-08-15','16:55:46',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (11,7,5,3,NULL,'2002-08-15','16:51:39',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (12,8,5,3,NULL,'2002-07-15','13:23:26',1,NULL,NULL,NULL,NULL,'Installerade ConfLogger i /opt/www/htdocs/ConfLogger. Mixtrade lite med Apache f�r att till�ta AllowOverride i /opt/www/htdocs och CGI-handler f�r .cgi-extensions.\n\nDatafiler i /net/gullan/system/config. Korrekta skrivr�ttigheter ej l�st.\n\nInstallerade perlmodulerna CGI::Application, HTML::Template och Date::Parse med hj�lp av:\n\nperl -MCPAN -eshell');
INSERT INTO History VALUES (13,8,5,3,NULL,'2002-07-16','14:50:34',1,NULL,NULL,NULL,NULL,'Installerade perlmodulen Apache::Htpasswd.\n\nUppgraderade ConfLogger.');
INSERT INTO History VALUES (14,8,5,3,NULL,'2002-07-17','12:00:26',1,NULL,NULL,NULL,NULL,'Gjorde n�gra �ndringar i ConfLogger.\n\n�ndrade �gare och r�ttigheter p� /net/gullan/system/config. Loggfilerna �r nu o�tkomliga f�r vanliga anv�ndare.');
INSERT INTO History VALUES (15,8,5,3,NULL,'2002-07-19','14:39:47',1,NULL,NULL,NULL,NULL,'Uppdaterade ConfLogger: En s�kerhetskopia p� datafilerna sparas i /net/gullan/system/config/backup varje g�ng filen sparas. P� det viset f�r man lite historik och katastrofberedskap; g�r n�got �t skogen, finns alltid den n�st senaste loggfilen tillg�nglig.');
INSERT INTO History VALUES (16,8,5,3,NULL,'2002-08-09','11:57:41',1,NULL,NULL,NULL,NULL,'Uppdaterade ConfLogger: Nu kan man se de senaste loggmeddelandena p� en enda sida.');
INSERT INTO History VALUES (17,8,5,3,NULL,'2002-08-16','11:21:45',1,NULL,NULL,NULL,NULL,'Uppdaterade ConfLogger.');
INSERT INTO History VALUES (18,9,5,3,NULL,'2002-07-19','12:47:23',2,NULL,NULL,NULL,NULL,'Bootade om servern p� grund av att ypserv-processen l�pte amok. Den har inte m�tt bra sedan str�mavbrottet i b�rjan p� Juni.');
INSERT INTO History VALUES (19,9,5,3,NULL,'2002-08-01','10:12:08',2,NULL,NULL,NULL,NULL,'Installerat SystemEDGE 4.1p2 SNMP-agent.');
INSERT INTO History VALUES (20,9,5,3,NULL,'2002-08-06','14:32:45',2,NULL,NULL,NULL,NULL,'Denna server �r nu admin host f�r gullan.');
INSERT INTO History VALUES (21,9,5,3,NULL,'2002-08-08','13:40:36',1,NULL,NULL,NULL,NULL,'Installerade libol-0.2.23 och syslog-ng-1.4.15 i /usr/local.\n\nLade upp konfigurationsfil f�r syslog-ng som /etc/syslog-ng/syslog-ng.conf. Syslog-ng skriver nu alla loggfiler till /var/log.\n\nInstallerade startscript f�r syslog-ng som /etc/init.d/syslog-ng och l�nkade det till /etc/rc0.d/K40syslog-ng, /etc/rc1.d/K40syslog-ng, /etc/rc2.d/S74syslog-ng samt /etc/rcS.d/K40syslog-ng.\n\nSt�ngde av Solaris syslogd (/etc/rc2.d/S74syslog -> /etc/rc2.d/K40syslog).\n\nInaktiverade /usr/lib/newsyslog i crontab.\n\nInstallerade logrotate.pl i /usr/local/sbin och lade till den i crontab.');
INSERT INTO History VALUES (22,9,5,3,NULL,'2002-08-14','09:35:29',1,NULL,NULL,NULL,NULL,'Konfigurerade om syslog-ng: Alla meddelanden sorteras p� respektive host i /var/log/hosts. Felmeddelanden loggas till /var/log/err och kritiska felmeddelanden till /var/log/crit.\n\n�ndrade s�kv�gar i /usr/local/sbin/logrotate.pl.');
INSERT INTO History VALUES (23,9,5,3,NULL,'2002-10-08','17:47:49',2,NULL,NULL,NULL,NULL,'B�rjat versionshantera DNS-konfiguration med sccs f�r att f� sp�rbarhet. T.ex /etc/EDUP/Named/bin/option.');
INSERT INTO History VALUES (24,10,5,3,NULL,'2002-07-26','14:28:10',2,NULL,NULL,NULL,NULL,'St�dat bort alla xterminaler i dhcp som inte har varit ig�ng under �r 2002.');
INSERT INTO History VALUES (25,10,5,3,NULL,'2002-08-01','10:12:43',2,NULL,NULL,NULL,NULL,'Installerat SystemEDGE 4.1p2 SNMP-agent.');
INSERT INTO History VALUES (26,10,5,3,NULL,'2002-08-09','16:28:51',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (27,10,5,3,NULL,'2002-09-06','19:38:51',3,NULL,NULL,NULL,NULL,'lagt till en parallell installation av ncbridge 4.0 under /export�/ncd_kas . Installationen �r pga att TS anv�ndare ska migreras till AVIONET och x-terminal installationen kan bara peka mot 1st server eller publicerad applikation. Har �ven patchat upp till rel 11. Har �ven skapat �nnu ett makro i dhcp managern, os.500_kas.');
INSERT INTO History VALUES (28,10,5,3,NULL,'2002-09-08','17:29:17',2,NULL,NULL,NULL,NULL,'Patchat med Solaris 8 recommeded patch cluster.');
INSERT INTO History VALUES (29,10,5,3,NULL,'2002-09-08','17:35:19',2,NULL,NULL,NULL,NULL,'�ndrat i nsswitch.conf alises: files nis passwd: files nis');
INSERT INTO History VALUES (30,11,5,3,NULL,'2002-08-09','16:32:48',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (31,12,5,3,NULL,'2002-08-09','16:34:51',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (32,13,5,3,NULL,'2002-08-09','16:36:49',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (33,13,5,3,NULL,'2002-09-08','17:32:32',2,NULL,NULL,NULL,NULL,'Installerat Solaris 8 recommeded patch cluster.');
INSERT INTO History VALUES (34,14,5,3,NULL,'2002-07-30','13:05:04',2,NULL,NULL,NULL,NULL,'Det har varit problem med cloning. Kan bero p� att anansi fick fel tid vid str�mavbrottet i maj. Anansi �r ej Y2k-s�ker. Nu g�r klockat r�tt p� anansi.');
INSERT INTO History VALUES (35,14,5,3,NULL,'2002-08-01','10:13:19',2,NULL,NULL,NULL,NULL,'Upggraderat SystemEDGE till version 4.1p2.');
INSERT INTO History VALUES (36,14,5,3,NULL,'2002-08-09','16:24:02',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (37,15,5,3,NULL,'2002-08-09','16:39:26',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (38,15,5,3,NULL,'2002-09-09','15:29:56',2,NULL,NULL,NULL,NULL,'�ndrat i sunadmin/jumpstart/scripts/finish.default f�r att ge en korrekt resolv.conf.\n\n�ndrat i cfengine f�r resolv.conf /gullan_vol1/sunadmin/cfengine/current/lib/cfengine/inputs');
INSERT INTO History VALUES (39,16,5,3,NULL,'2002-09-06','13:56:24',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (40,16,5,3,NULL,'2002-09-09','10:23:46',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (41,16,5,3,NULL,'2002-09-09','10:24:22',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust virus def 020809');
INSERT INTO History VALUES (42,17,5,3,NULL,'2002-08-14','13:32:04',6,NULL,NULL,NULL,NULL,'test av Dign�');
INSERT INTO History VALUES (43,17,5,3,NULL,'2002-09-06','13:56:34',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (44,17,5,3,NULL,'2002-09-09','10:25:24',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (45,17,5,3,NULL,'2002-09-09','10:25:49',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (46,18,5,3,NULL,'2002-08-14','14:34:05',4,NULL,NULL,NULL,NULL,'Uppdatering av FlexLM licenser. (Lars-�ke Larsson)');
INSERT INTO History VALUES (47,18,5,3,NULL,'2002-09-06','13:56:42',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (48,18,5,3,NULL,'2002-09-09','10:26:04',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (49,18,5,3,NULL,'2002-09-09','10:26:09',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (50,19,5,3,NULL,'2002-08-08','16:07:33',5,NULL,NULL,NULL,NULL,'SP6a (NT4) + Insight manager agent');
INSERT INTO History VALUES (51,19,5,3,NULL,'2002-08-08','16:10:49',5,NULL,NULL,NULL,NULL,'Uppaderat Mdac 2.7');
INSERT INTO History VALUES (52,19,5,3,NULL,'2002-09-02','15:26:06',5,NULL,NULL,NULL,NULL,'Uppgrade SQL pluss Oracle Networking 7.3');
INSERT INTO History VALUES (53,19,5,3,NULL,'2002-09-06','13:56:54',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (54,19,5,3,NULL,'2002-09-09','10:26:28',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (55,19,5,3,NULL,'2002-09-09','10:26:38',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (56,20,5,3,NULL,'2002-08-08','16:08:47',5,NULL,NULL,NULL,NULL,'Bytt disk som var trasig');
INSERT INTO History VALUES (57,20,5,3,NULL,'2002-09-06','13:56:59',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (58,20,5,3,NULL,'2002-09-09','10:26:54',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (59,20,5,3,NULL,'2002-09-09','10:26:58',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (60,20,5,3,NULL,'2002-10-01','10:10:03',4,NULL,NULL,NULL,NULL,'Taskigt med utrymme p� c:\\ Flyttade ett g�ng gamla databaser ifr�n \\system32\\ till e:\\old database\\. De hade inte anv�nts sedan 99.');
INSERT INTO History VALUES (61,21,5,3,NULL,'2002-08-08','16:10:18',5,NULL,NULL,NULL,NULL,'Omstart + Insight manager agent 5.40');
INSERT INTO History VALUES (62,21,5,3,NULL,'2002-09-06','13:57:07',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (63,21,5,3,NULL,'2002-09-09','10:27:21',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (64,21,5,3,NULL,'2002-09-09','10:27:26',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (65,21,5,3,NULL,'2002-10-01','15:11:50',4,NULL,NULL,NULL,NULL,'Test');
INSERT INTO History VALUES (66,21,5,3,NULL,'2002-10-11','10:08:49',4,NULL,NULL,NULL,NULL,'ClearquestWeb installerat o klart..(f�rra veckan)');
INSERT INTO History VALUES (67,21,5,3,NULL,'2002-10-23','12:30:32',4,NULL,NULL,NULL,NULL,'Installerat \"Service Release 1\" till ClearQuest, omstart (ig�r)');
INSERT INTO History VALUES (68,22,5,3,NULL,'2002-08-30','14:42:23',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (69,22,5,3,NULL,'2002-08-30','14:42:41',1,NULL,NULL,NULL,NULL,'Ntpdate skenade sedan 1 juni. Fixat med kill -HUP.');
INSERT INTO History VALUES (70,23,5,3,NULL,'2002-08-14','09:49:27',1,NULL,NULL,NULL,NULL,'Syslog loggar *.info till loghost (esbb11).');
INSERT INTO History VALUES (71,24,5,3,NULL,'2002-08-14','11:04:56',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (72,25,5,3,NULL,'2002-10-14','17:09:37',2,NULL,NULL,NULL,NULL,'Garvin var avslagen av n�gon, s� backup kunde ej tas p� denna server. Lite konstigheter i syslog: Oct 7 09:29:58 4A:garvin unix: WARNING: module 1 MSC: Module keyswitch turned OFF ( shutdown pending) Oct 7 09:30:03 7B:garvin syslog: {start,stop}midi entered Oct 7 09:30:03 3D:garvin nsd[233]: NIS server for domain KistaMicrowaves not respon ding.G (16%) Oct 7 09:30:28 1A:garvin unix: ALERT: (MAINT-NEEDED):module 1 MSC: Too many over te mperature interrupts Oct 7 09:30:28 1A:garvin unix: ALERT: (MAINT-NEEDED):module 1 MSC: Switching to def erred monitoring mode');
INSERT INTO History VALUES (73,26,5,3,NULL,'2002-08-09','16:43:48',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (74,26,5,3,NULL,'2002-08-29','11:07:54',2,NULL,NULL,NULL,NULL,'Anders Ryttermalm fr�n Sun Microsystems var h�r och bytte en trasig disk i A1000. Detta kunde ske under drift.');
INSERT INTO History VALUES (75,26,5,3,NULL,'2002-09-08','17:30:06',2,NULL,NULL,NULL,NULL,'Installerat Solaris 7 recommeded patch cluster.');
INSERT INTO History VALUES (76,26,5,3,NULL,'2002-10-01','14:54:55',2,NULL,NULL,NULL,NULL,'Bytt batteri.');
INSERT INTO History VALUES (77,27,5,3,NULL,'2002-07-15','16:07:08',1,NULL,NULL,NULL,NULL,'Loggfil skapad.');
INSERT INTO History VALUES (78,27,5,3,NULL,'2002-08-05','13:30:45',1,NULL,NULL,NULL,NULL,'St�ngde av postgress och raderade l�nken /etc/rc2.d/S97postgres (som pekade p� /etc/init.d/postgres) s� att postgress inte startar n�r godzilla bootar.\n\nTroligen kommer ingen att m�rka att postgress �r borta. Installationen har varit or�rd sedan 1998 och loggarna �r tomma. F�rmodligen var det n�gon Calle Dybedahl installerade under sin tid vid rodret.');
INSERT INTO History VALUES (79,27,5,3,NULL,'2002-08-05','14:42:20',1,NULL,NULL,NULL,NULL,'St�ngde av Apache webserver. Raderade l�nken /etc/rc3.d/S90apache (som pekade p� /etc/init.d/apache) s� att inte apache startar n�r godzilla bootar.\n\nStatistiken visade p� tv� bes�k per m�nad det senaste halv�ret. Man f�r misst�nka att de bes�karna helt enkelt f�rirrat sig in p� godzilla av misstag.');
INSERT INTO History VALUES (80,27,5,3,NULL,'2002-08-05','14:57:18',1,NULL,NULL,NULL,NULL,'St�ngde av FTP. Tj�nsten anv�nds inte.');
INSERT INTO History VALUES (81,27,5,3,NULL,'2002-08-05','15:26:47',1,NULL,NULL,NULL,NULL,'St�ngde av xfs (X Font Server) genom att redigera /etc/inetd.conf och k�ra kommandot \"fsadmin -d\". Fontservern har inte anv�nts sedan 1 juni.');
INSERT INTO History VALUES (82,27,5,3,NULL,'2002-08-05','15:42:41',1,NULL,NULL,NULL,NULL,'St�ngde av TFTP. De filer som fanns tillg�ngliga via TFTP har inte r�rts p� ett och ett halvt �r (access date �r 2001-02-02).');
INSERT INTO History VALUES (83,27,5,3,NULL,'2002-08-07','10:44:00',1,NULL,NULL,NULL,NULL,'Inaktiverade lmgrd (flexlm) genom att radera /etc/rc2.d/S99lmgrd.');
INSERT INTO History VALUES (84,27,5,3,NULL,'2002-08-07','13:25:37',1,NULL,NULL,NULL,NULL,'D�dade licensservern f�r hpads2002 och inaktiverade startupscriptet (/etc/rc3.d/S98hpeesof). Kopia p� scriptet finns i /etc/init.d.');
INSERT INTO History VALUES (85,27,5,3,NULL,'2002-08-07','14:02:53',1,NULL,NULL,NULL,NULL,'St�ngde av printertj�nsterna (lpsched) och raderade startscriptet (/etc/rc2.d/S80lp). I samma veva uppt�ckte jag att Godzilla har b�de start- och stopscript f�r lp i runlevel 2 (/etc/rc2.d/K20lp). Uppenbarligen startar Solaris tj�nsterna efter att de stoppats, eftersom det �nd� fungerade.');
INSERT INTO History VALUES (86,27,5,3,NULL,'2002-08-09','16:11:33',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (87,27,5,3,NULL,'2002-08-12','16:05:05',2,NULL,NULL,NULL,NULL,'Det visades sig att godzilla fortfarande anv�nds som web-server. iww2.kiere.ericsson.se. Tobias Loberg flyttar sina html-sidor till den nya web-servern (kiws01).');
INSERT INTO History VALUES (88,27,5,3,NULL,'2002-08-15','15:37:32',1,NULL,NULL,NULL,NULL,'Flyttade /opt/gnu till /sw/misc/oldstuff (gullan:/vol/vol1/export/sw/SunOS/misc/oldstuff). �ndrade s�kv�g gnu-before.sh, gnu-after.sh och vp12.sh i /net/gullan/company/initfiles.\n\nSyftet �r att f� bort /opt/gnu. Vissa applikationer kommer med all s�kerhet att sluta fungera, eftersom de bara flyttats till /sw/misc/oldstuff. Detta m�ste vi vara beredda p� n�r /opt/gnu tas bort ur NIS-mappen f�r automount och NFS-servern p� godzilla st�ngs av.\n\nDet h�r �r inte roligt.');
INSERT INTO History VALUES (89,27,5,3,NULL,'2002-08-21','11:56:53',1,NULL,NULL,NULL,NULL,'Startade printertj�nsterna igen. Tobias Loberg har problem att skriva ut fr�n GASK (piwin och pitool och allt vad det heter).');
INSERT INTO History VALUES (90,27,5,3,NULL,'2002-08-23','10:22:07',1,NULL,NULL,NULL,NULL,'St�ngde av perforce-tj�nsten och inaktiverade startscripten p� uppdrag av D/KM Peter Friberg (qsbpef).');
INSERT INTO History VALUES (91,27,5,3,NULL,'2002-08-30','15:14:16',1,NULL,NULL,NULL,NULL,'Filsystemet /var blev just fullt. Panikrensning frigjorde 628 kb. Hela partitionen �r p� 490 MB.');
INSERT INTO History VALUES (92,27,5,3,NULL,'2002-08-30','15:19:07',1,NULL,NULL,NULL,NULL,'Rensade /var/tmp. /var har nu 50% ledigt.');
INSERT INTO History VALUES (93,28,5,3,NULL,'2002-08-30','14:36:28',1,NULL,NULL,NULL,NULL,'Ntpdate skenade sedan 1 juni. kill -HUP l�ste problemet.');
INSERT INTO History VALUES (94,28,5,3,NULL,'2002-08-30','14:36:42',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (95,29,5,3,NULL,'2002-08-09','17:02:38',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (96,29,5,3,NULL,'2002-09-08','17:30:42',2,NULL,NULL,NULL,NULL,'Installerat Solaris 2.6 recommeded patch cluster.');
INSERT INTO History VALUES (97,30,5,3,NULL,'2002-07-26','10:58:11',2,NULL,NULL,NULL,NULL,'En 72 GB disk gick s�nder den 25/7, kl 16:03. Disk 8.35.');
INSERT INTO History VALUES (98,30,5,3,NULL,'2002-07-26','14:22:50',2,NULL,NULL,NULL,NULL,'ProAct levererade en ny 72GB disk idag kl. 12:00. Disken var av fel typ; zoned, is�llet f�r blocked. ProAct kunde ej leverera en disk av typen blocked f�r�n p� m�ndag 29/7.');
INSERT INTO History VALUES (99,30,5,3,NULL,'2002-07-30','13:02:42',2,NULL,NULL,NULL,NULL,'ProAct levererade r�tt disk idag kl. 12:15.');
INSERT INTO History VALUES (100,30,5,3,NULL,'2002-07-30','13:03:40',2,NULL,NULL,NULL,NULL,'Det �r 97% fullt p� vol2. Janne St�lberg st�dar bort dubblerade hemkataloger p� ntusers.');
INSERT INTO History VALUES (101,30,5,3,NULL,'2002-08-12','09:39:20',2,NULL,NULL,NULL,NULL,'Ut�kat antal shapshot. 14 st nightly snapshot sparas 12 st hourly snapshot sparas\n\nDetta gjordes f�r att slippa l�sa g�ra �terl�sningar fr�n band p� s�dant som anv�ndarna \"tappat\" under de senaste tv� veckorna.');
INSERT INTO History VALUES (102,30,5,3,NULL,'2002-09-05','11:05:56',2,NULL,NULL,NULL,NULL,'Trasig power supply, felanm�lt till ProAct. System Notification from gullan (Power supply 1 is powered off POWER_SUPPLY_DEGRADED!!!) ALERT');
INSERT INTO History VALUES (103,30,5,3,NULL,'2002-09-25','14:41:26',2,NULL,NULL,NULL,NULL,'Gullan bootade om ... Wed Sep 25 14:29:55 MES [rc:ALERT]: relog syslog PANIC: ../common/wafl/nfsops.c:5280: Assertion failure. in process wafl_lopri on release NetApp Release 6.1.2R2 on Wed');
INSERT INTO History VALUES (104,30,5,3,NULL,'2002-09-26','11:05:30',2,NULL,NULL,NULL,NULL,'Felet berodde p� en bug i ontap.\n\nBug Number/Title: 43870/Panic due to incorrect setup of the inode\'s lock record.\n\nProblem Category: WAFL\n\nRecommended Solution/Workaround: Upgrade to 6.1.3R2');
INSERT INTO History VALUES (105,31,5,3,NULL,'2002-07-16','15:32:39',1,NULL,NULL,NULL,NULL,'Maskinen �r installerad med JumpStart.\n\nDet gick inte att byta EPROM.');
INSERT INTO History VALUES (106,31,5,3,NULL,'2002-07-23','17:34:01',1,NULL,NULL,NULL,NULL,'Maskinen anv�nds f�r att testa loggning. Syslog �r utbytt mot syslog-ng, med startscript och allt.');
INSERT INTO History VALUES (107,31,5,3,NULL,'2002-08-08','14:53:19',1,NULL,NULL,NULL,NULL,'Syslog-ng loggar ocks� till esbb11, som skall bli permanent loghost i gul milj�.');
INSERT INTO History VALUES (108,32,5,3,NULL,'2002-08-15','17:06:34',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (109,33,5,3,NULL,'2002-08-14','09:06:12',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (110,33,5,3,NULL,'2002-08-14','12:36:40',1,NULL,NULL,NULL,NULL,'T�mde skrivark�n. Lp kr�ktes felmeddelanden:\n\nAug 14 12:35:43 js2/js2 printd[12610]: [ID 773474 lpr.error] map_in_file(dfA012js2) - mmap:Not enough space');
INSERT INTO History VALUES (111,33,5,3,NULL,'2002-09-08','17:31:55',2,NULL,NULL,NULL,NULL,'Installerat Solaris 8 recommeded patch cluster.');
INSERT INTO History VALUES (112,34,5,3,NULL,'2002-08-09','16:02:38',1,NULL,NULL,NULL,NULL,'HUP:ade syslogd s� att den loggar mot nya loghost (esbb11).');
INSERT INTO History VALUES (113,35,5,3,NULL,'2002-08-15','17:13:07',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (114,35,5,3,NULL,'2002-08-15','17:13:59',1,NULL,NULL,NULL,NULL,'HUP:ade ntpdate, som f�tt spader och kr�ktes i loggen. Spader har den haft sedan 1 juni. SIGHUP var allt som beh�vdes f�r att l�sa problemet.\n\nT�nk vad mycket man f�r veta n�r man b�rjar l�sa loggar! :-)');
INSERT INTO History VALUES (115,36,5,3,NULL,'2002-08-14','13:54:25',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (116,37,5,3,NULL,'2002-08-30','15:02:04',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (117,38,5,3,NULL,'2002-09-03','16:02:24',4,NULL,NULL,NULL,NULL,'Uppdatering till build 38f av NAVCE. (ig�r)\n\nKopierade upp den befintliga GRC.dat till dfs... Den var gammal och hade ej de schemalagda scanningarna.');
INSERT INTO History VALUES (118,38,5,3,NULL,'2002-09-06','13:56:10',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (119,38,5,3,NULL,'2002-10-23','15:13:50',7,NULL,NULL,NULL,NULL,'Lagt till 2 AT jobb som k�rs varje dag, kl 17.00 och 17.10. Dessa jobb l�gger �ver 2 filer p� \\\\kimdbc01\\pcinfo och inneh�ller: anv�ndare kontra grupp. AT jobben heter \"addusers_esbmd\" och \"addusers_avionet\" och k�rs av 2 funktions konton, esbmd\\fsblogg och avionet\\fsblogg. Filena f�r namnet: Dump from \"dom�n\" on \"datum\".txt.');
INSERT INTO History VALUES (120,39,5,3,NULL,'2002-09-06','13:57:27',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (121,39,5,3,NULL,'2002-09-19','16:08:41',4,NULL,NULL,NULL,NULL,'�tg�rdade fel i Eventviewern. Se http://support.microsoft.com/default.aspx?scid=kb;en-us;q267831\n\n/Daka + Neffler');
INSERT INTO History VALUES (122,39,5,3,NULL,'2002-09-19','16:16:42',4,NULL,NULL,NULL,NULL,'Licenser k�ptes till (Windows 2000 Server), s� att skriken tystnade i Eventviewern. Daka + Neffler');
INSERT INTO History VALUES (123,39,5,3,NULL,'2002-09-19','16:52:55',4,NULL,NULL,NULL,NULL,'Licenser k�ptes till (Windows 2000 Server), s� att skriken tystnade i Eventviewern. Daka + Neffler');
INSERT INTO History VALUES (124,40,5,3,NULL,'2002-09-06','13:57:36',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (125,41,5,3,NULL,'2002-09-06','14:07:07',4,NULL,NULL,NULL,NULL,'Uppdaterat Compaq-agenterna.');
INSERT INTO History VALUES (126,41,5,3,NULL,'2002-09-09','10:27:51',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (127,41,5,3,NULL,'2002-09-09','10:27:55',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (128,41,5,3,NULL,'2002-09-26','15:30:05',4,NULL,NULL,NULL,NULL,'Uppgradering av Doors till 6 fr�n 5.2');
INSERT INTO History VALUES (129,41,5,3,NULL,'2002-10-23','15:16:30',7,NULL,NULL,NULL,NULL,'Kimdbc01 f�r 2 filer varje dag fr�n kiav01, dessa filer �r dumpar av grupper kontra konton fr�n esbmd och avionet. filerna hamnar p� \\\\kimdbc01\\pcinfo.');
INSERT INTO History VALUES (130,43,5,3,NULL,'2002-09-09','10:33:41',5,NULL,NULL,NULL,NULL,'Omstart 020809');
INSERT INTO History VALUES (131,43,5,3,NULL,'2002-09-09','10:33:47',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (132,44,5,3,NULL,'2002-09-09','10:34:16',5,NULL,NULL,NULL,NULL,'omstart 020809');
INSERT INTO History VALUES (133,44,5,3,NULL,'2002-09-09','10:34:21',5,NULL,NULL,NULL,NULL,'Uppdaterat Etrust Virus def 020809');
INSERT INTO History VALUES (134,45,5,3,NULL,'2002-08-19','09:46:47',8,NULL,NULL,NULL,NULL,'2002-08-16 Applikationsloggen full. Servern startades om samt inst�llningarna f�r applikationsloggen �ndrades till \'Overwrite events as needed\'.');
INSERT INTO History VALUES (135,45,5,3,NULL,'2002-09-19','16:09:07',4,NULL,NULL,NULL,NULL,'�tg�rdade fel i Eventviewern. Se http://support.microsoft.com/default.aspx?scid=kb;en-us;q267831\n\n/Daka + Neffler');
INSERT INTO History VALUES (136,45,5,3,NULL,'2002-10-03','13:20:58',8,NULL,NULL,NULL,NULL,'Uppdaterade manuellt Nortons konfigurationsfil GRC.DAT till katalogen \\\\kipr01\\dfs\\progdist\\BU\\NortonGRC\\kista.\n\nFotnot: Det �r ju h�rifr�n som GRC.DAT h�mtas av AvioNets inloggningsskript f�r inkopiering p� anv�ndarnas dator vid varje inloggning.');
INSERT INTO History VALUES (137,46,5,3,NULL,'2002-08-08','16:08:17',5,NULL,NULL,NULL,NULL,'Insight manager agent 5.40');
INSERT INTO History VALUES (138,47,5,3,NULL,'2002-07-26','14:25:46',2,NULL,NULL,NULL,NULL,'F�rbereder uppgradering av Compaq insight manager XE 2.0, till version Compaq insight manager 7 SP1. Uppgradering g�rs f�r att det �r en allvarlig s�kerhetsbugg i SNMP-implementationen i Compaq insight manager XE 2.0.\n\nUppgraderingen kr�ver Windows NT service pack 6a eller h�gre.');
INSERT INTO History VALUES (139,47,5,3,NULL,'2002-08-16','10:13:53',4,NULL,NULL,NULL,NULL,'Stoppade Licens Logging Service. Pga fel i loggar.');
INSERT INTO History VALUES (140,47,5,3,NULL,'2002-09-26','15:37:32',4,NULL,NULL,NULL,NULL,'Var tvungen att starta om, hade h�ngt sig. �vervakningen fungerade ej.');
INSERT INTO History VALUES (141,48,5,3,NULL,'2002-09-08','17:49:15',9,NULL,NULL,NULL,NULL,'Installerat Allegro Free Viewer 13.5 p� servern, till�ggsapplikation best�lld av H�kan Sengoltz');
INSERT INTO History VALUES (142,48,5,3,NULL,'2002-09-08','17:49:39',9,NULL,NULL,NULL,NULL,'Skapat ny applikation group f�r Allegro Viewer');
INSERT INTO History VALUES (143,48,5,3,NULL,'2002-09-08','17:50:32',9,NULL,NULL,NULL,NULL,'Servern omstartat kl 17.06');
INSERT INTO History VALUES (144,48,5,3,NULL,'2002-09-19','10:12:52',9,NULL,NULL,NULL,NULL,'Bytt f�rsta hemsidan till iww.avionics.saab.se f�r IE.');
INSERT INTO History VALUES (145,49,5,3,NULL,'2002-08-20','10:02:52',9,NULL,NULL,NULL,NULL,'2002.08.19 kl 17.30 st�ngde av m�jligheten f�r nya inloggningar p� servern. Servern skall ominstalleras p� tisdag 20/8.');
INSERT INTO History VALUES (146,49,5,3,NULL,'2002-08-22','18:59:38',9,NULL,NULL,NULL,NULL,'Servern ominstallerad med win2000 ts och flyttad till Avionet dom�nen. Applikationsserver f�r Avionet TS. Ej helt klar �n.');
INSERT INTO History VALUES (147,49,5,3,NULL,'2002-09-05','08:52:13',5,NULL,NULL,NULL,NULL,'Utg�tt med omedelbar verkan. Kommer upp i ny skepnad i form av KITS02');
INSERT INTO History VALUES (148,50,5,3,NULL,'2002-08-22','18:56:45',9,NULL,NULL,NULL,NULL,'ICA browser service omstartad.');
INSERT INTO History VALUES (149,50,5,3,NULL,'2002-08-22','18:58:32',9,NULL,NULL,NULL,NULL,'Skapat ny application grupp, Verdi, som till�ter alla ts-users att k�ra Verdi fr�n kirdts01. Testat. Div sm� felmeddelande ang java f�s av anv�ndarna vid start. Fels�kning forts�tter p� fredag.');
INSERT INTO History VALUES (150,50,5,3,NULL,'2002-09-08','17:48:18',9,NULL,NULL,NULL,NULL,'Servern omstartat 17.36');
INSERT INTO History VALUES (151,50,5,3,NULL,'2002-09-19','10:13:32',9,NULL,NULL,NULL,NULL,'Bytt hemsida till iww.avionics.saab.se');
INSERT INTO History VALUES (152,51,5,3,NULL,'2002-09-19','10:13:56',9,NULL,NULL,NULL,NULL,'Bytt hemsida till iww.avionics.saab.se');
INSERT INTO History VALUES (153,52,5,3,NULL,'2002-09-19','10:14:15',9,NULL,NULL,NULL,NULL,'Bytt hemsida till iww.avionics.saab.se');
INSERT INTO History VALUES (154,53,5,3,NULL,'2002-09-19','10:14:52',9,NULL,NULL,NULL,NULL,'Installerat Budgetsystem (BBU)');
INSERT INTO History VALUES (155,54,5,3,NULL,'2002-10-01','15:14:47',4,NULL,NULL,NULL,NULL,'Flyttade loggarna som skapas av IIS fr�n C:\\WINNT\\system32\\LogFiles till e:\\inetpub\\logs\\\n\nAlla nya loggar som genereras l�ggs �ven d�r.');
INSERT INTO History VALUES (156,54,5,3,NULL,'2002-10-01','15:15:51',4,NULL,NULL,NULL,NULL,'�ndrade antalet sidor som ska ska sparas i cache fr�n 500 till 1500 i systeminst�llningarna p� EpiServer p.g.a. internwebben g�tt lusigt. (Daka + Jone) 30 Sept...');
INSERT INTO History VALUES (157,54,5,3,NULL,'2002-10-01','15:19:18',4,NULL,NULL,NULL,NULL,'�ndrade vad som ska loggas via IIS, kopierade inst�llningarna ifr�n ESEKINT245.');
INSERT INTO History VALUES (158,55,5,3,NULL,'2002-08-14','10:58:52',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (159,56,5,3,NULL,'2002-09-08','17:33:44',2,NULL,NULL,NULL,NULL,'Installerat Solaris 2.6 recommeded patch cluster.');
INSERT INTO History VALUES (160,57,5,3,NULL,'2002-08-22','10:51:56',1,NULL,NULL,NULL,NULL,'/var/tmp hade fel permissions (ej skrivbar f�r alla). Fixat.');
INSERT INTO History VALUES (161,57,5,3,NULL,'2002-08-23','10:16:49',1,NULL,NULL,NULL,NULL,'Installerade ett startscript f�r att starta Perforce server p� uppdrag av D/KM Peter Friberg (qsbpef). Scriptet �r installerat som /etc/init.d/perforce och l�nkat till /etc/rc2.d/S99perforce.\n\nPerforce �r ett CM-system. Perforce-servern (p4d) svarar p� fr�gor fr�n klienterna (p4) och ger dem de filer de beh�ver. Det liknar CVS pserver.');
INSERT INTO History VALUES (162,58,5,3,NULL,'2002-08-30','14:39:09',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (163,59,5,3,NULL,'2002-08-13','16:18:30',1,NULL,NULL,NULL,NULL,'Syslog loggar till loghost (esbb11).');
INSERT INTO History VALUES (164,59,5,3,NULL,'2002-09-08','17:31:14',2,NULL,NULL,NULL,NULL,'Installerat Solaris 2.6 recommeded patch cluster.');
INSERT INTO History VALUES (165,59,5,3,NULL,'2002-09-18','10:10:13',2,NULL,NULL,NULL,NULL,'Mothra gick ner i monitorl�ger ig�r kv�ll. Enligt loggarna �r det ett minnesfel. Felanm�lt till Sun.');
INSERT INTO History VALUES (166,59,5,3,NULL,'2002-09-20','10:55:08',2,NULL,NULL,NULL,NULL,'Sun var h�r i onsdags och bytte CPU1 kl 14:45.');
INSERT INTO History VALUES (167,60,5,3,NULL,'2002-08-30','14:59:10',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (168,61,5,3,NULL,'2002-08-30','14:52:05',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (169,61,5,3,NULL,'2002-09-16','12:24:38',2,NULL,NULL,NULL,NULL,'Uppgraderat OBP enligt instruktion fr�n Sun Microstystems. �ven lagt p� Solaris 2.6 Recommended patch cluster.');
INSERT INTO History VALUES (170,62,5,3,NULL,'2002-08-30','15:04:47',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (171,63,5,3,NULL,'2002-08-30','14:31:41',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (172,64,5,3,NULL,'2002-08-30','14:24:57',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (173,65,5,3,NULL,'2002-08-14','13:50:26',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (174,66,5,3,NULL,'2002-08-15','16:46:33',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (175,67,5,3,NULL,'2002-08-30','14:47:16',1,NULL,NULL,NULL,NULL,'Syslog loggar *.debug till loghost (esbb11).');
INSERT INTO History VALUES (176,67,5,3,NULL,'2002-08-30','14:47:33',1,NULL,NULL,NULL,NULL,'Ntpdate skenade sedan 1 juni. Fixat med kill -HUP.');
INSERT INTO History VALUES (178,1,-1,0,1,'2003-02-28','08:39:41',11,NULL,NULL,NULL,NULL,'testregistrering i demo syfte, bortse fr�n denna �nringsbeg�ran. /qsbpett');
INSERT INTO History VALUES (179,10,5,3,NULL,'2002-09-06','19:38:51',11,NULL,NULL,NULL,NULL,'lagt till en parallell installation av ncbridge 4.0 under /export�/ncd_kas . Installationen �r pga att TS anv�ndare ska migreras till AVIONET och x-terminal installationen kan bara peka mot 1st server eller publicerad applikation. Har �ven patchat upp till rel 11. Har �ven skapat �nnu ett makro i dhcp managern, os.500_kas.');
INSERT INTO History VALUES (180,19,5,3,NULL,'2003-01-10','07:54:15',4,NULL,NULL,NULL,NULL,'Bootade om, hade h�ngt sig. (Daka + Mogge)');
INSERT INTO History VALUES (181,20,5,3,NULL,'2003-01-03','13:01:57',2,NULL,NULL,NULL,NULL,'Servern hade d�tt den 30/12. Omstartade av Kaarina och Morgan.');
INSERT INTO History VALUES (182,21,5,3,NULL,'2002-11-08','14:57:22',4,NULL,NULL,NULL,NULL,'CqWeb lagd i eget minnesutrymme f�r att undvika att inetinfo.exe stoppas och inte kan startas.');
INSERT INTO History VALUES (183,21,5,3,NULL,'2003-01-15','14:11:47',3,NULL,NULL,NULL,NULL,'Datorn bootat om 10:15, vet ej varf�r �nnu. Vi har startat upp den 11:55. Detta p�verkade surfningen f�r alla som sitter i ESBMD och Unix milj�n.');
INSERT INTO History VALUES (184,21,5,3,NULL,'2003-02-17','09:36:32',4,NULL,NULL,NULL,NULL,'Startade om servern pga IISadmin tj�nsten hade h�ngt sig. ClearQuest via webb och surfning f�r ESBMD-maskiner stannade. Nere i ca 10 minuter. 13/2');
INSERT INTO History VALUES (185,30,5,3,NULL,'2003-01-14','17:38:09',2,NULL,NULL,NULL,NULL,'Ersatt tv� halvtrasiga diskar i vol3. Disk 8.25 och 8.29');
INSERT INTO History VALUES (186,30,5,3,NULL,'2003-02-26','08:35:37',2,NULL,NULL,NULL,NULL,'Beg�ran fr�n Tomas Stenarson: SGI klienterna i J�rf�lla skall inte kunna montera /vol/vol1/EPSIM p� gullan');
INSERT INTO History VALUES (187,74,5,3,NULL,'2003-01-17','15:45:24',12,NULL,NULL,NULL,NULL,'Test');
INSERT INTO History VALUES (188,38,5,3,NULL,'2003-01-15','10:26:58',7,NULL,NULL,NULL,NULL,'�ndrat client scannings tiden till fredagar kl 01.00 p� KIAV01 och JOAV01.');
INSERT INTO History VALUES (189,40,5,3,NULL,'2002-11-20','08:38:28',4,NULL,NULL,NULL,NULL,'Omstart (ig�r). P.g.a. installation av Oracle-klient. (Daka + Janne Lindqvist)');
INSERT INTO History VALUES (190,40,5,3,NULL,'2002-11-20','10:10:00',4,NULL,NULL,NULL,NULL,'Verifierade backup p� databaser via Legato, En enkel verify, allt gick bra.');
INSERT INTO History VALUES (191,40,5,3,NULL,'2002-11-20','10:10:40',4,NULL,NULL,NULL,NULL,'Tog bort databaser: gforce + ep17vu, lade till CRDBA enligt best�llning av T. Loberg');
INSERT INTO History VALUES (192,40,5,3,NULL,'2003-01-27','16:33:29',4,NULL,NULL,NULL,NULL,'Uppdatering till SQL SP3. 2st omstarter. (Daka + Staffan)');
INSERT INTO History VALUES (193,43,5,3,NULL,'2002-10-24','15:02:08',4,NULL,NULL,NULL,NULL,'Omstart p.g.a. Trend stoppade System Attendant + MTA tj�nsterna.');
INSERT INTO History VALUES (194,43,5,3,NULL,'2002-11-08','15:03:01',4,NULL,NULL,NULL,NULL,'Omstart p.g.a. Trend stoppade System Attendant + MTA tj�nsterna. Gick ej att starta tj�nsterna utan en omstart.');
INSERT INTO History VALUES (195,43,5,3,NULL,'2003-01-13','13:58:09',4,NULL,NULL,NULL,NULL,'Omstart av alla VeritasBackup-servicear..hade h�nt sig.');
INSERT INTO History VALUES (196,43,5,3,NULL,'2003-01-29','08:09:19',4,NULL,NULL,NULL,NULL,'Omstart, seg. (Daka + Crutanen)');
INSERT INTO History VALUES (197,43,5,3,NULL,'2003-01-29','08:09:41',4,NULL,NULL,NULL,NULL,'Omstart, seg. (Daka + Crutanen)');
INSERT INTO History VALUES (198,43,5,3,NULL,'2003-02-24','13:47:34',4,NULL,NULL,NULL,NULL,'Mailtj�nster stannade 21:42 21/2 pga virusattack. Kunde startas igen vid uppt�ckt utan problem 24/2 ca 06:30. Inga andra fel...(Crutanen).');
INSERT INTO History VALUES (199,43,5,3,NULL,'2003-02-24','13:58:07',4,NULL,NULL,NULL,NULL,'Omkonfigurering av antivirusprogram (E-trust) pga stopp vid virusattack. Enl MS Q245822 exkluderades ett antal kataloger i realtids- och schemalagd scanning. Se dokument \\\\kipr01\\e\\Program loader\\Admin\\Supporten\\Kista\\Instruktioner & Tips\\Exkludering av filer i E-trust p� KIMDMX01.txt. Dokumentet finns �ven p� desktop p� KIMDMX01.');
INSERT INTO History VALUES (200,45,5,3,NULL,'2003-02-17','09:40:35',4,NULL,NULL,NULL,NULL,'Startade om servern. Den svarade inte p� nya n�tverkskopplingar n�r anv�ndare loggade p� avionet-dom�nen, vilket resulterade att de inte kunde logga p�. Unders�ker orsaken till detta. Troligen kombination av fragmenterad disk + m�nga �ppna filer p� \\\\kipr01\\e$ Anv�ndare som redan var inloggade drabbades ej, bara personer som f�rs�kte logga p�. Servern nere ca 10 minuter, 13/2.');
INSERT INTO History VALUES (201,47,5,3,NULL,'2003-02-27','13:43:30',4,NULL,NULL,NULL,NULL,'Lade till \\\\gullan\\ntusers3 i virusscanningen, exkluderade \\\\gullan\\ntusers3\\~snapshot. (KIRDMG01 scannar hemkataloger + mera p� Gullan).');
INSERT INTO History VALUES (202,48,5,3,NULL,'2002-12-09','09:26:25',9,NULL,NULL,NULL,NULL,'Konfigurerat E-trust s� att uppdateringar h�mtas fr�n server enligt standard p� gul. Omstart av server.');
INSERT INTO History VALUES (203,50,5,3,NULL,'2002-12-09','09:27:22',9,NULL,NULL,NULL,NULL,'Konfigurat e-trust s� att uppdateringar h�mtas fr�n standard st�lle p� gul n�t. Omboot av server.');
INSERT INTO History VALUES (204,51,5,3,NULL,'2002-12-09','09:27:32',9,NULL,NULL,NULL,NULL,'Konfigurat e-trust s� att uppdateringar h�mtas fr�n standard st�lle p� gul n�t. Omboot av server.');
INSERT INTO History VALUES (205,52,5,3,NULL,'2002-12-09','09:27:53',9,NULL,NULL,NULL,NULL,'Konfigurat e-trust s� att uppdateringar h�mtas fr�n standard st�lle p� gul n�t.');
INSERT INTO History VALUES (206,54,5,3,NULL,'2002-10-25','16:09:57',4,NULL,NULL,NULL,NULL,'25/10 omstart p.g.a. fel i EpiServer, som senare �tg�rdades av Elektropost.');
INSERT INTO History VALUES (207,54,5,3,NULL,'2002-12-12','08:33:25',9,NULL,NULL,NULL,NULL,'Christine startade om IIS. Det gick inte att komma �t intranet, externt fungerade det ok, proxy pac var ok.');
INSERT INTO History VALUES (208,54,5,3,NULL,'2003-01-13','13:57:23',4,NULL,NULL,NULL,NULL,'Startade om IIS. Det gick inte att komma �t intranet..');
INSERT INTO History VALUES (209,54,5,3,NULL,'2003-01-27','16:33:42',4,NULL,NULL,NULL,NULL,'Uppdatering till SQL SP3. 2st omstarter. (Daka + Staffan)');
INSERT INTO History VALUES (210,54,5,3,NULL,'2003-02-11','07:53:06',3,NULL,NULL,NULL,NULL,'IIS tj�nsten omstartad, gick ej att komma �t internwebben. Fick samtal fr�n helpdesk 07.40, fungerade igen 07.45.');
INSERT INTO History VALUES (211,38,4,3,NULL,'2003-03-02','09:56:41',4,NULL,NULL,NULL,NULL,'Installation av SP3.');
INSERT INTO History VALUES (212,39,4,3,NULL,'2003-03-02','09:57:18',4,NULL,NULL,NULL,NULL,'Installation av SP3.');
INSERT INTO History VALUES (213,40,4,3,NULL,'2003-03-02','09:57:39',4,NULL,NULL,NULL,NULL,'Installation av SP3.');
INSERT INTO History VALUES (214,40,1,3,NULL,'2003-03-02','09:58:25',4,NULL,NULL,NULL,NULL,'');
INSERT INTO History VALUES (215,45,4,3,NULL,'2003-03-02','09:59:10',4,NULL,NULL,NULL,NULL,'Installation av SP3.');
INSERT INTO History VALUES (216,52,4,3,NULL,'2003-03-02','09:59:44',4,NULL,NULL,NULL,NULL,'Installation av SP3.');
INSERT INTO History VALUES (217,53,4,3,NULL,'2003-03-02','10:00:04',4,NULL,NULL,NULL,NULL,'Installation av SP3.');
INSERT INTO History VALUES (218,54,4,3,NULL,'2003-03-02','10:00:23',4,NULL,NULL,NULL,NULL,'Installation av SP3.');
INSERT INTO History VALUES (219,21,5,3,NULL,'2003-03-01','10:00:59',4,NULL,NULL,NULL,NULL,'Byte av h�rdvara, trasig chassie-fl�kt.');
INSERT INTO History VALUES (220,76,1,3,NULL,'2003-03-01','10:03:30',4,NULL,NULL,NULL,NULL,'Installation av RCO-klient.');
INSERT INTO History VALUES (221,77,1,3,NULL,'2003-03-01','10:04:01',4,NULL,NULL,NULL,NULL,'Installation av RCO-klient.');
INSERT INTO History VALUES (222,39,1,3,NULL,'2003-03-02','10:05:01',4,NULL,NULL,NULL,NULL,'Avinstallation av Wins.');
INSERT INTO History VALUES (223,39,1,3,NULL,'2003-03-03','15:17:34',7,NULL,NULL,NULL,NULL,'Lagt till och aktiverat ett DHCP scope \"LabbN�t\" 172.17.64.100-200, detta scope skall t�cka upp de klienter som beh�ver ett n�golunda frist�ende n�t f�r maskiner som ligger i ERDSE06 dom�nen, d� de minimalt skall p�verka �vriga maskiner p� andra n�t.');
INSERT INTO History VALUES (224,21,2,3,NULL,'2003-03-06','10:14:28',4,NULL,NULL,NULL,NULL,'Pga felaktig konfigurering av eventloggen fylldes \"Application log\" och chokade servern. Detta ledde till att clearquest webbverktyg inte fungerade. Var tvungen att starta om. Nere i ca 4 minuter.');
INSERT INTO History VALUES (225,54,1,3,NULL,'2003-03-13','07:42:06',7,NULL,NULL,NULL,NULL,'Startat om IIS tj�nsten, d� den kr�vde l�sen f�r klienterna d� de bes�ker Intran�tet.');
INSERT INTO History VALUES (226,45,1,3,NULL,'2003-03-26','17:17:21',5,NULL,NULL,NULL,NULL,'Startade om servern. Den svarade inte DFS eller E$.');
INSERT INTO History VALUES (227,54,1,3,NULL,'2003-04-08','13:01:14',4,NULL,NULL,NULL,NULL,'Uppgraderade EpiServer till 3.50 + installerade en processor till + ytterligare 1Gb RAM-minne. Nere ca 15 minuter.');
INSERT INTO History VALUES (228,38,2,3,NULL,'2003-04-08','13:02:53',4,NULL,NULL,NULL,NULL,'Byte av \"Power Supply\". Nere 4 timmar.');
INSERT INTO History VALUES (229,54,1,3,NULL,'2003-04-14','07:42:59',7,NULL,NULL,NULL,NULL,'Startat om IIS tj�nsten, d� den kr�vde l�sen f�r klienterna d� de bes�ker Intran�tet.');
INSERT INTO History VALUES (230,45,1,3,NULL,'2003-05-06','09:30:16',4,NULL,NULL,NULL,NULL,'Lade till w2k.saabtech.se i proxy.pac p� uppdrag av Christine Rutanen.');
INSERT INTO History VALUES (231,43,5,3,NULL,'2003-05-16','13:24:25',3,NULL,NULL,NULL,NULL,'Omstart av servern. Den hade l�st sig. Nere ca 10 min vid omstart.');
INSERT INTO History VALUES (232,9,1,3,NULL,'2003-07-01','16:39:49',1,NULL,NULL,NULL,NULL,'Lade till /sw/gnat i NIS-mappen auto_sw (se /var/yp/automount).');
INSERT INTO History VALUES (233,30,1,3,NULL,'2003-07-01','16:40:42',1,NULL,NULL,NULL,NULL,'Skapade katalogen /vol1/sw/gnat och satte �garskap till esbkjem (Kjell Mesch).');
INSERT INTO History VALUES (234,11,1,3,NULL,'2003-07-10','08:47:55',1,NULL,NULL,NULL,NULL,'Aktiverade echo-tj�nsten (UDP/TCP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (235,12,1,3,NULL,'2003-07-10','08:59:22',1,NULL,NULL,NULL,NULL,'Aktiverade echo-tj�nsten (TCP/UDP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (236,13,1,3,NULL,'2003-07-10','09:53:32',1,NULL,NULL,NULL,NULL,'Aktiverade echo (TCP/UDP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (237,79,1,3,NULL,'2003-07-10','09:56:42',1,NULL,NULL,NULL,NULL,'Aktiverade echo (TCP/UDP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (238,9,1,3,NULL,'2003-07-10','10:00:16',1,NULL,NULL,NULL,NULL,'Aktiverade echo (TCP/UDP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (239,29,1,3,NULL,'2003-07-10','13:34:21',1,NULL,NULL,NULL,NULL,'Aktiverade echo (TCP/UDP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (240,8,1,3,NULL,'2003-07-10','13:35:00',1,NULL,NULL,NULL,NULL,'Aktiverade echo (UDP/TCP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (241,10,1,3,NULL,'2003-07-10','14:24:07',1,NULL,NULL,NULL,NULL,'Aktiverade echo (UDP/TCP port 7) i /etc/inetd.conf.');
INSERT INTO History VALUES (242,54,2,3,NULL,'2003-07-14','07:45:24',7,NULL,NULL,NULL,NULL,'Startade om IIS-tj�nsten, den hade h�ngt sig.');
INSERT INTO History VALUES (243,43,2,3,NULL,'2003-07-28','15:23:55',4,NULL,NULL,NULL,NULL,'Ett par servicear gick ner och stoppade all mailtrafik. Startade dom igen, allt funkar. ');
INSERT INTO History VALUES (244,9,1,3,NULL,'2003-08-07','15:12:01',1,NULL,NULL,NULL,NULL,'En del rader i /etc/group �r f�r l�nga, vilket st�ller till problem. Jag splittade gruppen g4s i tv� grupper (g4s och g4s2) med samma GID. Enligt uppgift kan detta st�lla till problem f�r ClearCase, men det �terst�r att se. :-)');

--
-- Dumping data for table 'HistoryType'
--


INSERT INTO HistoryType VALUES (1,'Konfigurationsf�r�ndring');
INSERT INTO HistoryType VALUES (2,'Avvikelserapportering');
INSERT INTO HistoryType VALUES (0,'(odefinierad)');
INSERT INTO HistoryType VALUES (4,'Programuppdatering');
INSERT INTO HistoryType VALUES (5,'Information');
INSERT INTO HistoryType VALUES (-1,'�ndringsbeg�ran');

--
-- Dumping data for table 'ImportanceLevel'
--


INSERT INTO ImportanceLevel VALUES (0,'Ointressant');

--
-- Dumping data for table 'Inventory'
--


INSERT INTO Inventory VALUES (1,'akane',1,NULL,'172.17.65.212','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (2,'al',1,NULL,'172.17.65.204','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (3,'anansi',1,'http (www.esb.ericsson.se)','172.17.65.108','Dell -pc','RedHat Linux 5.1');
INSERT INTO Inventory VALUES (4,'athena',1,'NIS, DNS f�r Mj�rdevi','172.17.76.190','Sun Ultra-1','Solaris 2.6');
INSERT INTO Inventory VALUES (5,'bounce',1,'Studsar ekonomidata mellan Ericsson och Saab .Samt tidrapporteringsdata till SAP/R3.','172.17.65.16','Dell - pc','RedHat Linux 5.1');
INSERT INTO Inventory VALUES (6,'bounty',1,'EDA','172.17.65.104','HP 9000/715/F','HP-UX B.10.20');
INSERT INTO Inventory VALUES (7,'cindy',1,NULL,'172.17.65.139','HP 9000/715/75','HP-UX B.10.20');
INSERT INTO Inventory VALUES (8,'cl01',1,'ber�knings-maskin (belastning)','172.17.65.110','Sun Ultra 10','Solaris 8');
INSERT INTO Inventory VALUES (9,'esbb11',1,'NIS-master, DNS-server, loghost.','172.17.65.77','Sun Netra T1 105','Solaris 8');
INSERT INTO Inventory VALUES (10,'esbb12',1,'DHCP samt agerar som printserver','172.17.65.78','Sun netra T1 105','Solaris 8');
INSERT INTO Inventory VALUES (11,'esbb14',1,'license-server =master ,  NTP, DNS slav','172.17.65.79','Sun netra T1 105','Solaris 8');
INSERT INTO Inventory VALUES (12,'esbb15',1,'license -server =slave, NTP, DNS slav','172.17.65.80','Sun netra T1 105','Solaris 8');
INSERT INTO Inventory VALUES (13,'esbb16',1,'license -server =slave, NTP, DNS slav','172.17.65.81','Sun netra T1 105','Solaris 8');
INSERT INTO Inventory VALUES (14,'esbb1',1,'Backup-server med LegatoNetworker','172.17.65.32','Sun enterprise 220R','Solaris 7');
INSERT INTO Inventory VALUES (15,'esbb2',1,'jump-server','172.17.65.102','Sun Ultra 5','Solaris 8');
INSERT INTO Inventory VALUES (16,'ESEKINT150',1,'Printserver','172.17.68.10','Proliant 5000','NT 4.0');
INSERT INTO Inventory VALUES (17,'ESEKINT151',1,'Gammal hemkatalog- och gruppkatalogserver','172.17.68.11','Proliant 5000','NT 4.0');
INSERT INTO Inventory VALUES (18,'ESEKINT152',1,'BDC','172.17.68.12','Proliant 4500','NT 4.0');
INSERT INTO Inventory VALUES (19,'ESEKINT153',1,'SQL Server','172.17.68.13','Proliant 5500','NT 4.0');
INSERT INTO Inventory VALUES (20,'ESEKINT154',1,'Oracle Server','172.17.68.14','Proliant 2500','NT 4.0');
INSERT INTO Inventory VALUES (21,'ESEKINT245',1,'Gamla Internwebben','172.17.68.24','Proliant 5500','NT 4.0');
INSERT INTO Inventory VALUES (22,'fester',1,NULL,'172.17.65.197','Sun Ultra 2','Solaris 2.6');
INSERT INTO Inventory VALUES (23,'fladdermus',1,'Testmaskin f�r CSC.','172.17.65.112','Sun Ultra 10','Solaris 8');
INSERT INTO Inventory VALUES (24,'fnatte',1,'EDA','172.17.65.22','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (25,'garvin',1,'EPSIM','172.17.64.52','SGI','IRIX 6.5');
INSERT INTO Inventory VALUES (26,'gcc01',1,'clearcase vob+vy-server','172.17.65.20','Sun enterprise 250','Solaris 7');
INSERT INTO Inventory VALUES (27,'godzilla',1,'lp, dns, nis, loghost, mailhost, nfs, adminhost gullan, gaprint, licenser (perforce, exco, rational, flexlm, hpads, framemaker), ftp, tftp, xfs, http, postgres','172.17.65.8','Sun enterprise 3000','Solaris 2.6');
INSERT INTO Inventory VALUES (28,'gomez',1,NULL,'172.17.65.195','Sun Ultra 2','Solaris 2.6');
INSERT INTO Inventory VALUES (29,'gorgo',1,'login-server f�r EDA','172.17.65.31','Sun enterprise 250','Solaris 2.6');
INSERT INTO Inventory VALUES (30,'gullan',1,'Filserver f�r Unix och NT','172.17.65.60','NetApp F760','Ontap 6.3.1R1');
INSERT INTO Inventory VALUES (31,'jefferson',1,'Testmaskin f�r loggning.','172.17.65.213','Sun Ultra 5','Solaris 8');
INSERT INTO Inventory VALUES (32,'joakim',1,NULL,'172.17.65.8','Sun Ultra 5/10','Solaris 2.5.1');
INSERT INTO Inventory VALUES (33,'js2',1,'Testmaskin f�r ASA','172.17.65.75','Sun Blade 100','Solaris 8');
INSERT INTO Inventory VALUES (34,'k2',1,'login-server, skall termineras!','172.17.65.10','HP 9000/879/k260','HP-UX 10.20');
INSERT INTO Inventory VALUES (35,'kajsa',1,NULL,'172.17.65.25','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (36,'kalle',1,'EDA','172.17.65.24','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (37,'kelly',1,NULL,'172.17.65.206','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (38,'KIAV01',1,'Antivirus Server','172.17.69.14','Proliant DL 360','Windows 2000 Server');
INSERT INTO Inventory VALUES (39,'KIDC01',1,'DC AvioNet','172.17.69.10','Proliant DL 380','Windows 2000 Advanced Server');
INSERT INTO Inventory VALUES (40,'KIDS01',1,'SQL Server (2000)','172.17.69.15','Proliant DL 380 G2','Windows 2000 Server');
INSERT INTO Inventory VALUES (41,'KIMDBC01',1,'BDC','172.17.68.21','Proliant 5500','NT 4.0');
INSERT INTO Inventory VALUES (42,'KIMDMS01',1,'SMS Server','172.17.68.22','Proliant 5500','NT 4.0');
INSERT INTO Inventory VALUES (43,'KIMDMX01',1,'Exchange 5.5 (Mail)','172.17.68.23','Proliant 7000','NT 4.0');
INSERT INTO Inventory VALUES (44,'KIMDPC01',1,'PDC','172.17.68.20','Proliant 5500','NT 4.0');
INSERT INTO Inventory VALUES (45,'KIPR01',1,'DFS-server AvioNet','172.17.69.11','Proliant DL 380','Windows 2000 Server');
INSERT INTO Inventory VALUES (46,'KIRDBC01',1,'BDC / Support','172.17.68.15','Proliant ML 570','NT 4.0');
INSERT INTO Inventory VALUES (47,'KIRDMG01',1,'�vervakning; Compaq Insight Manager XE 7.0','172.17.68.16','Proliant DL 380','NT 4.0');
INSERT INTO Inventory VALUES (48,'KIRDTS01',1,'Terminal Server. (Appl)','172.17.70.3','Proliant 5500','NT 4.0 Terminal Server');
INSERT INTO Inventory VALUES (49,'KIRDTS02',1,'Terminal Server. (Bas)','172.17.70.5','Proliant DL 380','NT 4.0 Terminal Server');
INSERT INTO Inventory VALUES (50,'KIRDTS03',1,'Terminal Server. (Bas)','172.17.70.6','Proliant DL 380','NT 4.0 Terminal Server');
INSERT INTO Inventory VALUES (51,'KIRDTS04',1,'Terminal Server. (Bas)','172.17.70.7','Proliant 5500','NT 4.0 Terminal Server');
INSERT INTO Inventory VALUES (52,'KITS01',1,'Terminal Server (2000)','172.17.69.13','Proliant DL 380 G2','Windows 2000 Server');
INSERT INTO Inventory VALUES (53,'KITS02',1,'Teminal Server (Appl)','172.17.69.16','Proliant DL 38','Windows 2000 Terminal Server');
INSERT INTO Inventory VALUES (54,'KIWS01',1,'Nya internwebben,  EpiServer','172.17.69.12','Proliant DL 360','Windows 2000 Server');
INSERT INTO Inventory VALUES (55,'knatte',1,'EDA','172.17.65.21','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (56,'laban',1,'firewall-1','172.17.95.18','Sun Ultra 10','Solaris 2.6');
INSERT INTO Inventory VALUES (57,'miranda',1,'Perforce','172.17.65.217','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (58,'morticia',1,NULL,'172.17.65.196','Sun Ultra 2','Solaris 2.6');
INSERT INTO Inventory VALUES (59,'mothra',1,'inloggning, license-server','172.17.65.55','Sun Enterprise 250','Solaris 2.6');
INSERT INTO Inventory VALUES (60,'peggy',1,NULL,'172.17.65.205','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (61,'pugsley',1,NULL,'172.17.65.199','Sun Ultra 2','Solaris 2.6');
INSERT INTO Inventory VALUES (62,'ranma',1,NULL,'172.17.65.210','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (63,'razor',1,'EDA','172.17.65.146','Sun SPARC Classic','Solaris 2.6');
INSERT INTO Inventory VALUES (64,'saraswati',1,'IS/IT-support','172.17.65.114','Sun Blade 100','Solaris 8');
INSERT INTO Inventory VALUES (65,'tjatte',1,'EDA','172.17.65.23','Sun Ultra 5/10','Solaris 2.6');
INSERT INTO Inventory VALUES (66,'viggen',1,'ts500','172.17.65.150','HP 9000/715/50','HP-UX B.10.20');
INSERT INTO Inventory VALUES (67,'wednesday',1,NULL,'172.17.65.198','Sun Ultra 2','Solaris 2.6');
INSERT INTO Inventory VALUES (68,'feet',1,'License server (flexlm, LUM) [J�nk�ping]','172.17.80.110','IBM T43P','AIX 4.3.2');
INSERT INTO Inventory VALUES (69,'filer',1,'Filserver i J�nk�pinh, 3 Volymer','172.17.80.41','NetApp F810','Ontap 6.1.2R3');
INSERT INTO Inventory VALUES (70,'idaho',1,'Oracle server f�r pcms','172.17.80.44','Sun Enterprise 220R','Solaris 5.6');
INSERT INTO Inventory VALUES (71,'jkps02',1,'nis+ prim�r  [J�nk�ping]','172.17.80.42','','Sun OS 2.6');
INSERT INTO Inventory VALUES (72,'JODC01',1,'DC i Avionet placerad i J�nk�ping','172.17.80.10','Compaq DL 380','Windows 2000');
INSERT INTO Inventory VALUES (73,'JOPR01',1,'Printer och programladdningsserver i Avionet dom�nen','172.17.80.11','Compaq DL 380','Windows 2000');
INSERT INTO Inventory VALUES (74,'JORDW301',1,'K�r programmet COOKPIT','172.17.80.25','Compaq DL 380','Window 2000');
INSERT INTO Inventory VALUES (75,'JOTS01',1,'Terminal server i J�nk�ping','172.17.82.13','Compaq DL 580','Windows 2000');
INSERT INTO Inventory VALUES (76,'KISAP01',1,'SAP Produktionsserver','172.17.69.17','Proliant DL 380 G3','Windows 2000 Server');
INSERT INTO Inventory VALUES (77,'KISAP02',1,'SAP Utvecklingsserver','172.17.69.18','Proliant DL 380 G3','Windows 2000 Server');
INSERT INTO Inventory VALUES (78,'nevada',1,'backupserver och nis+ sekund�r [J�nk�ping]','172.17.80.43','Sun Enterprise 220R','Solaris 8');
INSERT INTO Inventory VALUES (79,'ga01',1,'inloggningsserver','172.17.65.57','Sun Enterprise 420R','Solaris 8');
INSERT INTO Inventory VALUES (80,'ga01',1,'Ers�ttare f�r mothra, inloggningsserver','172.17.65.57','Sun Fire 280R','Solaris 8');

--
-- Dumping data for table 'InventoryType'
--


INSERT INTO InventoryType VALUES (1,'Server');
INSERT INTO InventoryType VALUES (2,'N�tverksutrustning');
INSERT INTO InventoryType VALUES (3,'Arbetsstation');

--
-- Dumping data for table 'Maintenance'
--


INSERT INTO Maintenance VALUES (0,'(ej planerad)',NULL,NULL);
INSERT INTO Maintenance VALUES (13,'2003-06','2003-06-07','2003-06-08');
INSERT INTO Maintenance VALUES (12,'2003Q2','2003-02-28','2003-03-02');

--
-- Dumping data for table 'Priority'
--


INSERT INTO Priority VALUES (1,'Meningsl�s');
INSERT INTO Priority VALUES (2,'L�g');
INSERT INTO Priority VALUES (3,'Normal');
INSERT INTO Priority VALUES (4,'H�g');
INSERT INTO Priority VALUES (5,'Panik');

--
-- Dumping data for table 'Request'
--


INSERT INTO Request VALUES (1,2,1,2,5,12,11,'2003-02-28',1,NULL,NULL,NULL,NULL,'TEST: Installation av ConfLogger.','Installation av ConfLogger. Det h�r �r bara ett test.');

--
-- Dumping data for table 'RequestLog'
--



--
-- Dumping data for table 'RequestStatus'
--


INSERT INTO RequestStatus VALUES (1,'Ny');
INSERT INTO RequestStatus VALUES (2,'Planerad');
INSERT INTO RequestStatus VALUES (4,'Avslutad');
INSERT INTO RequestStatus VALUES (3,'Godk�nd');
INSERT INTO RequestStatus VALUES (0,'(ok�nd)');

--
-- Dumping data for table 'RequestType'
--


INSERT INTO RequestType VALUES (1,'Konfigurationsf�r�ndring');
INSERT INTO RequestType VALUES (2,'Programinstallation');
INSERT INTO RequestType VALUES (3,'H�rdvara');
INSERT INTO RequestType VALUES (4,'Patchning');

--
-- Dumping data for table 'Session'
--



--
-- Dumping data for table 'User'
--


INSERT INTO User VALUES (1,'qsbstsv','Stefan Svensson','4J9qdZP4rjmU6lfhEX9n7A','kc+Iqz3h4+zTX4f6oxs/Lw',1060261904,1060262133,'136.163.203.4',1,1,1,1,1,1,1,1,1,1,1,1,1);
INSERT INTO User VALUES (2,'qsbtsnn','Tomas Nilsson','xjiAfMM2Ab1KKFpbr4rA6A','e5IkV1w9D7r0FPaP24ByBg',1057826581,1057826780,'146.75.247.241',1,1,1,1,1,1,1,1,0,0,0,1,0);
INSERT INTO User VALUES (3,'qsbchru','Christine Rutanen','iqGsdZWM55z44zwmuLJMtQ','yqu6WPbayESMnCy92FWp5A',1059399325,1059399605,'136.163.203.4',1,1,1,1,1,1,1,1,0,1,1,1,1);
INSERT INTO User VALUES (4,'qsbdaka','Daniel Karlsson','4dLs076U09vJz67bHH29Ow','pJdgGwrN23+6gfIrHvD3Iw',1059398616,1059398688,'136.163.203.3',1,1,1,1,1,1,0,1,0,0,0,1,0);
INSERT INTO User VALUES (5,'qsbjone','Johan Neffler',NULL,NULL,NULL,NULL,NULL,1,1,1,0,0,1,0,1,0,0,0,1,0);
INSERT INTO User VALUES (6,'qsbgdig','G�ran Dign�',NULL,NULL,NULL,NULL,NULL,1,1,1,0,0,1,0,1,0,0,1,1,0);
INSERT INTO User VALUES (7,'qsbmolj','Morgan Ljustermo','qzLiv60QJNPKgv1xGWjGVA','aE7r7x9eL/yw2U/iaGV8fA',1059035938,1059036015,'136.163.203.4',1,1,1,1,1,1,0,1,0,0,0,1,0);
INSERT INTO User VALUES (8,'qsbanre','Anna Regn�s',NULL,NULL,NULL,NULL,NULL,1,1,1,1,1,1,1,1,0,1,0,1,0);
INSERT INTO User VALUES (9,'qsbkaci','Kaarina Cifuentes','QtQXmKo5aieLRE38ijXq4Q','kGc8Po0cwvVjH8RxGmuTrA',1057843835,1057843982,'136.163.203.4',1,1,1,1,1,1,0,1,0,0,0,1,0);
INSERT INTO User VALUES (10,'qsbclgu','Claes Gummesson','nquJNv17k3h3oQOkduZukw',NULL,NULL,NULL,NULL,1,1,1,0,0,1,0,1,1,0,1,1,0);
INSERT INTO User VALUES (11,'qsbpett','Peter Tisell','4NTiToy6UlGkzhsHRFMbyw','8LMz3qbpc/Q46CEM4WSBPA',1050058562,1050058876,'136.163.203.4',1,1,1,1,1,1,0,1,0,1,0,1,1);
INSERT INTO User VALUES (12,'qsafrha','qsafrha',NULL,NULL,NULL,NULL,NULL,1,1,1,0,0,1,0,1,0,0,0,1,0);
INSERT INTO User VALUES (13,'qsbinni','Sven Nilsson','ZI4g2VxBYZBW+DY22QAY+w','MsEEQY3UGzhSc9l8Cd+ERw',1060003669,1060003689,'136.163.203.4',1,1,1,1,1,1,0,1,0,0,0,1,0);

