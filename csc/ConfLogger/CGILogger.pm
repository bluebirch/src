# $Id$
#
# This is a simple package to print nicely formatted error messages to
# the CGI error log file.
#
# $Log$
# Revision 1.3  2002/06/04 14:16:23  stefan
# Added debugmsg and infomsg.
#
# Revision 1.2  2002/04/02 19:34:31  stefan
# Instead of main file, print the calling subroutine in the message
# prefix.
#
# Revision 1.1  2002/02/28 14:11:31  stefan
# First version, featuring a simple "logmsg" function.
#
#
package CGILogger;

use strict;
use warnings;

BEGIN {
    use Exporter();
    our (@ISA, @EXPORT, @EXPORT_OK);
    @ISA = qw(Exporter);
    @EXPORT = qw(logmsg debugmsg infomsg);
    @EXPORT_OK = qw(logmsg debugmsg infomsg);
}
our @EXPORT_OK;

# Stolen from CGI::Carp
sub stamp {
    my $time = scalar(localtime);
    my ($package,undef, undef, $sub) = caller(2);
    return "[$time] $sub: ";
}

sub logmsg {
    my $stamp = stamp;
    print STDERR $stamp,  @_, "\n";
}

sub debugmsg {
    my $stamp = stamp;
    print STDERR $stamp, @_, "\n";
}

sub infomsg {
    print STDERR "[", scalar( localtime ), "] ", @_, "\n";
}

1;

