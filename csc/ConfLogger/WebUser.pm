# $Id$
#
# User Object
#
# $Log$
# Revision 1.4  2002/10/31 16:00:29  stefan
# New method 'checkpermission'.
#
# Revision 1.3  2002/10/30 20:04:58  stefan
# Added fullname method.
#
# Revision 1.2  2002/10/30 19:55:08  stefan
# Added some data access methods.
#
# Revision 1.1  2002/10/30 15:10:03  stefan
# Basic User Authentication object. Stolen from KNForum, but very, very
# simplified.
#

package WebUser;
use strict;
use MIME::Base64;
use Crypt::RC4;
use Storable qw(freeze thaw);

my $AUTH_KEY = 'adspof86wqy4htsalkdjgaakj763';
my $AUTH_TIME = 21600;

sub new {
    my $proto  = shift;
    my $packed = shift;
    my $class = ref($proto) || $proto;
    my $self = {};

    bless( $self, $class );

    if ($packed) {
	$self->unpack( $packed );
    }

    return $self;
}

sub pack {
    my $self = shift;

    # Set timestamp
    $self->{timestamp} = time;

    return $self->encrypt( freeze( $self ) );
}

sub unpack {
    my ($self, $packed) = @_;
    if ($packed) {
	%$self = %{ thaw( $self->decrypt( $packed ) ) };
	return 1;
    }
    else {
	return 0;
    }
}

# Validate session key. If key is valid, load user info and return
# user name (login id). If invalid, return undef.
sub validate_key {
    my ($self, $key) = @_;

    my %key = %{ thaw( $self->decrypt( $key ) ) };

    if ($key{user} && $key{time} && time - $key{time} < $self->{auth_time}) {
	$self->load( $key{user} );
	return $self->user;
    }
    return undef;
}

# Encrypt with RC4 and encode in Base64.
sub encrypt {
    my ($self, $data) = @_;

    return encode_base64( RC4( $AUTH_KEY, $data ), '' );
}

# Decode Base64 and decrypt with RC4.
sub decrypt {
    my ($self, $data) = @_;

    return $data ?  RC4( $AUTH_KEY, decode_base64( $data ) ) : undef;
}

# Check permissions (that is, runmode)
sub checkpermission {
    my ($self, $permission) = @_;

    return ($self->{'Priv_'.lc( $permission )})
}

######################################################################
# Instance data access
#
sub id {
    my $self = shift;
    $self->{ID} = shift if (@_);
    return $self->{ID};
}
sub name {
    my $self = shift;
    $self->{Name} = shift if (@_);
    return $self->{Name};
}
sub fullname {
    my $self = shift;
    $self->{Fullname} = shift if (@_);
    return $self->{Fullname};
}
sub password {
    my $self = shift;
    $self->{Password} = shift if (@_);
    return $self->{Password};
}
sub sessionactivity {
    my $self = shift;
    $self->{SessionActivity} = shift if (@_);
    return $self->{SessionActivity};
}

1;

