-- $Id$
--
-- $Log$
-- Revision 1.1  2002/10/29 14:43:50  stefan
-- Preliminary database structure.


--
-- The history table is the event log.
--

CREATE TABLE History (
  ID int(11) NOT NULL auto_increment,
  Equipment int(11) default NULL,
  Type tinyint(4) NOT NULL default '0',
  Importance tinyint(4) default NULL,
  Request int(11) default NULL,
  Date date NOT NULL default '0000-00-00',
  Time time NOT NULL default '00:00:00',
  User int(11) NOT NULL default '0',
  Created date default NULL,
  CreatedBy int(11) default NULL,
  Modified date default NULL,
  ModifiedBy int(11) default NULL,
  Text text,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- HistoryType defines the different types of history messages.
--

CREATE TABLE HistoryType (
  ID tinyint(4) NOT NULL auto_increment,
  Type varchar(30) default NULL,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- The different types of importance of the history messages.
--

CREATE TABLE ImportanceLevel (
  ID tinyint(4) NOT NULL default '0',
  Importance varchar(30) default NULL,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- The Inventory table holds information on all equipment in the
-- system.
--

CREATE TABLE Inventory (
  ID int(11) NOT NULL auto_increment,
  Name varchar(30) NOT NULL default '',
  Type tinyint(4) NOT NULL default '0',
  Description varchar(255) default NULL,
  IPAddress varchar(15) default NULL,
  Hardware varchar(30) default NULL,
  OperatingSystem varchar(30) default NULL,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- The different types of inventory stuff.
--

CREATE TABLE InventoryType (
  ID tinyint(4) NOT NULL auto_increment,
  Type varchar(20) NOT NULL default '',
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- The Maintenance table holds the planned maintenance events.
--

CREATE TABLE Maintenance (
  ID int(11) NOT NULL auto_increment,
  Title varchar(20) default NULL,
  StartDate date default NULL,
  EndDate date default NULL,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- Priorities for maintenance requests.
--

CREATE TABLE Priority (
  ID int(11) NOT NULL auto_increment,
  Priority varchar(20) default NULL,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- Pending and complete requests.
--

CREATE TABLE Request (
  ID int(11) NOT NULL auto_increment,
  Status tinyint(4) NOT NULL default '0',
  Priority tinyint(4) NOT NULL default '0',
  Type tinyint(4) NOT NULL default '0',
  Equipment int(11) NOT NULL default '0',
  Maintenance int(11) NOT NULL default '0',
  Owner int(11) default NULL,
  Submitted date default NULL,
  SubmittedBy int(11) default NULL,
  Approved date default NULL,
  ApprovedBy int(11) default NULL,
  Closed date default NULL,
  ClosedBy int(11) default NULL,
  Title varchar(128) NOT NULL default '',
  Description text,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- What has happened with each request. I don't think it is used right now.
--

CREATE TABLE RequestLog (
  ID int(11) NOT NULL auto_increment,
  Request int(11) NOT NULL default '0',
  DateTime timestamp(14) NOT NULL,
  Message varchar(50) default NULL,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- Status of requests.
--

CREATE TABLE RequestStatus (
  ID tinyint(4) NOT NULL default '0',
  Status varchar(30) NOT NULL default '',
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- Request types.
--

CREATE TABLE RequestType (
  ID tinyint(4) NOT NULL auto_increment,
  Type varchar(30) default NULL,
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- Session management (cookies and stuff).
--

CREATE TABLE Session (
  ID varchar(32) NOT NULL default '',
  User int(11) NOT NULL default '0',
  Start int(10) unsigned NOT NULL default '0',
  Last int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (ID)
) TYPE=MyISAM;

--
-- Remote users.
--

CREATE TABLE User (
  ID int(11) NOT NULL auto_increment,
  Name varchar(10) NOT NULL default '',
  Fullname varchar(50) default NULL,
  Password varchar(24) default NULL,
  SessionID varchar(24) default NULL,
  SessionStart int(10) unsigned default NULL,
  SessionActivity int(10) unsigned default NULL,
  SessionIP varchar(15) default NULL,
  Priv_start tinyint(4) NOT NULL default '1',
  Priv_list tinyint(4) NOT NULL default '1',
  Priv_showlog tinyint(4) NOT NULL default '1',
  Priv_servedit tinyint(4) NOT NULL default '0',
  Priv_edit tinyint(4) NOT NULL default '0',
  Priv_recent tinyint(4) NOT NULL default '1',
  Priv_reqedit tinyint(4) NOT NULL default '0',
  Priv_reqlist tinyint(4) NOT NULL default '1',
  Priv_reqapprove tinyint(4) NOT NULL default '0',
  Priv_commit tinyint(4) NOT NULL default '0',
  Priv_plan tinyint(4) NOT NULL default '0',
  Priv_actionlist tinyint(4) NOT NULL default '1',
  Priv_admin tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (ID)
) TYPE=MyISAM;
