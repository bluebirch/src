# $Id$
#
# This is a simple module for configuration logging. See index.cgi for
# further description and configuration.
#
# $Log$
# Revision 1.40  2003/08/20 09:14:38  stefan
# The correct server viewed is shown in the quicklist.
#
# Revision 1.39  2003/08/20 08:45:43  stefan
# Changed timout to two hours instead of 15 minutes.
#
# Revision 1.38  2003/08/14 12:25:26  stefan
# Removed obsolete usage of Apache::Htpasswd. Removed two scalar
# reference assignments, which obviosly is no longer allowed in Perl
# 5.8.
#
# Revision 1.37  2003/02/28 13:33:04  stefan
# Added more suitable debug logging.
#
# Revision 1.36  2003/02/27 13:47:25  stefan
# Added password changing routines, but there's a bug in them. You can't
# add a password to a new user. Password update should occur after
# information update or add new user.
#
# Revision 1.35  2003/02/27 09:33:29  stefan
# Fixed redirection bug. Changed session timeout to 15 minutes.
#
# Revision 1.34  2003/02/25 09:05:10  stefan
# Added 'admin' view.
#
# Revision 1.33  2003/02/24 07:53:01  stefan
# Added 'actionlist' view. Minnor changes and bugfixes in other views.
#
# Revision 1.32  2003/02/20 09:11:29  stefan
# Added named levels of priority in all request views.
#
# Added workaround for bug with DBI->do().
#
# Reworked client browser redirection.
#
# Revision 1.31  2003/02/18 08:15:07  stefan
# Fixed time field in reqedit and commit views.
#
# Revision 1.30  2003/02/17 15:51:31  stefan
# Added time field in edit view.
#
# Revision 1.29  2003/02/17 15:05:36  stefan
# Removed maintenance field from reqedit view.
#
# Revision 1.28  2003/01/23 15:31:25  stefan
# ConfDB.pm cleaned up. Methods now have proper names.
#
# Revision 1.27  2003/01/17 13:26:54  stefan
# Begun cleaning up CondDB.pm.
#
# Revision 1.26  2003/01/17 11:56:33  stefan
# Updated 'showlog' view. Maybe there's more to it. Maybe I don't want
# the list of pending request to use the same layout as the 'reqlist'
# view. I'll have to change that later.
#
# Revision 1.25  2003/01/06 16:27:37  stefan
# Removed SecurityClass from servedit view.
#
# Revision 1.24  2003/01/06 15:19:44  stefan
# Fixed ID bug in showlog.
#
# Revision 1.23  2003/01/06 15:12:57  stefan
# Added a simple default start page.
#
# Revision 1.22  2002/12/12 21:00:53  stefan
# Fixed a faulty paragraph split.
#
# Revision 1.21  2002/12/12 20:44:02  stefan
# Whee. Here we go. Think I've got enough to show the customer now.
#
# Revision 1.20  2002/12/12 16:30:49  stefan
# Finished commit view and some other stuff.
#
# Revision 1.19  2002/12/09 08:34:20  stefan
# Begun working on commit view.
#
# Revision 1.18  2002/12/08 19:37:01  stefan
# Finished reqapprove view.
#
# Revision 1.17  2002/12/08 18:39:10  stefan
# Added method reqapprove.
#
# Revision 1.16  2002/11/15 10:56:50  stefan
# I think I have finished 'reqedit' now. Added runmode 'reqlist'.
#
# Revision 1.15  2002/11/07 21:35:36  stefan
# Almost finished 'reqedit' view.
#
# Revision 1.14  2002/11/06 21:00:50  stefan
# Added method 'reqedit' to edit a configuration change request. There's
# more work to do; it does not yet update records.
#
# Fixed bug in 'access_denied' view.
#
# Added method 'now'.
#
# Revision 1.13  2002/11/04 20:55:58  stefan
# Fixed 'recent' view and added quicklist to sidebar.
#
# Revision 1.12  2002/11/04 20:29:44  stefan
# Fixed view 'edit'.
#
# Revision 1.11  2002/11/03 19:50:21  stefan
# Halfway through 'edit' view.
#
# Revision 1.10  2002/11/03 19:20:31  stefan
# Fixed 'showlog' view.
#
# Revision 1.9  2002/11/03 18:45:24  stefan
# Finished 'servedit' view.
#
# Revision 1.8  2002/10/31 16:00:03  stefan
# Changed runmode variable to 'view'.
#
# Almost finished 'servedit' runmode.
#
# Revision 1.7  2002/10/30 20:05:37  stefan
# Runmode 'list' works. Added instance data method 'user'.
#
# Revision 1.6  2002/10/30 19:53:54  stefan
# Added session handling and cookie authenticataion. New runmodes
# 'login' and 'logout'. Support for true database.
#
# Revision 1.5  2002/08/09 09:14:39  stefan
# Fixed numeric conversion bug.
#
# Revision 1.4  2002/08/09 08:58:07  stefan
# Added runmode 'recent' which displays the latest messages for all
# servers.
#
# Revision 1.3  2002/07/16 12:40:28  stefan
# Added support for htpasswd-files with Apache::Htpasswd. The users
# full name can be specified in the htpasswd file, and is displayed
# on the web interface. Full name is not used in the log files. In
# the future, there could be a web based user administration tool.
#
# Revision 1.2  2002/07/16 12:07:23  stefan
# Added new runmode 'edit' to edit (or add) a specific log message.
#
# Revision 1.1  2002/07/12 11:35:23  stefan
# First version.
#
package ConfLogger;
use base 'CGI::Application';
use strict;
use ConfDB;
#use Apache::Htpasswd;
use CGILogger;
use Digest::MD5 qw(md5_hex md5_base64);
use POSIX qw(strftime);

my $SESSIONTIMEOUT = 7200; # seconds

sub setup {
    my $self = shift;
    my $q = $self->query;
    my $u;

    my $client = ($ENV{REMOTE_ADDR} || 'local');

    # Define run modes
    $self->run_modes( start => \&start,
		      list => \&list,
		      showlog => \&showlog,
		      edit => \&edit,
		      servedit => \&servedit,
		      env => \&env,
		      recent => \&recent,
		      login => \&login,
		      logout => \&logout,
		      access_denied => \&access_denied,
		      reqedit => \&reqedit,
		      reqlist => \&reqlist,
		      reqapprove => \&reqapprove,
		      commit => \&commit,
		      plan => \&plan,
		      actionlist => \&actionlist,
		      admin => \&admin,
		      'AUTOLOAD' => \&access_denied );

    # Define default mode
    $self->start_mode( 'start' );

    # Define runmode variable
    $self->mode_param( 'view' );

    # Initialize database
    my $db = new ConfDB;
    $self->db( $db );

    # DEBUG - show variables
#      debugmsg "parameters:";
#      my $vars = $q->Vars;
#      foreach my $param (sort keys %$vars) {
#  	debugmsg sprintf( "  %-15s = [%s]", $param, $$vars{$param} );
#      }

    # Get current runmode
    my $view = ($q->param( 'view' ) || $self->start_mode());

    # Check session id
    my $sessionid = $q->cookie( 'SessionID' );

    if ($sessionid) {
#	debugmsg "SessionID = $sessionid";

	$u = $db->session_check( $sessionid );

	if ($u) {
	    # Check session age
	    if (time - $u->sessionactivity <= $SESSIONTIMEOUT) {
		$db->session_update( $sessionid );
		$self->user( $u );
	    }
	    else {
		$q->param( reason => sprintf( 'Du har varit inaktiv i mer �n %d minuter.', $SESSIONTIMEOUT / 60 ) );
		$view = 'logout';
		debugmsg "session has timed out; take action";
	    }
	}
	else {
	    $q->param( reason => "Ogiltig kaka." );
	    $view = 'logout';
	    debugmsg "this is an invalid SessionID; take action";
	}
    }
    else {
	# No session id. Must login.
	debugmsg "No SessionID from $client. Must login.";
	$view = 'login';
    }

    # Check user permissions except for login and logout runmodes
    if ($u && $view !~ m/^(login|logout)$/) {
#	debugmsg "checking permissions";
	unless ($u->checkpermission( $view )) {
	    # Set access_denies runmode
	    $view =  'access_denied';
	}
    }

    debugmsg "display view '$view' to $ENV{REMOTE_ADDR}", ( $u ? ' (' . $u->name . ')' : '' );
    $q->param( 'view' => $view );
}

#
# Login and create session key
#
sub login {
    my $self = shift;
    my $q = $self->query;

    if ($q->param('login')) {
	my $user = $q->param( 'user' );
	my $pw = $q->param( 'password' );
	my $db = $self->db;

	if ($user && $pw) {

	    $pw = md5_base64( $pw );

	    debugmsg "trying to login; user=$user; pw=$pw, ", $q->param( 'password' );

	    my $u = $db->session_checkuser( $user, $pw );

	    if ($u) {

		# create session key
		my $sessionkey = md5_base64( $user . time . $ENV{REMOTE_ADDR} );
		if ($db->session_newkey( $u, $sessionkey,
					 ($ENV{REMOTE_ADDR} || 'local') )) {
		    debugmsg "login success; now redirect";

		    # Create session cookie
		    my $cookie = $q->cookie( -name => 'SessionID',
					     -value => $sessionkey,
					     -expires => '+1d',
					     -path => '/' );

		    # Redirect client browser
		    return $self->redirect( $q->url, -cookie => $cookie );

		} else {
		    logmsg "set sessionkey failed";
		    $q->param( msg => 'Internt fel. Kontakta systemansvarig.' );
		}
	    } else {
		debugmsg "login not accepted";
		$q->param( msg => 'Felaktigt anv�ndarnamn eller l�senord.' );
	    }
	} else {
	    $q->param( msg => 'Ange anv�ndarnamn och l�senord f�r att logga in.' );
	}
    }

    my $t = $self->load_tmpl( 'login.tmpl',
			      associate => $q );

    return $t->output;
}

#
# Display deny-access page
#
sub logout {
    my $self = shift;
    my $q = $self->query;

    my $t = $self->load_tmpl( 'logout.tmpl',
			      associate => $q );

    # Create a dummy session key cookie
    my $cookie = $q->cookie( -name => 'SessionID',
			     -value => '*',
			     -expires => '-1d',
			     -path => '/' );

    # Store cookie in HTTP headers
    $self->header_props( -cookie => $cookie );

    return $t->output;
}

#
# Display a start page. This is a simple page with no forms.
#
sub start {
    my $self = shift;
    my $t = $self->load_tmpl( 'start.tmpl' );
    $self->set_defaults( $t );
    return $t->output;
}

#
# Display a list of available servers
#
sub list {
    my $self = shift;
    my $u = $self->user;

    my $list = $self->db->equipment_longlist;

    my $t = $self->load_tmpl( 'list.tmpl',
			      die_on_bad_params => 0 );

    $self->set_defaults( $t );

    $t->param( list => $list );

    return $t->output;
}

#
# Show log for a specific machine.
#
sub showlog {
    my $self = shift;

    my $q = $self->query;
    my $db = $self->db;
    my $u = $self->user;

    # Get current id
    my $id = ($q->param( 'ID' ) || $q->param( 'id' ));

    # Get equipment info
    my $equipment = $db->equipment_get( $id );

    # Get logs for specific host
    my $logdata = $db->history_listbyequipment( $id );

    # Split log messages into paragraphs
    $self->parabreak( $logdata, 'Text' );

    # Get list of pending requests for this host
    my $requests = $db->request_getlist( $id );

    # Load template and set parameters
    my $t = $self->load_tmpl( 'showlog.tmpl',
			      die_on_bad_params => 0,
			      global_vars => 0 );

    $self->set_defaults( $t );

    $t->param( $equipment );
    $t->param( log => $logdata,
	       requests => $requests );

    return $t->output;
}

#
# Edit a log message (new or old)
#
sub edit {
    my $self = shift;
    my $q = $self->query;
    my $db = $self->db;
    my $data = $q->Vars;
    my $message;

    # Set ID
    $$data{ID} = $$data{id}
      if ($$data{id});

    # Save form data if save button was pressed
    if ($$data{save}) {

	# Some sanity checks
	$message = "Felaktigt datum '$$data{Date}'."
	  if (!$$data{Date} || $$data{Date} !~ m/^\d\d\d\d-\d\d-\d\d$/);

	if ($$data{Time} && $$data{Time} =~ m/^\d\d(:\d\d)?(:\d\d)?$/) {
	    if (!$1) {
		$$data{Time} .= ':00:00';
	    } elsif (!$2) {
		$$data{Time} .= ':00';
	    }
	}
	else {
	    $message = "Felaktig tidsangivelse '$$data{Time}'."
	}

	# Some sanity checks
	unless ($message) {

	    # Update record
	    if ($$data{ID}) {
		unless ($db->history_update( $$data{ID},
					     $$data{Equipment},
					     $$data{Type},
#					     $$data{SecurityClass},
					     $$data{Importance},
					     $$data{Date},
					     $$data{Time},
					     $$data{User},
					     $$data{Text} )) {

		    $message = "Uppdatering misslyckades";
		}
	    }

	    # Or add new
	    else {
		unless ($db->history_new( $$data{Equipment},
					  $$data{Type},
#					  $$data{SecurityClass},
					  $$data{Importance},
					  $$data{Date},
					  $$data{Time},
					  $$data{User},
					  $$data{Text} )) {
		    $message = "Det d�r gick inge bra. N�got blev fel.";
		}
	    }

	    # If successful, redirect client browser to showlog view
	    unless ($message) {
		return $self->redirect( "?view=showlog&ID=$$data{Equipment}" );
	    }
	}
    }

    # Return somewhere if cancel button is pressed
    elsif ($$data{cancel}) {
	return $self->redirect( "?view=showlog&ID=$$data{Equipment}" );
    }

    # Get log entry
    elsif ($$data{ID}) {
	$data = $db->history_get( $$data{ID} );
    }

    # Or else, set default values
    else {
	$$data{Type} = 1
	  unless ($$data{Type});
#  	$$data{SecurityClass} = 1
#  	  unless ($$data{SecurityClass});
	$$data{Importance} = 3
	  unless ($$data{Importance} );
	$$data{User} = $self->user->id
	  unless ($$data{User});
	$$data{Date} = $self->now
	  unless ($$data{Date});
	$$data{Time} = $self->curtime
	  unless ($$data{Time});
    }


    # Get inventory list
    my $inventory = $db->equipment_list;
    $self->mark_selected( $inventory, 'ID', $$data{Equipment} )
      if ($$data{Equipment});

    # Get history types
    my $historytypes = $db->history_types;
    $self->mark_selected( $historytypes, 'ID', $$data{Type} )
      if ($$data{Type});

    # Get list of users
    my $userlist = $db->user_list;
    $self->mark_selected( $userlist, 'ID', $$data{User} )
      if ($$data{User});

#      # Get securityclasses
#      my $classes = $db->securityclasses;
#      $self->mark_selected( $classes, 'ID', $$data{SecurityClass} )
#        if ($$data{SecurityClass});

    # Create template
    my $t = $self->load_tmpl( 'edit.tmpl',
			      die_on_bad_params => 0 );

    $self->set_defaults( $t );

    $t->param( $data );

    $t->param( inventory => $inventory,
	       types => $historytypes,
#	       securityclasses => $classes,
	       userlist => $userlist );

    $t->param( errmsg => $message )
      if ($message);

    return $t->output;
}

#
# Edit server information
#
sub servedit {
    my $self = shift;
    my $q = $self->query;
    my $db = $self->db;
    my $data = $q->Vars; # used to store host log data
    my $message; # error message

    $$data{ID} = $$data{id}
      if ($$data{id});

    # If "save" button was pressed, save the form data.
    if ($$data{save}) {

	# Some sanity checks
	$message = "Ogiltigt namn '$$data{Name}'."
	  if (!$$data{Name} || $$data{Name} !~ m/^\w+$/);

	$message = "Felaktig IP-adress '$$data{IPAddress}'."
	  if ($$data{IPAddress} && $$data{IPAddress} !~ m/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/);

	unless ($message) {

	    if ($$data{ID}) {
		# Update record
		unless ($db->equipment_update( $$data{ID}, $$data{Name},
					       $$data{Type},
					       $$data{Description},
					       $$data{IPAddress},
					       $$data{Hardware},
					       $$data{OperatingSystem} )) {
		    $message = "Uppdatering misslyckades";
		}
	    }
	    else {
		# Add new record
		my $newid = $db->equipment_new( $$data{Name},
						$$data{Type},
						$$data{Description},
						$$data{IPAddress},
						$$data{Hardware},
						$$data{OperatingSystem} );
		if ($newid <= 0) {
		    $message = "Inl�ggning av ny data misslyckades";
		} else {
		    $$data{ID} = $newid;
		}
	    }

	    # If update/insert operation was successfull, redirect
	    # client browser to list view.
	    unless ($message) {
		return $self->redirect( '?view=showlog&ID='.$$data{ID} );
	    }
	}
    }

    # Get log data from database
    elsif ($$data{ID}) {
	$data = $db->equipment_get( $$data{ID} );
    }

    # Get inventorytypes
    my $types = $db->equipment_types;
    $self->mark_selected( $types, 'ID', $$data{Type} )
      if ($$data{Type});

    # Get securityclasses
#      my $classes = $db->securityclasses;
#      $self->mark_selected( $classes, 'ID', $$data{SecurityClass} )
#        if ($$data{SecurityClass});

    # Create HTML output from template
    my $t = $self->load_tmpl( 'servedit.tmpl',
			      die_on_bad_params => 0 );

    # Set default parameters
    $self->set_defaults( $t );

    # Set all parameters
    $t->param( $data );

    # Add lists
    $t->param( inventorytypes => $types );

    # Set error message, if any
    $t->param( errmsg => $message );

    return $t->output;
}

#
# Show list of change requests
#
sub reqlist {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;
    my $u = $self->user;

    my $t = $self->load_tmpl( 'reqlist.tmpl' );

    my $requests = $db->request_getlist;

    $self->set_defaults( $t );

    $t->param( requests => $requests );

    return $t->output;
}

#
# Edit a new change request
#
sub reqedit {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;
    my $u = $self->user;
    my $data = $q->Vars;
    my $message;

    # Set ID
    $$data{ID} = $$data{id}
      if ($$data{id});

    # Save form data if save button was pressed
    if ($$data{save}) {

	# Some sanity checks
	$message = "Du m�ste s�tta en rubrik."
 	  if (!$$data{Title});

	unless ($message) {

	    # Update record
	    if ($$data{ID}) {
  		debugmsg "update record with the following data:";
    		for my $key (sort keys %$data) {
    		    debugmsg "  $key = $$data{$key}";
    		}

		unless ($db->request_update( $$data{ID},
					     $$data{Status},
					     $$data{Priority},
					     $$data{Type},
					     $$data{Equipment},
					     $$data{Owner},
					     $$data{Title},
					     $$data{Description} ) ) {

		    $message = "Uppdatering misslyckades.";
		}
	    }

	    # Add new
	    else {
  		debugmsg "add record with the following data:";
  		for my $key (sort keys %$data) {
  		    debugmsg "  $key = $$data{$key}";
  		}
		unless ($db->request_new( $$data{Status},
					  $$data{Priority},
					  $$data{Type},
					  $$data{Equipment},
					  $$data{Owner},
					  $self->now,
					  $u->id,
					  undef,
					  undef,
					  undef,
					  undef,
					  $$data{Title},
					  $$data{Description} ) ) {

		    $message = "Inl�ggning av ny beg�ran misslyckades.";
		}
	    }

	    # If successful, redirect client browser to showlog view
	    unless ($message) {
		return $self->redirect( "?view=reqlist" );
	    }
	}


    }

    # Return somewhere if cancel button is pressed
    elsif ($$data{cancel}) {
	return $self->redirect( "?view=reqlist" );
    }

    # Get request
    elsif ($$data{ID}) {
  	$data = $db->request_get( $$data{ID} );
    }

    # Or else, set default values
    else {
	# Set default values
	$$data{Status} = 1 unless ($$data{Status});
	$$data{Priority} = 3 unless ($$data{Priority});
	$$data{Owner} = $self->user->id
	  unless ($$data{Owner});
	$$data{Type} = 1 unless ($$data{Type});
	$$data{Maintenance} = 0 unless ($$data{Maintenance});
    }


    if ($$data{ID}) {
	# Get log entries for selected request
	$$data{Log} = $db->history_listbyrequest( $$data{ID} );

	# Break paragraphs (insert <br>)
	$self->parabreak( $$data{Log}, 'Text' );
    }

    # Get inventory
    my $inventory = $db->equipment_list;
    $self->mark_selected( $inventory, 'ID', $$data{Equipment} )
      if ($$data{Equipment});

    # Get name of inventory
    $$data{LongEquipment} = $db->equipment_name( $$data{Equipment} );
    debugmsg "LongEquipment=$$data{LongEquipment}";

    # Get request types
    my $types = $db->request_types;
    $self->mark_selected( $types, 'ID', $$data{Type} )
      if ($$data{Type});

    # Get list of users
    my $userlist = $db->user_list;
    $self->mark_selected( $userlist, 'ID', $$data{Owner} )
      if ($$data{Owner});

    # Get list of priorities
    my $priorities = $db->priority_list;
    $self->mark_selected( $priorities, 'ID', $$data{Priority} )
      if ($$data{Priority});

    # Get name of owner
    $$data{LongOwner} = $db->user_fullname( $$data{Owner} )
      if ($$data{Owner});

    # Get name of request status
    $$data{LongStatus} = $db->request_status_name( $$data{Status} );

    # Get name of priority
    $$data{LongPriority} = $db->priority_name( $$data{Priority} );

    # Get name of request type
    $$data{LongType} = $db->request_type_name( $$data{Type} );

    # Get name of submitter
    $$data{LongSubmittedBy} = $db->user_fullname( $$data{SubmittedBy} )
      if ($$data{SubmittedBy});

    # Get name of approver
    $$data{LongApprovedBy} = $db->user_fullname( $$data{ApprovedBy} )
      if ($$data{ApprovedBy});

    # Get name of closer, or whatever it is called
    $$data{LongClosedBy} = $db->user_fullname( $$data{ClosedBy} )
      if ($$data{ClosedBy});

    my $t = $self->load_tmpl( 'reqedit.tmpl',
			      die_on_bad_params => 0 );

    # Set default parameters
    $self->set_defaults( $t );

    # Set parameters based on status
    $self->set_status( $t, $$data{Status} );

    # Set all other parameters
    $t->param( $data );

    # And yet some more parameters
    $t->param( inventory => $inventory,
	       types => $types,
	       userlist => $userlist,
	       priorities => $priorities,
	       errmsg => $message );

    return $t->output;
}

#
# Approve change request
#
sub reqapprove {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;
    my $u = $self->user;

    my $data = $q->Vars;

    # If approve button was presed, process approvements
    if ($$data{approve}) {
	foreach my $param (%$data) {
	    if ($param =~ m/^approved(\d+)$/) {
		my $id = $1;
		debugmsg "Approve request id $id; user=", $u->id, "; name=",
		  $u->name;
		unless ($db->request_approve( $id, $u->id, $self->now )) {
		    debugmsg "approve failed";
		}
	    }
	}
    }

    # Get list of planned maintenance
    my $maintenance = $db->maintenance_list;
    $self->mark_selected( $maintenance, 'ID', $$data{Maintenance} )
      if ($$data{Maintenance});

    # Get list of requests for selected maintenance
    my $approvelist = [];
    $approvelist = $db->request_approvelist( $$data{Maintenance} )
      if ($$data{Maintenance});

    # Split text paragraphs
    $self->parasplit( $approvelist, 'Description' );

    my $t = $self->load_tmpl( 'reqapprove.tmpl',
			      die_on_bad_params => 0);

    $self->set_defaults( $t );

#    $t->param( $data );

    $t->param( maintenancelist => $maintenance,
	       requests => $approvelist );

    return $t->output;
}

#
# Edit a new change request
#
sub commit {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;
    my $u = $self->user;
    my $data;
    my $message;

    my $id = ($q->param( 'ID' ) || $q->param( 'id' ));

    # Get request data.
    %$data = (%{$q->Vars}, %{$db->request_get( $id )} );

    # FIXME: Bail out on error


    # Save form data if save button was pressed
    if ($$data{save}) {

  	# Some sanity checks
  	$message = "Du m�ste skriva ett loggmeddelande."
   	  if (!$$data{Text});
	$message = "Du f�r inte st�nga ett �rende som inte �r godk�nt."
	  if ($$data{close} && $$data{Status} != 3);

	unless ($message) {
	    # FIXME: Type & securityclass
	    debugmsg "new log message for $$data{Equipment}";
	    $db->history_new( $$data{Equipment}, -1, 0, $self->now,
			      $self->curtime, $u->id,
			      $$data{Text}, $$data{ID} );

	    if ($$data{close}) {
		debugmsg "close request";
		$db->request_close( $$data{ID}, $self->now, $u->id );
		$$data{Status} = 4;
	    }

	    delete $$data{Text};
	}
    }
    elsif ($$data{cancel}) {
	return $self->redirect( '?view=reqlist' );
    }

    # Get log entries for selected request
    $$data{Log} = $db->history_listbyrequest( $$data{ID} );

    # Break paragraphs in description
    $$data{Description} =~ s/\r?\n/<br>/gs;

    # Break paragraphs (insert <br>)
    $self->parabreak( $$data{Log}, 'Text' );

    # Get name of request status
    $$data{LongStatus} = $db->request_status_name( $$data{Status} );

    # Get name of priority
    $$data{LongPriority} = $db->priority_name( $$data{Priority} );

    # Get name of request type
    $$data{LongType} = $db->request_type_name( $$data{Type} );

    # Get name of inventory
    $$data{LongEquipment} = $db->equipment_name( $$data{Equipment} );

    # Get name of owner
    $$data{LongOwner} = $db->user_fullname( $$data{SubmittedBy} )
      if ($$data{Owner});

    # Get name of submitter
    $$data{LongSubmittedBy} = $db->user_fullname( $$data{SubmittedBy} )
      if ($$data{SubmittedBy});

    # Get name of approver
    $$data{LongApprovedBy} = $db->user_fullname( $$data{ApprovedBy} )
      if ($$data{ApprovedBy});

    my $t = $self->load_tmpl( 'commit.tmpl',
			      die_on_bad_params => 0 );

    # Set default parameters
    $self->set_defaults( $t );

    # Set parameters based on current status
    $self->set_status( $t, $$data{Status} );

    # Set error message
    $t->param( errmsg => $message );

    # Set specific parameters
    $t->param( $data );

    return $t->output;
}

#
# Plan maintenance
#
sub plan {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;
    my $data = $q->Vars;
    my $message;


    # Check if save button was pressed
    if ($$data{save}) {

	# FIXME: Should do some insanity checks here

	if ($$data{ID} && $$data{ID} > 0) {
	    unless ($db->maintenance_update( $$data{ID},
					     $$data{Title},
					     $$data{StartDate},
					     $$data{EndDate} )) {
		$message = "Uppdatering misslyckades";
	    }
	}
	else {
	    debugmsg "add new maintenance";
	    my $id = $db->maintenance_new( $$data{Title}, $$data{StartDate},
					   $$data{EndDate} );

	    debugmsg "new ID is $id";

	    if ($id > 0) {
		$$data{ID} = $id;
	    } else {
		$message = "Lagring av v�rden misslyckades.";
	    }
	}
    }

    # Check if delete button was pressed
    elsif ($$data{delete} && $$data{ID} && $$data{ID} >= 1) {
	if ($db->maintenance_remove( $$data{ID} )) {
	    delete $$data{ID};
	}
	else {
	    $message = "Radering misslyckades";
	}
    }

    # Check if add button was pressed
    elsif ($$data{add} && $$data{ID} && $$data{ID} >= 1) {
	foreach my $param (keys %$data) {
	    if (my ($id) = $param =~ m/^req(\d+)/) {
		$db->request_attachtomaintenance( $id, $$data{ID} );
	    }
	}
    }

    # Check if remove button was pressed
    elsif ($$data{remove} && $$data{ID} && $$data{ID} >= 1) {
	foreach my $param (keys %$data) {
	    if (my ($id) = $param =~ m/^req(\d+)/) {
		$db->request_attachtomaintenance( $id );
	    }
	}
    }

    # Get information on selected maintenance
    my $planned = [];
    my $not_planned = [];
    if (!$$data{ID} || $$data{ID} == -1) {
	delete @$data{('ID', 'Title', 'StartDate', 'EndDate')};
    }
    else {
	debugmsg "Get information for maintenance $$data{ID}";
	%$data = (%$data, %{$db->maintenance_get( $$data{ID} )} );

	# Get list of requests for selected maintenance - both those
	# who are scheduled for this event, and those who are not.
	$planned = $db->request_listbymaintenance( $$data{ID} );
	$not_planned = $db->request_listbymaintenance( $$data{ID}, 1 );

    }

    # Get list of all maintenance events
    my $maintenance = [ { Title => 'Ny', ID => -1 },
			 @{$db->maintenance_list} ];
    $self->mark_selected( $maintenance, 'ID', $$data{ID} )
      if ($$data{ID});

    # Initialize template
    my $t = $self->load_tmpl( 'plan.tmpl',
			      die_on_bad_params => 0 );

    # Set default parameters
    $self->set_defaults( $t );

    # Set error message
    $t->param( errmsg => $message );

    # Set specific parameters
    $t->param( $data );
    $t->param( maintenancelist => $maintenance,
	       planned => $planned,
	       not_planned => $not_planned );

    return $t->output;

}

#
# A list of requests to be done before a maintenance
#

sub actionlist {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;
    my $data = $q->Vars;

    # Get list of all maintenance events
    my $maintenance = $db->maintenance_list;
    $self->mark_selected( $maintenance, 'ID', $$data{ID} )
      if ($$data{ID});

    # Get list of requests for selected maintenance
    my ($approved, $not_approved) = ( [], [] );
    if ($$data{ID} > 1) {
	$approved = $db->request_allbymaintenance( $$data{ID}, 1 );
	$not_approved = $db->request_allbymaintenance( $$data{ID} );
    }

    # Do linebreaks
    $self->parabreak( $approved, 'Description' );
    $self->parabreak( $not_approved, 'Description' );

    # Initialize template
    my $t;

    if ($$data{print}) {
	$t = $self->load_tmpl( 'actionlist_printable.tmpl' );

	my $maint = $db->maintenance_get( $$data{ID} );

	$t->param( $maint );

	$t->param( approved => $approved,
		   not_approved => $not_approved );
    }
    else {
	$t = $self->load_tmpl( 'actionlist.tmpl' );
	$self->set_defaults( $t );
	$t->param( maintenancelist => $maintenance,
		   approved => $approved,
		   not_approved => $not_approved,
		   ID => $$data{ID} );
    }

    return $t->output;
}

#
# Display recent activity for all servers.
#
sub recent {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;

    # Get number of days
    my $days = $q->param( 'days' );
    $days = 7 unless ($days && $days > 0);

    # Get recent log entries
    my $logentries = $db->history_recent( $days );

    # Split log messages into paragraphs
    $self->parabreak( $logentries, 'Text' );

    # Initialize template
    my $t = $self->load_tmpl( 'recent.tmpl',
			      die_on_bad_params => 0 );

    $self->set_defaults( $t );

    $t->param( log => $logentries );
    $t->param( days => $days );

    return $t->output;
}

#
# Administer user access
#
sub admin {
    my $self = shift;
    my $db = $self->db;
    my $q = $self->query;
    my $data = $self->query->Vars;
    my $u = $self->user;
    my $errmsg;

    debugmsg "ID=$$data{ID}";

    # Save info if save button was pressed
    if ($$data{save} && $$data{ID}) {

	# Set permissions to meaningful values
	my @fields = qw(Priv_start Priv_list Priv_showlog Priv_servedit Priv_edit Priv_recent Priv_reqedit Priv_reqlist Priv_reqapprove Priv_commit Priv_plan Priv_actionlist Priv_admin);
	foreach my $field (@fields) {
	    if ($$data{$field}) {
		$$data{$field} = 1;
	    } else {
		$$data{$field} = 0;
	    }
	}

	unless ($$data{Name} && $$data{Fullname}) {
	    $errmsg = "Du m�ste ange login och namn.";
	}

	# Check password
	my $update_password = 0;
	if ($$data{Password}) {
	    if ($$data{Verify} && $$data{Password} eq $$data{Verify}) {
		# save password
#		debugmsg "set password for $$data{ID} ($$data{Name})";
		$update_password = $db->user_setpass
		  ( $$data{ID},
		    md5_base64( $$data{Password} )
		  );
	    } 
	    else {
		$errmsg = "L�senordet kunde inte verifieras. F�rs�k igen."
	    }
	}

	# User ID 1 will always be an administrator
	if ($$data{ID} == 1) {
	    $$data{Priv_admin} = 1;
	}

	# Update database
	unless ($errmsg) {

	    my $result;
	    if ($$data{ID} < 1) {
		$result = $db->user_new( @$data{'Name', 'Fullname',
						'Priv_start', 'Priv_list',
						'Priv_showlog',
						'Priv_servedit',
						'Priv_edit',
						'Priv_recent',
						'Priv_reqedit',
						'Priv_reqlist',
						'Priv_reqapprove',
						'Priv_commit', 'Priv_plan',
						'Priv_actionlist',
						'Priv_admin'} );
		if ($result > 0) {
		    $$data{ID} = $result;
		} else {
		    $errmsg = "Det blev n�got fel. Informationen �r inte sparad.";
		}
	    }
	    else {
		$result = $db->user_update( @$data{'Name', 'Fullname',
						   'Priv_start', 'Priv_list',
						   'Priv_showlog',
						   'Priv_servedit',
						   'Priv_edit',
						   'Priv_recent',
						   'Priv_reqedit',
						   'Priv_reqlist',
						   'Priv_reqapprove',
						   'Priv_commit', 'Priv_plan',
						   'Priv_actionlist',
						   'Priv_admin', 'ID'} );
		if ($result != 1 && !$update_password) {
		    $errmsg = "Nu blev det fel. Informationen �r inte sparad.";
		}
	    }
	}
    }

    # Set default ID to current user
    $$data{ID} = $u->id unless ($$data{ID});

    # Get user info
    my $userinfo = $db->user_info( $$data{ID} );

    # Get list of users
    my $userlist = [ { Fullname => '&mdash; ny &mdash;', Name => '&mdash;',
			ID => -1 },
		      @{$db->user_list} ];
    $self->mark_selected( $userlist, 'ID', $$data{ID} )
      if ($$data{ID});

    # Load template
    my $t = $self->load_tmpl( 'admin.tmpl',
			      die_on_bad_params => 0 );

    $self->set_defaults( $t );

    $t->param( $userinfo );
    $t->param( userlist => $userlist,
	       errmsg => $errmsg );

    return $t->output;
}

#
# Display deny accessed page
#
sub access_denied {
    my $self = shift;

    my $t = $self->load_tmpl( 'access_denied.tmpl',
			      die_on_bad_params => 0 );

    $self->set_defaults( $t );

    return $t->output;
}

#
# Set default template parameters
#
sub set_defaults {
    my ($self, $t) = @_;
    my $u = $self->user;
    my $db = $self->db;
    my $q = $self->query;

    if ($u) {

	my $quicklist = $db->equipment_list;

	# An ugly hack to set ID in quicklist
	if ($q->param( 'view' ) eq 'showlog') {
	    my $id = $q->param( 'ID' );
	    if ($id) {
		$self->mark_selected( $quicklist, 'ID', $id );
	    }
	}
	    
	$t->param( logged_in => 1,
#		   sessionuser => $u->name,
		   sessionname => $u->fullname,
		   sessionip => $u->{SessionIP},
		   quicklist => $quicklist );
    }
    else {
	$t->param( logged_in => 0 );
    }

    # Set view in template
#    $t->param( 'view' => $q->param( 'view' ) );
}

#
# Set template parameters according to status
#
sub set_status {
    my ($self, $t, $status) = @_;

    if ($status >= 1) {
	$t->param( status_new => 1 );
    }
    if ($status >= 2) {
	$t->param( status_planned => 1 );
    }
    if ($status >= 3) {
	$t->param( status_approved => 1 );
    }
    if ($status >= 4) {
	$t->param( status_closed => 1 );
    }
}

#
# Display environment variables. Just for testing.
#
sub env {
    my $self = shift;
    my $q = $self->query;

    my $line = $q->start_html( 'Environment' ) . $q->h1( 'Environment' );
    foreach my $key (sort keys %ENV) {
	$line .= $q->p( "$key = $ENV{$key}" );
    }
    $line .= $q->end_html;

    return $line;
}

#
# Return an error page.
#
sub errpage {
    my ($self, $msg) = @_;
    my $q = $self->query;

    return $q->start_html( 'Fel! Fel! Fel!' ) . 
      $q->h1( 'Fel! Fel! Fel!' ) . $q->p( $msg || 'Oj. Nu blev det fel.' ) .
	$q->end_html;
}

#
# Redirect client browser
#
sub redirect {
    my ($self, $url) = splice( @_, 0, 2 );
    my $q = $self->query;

    $url = $q->url . $url unless ($url =~ m{^https?://});

    $self->header_props( -uri => $url, @_ );
    $self->header_type( 'redirect' );

    # Return with an empty page
    return $q->start_html( 'Omdirigering' ) .
      $q->p( 'Klicka h�r f�r att g� vidare:',
	     $q->a({href=>$url}, $url) );
}

#
# Mark an option 'selected' for select lists
#
sub mark_selected {
    my ($self, $ary_ref, $key, $value) = @_;

    foreach my $hash_ref (@$ary_ref) {
	if ($$hash_ref{$key} && $$hash_ref{$key} eq $value) {
	    $$hash_ref{selected} = 1;
	    return 1;
	}
    }
    return 0;
}

#
# Split field into paragraphs
#
sub parasplit {
    my ($self, $aryref, $key) = @_;

    # Split log messages into paragraphs
    foreach my $ref (@$aryref) {
	@{$$ref{Paragraphs}} = map { 'Text' => $_ }, split /[\n\r]+/,
	  @$ref{$key};
	delete $$ref{$key};
    }
}

#
# Change newlines to <br>
#
sub parabreak {
    my ($self, $aryref, $key) = @_;

#    debugmsg "do things on [$aryref]";

#    debugmsg @$aryref;

    foreach my $ref (@$aryref) {
	$$ref{$key} =~ s/\r?\n/<br>/gs;
    }
}

#
# handle ConfDB object
#
sub db {
    my $self = shift;
    $self->{db} = shift if (@_);
    return $self->{db};
}

#
# Handle webuser object
#
sub user {
    my $self = shift;
    $self->{user} = shift if (@_);
    return $self->{user};
}

#
# Return current date
#
sub now {
    my ($self, $time) = @_;
    return $time ? strftime( '%Y-%m-%d', localtime $time ) :
      strftime( '%Y-%m-%d', localtime );
}

#
# Return current time
#
sub curtime {
    my ($self, $time) = @_;
    return $time ? strftime( '%H:%M:%S', localtime $time ) :
      strftime( '%H:%M:%S', localtime );
}

# alles ist richtig
1;
