# $Id$
#
# Module for data handling. All comments are saved in flat
# textfiles. Can be changed to an SQL server. At least, that's the
# purpose with this module.
#
# $Log$
# Revision 1.40  2003/02/27 13:47:40  stefan
# Added user_setpass.
#
# Revision 1.39  2003/02/25 09:05:10  stefan
# Added 'admin' view.
#
# Revision 1.38  2003/02/24 07:53:01  stefan
# Added 'actionlist' view. Minnor changes and bugfixes in other views.
#
# Revision 1.37  2003/02/20 09:11:29  stefan
# Added named levels of priority in all request views.
#
# Added workaround for bug with DBI->do().
#
# Reworked client browser redirection.
#
# Revision 1.36  2003/02/18 08:15:06  stefan
# Fixed time field in reqedit and commit views.
#
# Revision 1.35  2003/02/17 16:04:13  stefan
# Bugfix in user_fullname.
#
# Revision 1.34  2003/02/17 15:51:57  stefan
# Added time field in some history methods.
#
# Revision 1.33  2003/02/17 15:06:25  stefan
# Removed maintenance from request_new and request_update. Added
# maintenance information to request_get.
#
# Revision 1.32  2003/01/23 15:31:02  stefan
# Major cleanup. All methods have proper names.
#
# Revision 1.31  2003/01/17 13:26:38  stefan
# Begun code cleanup. This is needed...
#
# Revision 1.30  2003/01/17 11:57:06  stefan
# Changed 'get_reqlist' to optionally accept an equipment ID.
#
# Revision 1.29  2003/01/06 16:25:46  stefan
# 'new_equipment' returns the ID of inserted records.
#
# Revision 1.28  2003/01/06 16:19:43  stefan
# Started some structuring of this source file. Removed SecurityClass
# from Equipment methods (at least, most of them; maybe I missed some).
#
# Revision 1.27  2003/01/06 15:27:01  stefan
# Removed SecurityClass from 'list' method.
#
# Revision 1.26  2002/12/12 20:54:16  stefan
# Changes in 'recent' method.
#
# Revision 1.25  2002/12/12 20:45:00  stefan
# Added some methods for the plan view.
#
# Revision 1.24  2002/12/12 16:31:55  stefan
# Modified get_request method. Added close_request method.
#
# Revision 1.23  2002/12/09 08:34:06  stefan
# Added method get_reqlog. Needs some work.
#
# Revision 1.22  2002/12/08 19:37:18  stefan
# Added method approve_request.
#
# Revision 1.21  2002/12/08 18:35:29  stefan
# Added method get_reqapprovelist.
#
# Revision 1.20  2002/11/15 10:57:13  stefan
# Lots of new methods. I think I should sort them in some way to get some
# structure of this file.
#
# Revision 1.19  2002/11/07 21:34:35  stefan
# Upgraded DBI and changed all selectall_hashref to selectall_arrayref.
#
# Revision 1.18  2002/11/06 20:58:54  stefan
# Added methods 'get_fullname', 'new_request', 'get_request',
# 'history_status', 'request_types', 'request_status_name'.
#
# Revision 1.17  2002/11/04 20:59:15  stefan
# Source cleanup. Removed old methods. Now, only DBI access remains.
#
# Revision 1.16  2002/11/04 20:55:58  stefan
# Fixed 'recent' view and added quicklist to sidebar.
#
# Revision 1.15  2002/11/04 20:30:06  stefan
# Fixed view 'edit'.
#
# Revision 1.14  2002/11/03 19:50:21  stefan
# Halfway through 'edit' view.
#
# Revision 1.13  2002/10/31 15:59:18  stefan
# Added some new methods with DBI access.
#
# Revision 1.12  2002/10/30 20:05:59  stefan
# Adapted 'list' method to DBI.
#
# Revision 1.11  2002/10/30 19:54:41  stefan
# Added basic database access with DBI. Addes functions for user
# authentication and session handling.
#
# Revision 1.10  2002/08/16 09:16:09  stefan
# Changed sort order. All files are sorted ascending, while all logs are
# sorted descending internally and on the web interface. The most recent
# information is displayed first.
#
# Removed some commented code and fixed addlog method.
#
# Revision 1.9  2002/08/09 09:57:02  stefan
# Changed date format to Sweden. The target server does not support
# swedish locale, so we can't use strftime. Stupid, really stupid.
#
# Revision 1.8  2002/08/09 08:57:29  stefan
# Fixed log message.
#
# Revision 1.7  2002/08/09 08:56:06  stefan
# Added method 'recent' which returns the x number of days of log
# messages.
#
# Some code cleaning.
#
# Revision 1.6  2002/07/19 12:23:00  stefan
# write_log always creates a backup of the file.
#
# Revision 1.5  2002/07/16 12:40:28  stefan
# Added support for htpasswd-files with Apache::Htpasswd. The users
# full name can be specified in the htpasswd file, and is displayed
# on the web interface. Full name is not used in the log files. In
# the future, there could be a web based user administration tool.
#
# Revision 1.4  2002/07/16 12:06:08  stefan
# Added functions to find a specific log message and changing its content.
#
# Revision 1.3  2002/07/15 13:56:27  stefan
# Added support for several paragraphs in log messages.
#
# Revision 1.2  2002/07/15 11:50:24  stefan
# Added some error handling into the 'list' method.
#
# Revision 1.1  2002/07/12 11:35:43  stefan
# First version.
#

package ConfDB;

use strict;
use Date::Parse;
use WordWrap;
use POSIX qw(strftime);
use locale;
use DBI;
use CGILogger;
use WebUser;

my $DSN = 'DBI:mysql:ConfLogger:localhost';
my $USER = 'ConfLogger';
my $PASSWORD = 'murvel';

######################################################################
# Constructor
#
# Create new ConfDB object.
#
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self = {};

    # Get parameters
    my $params = shift;
    foreach my $key (keys %$params) {
	$self->{$key} = $$params{$key};
    }

    bless( $self, $class );

    $self->connect or die "No database connection";

    return $self;
}

#
# Initialize DBI
#
sub connect {
    my $self = shift;

    unless ($self->{dbh}) {
	unless ($self->{dbh} = DBI->connect( $DSN, $USER, $PASSWORD )) {
	    logmsg $DBI::errstr;
	}
	return $self->{dbh};
    }
#    $self->{dbh}->trace(2);
}

######################################################################
# Session and user information methods
#

#
# Get user info, return WebUser object
#
#  sub get_user_info {
#      my ($self,$user) = @_;

#      return undef unless ($user);

#      my $sql = q{SELECT * FROM User WHERE Name=?};

#      my $ary_ref = $self->{dbh}->selectall_arrayref( $sql,
#  						    { Slice => {} },
#  						    $user );

#      if (scalar @$ary_ref > 0) {
#  	bless( $$ary_ref[0], 'WebUser' );
#  	return $$ary_ref[0];
#      }
#      else {
#  	return undef;
#      }
#  }

#
# Get user info based on user name and password, return WebUser object
#
sub session_checkuser {
    my ($self,$user,$pw) = @_;

    return undef unless ($user && $pw);

    my $SQL = q{SELECT * FROM User WHERE Name=? AND Password=?};

    my $ary_ref = $self->{dbh}->selectall_arrayref( $SQL,
						    { Slice => {} },
						    $user, $pw );
    if (scalar @$ary_ref > 0) {
	bless( $$ary_ref[0], 'WebUser' );
	return $$ary_ref[0];
    }
    else {
	return undef;
    }
}

#
# Set session key, return true or false
#
sub session_newkey {
    my ($self,$u,$sessionkey,$remote_ip) = @_;

    return 0 unless ($u && $sessionkey && $remote_ip);

    my $time = time;

    my $SQL =
      q{UPDATE User
        SET SessionID=?, SessionStart=?, SessionActivity=?, SessionIP=?
        WHERE Name=? AND Password=?};

    return $self->sql( $SQL, $sessionkey, $time, $time,
		       $remote_ip, $u->name, $u->password );
}

#
# Check session, return webuser object
#
sub session_check {
    my ($self, $sessionid) = @_;

    return undef unless ($sessionid);

    my $SQL = q{SELECT * FROM User WHERE SessionID=?};

    my $ary_ref = $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
						    $sessionid );

    if (scalar @$ary_ref > 0) {
	bless( $$ary_ref[0], 'WebUser' );
	return $$ary_ref[0];
    }
    else {
	return undef;
    }
}

#
# Update timestamp of session key, return true or false
#
sub session_update {
    my ($self, $sessionid) = @_;

    return 0 unless ($sessionid);

    my $SQL = q{UPDATE User SET SessionActivity=? WHERE SessionID=?};

    return $self->sql( $SQL, time, $sessionid );
}

#
# Get user name for uid
#
sub user_fullname {
    my ($self, $id) = @_;

    return '' unless ($id);

    my $SQL = q{SELECT Fullname FROM User WHERE ID=?};

    return scalar $self->{dbh}->selectrow_array( $SQL, {}, $id );
}

#
# Get user info
#
sub user_info {
    my ($self, $id) = @_;

    my $SQL = q{SELECT ID, Name, Fullname, Priv_start, Priv_list,
                       Priv_showlog, Priv_servedit, Priv_edit, Priv_recent,
                       Priv_reqedit, Priv_reqlist, Priv_reqapprove,
                       Priv_commit, Priv_plan, Priv_actionlist, Priv_admin
                FROM User
                WHERE ID=?};

    my $ary_ref = $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
						    $id );

    return scalar @$ary_ref > 0 ? $$ary_ref[0] : {};
}

#
# Save user info
#
sub user_update {
    my $self = shift;

    return 0 unless (scalar @_ == 16);

    my $SQL = q{UPDATE User
                SET Name=?, Fullname=?, Priv_start=?, Priv_list=?,
                    Priv_showlog=?, Priv_servedit=?, Priv_edit=?,
                    Priv_recent=?, Priv_reqedit=?, Priv_reqlist=?,
                    Priv_reqapprove=?, Priv_commit=?, Priv_plan=?,
                    Priv_actionlist=?, Priv_admin=?
                WHERE ID=?};

    return $self->sql( $SQL, @_ );
}

#
# Add new user
#
sub user_new {
    my $self = shift;

    return 0 unless (scalar @_ == 15);

    my $SQL = q{INSERT INTO User (Name, Fullname, Priv_start, Priv_list,
                            Priv_showlog, Priv_servedit, Priv_edit,
                            Priv_recent, Priv_reqedit, Priv_reqlist,
                            Priv_reqapprove, Priv_commit, Priv_plan,
                            Priv_actionlist, Priv_admin)
                VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)};

    my $rows = $self->sql( $SQL, @_ );

    return $rows == 1 ? $self->{dbh}->{mysql_insertid} : -1;
}

#
# Set user password
#
sub user_setpass {
    my $self = shift;

    return 0 unless (scalar @_ == 2);

    my $SQL = q{UPDATE User SET Password=? WHERE ID=?};

    return $self->sql( $SQL, $_[1], $_[0] );
}

#
# Get a list of users
#
sub user_list {
    my $self = shift;
    my $SQL = q{SELECT ID, Name, Fullname FROM User ORDER BY Fullname};
    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
}

######################################################################
# 'History' methods
#

#
# Return a specific log entry, return hash.
#
sub history_get {
    my ($self, $id) = @_;

    # Verify parameters
    return {} unless ($id);

    my $SQL =
      q{SELECT History.ID, History.Equipment, History.Type, History.Time,
               History.Importance, History.Request, History.Date, History.User,
               History.Created, History.CreatedBy, History.Modified,
               History.ModifiedBy, History.Text,
               Inventory.Name, Inventory.Type AS EquipmentType,
               User.Name AS UserName, User.Fullname AS UserFullname
        FROM History, Inventory, User
        WHERE History.ID=? AND History.Equipment=Inventory.ID
              AND History.User=User.ID};

    my $ary_ref = $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
						    $id );

    return scalar @$ary_ref > 0 ? $$ary_ref[0] : {};
}

#
# Return history list for a specific equipment id
#
sub history_listbyequipment {
    my ($self, $id) = @_;

    return [] unless ($id);

    my $SQL =
      q{SELECT History.ID, History.Type, History.Importance,
               History.Request, History.Date, 
               TIME_FORMAT( History.Time, '%k:%i' ) AS Time,
               History.Text, History.User,
               User.Fullname, HistoryType.Type AS LongType
        FROM History, User, HistoryType
        WHERE Equipment=? AND History.User=User.ID
              AND History.Type=HistoryType.ID
        ORDER BY Date DESC, Time DESC};

    return $self->{dbh}->selectall_arrayref( $SQL,
					     { Slice => {} },
					     $id );
}

#
# Find the latest log entries
#
sub history_recent {
    my ($self, $days) = @_;

    # Set default number of days
    $days = 7 unless ($days);

    my $SQL = q{SELECT History.ID, History.Type,
                       History.Importance, History.Request, History.Date,
                       History.Text, History.User, User.Fullname,
                       TIME_FORMAT( History.Time, '%k:%i' ) AS Time,
                       HistoryType.Type AS LongType,
                       Inventory.Name AS LongEquipment
                FROM History, User, HistoryType, Inventory
                WHERE To_Days(CurDate()) - To_Days(Date) <= ?
                      AND History.User=User.ID
                      AND History.Type=HistoryType.ID
                      AND History.Equipment=Inventory.ID
                ORDER BY Date DESC, Time DESC};

    return $self->{dbh}->selectall_arrayref( $SQL , { Slice => {} }, $days );
}

#
# Return log information for a specific request
#
sub history_listbyrequest {
    my ($self, $request) = @_;

    return [] unless ($request);

    my $SQL =
      q{SELECT History.ID, History.Type,
               History.Importance, History.Request, History.Date,
               History.Text, History.User, User.Fullname,
               TIME_FORMAT( History.Time, '%k:%i' ) AS Time,
               HistoryType.Type AS LongType,
               Inventory.Name AS LongEquipment
        FROM History, User, HistoryType, Inventory
        WHERE Request=? AND History.User=User.ID
              AND History.Type=HistoryType.ID
              AND History.Equipment=Inventory.ID
        ORDER BY Date DESC, Time DESC};

    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} }, $request );
}

#
# Update history
#
sub history_update {
    my ($self, $id, $equipment, $type, $importance,
	$date, $time, $user, $text ) = @_;

    return 0 unless ($id);

    my $SQL =
      q{UPDATE History
        SET Equipment=?, Type=?, Importance=?,
            Date=?, Time=?, User=?, Text=?
        WHERE ID=?};

    return $self->sql( $SQL, $equipment, $type, $importance,
		       $date, $time, $user, $text, $id );
}

#
# Add new history
#
sub history_new {
    my ($self, $equipment, $type, $importance,
	$date, $time, $user, $text, $request ) = @_;

    if ($request) {
	my $SQL =
	  q{INSERT INTO History (Equipment, Type, Importance,
                                 Date, Time, User, Text, Request)
                   VALUES (?, ?, ?, ?, ?, ?, ?, ?)};

	return $self->sql( $SQL, $equipment, $type,
			   $importance, $date, $time, $user, $text,
			   $request );
    }
    else {
	my $SQL =
	  q{INSERT INTO History (Equipment, Type, Importance,
                                 Date, Time, User, Text)
                   VALUES (?, ?, ?, ?, ?, ?, ?)};

	return $self->sql( $SQL, $equipment, $type,
			   $importance, $date, $time, $user, $text );
    }
}

#
# Get history types
#
sub history_types {
    my $self = shift;

    my $SQL = q{SELECT * FROM HistoryType ORDER BY Type};

    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
}

#
# Get history status
#
sub history_status {
    my $self = shift;

    my $SQL = q{SELECT * FROM HistoryStatus ORDER BY Type};

    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
}



######################################################################
# 'Equipment' methods
#

#
# Return equipment information
#
sub equipment_get {
    my ($self,$id) = @_;

    return {} unless ($id);

    my $SQL =
      q{SELECT Inventory.ID, Name, InventoryType.Type AS LongType,
               Inventory.Type, Description, IPAddress, Hardware,
               OperatingSystem
        FROM Inventory, InventoryType
        WHERE Inventory.ID=? AND Inventory.Type=InventoryType.ID};

    my $ary_ref = $self->{dbh}->selectall_arrayref( $SQL,
						    { Slice => {} }, $id );

    return scalar @$ary_ref > 0 ? $$ary_ref[0] : {};
}

#
# Update equipment
#
sub equipment_update {
    my ($self, $id, $name, $type, $description, $ipaddress,
	$hardware, $operatingsystem) = @_;

    return 0 unless ($id && $name);

    my $SQL =
      q{UPDATE Inventory
        SET Name=?, Type=?, Description=?, IPAddress=?, Hardware=?,
            OperatingSystem=?
        WHERE ID=?};

    return $self->sql( $SQL, $name, $type, $description,
		       $ipaddress, $hardware, $operatingsystem,
		       $id );
}

#
# Add new equipment; return the ID of the new equipment
#
sub equipment_new {
    my ($self, $name, $type, $description, $ipaddress,
	$hardware, $operatingsystem) = @_;

    return 0 unless ($name);

    my $SQL = q{INSERT Inventory (Name, Type, Description, IPAddress,
                       Hardware, OperatingSystem)
                VALUES (?,?,?,?,?,?)};

    my $return = $self->sql( $SQL, $name, $type,
			     $description, $ipaddress, $hardware,
			     $operatingsystem );

    return $return ? $self->{dbh}->{mysql_insertid} : -1;
}

#
# Get equipment types
#
sub equipment_types {
    my $self = shift;
    my $SQL = q{SELECT * FROM InventoryType ORDER BY Type};
    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
}

#
# Get a list of all inventory
#
sub equipment_list {
    my $self = shift;
    my $SQL = q{SELECT ID, Name from Inventory ORDER BY Name};
    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
}

#
# Get name of inventory item
#
sub equipment_name {
    my ($self,$id) = @_;
    return undef unless ($id);
    my $SQL = q{SELECT Name FROM Inventory WHERE ID=?};
    return $self->{dbh}->selectrow_array( $SQL, {}, $id );
}

#
# Return an array-of-hashes with all servers in the database.
#
sub equipment_longlist {
    my $self = shift;

    my $SQL = q{SELECT Inventory.ID, Name, InventoryType.Type, Description,
                       IPAddress, Hardware, OperatingSystem
                FROM Inventory, InventoryType
                WHERE Inventory.Type=InventoryType.ID
                ORDER BY Name};

    my $list = $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );

    # Return reference to array or array itself
    return wantarray ? @$list : $list;
}

######################################################################
# 'Request' methods
#

#
# Add a new request
#
sub request_new {
    my $self = shift;

    return 0 unless (scalar @_ == 13);

    my $SQL = q{INSERT INTO Request (Status, Priority, Type, Equipment,
                                     Owner, Submitted,
                                     SubmittedBy, Approved, ApprovedBy,
                                     Closed, ClosedBy, Title, Description)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)};


    # I don't know why I can't use do() here, but who cares? This
    # works.
    return $self->sql( $SQL, @_ );
}

#
# Update request
#
sub request_update {
    my ($self, $id) = splice @_, 0, 2;

    return 0 unless (scalar @_ == 7);

    my $SQL = q{UPDATE Request
                SET Status=?, Priority=?, Type=?, Equipment=?,
                    Owner=?, Title=?, Description=?
                WHERE ID=?};

    return $self->sql( $SQL, @_, $id );
}

#
# Close a request
#
sub request_close {
    my ($self, $id, $date, $uid) = @_;

    my $SQL = q{UPDATE Request
                SET Status=4, Closed=?, ClosedBy=?
                WHERE ID=?};

    return $self->sql( $SQL, $date, $uid, $id );
}

#
# Attach a request to a specific maintenance event
#
sub request_attachtomaintenance {
    my ($self, $request, $id) = @_;

    if ($id) {
	debugmsg "attach req $request to maint $id";
	my $SQL = q{UPDATE Request
                    SET Status = IF( Status = 1, 2, Status),
                        Maintenance = ?
                    WHERE ID = ?};

	return $self->sql( $SQL, $id, $request );
    } else {
	debugmsg "release req $request";
	my $SQL = q{UPDATE Request 
                    SET Status = IF( Status = 2, 1, Status),
                        Maintenance = NULL
                    WHERE ID = ?};

	return $self->sql( $SQL, $request );
    }
}


#
# Get list of all requests. Optionally, get list for a specific
# equipment.
#
sub request_getlist {
    my ($self, $equipment) = @_;

    if ($equipment) {
	my $SQL = q{SELECT Request.ID, Request.Title, Request.Submitted,
                           RequestType.Type, RequestStatus.Status,
                           Maintenance.Title AS Maintenance,
                           Inventory.Name AS Equipment,
                           Priority.Priority
                    FROM Request, RequestStatus, RequestType, Maintenance,
                         Inventory, Priority
                    WHERE Request.Equipment = ?
                          AND Request.Status = RequestStatus.ID
                          AND Request.Type = RequestType.ID
                          AND Request.Maintenance = Maintenance.ID
                          AND Request.Equipment = Inventory.ID
                          AND Request.Priority = Priority.ID
                    ORDER BY Request.Submitted DESC};

	return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
						 $equipment );
    }
    else {
	my $SQL = q{SELECT Request.ID, Request.Title, Request.Submitted,
                           RequestType.Type, RequestStatus.Status,
                           Maintenance.Title AS Maintenance,
                           Inventory.Name AS Equipment,
                           Priority.Priority
                    FROM Request, RequestStatus, RequestType, Maintenance,
                         Inventory, Priority
                    WHERE Request.Status = RequestStatus.ID
                          AND Request.Type = RequestType.ID
                          AND Request.Maintenance = Maintenance.ID
                          AND Request.Equipment = Inventory.ID
                          AND Request.Priority = Priority.ID
                    ORDER BY Request.Status, Request.Submitted};

	return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
    }
}

#
# Get list of requests for a specific maintenance
#
sub request_listbymaintenance {
    my ($self, $maintenance_id, $negate) = @_;

    my $SQL = q{SELECT Request.ID, Request.Title, RequestType.Type,
                       RequestStatus.Status,
                       Inventory.Name AS Equipment,
                       Priority.Priority
                FROM Request, RequestType, RequestStatus, Inventory, Priority
                WHERE Request.Maintenance } . ( $negate ? '!=' : '=' ) . q{?
                      AND Request.Status <= 3
                      AND Request.Type = RequestType.ID
                      AND Request.Status = RequestStatus.ID
                      AND Request.Equipment = Inventory.ID
                      AND Request.Priority = Priority.ID
                ORDER BY Inventory.Name};

    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
					     $maintenance_id );
}

#
# Get list of all requests for a specific maintenance. Actually, it's
# quite stupid that I have to write a new method for this, but I can't
# help it since I made this stupid design in the first place. Go write
# a proper database wrapper instead!
#
sub request_allbymaintenance {
    my ($self, $maintenance_id, $apvd ) = @_;

    my $SQL = q{SELECT Request.ID, Request.Title, RequestType.Type,
                       Request.Description,
                       RequestStatus.Status,
                       Inventory.Name AS Equipment,
                       Priority.Priority,
                       User.Fullname AS Owner
                FROM Request, RequestType, RequestStatus, Inventory, Priority,
                     User
                WHERE Request.Maintenance = ?
                      AND Request.Status} . ( $apvd ? '>=' : '<' ) . q{ 3
                      AND Request.Type = RequestType.ID
                      AND Request.Status = RequestStatus.ID
                      AND Request.Equipment = Inventory.ID
                      AND Request.Priority = Priority.ID
                      AND Request.Owner = User.ID
                ORDER BY Inventory.Name, Request.Priority DESC};

    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
					     $maintenance_id );
}

#
# Get list of requests to approve, based on maintenance
#
sub request_approvelist {
    my ($self, $maintenance_id) = @_;

    my $SQL = q{SELECT Request.ID, Request.Title, Request.Description,
                       RequestType.Type, Inventory.Name AS Equipment,
                       IF( Request.Status = 3, 1, 0) AS Approved,
                       Priority.Priority
                FROM Request, RequestType, Inventory, Priority
                WHERE Request.Maintenance=?
                      AND (Request.Status = 2 OR Request.Status = 3)
                      AND Request.Type = RequestType.ID
                      AND Request.Equipment = Inventory.ID
                      AND Request.Priority = Priority.ID
                ORDER BY Request.Priority DESC, Request.Submitted};

    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
					     $maintenance_id );
}

#
# Mark request approved
#
sub request_approve {
    my ($self, $request, $uid, $date) = @_;

    return 0 unless ($uid && $date);

    my $SQL = q{UPDATE Request
                SET Status=3, Approved=?, ApprovedBy=?
                WHERE ID=? AND Status=2};

    my $rows = $self->sql( $SQL, $date, $uid, $request );
    return $rows == 1 ? 1 : 0;
}

#
# Get single request
#
sub request_get {
    my ($self, $id) = @_;

    return {} unless ($id);

    my $SQL = q{SELECT Request.ID, Request.Status, Request.Priority,
                       Request.Type, Request.Equipment,
                       Request.Maintenance, Request.Owner,
                       Request.Submitted, Request.SubmittedBy,
                       Request.Approved, Request.ApprovedBy,
                       Request.Closed, Request.ClosedBy,
                       Request.Title, Request.Description,
                       Maintenance.Title AS MaintenanceTitle,
                       Maintenance.StartDate AS MaintenanceStartDate,
                       Maintenance.EndDate AS MaintenanceEndDate
                FROM Request, Maintenance
                WHERE Request.ID=?
                      AND Request.Maintenance=Maintenance.ID};

    my $ary_ref = $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
						    $id );

    return scalar @$ary_ref > 0 ? $$ary_ref[0] : {};
}

#
# Get request types
#
sub request_types {
    my $self = shift;
    my $SQL = q{SELECT * FROM RequestType ORDER BY Type};
    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
}

#
# Get request status name
#
sub request_status_name {
    my ($self,$id) = @_;
    return undef unless ($id);
    my $SQL = q{SELECT Status FROM RequestStatus WHERE ID=?};
    return $self->{dbh}->selectrow_array( $SQL, {}, $id );
}

#
# Get request type name
#
sub request_type_name {
    my ($self,$id) = @_;
    return undef unless ($id);
    my $SQL = q{SELECT Type FROM RequestType WHERE ID=?};
    return $self->{dbh}->selectrow_array( $SQL, {}, $id );
}

######################################################################
# Maintenance methods
#

#
# Get a list of planned maintenance
#
sub maintenance_list {
    my $self = shift;
    my $SQL = q{SELECT * FROM Maintenance ORDER BY Startdate};
    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );
}

#
# Get information for a specific maintenance. Return hash reference.
#
sub maintenance_get {
    my ($self, $id) = @_;
    return {} unless ($id);
    my $SQL = q{SELECT * FROM Maintenance WHERE ID=?};
    my $ary_ref = $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} },
						    $id );
    return scalar @$ary_ref > 0 ? $$ary_ref[0] : {};
}

#
# Update information for a specific maintenance event
#
sub maintenance_update {
    my ($self, $id) = splice @_, 0, 2;

    return 0 unless (scalar @_ == 3);

    my $SQL = q{UPDATE Maintenance
                SET Title=?, StartDate=?, EndDate=?
                WHERE ID=?};

    return $self->sql( $SQL, @_, $id );
}

#
# Add a new maintenance entry, return new ID
#
sub maintenance_new {
    my $self = shift;

    return -1 unless (scalar @_ == 3);

    my $SQL = q{INSERT INTO Maintenance (Title, StartDate, EndDate)
                VALUES (?, ?, ?)};

    my $return =  $self->sql( $SQL, @_ );

    return $return ? $self->{dbh}->{mysql_insertid} : -1;
}

#
# Remove maintenance
#
sub maintenance_remove {
    my ($self, $maint) = @_;

    my $SQL = q{UPDATE Request
                SET Status = IF( Status=2, 1, Status ), Maintenance = NULL
                WHERE Maintenance = ?};
    my $changes = $self->sql( $SQL, $maint );

    $SQL = q{DELETE FROM Maintenance WHERE ID = ?};
    return $self->sql( $SQL, $maint );
}

######################################################################
# Priority methods
#

sub priority_list {
    my $self = shift;
    my $SQL = q{SELECT * FROM Priority ORDER BY ID};
    return $self->{dbh}->selectall_arrayref( $SQL, { Slice => {} } );

}

#
# Get priority name
#
sub priority_name {
    my ($self,$id) = @_;
    return undef unless ($id);
    my $SQL = q{SELECT Priority FROM Priority WHERE ID=?};
    return $self->{dbh}->selectrow_array( $SQL, {}, $id );
}



######################################################################
# Other methods
#

# Wrapper around DBI->do() to overcome some stupid bugs
sub sql {
    my ($self, $sql) = splice @_, 0, 2;

    # Make SQL readable in log files
    $sql =~ s/\s{2,}/ /gs;

    my $sth = $self->{dbh}->prepare( $sql );
#    $sth->trace(2);
    my $rv = $sth->execute( @_ );
    my $rows = $sth->rows;

#    debugmsg "execute '$sql'; bind params @_; rv=$rv; rows=$rows";

    return $rows;
}

# Some debug stuff that is no longer used
sub dump_para {
    my ($self,$aryref,$title) = @_;
    print STDERR "$title:\n";
    for (my $i = 0; $i <= scalar @$aryref-1; $i++) {
	print STDERR "$i: $$aryref[$i]{text}\n";
    }
    print STDERR "---\n\n";
}

# True!

1;

