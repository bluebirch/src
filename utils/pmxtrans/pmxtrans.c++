// pmxtrans - transpose chords and keys in pmx files
//
// $Id$
//

#include <strings.h>
#include <iostream.h>

int transposed = 0;

void processline( char * );

int main( int argc, char **argv )
{
  char textline[256];

  while (!cin.eof()) {
    cin.getline( textline, sizeof( textline ));
    processline( textline );
    cout << textline << endl;
  }
}

void processline( char *line )
{
  char *p;
  char chord[10];
  int accent;
  int chordlen;
  char tmp[256];

  p = line;
  while ((p = strstr( p, "\\uc")) != NULL) {
    chord[0] = p[4];
    if (strncmp( &p[5], "\\sharp", 6 ) == 0) {
      chordlen = 11;
      switch (chord[0]) {
      case 'C': chord[0] = 'B'; accent = -1; break;
      case 'D': chord[0] = 'C'; accent = 0; break;
      case 'E': chord[0] = 'D'; accent = 0; break;
      case 'F': chord[0] = 'E'; accent = -1; break;
      case 'G': chord[0] = 'F'; accent = 0; break;
      case 'A': chord[0] = 'G'; accent = 0; break;
      case 'H':
      case 'B': chord[0] = 'A'; accent = 0; break;
      default: chord[0] = '\0';
      }
    } else if (strncmp( &p[5], "\\flat", 5 ) == 0) {
      chordlen = 10;
      switch (chord[0]) {
      case 'C': chord[0] = 'A'; accent = -1; break;
      case 'D': chord[0] = 'B'; accent = -1; break;
      case 'E': chord[0] = 'C'; accent = 0; break;
      case 'F': chord[0] = 'D'; accent = -1; break;
      case 'G': chord[0] = 'E'; accent = -1; break;
      case 'A': chord[0] = 'F'; accent = 0; break;
      case 'H':
      case 'B': chord[0] = 'G'; accent = 0; break;
      default: chord[0] = '\0';
      }
    } else {
      chordlen = 5;
      switch (chord[0]) {
      case 'C': chord[0] = 'A'; accent = 0; break;
      case 'D': chord[0] = 'H'; accent = 0; break;
      case 'E': chord[0] = 'C'; accent = 1; break;
      case 'F': chord[0] = 'D'; accent = 0; break;
      case 'G': chord[0] = 'E'; accent = 0; break;
      case 'A': chord[0] = 'F'; accent = 1; break;
      case 'H':
      case 'B': chord[0] = 'G'; accent = 1; break;
      default: chord[0] = '\0';
      }
    }
    if (chord[0] == '\0') return;
    switch (accent) {
    case -1: strcpy( &chord[1], "\\flat{}\0" ); break;
    case 0: chord[1] = '\0'; break;
    case 1: strcpy( &chord[1], "\\sharp{}\0" ); break;
    }

    // jaha, dags f�r lite klippochklistra
    strcpy( tmp, &p[ chordlen ] );
    strcpy( &p[4], chord );
    strcat( line, tmp );
    p++;
  }

  int key;

  // Skifta tonart
  if (((p = strchr( line, 'K' )) != NULL) &&
      ((p[1] == '+') || (p[1] == '-')) &&
      ((p[3] == '+') || (p[3] == '-'))) {
    //cerr << "Key change: K" << p[1] << p[2] << p[3] << p[4];
    if (!transposed) {
      p[1] = '+';
      p[2] = '5';
      key = p[4] - '0';
      if (p[3] == '-') key = -key;
      key += 3;
      if (key >= 7) key -= 12;
      if (key >= 0) {
	p[3] = '+'; 
	p[4] = key + '0';
      } else {
	p[3] = '-';
	p[4] = '0' - key;
      }
      transposed = 1;
    }
    //cerr << " -> K" << p[1] << p[2] << p[3] << p[4] << endl;
  }

  // L�gg till instrumentnamn
  if (strstr( line, "Tt" )) {
    cout << "Ti" << endl;
    cout << "Altsax" << endl;
  }
}
  
