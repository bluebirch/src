#!/usr/bin/perl -w
#
# $Id$
#
# Copy ID3-tags.
#
use strict;
use MP3::ID3Lib;
use Getopt::Std;

my %opt;
unless (getopts( 's:d:', \%opt )) {
    exit 2;
}

my $SRC_TAG = $opt{s};
my $DST_TAG = $opt{d};

if (!($SRC_TAG && $DST_TAG)) {
    print STDERR "Specify both source and destination tag\n";
    exit 2;
}

print "Copying information from $SRC_TAG to $DST_TAG\n";

foreach my $file (@ARGV) {
    print "Processing $file...\n";
    my $id3 = MP3::ID3Lib->new( $file );
    my $value;
    foreach my $frame (@{$id3->frames}) {
	$value = $frame->value if ($frame->code eq $SRC_TAG);
    }
    if ($value) {
	$id3->add_frame( $DST_TAG, $value );
	$id3->commit;
    }
}
