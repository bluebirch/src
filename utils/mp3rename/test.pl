#!/usr/lib/perl -w
#
# $Id$
#
# This program scans a tree structure of audio files with id3-tags.
#
# $Log$
# Revision 1.6  2002/10/27 06:07:52  stefan
# Don't change name of bad files. Just move them.
#
# Revision 1.5  2002/10/25 16:51:07  stefan
# Some bugfixes. Added option -A.
#
# Revision 1.4  2002/10/21 20:07:20  stefan
# Added statistics and help page.
#
# Revision 1.3  2002/10/21 07:07:54  stefan
# Bugfixes.
#
# Revision 1.2  2002/10/21 07:04:59  stefan
# Purge of empty directories. Moving of bad files.
#
# Todo: use of pjoin, help page, statistics.
#
# Revision 1.1  2002/10/20 20:33:32  stefan
# Basic operation.
#
use strict;
use MP3::ID3Lib;
use File::Find;
use File::Basename;
use File::Path;
use Getopt::Std;
use Cwd qw(realpath cwd);

# Configurationi variables
my $PATTERN = '<TPE1>/<TPE1> - <TALB> - <TRCK> - <TIT2>.mp3';
my $SUFFIXES = q{\.(mp3|MP3)};

# Internal (do not change) variables
my @FILES;
my %FILEINFO;
my $BASE_DIR = cwd();
my $purgelastdir = '';
my %opt;

######################################################################
# Program start
#

# Get command line arguments
unless (getopts( 'AhrqPbd:', \%opt )) {
    print "Try -h for help.\n";
    exit 2;
}

# Display help page if requested
if ($opt{h}) {
    print "Usage: ", basename($0), " [-hirvPbS] <base_directory>\n";
    print "\n";
    print "  -r  Rename/move audio files based on ID3 tag\n";
    print "  -P  Purge empty directories\n";
    print "  -b  Move bad audio files to basedir/BAD.\n";
#    print "  -i  Create index.html in base directory\n";
    print "  -h  Display this help page.\n";
    print "  -A  All of the options above\n";
    print "  -v  Verbose operation\n";
    print "  -S  Show summary\n";
    print "\n";
    exit 0;
}

# Set options for ALL
if ($opt{A}) {
    $opt{r} = 1;
    $opt{P} = 1;
    $opt{b} = 1;
    $opt{i} = 1;
}

# Make sure we've got something to do
unless ($opt{r} || $opt{P} || $opt{b} || $opt{i}) {
    print STDERR "You must specify at least one of the -r, -P, -b or -i options. Try -h for help.\n";
    exit 2;
}

# Get dirs to scan; if none specified, use current working directory
my @DIRS = @ARGV;
push @DIRS, cwd if (scalar @DIRS == 0);

# Set base directory
$BASE_DIR = realpath( $opt{d} ? $opt{d} : $DIRS[0] );
print "Base directory: $BASE_DIR\n" unless ($opt{q});



######################################################################
# Here starts the actual work

# Scan for audio files
print "Scanning for audio files...\n" unless ($opt{q});
scan_files( @ARGV );

# Rename files
if ($opt{r}) {
    rename_files( $BASE_DIR );
}

# Purge base dir
if ($opt{P}) {
    print "Purging $BASE_DIR...\n" unless ($opt{q});
    finddepth( \&purge, $BASE_DIR );
}

exit 0;

######################################################################
# Here comes the subroutines

sub unharmful {
    my $string = shift;
    $string =~ tr{/}{_};
    return $string;
}

sub new_name {
    my $frames = shift;
    my $newname = $PATTERN;

    foreach my $frame (keys %$frames) {
	if ($frame eq 'TRCK') {
	    no warnings;
	    $$frames{$frame} = sprintf( '%02d', $$frames{$frame} );
	}
	$newname =~ s/<$frame>/&unharmful( $$frames{$frame} )/eg;
    }
    $newname =~ s/<\w\w\w\w>/Unknown/g;
    return $newname;
}

sub rename_files {
    my $base_dir = realpath( shift );

    print "Renaming files...\n" unless ($opt{q});

    foreach my $file (keys %FILEINFO) {
	my $new_name = $base_dir . '/' . &new_name( $FILEINFO{$file} );;

	next if ($new_name eq $file);

	my ($name, $path, $suffix) = fileparse( $new_name, $SUFFIXES );

	# Verify that new directory exists
	unless (-d $path) {
	    my @dirs = mkpath( $path );
	    if (scalar @dirs == 0) {
		print STDERR "could not create dir $path\n";
	    }
	    next;
	}

	# Find a name that does not exist
	my $extra = '';
	while (-f $path . $name . $extra . $suffix) {
	    $extra--;
	    $extra = sprintf( '%04d', $extra );
	}
	$new_name = $path . $name . $extra . $suffix;

	# Rename file
	unless (-f $new_name ) {
	    if (!rename( $file, $new_name )) {
		print STDERR "rename failed: $file\n";
		print STDERR "           to: $new_name\n";
	    }
	}

	# Display error message if file exists
	else {
	    print STDERR "$new_name is in the way\n";
	}
    }
}

sub scan_files {
    foreach my $path (@_) {
	$path = realpath( $path );
	print "  ... scanning $path\n" unless ($opt{q});
	find( \&scan, $path );
    }
    print "  ... ", scalar @FILES, " files; getting ID3 information\n" unless ($opt{q});
    foreach my $file (@FILES) {
	my $id3 = MP3::ID3Lib->new($file);
	%{$FILEINFO{$file}} = map { $_->code => $_->value } @{$id3->frames};
    }
}

sub scan {
    my $file = $_;

    # Verify that this is a regular file
    return unless (-f $file);

    # Check file name suffix
    return unless ($file =~ m/$SUFFIXES$/);

    push @FILES, $File::Find::name;
}

# Purge procedure
sub purge {
    if (-d $_ && $File::Find::name ne $purgelastdir) {
	unless (rmdir $File::Find::name) {
	    print STDERR "Failed to remove empty dir $File::Find::name\n";
	}
# 	else {
# 	    $stats{purged}++;
# 	}
    }
    else {
	$purgelastdir = $File::Find::dir
    }
}


scan_files( @ARGV );
finddepth( \&purge, '../../files/mp3' );

#print "$_\n" foreach @FILES;

# foreach my $file (@ARGV) {
#     my $id3 = MP3::ID3Lib->new($file);
#     %{$filelist{$file}} = map { $_->code => $_->value } @{$id3->frames};
# }

# foreach my $file (keys %filelist) {
# #    print "$file:\n";
# #     foreach my $code (keys %{$filelist{$file}}) {
# # 	print "  $code: $filelist{$file}{$code}\n";
# #     }
#     print &new_name( $filelist{$file} ), "\n";
# }
