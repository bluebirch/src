/* $Id$
 *
 * setcnpw - ett snabbt hack f�r att kunna knappa in olika l�senord
 * till pppd
 *
 * $Log$
 */

#include <strings.h>
#include <iostream.h>
#include <fstream.h>

#define CHATFILE "/etc/sysconfig/network-scripts/chat-ppp1"
#define TMPFILE "/tmp/setcnpw.tmp"

int main( int argc, char *argv[] )
{
  char password[256];
  char *filename = CHATFILE;

  if (argc != 2) {
    cerr << "Ange l�senord som enda paramter" << endl;
    exit(1);
  } else {
    strcpy( password, argv[1] );
    cout << "S�tter l�senord " << password << "." << endl;
  }

  class fstream chatfile;
  chatfile.open( filename, ios::in );
  if (!chatfile) {
    cerr << "Kan inte �ppna " << filename << " f�r l�sning" << endl;
    exit(1);
  }

  class fstream tmpfile;
  tmpfile.open( TMPFILE, ios::out, 600 );
  if (!tmpfile) {
    cerr << "Kan inte �ppna tempor�r fil f�r skrivning" << endl;
    chatfile.close();
    exit(1);
  }

  char textline[256];
  while (!chatfile.eof()) {
    chatfile.getline( textline, sizeof( textline ));
    if (strstr( textline, "'ord:'" ) != NULL) {
      tmpfile << "'ord:' '" << password << "'" << endl;
    } else {
      tmpfile << textline << endl;
    }
  }

  chatfile.close();
  tmpfile.close();

  tmpfile.open( TMPFILE, ios::in );
  if (!tmpfile) {
    cerr << "Kan inte �ppna tempor�r fil f�r l�sning" << endl;
    exit(1);
  }

  chatfile.open( filename, ios::out, 600 );
  if (!chatfile) {
    cerr << "Kan inte �ppna " << filename << " f�r skrivning" << endl;
    tmpfile.close();
    exit(1);
  }

  while (!tmpfile.eof()) {
    tmpfile.getline( textline, sizeof( textline ));
    chatfile << textline << endl;
  }

  tmpfile.close();
  chatfile.close();

}
